package com.vpiano.housekeyboard;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.twobard.pianoview.Piano;
import com.twobard.pianoview.Piano.PianoKeyListener;
import com.vpiano.housekeyboard.R;

@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
public class MainActivity extends Activity implements OnClickListener {

	Piano piano;
	ImageView rht_bt, rht_prev, rht_next, metro_bt, metro_prev, metro_next, sound_bt, sound_prev, sound_next;
	ImageView s1, s2, s3, s4, s5, s6, s7, s8;
	ImageView t1, t2, t3, t4;
	TextView rht_txt, metro_txt, sound_txt;
	MediaPlayer rhtmp, metromp, soundmp;
	MediaPlayer keymp1, keymp2, keymp3, keymp4, keymp5, keymp6, keymp7, keymp8, keymp9, keymp10, keymp11, keymp12,
			keymp13, keymp14, keymp15, keymp16, keymp17, keymp18, keymp19, keymp20, keymp21, keymp22, keymp23, keymp0;

	MediaPlayer padmp1, padmp2, padmp3, padmp4, padmp5, padmp6, padmp7, padmp8;
	View theme;
	SeekBar rht_seekbar, sound_seekbar;
	private static String audioFilePath;
	MediaRecorder mediaRecorder;
	boolean isRecording = false;
	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private InterstitialAd interstitial;
	AdRequest adRequest;

	ImageView start, pause, menu;
	AdView adView ;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.firstscrren);
		adView = (AdView) findViewById(R.id.adView);
		displayInterstitial();
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.navdrawer);
		String[] values1 = new String[] { "Rate Now", "Info", "More Apps" };
		ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,
				android.R.id.text1, values1);
		mDrawerList.setAdapter(adapter1);
		mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Intent intentweb = new Intent(getcontext(), WebBrowser.class);
				switch (position) {
				case 0:
					showratedialog();
					mDrawerLayout.closeDrawer(Gravity.RIGHT);
					break;
				case 1:
					intentweb.putExtra("URL", "https://play.google.com/store/apps/developer?id=DjDeve");
					startActivity(intentweb);
					mDrawerLayout.closeDrawer(Gravity.RIGHT);

					break;
				case 2:
					intentweb.putExtra("URL", "https://play.google.com/store/apps/developer?id=DjDeve");
					startActivity(intentweb);
					mDrawerLayout.closeDrawer(Gravity.RIGHT);

					break;

				}

			}
		});

		rhtmp = new MediaPlayer();
		metromp = new MediaPlayer();
		padmp1 = new MediaPlayer();
		padmp2 = new MediaPlayer();
		padmp3 = new MediaPlayer();
		padmp4 = new MediaPlayer();
		padmp5 = new MediaPlayer();
		padmp6 = new MediaPlayer();
		padmp7 = new MediaPlayer();
		padmp8 = new MediaPlayer();

		soundmp = new MediaPlayer();
		keymp1 = new MediaPlayer();
		keymp2 = new MediaPlayer();
		keymp3 = new MediaPlayer();
		keymp4 = new MediaPlayer();
		keymp5 = new MediaPlayer();
		keymp6 = new MediaPlayer();
		keymp7 = new MediaPlayer();
		keymp8 = new MediaPlayer();
		keymp9 = new MediaPlayer();
		keymp10 = new MediaPlayer();
		keymp11 = new MediaPlayer();
		keymp12 = new MediaPlayer();
		keymp13 = new MediaPlayer();
		keymp14 = new MediaPlayer();
		keymp15 = new MediaPlayer();
		keymp16 = new MediaPlayer();
		keymp17 = new MediaPlayer();
		keymp18 = new MediaPlayer();
		keymp19 = new MediaPlayer();
		keymp20 = new MediaPlayer();
		keymp21 = new MediaPlayer();
		keymp22 = new MediaPlayer();
		keymp23 = new MediaPlayer();
		keymp0 = new MediaPlayer();
		rht_bt = (ImageView) findViewById(R.id.rht_bt);
		rht_prev = (ImageView) findViewById(R.id.rht_prev);
		rht_next = (ImageView) findViewById(R.id.rht_next);
		rht_txt = (TextView) findViewById(R.id.rht_txt);

		metro_bt = (ImageView) findViewById(R.id.metro_bt);
		metro_prev = (ImageView) findViewById(R.id.metro_prev);
		metro_next = (ImageView) findViewById(R.id.metro_next);
		metro_txt = (TextView) findViewById(R.id.metro_txt);

		sound_bt = (ImageView) findViewById(R.id.sound_bt);
		sound_prev = (ImageView) findViewById(R.id.sound_prev);
		sound_next = (ImageView) findViewById(R.id.sound_next);
		sound_txt = (TextView) findViewById(R.id.sound_txt);

		sound_txt.setText(getsound(1));
		start = (ImageView) findViewById(R.id.start);
		pause = (ImageView) findViewById(R.id.pause);

		menu = (ImageView) findViewById(R.id.menubt);
		start.setOnClickListener(new OnClickListener() {

			@SuppressLint("SimpleDateFormat")
			@Override
			public void onClick(View v) {

				try {

					if (!isRecording) {
						String extStorageDirectory = Environment.getExternalStorageDirectory().getAbsolutePath();
						File folder = new File(extStorageDirectory, getResources().getString(R.string.app_name));
						if (!folder.exists()) {
							folder.mkdir();
						} else {
							String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
							audioFilePath = folder.getAbsolutePath() + "/" + timeStamp + ".mp3";

						}
						Toast.makeText(getcontext(), "recording stated", Toast.LENGTH_SHORT).show();
						PackageManager pmanager = getPackageManager();
						if (pmanager.hasSystemFeature(PackageManager.FEATURE_MICROPHONE)) {
							mediaRecorder = new MediaRecorder();
							mediaRecorder.setAudioSource(MediaRecorder.AudioSource.DEFAULT);
							mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
							mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
							mediaRecorder.setOutputFile(audioFilePath);
							mediaRecorder.prepare();
							mediaRecorder.start();
							isRecording = true;
						}
					} else {
						Toast.makeText(getcontext(), "recording  is already stated", Toast.LENGTH_SHORT).show();
					}

				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});

		pause.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (isRecording) {
					isRecording = false;
					Toast.makeText(getcontext(), "music file saved in SD card  ", Toast.LENGTH_SHORT).show();
					mediaRecorder.stop();
					mediaRecorder.reset();
					mediaRecorder.release();
				}

			}
		});

		s1 = (ImageView) findViewById(R.id.s1);
		s2 = (ImageView) findViewById(R.id.s2);
		s3 = (ImageView) findViewById(R.id.s3);
		s4 = (ImageView) findViewById(R.id.s4);
		s5 = (ImageView) findViewById(R.id.s5);
		s6 = (ImageView) findViewById(R.id.s6);
		s7 = (ImageView) findViewById(R.id.s7);
		s8 = (ImageView) findViewById(R.id.s8);

		t1 = (ImageView) findViewById(R.id.t1);
		t2 = (ImageView) findViewById(R.id.t2);
		t3 = (ImageView) findViewById(R.id.t3);
		t4 = (ImageView) findViewById(R.id.t4);

		theme = (View) findViewById(R.id.topbar);

		menu.setOnClickListener(this);
		rht_bt.setOnClickListener(this);
		rht_prev.setOnClickListener(this);
		rht_next.setOnClickListener(this);

		metro_bt.setOnClickListener(this);
		metro_prev.setOnClickListener(this);
		metro_next.setOnClickListener(this);

		s1.setOnClickListener(this);
		s2.setOnClickListener(this);
		s3.setOnClickListener(this);
		s4.setOnClickListener(this);
		s5.setOnClickListener(this);
		s6.setOnClickListener(this);
		s7.setOnClickListener(this);
		s8.setOnClickListener(this);

		sound_bt.setOnClickListener(this);
		sound_prev.setOnClickListener(this);
		sound_next.setOnClickListener(this);

		t1.setOnClickListener(this);
		t2.setOnClickListener(this);
		t3.setOnClickListener(this);
		t4.setOnClickListener(this);

		rht_seekbar = (SeekBar) findViewById(R.id.rht_seekbar);
		sound_seekbar = (SeekBar) findViewById(R.id.sound_seekbar);

		rht_seekbar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {

			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {

			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				float soundvol = (progress) / 100.0f;
				rhtmp.setVolume(soundvol, soundvol);
			}
		});

		sound_seekbar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {

			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {

			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

				float soundvol = (progress) / 100.0f;
				keymp1.setVolume(soundvol, soundvol);
				keymp2.setVolume(soundvol, soundvol);
				keymp3.setVolume(soundvol, soundvol);
				keymp4.setVolume(soundvol, soundvol);
				keymp5.setVolume(soundvol, soundvol);
				keymp6.setVolume(soundvol, soundvol);
				keymp7.setVolume(soundvol, soundvol);
				keymp8.setVolume(soundvol, soundvol);
				keymp9.setVolume(soundvol, soundvol);
				keymp10.setVolume(soundvol, soundvol);
				keymp11.setVolume(soundvol, soundvol);
				keymp12.setVolume(soundvol, soundvol);
				keymp13.setVolume(soundvol, soundvol);
				keymp14.setVolume(soundvol, soundvol);
				keymp15.setVolume(soundvol, soundvol);
				keymp16.setVolume(soundvol, soundvol);
				keymp17.setVolume(soundvol, soundvol);
				keymp18.setVolume(soundvol, soundvol);
				keymp19.setVolume(soundvol, soundvol);
				keymp20.setVolume(soundvol, soundvol);
				keymp21.setVolume(soundvol, soundvol);
				keymp22.setVolume(soundvol, soundvol);
				keymp23.setVolume(soundvol, soundvol);
				keymp0.setVolume(soundvol, soundvol);

			}
		});

		piano = (Piano) findViewById(R.id.pi);
		piano.setPianoKeyListener(new PianoKeyListener() {
			@Override
			public void keyPressed(int id, int action) {

				if (action == MotionEvent.ACTION_DOWN) {
					Log.e("pino", "action:" + action + " id: " + id);

					if (Integer.valueOf(MusicLoopClass.mpsound) == 1) {
						play((getsound(Integer.valueOf(MusicLoopClass.mpsound)) + id + ".ogg"), id);

					} else {
						play((getsound(Integer.valueOf(MusicLoopClass.mpsound)) + id + ".ogg"), id);

					}

				}
			}
		});
	}

	private void play(String sd, int id) {

		switch (id) {
		case 0:
			playkeyboard0(sd);
			break;
		case 1:
			playkeyboard1(sd);
			break;
		case 2:
			playkeyboard2(sd);
			break;
		case 3:
			playkeyboard3(sd);
			break;
		case 4:
			playkeyboard4(sd);
			break;
		case 5:
			playkeyboard5(sd);
			break;
		case 6:
			playkeyboard6(sd);
			break;
		case 7:
			playkeyboard7(sd);
			break;
		case 8:
			playkeyboard8(sd);
			break;
		case 9:
			playkeyboard9(sd);
			break;
		case 10:
			playkeyboard10(sd);
			break;
		case 11:
			playkeyboard11(sd);
			break;
		case 12:
			playkeyboard12(sd);
			break;
		case 13:
			playkeyboard13(sd);
			break;
		case 14:
			playkeyboard14(sd);
			break;
		case 15:
			playkeyboard15(sd);
			break;
		case 16:
			playkeyboard16(sd);
			break;
		case 17:
			playkeyboard17(sd);
			break;
		case 18:
			playkeyboard18(sd);
			break;
		case 19:
			playkeyboard19(sd);
			break;
		case 20:
			playkeyboard20(sd);
			break;
		case 21:
			playkeyboard21(sd);
			break;
		case 22:
			playkeyboard22(sd);
			break;
		case 23:
			playkeyboard23(sd);
			break;

		default:
			break;
		}

	}

	private Context getcontext() {

		return MainActivity.this;
	}

	boolean ist1setdata = false, istrack1play = false;

	private void playrht(String musicname) {
		try {
			ist1setdata = true;
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			rhtmp.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			rhtmp.prepare();
			rhtmp.setLooping(true);
			rhtmp.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					rhtmp.start();
					istrack1play = true;
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	boolean issounddata = false, issoundplay = false;

	private void playsound(String musicname) {
		try {
			issounddata = true;
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			soundmp.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			soundmp.prepare();
			soundmp.setLooping(true);
			soundmp.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					soundmp.start();
					issoundplay = true;
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playpad1(String musicname) {
		try {

			padmp1.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			padmp1.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			padmp1.prepare();
			padmp1.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					padmp1.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playpad2(String musicname) {
		try {

			padmp2.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			padmp2.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			padmp2.prepare();
			padmp2.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					padmp2.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playpad3(String musicname) {
		try {

			padmp3.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			padmp3.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			padmp3.prepare();
			padmp3.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					padmp3.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playpad4(String musicname) {
		try {

			padmp4.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			padmp4.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			padmp4.prepare();
			padmp4.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					padmp4.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playpad5(String musicname) {
		try {

			padmp5.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			padmp5.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			padmp5.prepare();
			padmp5.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					padmp5.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playpad6(String musicname) {
		try {

			padmp6.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			padmp6.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			padmp6.prepare();
			padmp6.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					padmp6.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playpad7(String musicname) {
		try {

			padmp7.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			padmp7.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			padmp7.prepare();
			padmp7.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					padmp7.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playpad8(String musicname) {
		try {

			padmp8.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			padmp8.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			padmp8.prepare();
			padmp8.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					padmp8.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard1(String musicname) {
		try {

			keymp1.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			keymp1.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp1.prepare();
			keymp1.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp1.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard2(String musicname) {
		try {

			keymp2.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			keymp2.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp2.prepare();
			keymp2.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp2.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard3(String musicname) {
		try {

			keymp3.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			keymp3.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp3.prepare();
			keymp3.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp3.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard4(String musicname) {
		try {

			keymp4.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			keymp4.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp4.prepare();
			keymp4.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp4.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard5(String musicname) {
		try {

			keymp5.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			keymp5.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp5.prepare();
			keymp5.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp5.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard6(String musicname) {
		try {

			keymp6.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			keymp6.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp6.prepare();
			keymp6.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp6.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard7(String musicname) {
		try {

			keymp7.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			keymp7.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp7.prepare();
			keymp7.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp7.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard8(String musicname) {
		try {

			keymp8.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			keymp8.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp8.prepare();
			keymp8.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp8.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard9(String musicname) {
		try {

			keymp9.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			keymp9.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp9.prepare();
			keymp9.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp9.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard10(String musicname) {
		try {

			keymp10.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			keymp10.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp10.prepare();
			keymp10.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp10.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard11(String musicname) {
		try {

			keymp11.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			keymp11.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp11.prepare();
			keymp11.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp11.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard12(String musicname) {
		try {

			keymp12.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			keymp12.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp12.prepare();
			keymp12.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp12.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard13(String musicname) {
		try {

			keymp13.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			keymp13.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp13.prepare();
			keymp13.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp13.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard14(String musicname) {
		try {

			keymp14.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			keymp14.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp14.prepare();
			keymp14.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp14.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard15(String musicname) {
		try {

			keymp15.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			keymp15.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp15.prepare();
			keymp15.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp15.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard16(String musicname) {
		try {

			keymp16.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			keymp16.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp16.prepare();
			keymp16.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp16.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard17(String musicname) {
		try {

			keymp17.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			keymp17.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp17.prepare();
			keymp17.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp17.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard18(String musicname) {
		try {

			keymp18.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			keymp18.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp18.prepare();
			keymp18.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp18.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard19(String musicname) {
		try {

			keymp19.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			keymp19.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp19.prepare();
			keymp19.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp19.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard20(String musicname) {
		try {

			keymp20.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			keymp20.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp20.prepare();
			keymp20.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp20.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard21(String musicname) {
		try {

			keymp21.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			keymp21.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp21.prepare();
			keymp21.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp21.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard22(String musicname) {
		try {

			keymp22.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			keymp22.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp22.prepare();
			keymp22.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp22.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard23(String musicname) {
		try {

			keymp23.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			keymp23.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp23.prepare();
			keymp23.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp23.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard0(String musicname) {
		try {

			keymp0.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			keymp0.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp0.prepare();
			keymp0.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp0.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	boolean issetmetrodata = false, ismetroplay = false;

	private void playmusicmetro(String musicname) {
		try {
			issetmetrodata = true;
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			metromp.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			metromp.prepare();
			metromp.setLooping(true);
			metromp.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					metromp.start();
					ismetroplay = true;
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.menubt:
			mDrawerLayout.openDrawer(Gravity.RIGHT);
			break;
		case R.id.rht_bt:
			if (rhtmp.isPlaying()) {
				rhtmp.pause();
				istrack1play = false;
				Log.e("", "pause song");
				rht_bt.setImageResource(R.drawable.play_bt);
			} else {
				if (ist1setdata) {
					istrack1play = true;
					rhtmp.start();
					Log.e("", "play song");
					rht_bt.setImageResource(R.drawable.pause_bt);
				} else {
					Log.e("", "play  first time song");
					rht_bt.setImageResource(R.drawable.pause_bt);
					playrht("t" + MusicLoopClass.mploo1 + ".ogg");
				}
			}
			break;
		case R.id.rht_prev:

			rhtmp.reset();
			int downname1 = Integer.valueOf(MusicLoopClass.mploo1) - 1;

			if (downname1 == 0) {
				MusicLoopClass.mploo1 = "15";
			} else {
				MusicLoopClass.mploo1 = downname1 + "";
			}

			rht_bt.setImageResource(R.drawable.pause_bt);
			rht_txt.setText("House" + MusicLoopClass.mploo1);
			playrht("t" + MusicLoopClass.mploo1 + ".ogg");
			break;

		case R.id.rht_next:
			rhtmp.reset();
			int upname1 = Integer.valueOf(MusicLoopClass.mploo1) + 1;
			if (upname1 == 16) {
				MusicLoopClass.mploo1 = "1";
			} else {
				MusicLoopClass.mploo1 = upname1 + "";
			}
			rht_bt.setImageResource(R.drawable.pause_bt);
			rht_txt.setText("House" + MusicLoopClass.mploo1);
			playrht("t" + MusicLoopClass.mploo1 + ".ogg");
			break;

		case R.id.sound_bt:
//			if (soundmp.isPlaying()) {
//				soundmp.pause();
//				issoundplay = false;
//				sound_bt.setImageResource(R.drawable.play_bt);
//			} else {
//				if (issounddata) {
//					issoundplay = true;
//					soundmp.start();
//					sound_bt.setImageResource(R.drawable.pause_bt);
//				} else {
//					sound_bt.setImageResource(R.drawable.pause_bt);
//					playsound(getsound(Integer.valueOf(MusicLoopClass.mpsound)) + ".mp3");
//
//				}
//			}
			break;
		case R.id.sound_prev:

			soundmp.reset();
			int downsound = Integer.valueOf(MusicLoopClass.mpsound) - 1;

			if (downsound == 0) {
				MusicLoopClass.mpsound = "4";
			} else {
				MusicLoopClass.mpsound = downsound + "";
			}
			sound_bt.setImageResource(R.drawable.pause_bt);
			sound_txt.setText(getsound(Integer.valueOf(MusicLoopClass.mpsound)));

			if (Integer.valueOf(MusicLoopClass.mpsound) == 1) {
//				playsound(getsound(Integer.valueOf(MusicLoopClass.mpsound)) + ".wav");

			} else {
//				playsound(getsound(Integer.valueOf(MusicLoopClass.mpsound)) + ".mp3");
			}
			break;

		case R.id.sound_next:
			soundmp.reset();
			int upsound = Integer.valueOf(MusicLoopClass.mpsound) + 1;
			if (upsound == 5) {
				MusicLoopClass.mpsound = "1";
			} else {
				MusicLoopClass.mpsound = upsound + "";
			}
			sound_bt.setImageResource(R.drawable.pause_bt);
			sound_txt.setText(getsound(Integer.valueOf(MusicLoopClass.mpsound)));

			if (Integer.valueOf(MusicLoopClass.mpsound) == 1) {
//				playsound(getsound(Integer.valueOf(MusicLoopClass.mpsound)) + ".wav");

			} else {
//				playsound(getsound(Integer.valueOf(MusicLoopClass.mpsound)) + ".mp3");
			}

			break;

		case R.id.metro_bt:
			if (metromp.isPlaying()) {
				metromp.pause();
				ismetroplay = false;
				metro_bt.setImageResource(R.drawable.play_bt);
			} else {
				if (issetmetrodata) {
					ismetroplay = true;
					metromp.start();
					metro_bt.setImageResource(R.drawable.pause_bt);
				} else {
					metro_bt.setImageResource(R.drawable.pause_bt);
					playmusicmetro("r" + MusicLoopClass.mpmetro + ".ogg");
				}
			}
			break;
		case R.id.metro_prev:
			metromp.reset();
			int downmetro = Integer.valueOf(MusicLoopClass.mpmetro) - 1;

			if (downmetro == 0) {
				MusicLoopClass.mpmetro = "10";
			} else {
				MusicLoopClass.mpmetro = downmetro + "";
			}

			metro_bt.setImageResource(R.drawable.pause_bt);
			metro_txt.setText(getbpm(Integer.valueOf(MusicLoopClass.mpmetro)));
			playmusicmetro("r" + Integer.valueOf(MusicLoopClass.mpmetro) + ".ogg");
			break;

		case R.id.metro_next:
			metromp.reset();
			int upmetro = Integer.valueOf(MusicLoopClass.mpmetro) + 1;
			if (upmetro == 11) {
				MusicLoopClass.mpmetro = "1";
			} else {
				MusicLoopClass.mpmetro = upmetro + "";
			}
			metro_bt.setImageResource(R.drawable.pause_bt);
			metro_txt.setText(getbpm(Integer.valueOf(MusicLoopClass.mpmetro)));
			playmusicmetro("r" + Integer.valueOf(MusicLoopClass.mpmetro) + ".ogg");
			break;

		case R.id.s1:
			playpad1("p1.ogg");
			break;
		case R.id.s2:
			playpad2("p2.ogg");
			break;
		case R.id.s3:
			playpad3("p3.ogg");
			break;
		case R.id.s4:
			playpad4("p4.ogg");
			break;
		case R.id.s5:
			playpad5("p5.ogg");
			break;
		case R.id.s6:
			playpad6("p6.ogg");
			break;
		case R.id.s7:
			playpad7("p7.ogg");
			break;
		case R.id.s8:
			playpad8("p8.ogg");
			break;

		case R.id.t1:
			setthemedefault();
			t1.setImageResource(R.drawable.a_color);
			theme.setBackground(getResources().getDrawable(R.drawable.back1));
			break;

		case R.id.t2:
			setthemedefault();
			t2.setImageResource(R.drawable.b_color);
			theme.setBackground(getResources().getDrawable(R.drawable.back2));
			break;

		case R.id.t3:
			setthemedefault();
			t3.setImageResource(R.drawable.c_color);
			theme.setBackground(getResources().getDrawable(R.drawable.back3));
			break;

		case R.id.t4:
			setthemedefault();
			t4.setImageResource(R.drawable.d_color);
			theme.setBackground(getResources().getDrawable(R.drawable.back4));
			break;

		default:
			break;
		}
	}

	private void setthemedefault() {

		t1.setImageResource(R.drawable.a);
		t2.setImageResource(R.drawable.b);
		t3.setImageResource(R.drawable.c);
		t4.setImageResource(R.drawable.d);

	}

	private String getsound(int ins) {
		String name = "";
		switch (ins) {

		case 1:
			name = "Piano";
			break;
		case 4:
			name = "Organ";
			break;
		case 2:
			name = "String";
			break;
		case 3:
			name = "Guitar";
			break;
		default:
			break;
		}
		return name;
	}

	private String getbpm(int ins) {
		String name = "";
		switch (ins) {

		case 1:
			name = "Bpm100";
			break;

		case 2:
			name = "Bpm105";
			break;
		case 3:
			name = "Bpm110";
			break;
		case 4:
			name = "Bpm115";
			break;
		case 5:
			name = "Bpm120";
			break;
		case 6:
			name = "Bpm125";
			break;
		case 7:
			name = "Bpm130";
			break;
		case 8:
			name = "Bpm135";
			break;
		case 9:
			name = "Bpm140";
			break;
		case 10:
			name = "Bpm145";
			break;
		default:
			break;
		}
		return name;
	}

	private void showratedialog() {
		new AlertDialog.Builder(this).setTitle("Do you want to rate this application?")
				.setPositiveButton("Never", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						// continue with delete
						dialog.dismiss();
					}
				}).setNegativeButton("not now", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						// do nothing
						dialog.dismiss();
					}
				}).setNeutralButton("Rate Now", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						// do nothing
						startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id="
								+ "com.vpiano.housekeyboard")));
					}
				}).show();
	}

	@Override
	public void onBackPressed() {
		displayInterstitial();
		showclose();

	}

	private void showclose() {
		new AlertDialog.Builder(this).setTitle("Do you really want to shut down the app?")
				.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {

						setrelese();
						finish();
						return;
					}
				}).setNegativeButton("No", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				}).show();
	}

	private void setrelese() {
		rhtmp.release();
		metromp.release();
		padmp1.release();
		padmp2.release();
		padmp3.release();
		padmp4.release();
		padmp5.release();
		padmp6.release();
		padmp7.release();
		padmp8.release();
		soundmp.release();
		keymp1.release();
		keymp2.release();
		keymp3.release();
		keymp4.release();
		keymp5.release();
		keymp6.release();
		keymp7.release();
		keymp8.release();
		keymp9.release();
		keymp10.release();
		keymp11.release();
		keymp12.release();
		keymp13.release();
		keymp14.release();
		keymp15.release();
		keymp16.release();
		keymp17.release();
		keymp18.release();
		keymp19.release();
		keymp20.release();
		keymp21.release();
		keymp22.release();
		keymp23.release();
		keymp0.release();
	}

	public void displayInterstitial() {
		// if (interstitial.isLoaded()) {

		adRequest = new AdRequest.Builder().build();
		
		adView.loadAd(adRequest);
		interstitial = new InterstitialAd(getcontext());
		interstitial.setAdUnitId("ca-app-pub-3805784166574868/9242967104");
		interstitial.loadAd(adRequest);
		interstitial.setAdListener(new AdListener() {
			public void onAdLoaded() {
				// displayInterstitial();
				interstitial.show();
			}
		});
		// }
	}
}

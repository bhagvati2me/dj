package com.djmix.djelectrostation;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.DragEvent;
import android.view.View;
import android.view.View.DragShadowBuilder;
import android.view.View.OnClickListener;
import android.view.View.OnDragListener;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import com.djmix.djelectrostation.R;

public class djactivity extends Activity implements OnClickListener {

	MusicAdapter musicadapter;
	ArrayList<Musicmodel> musiclist = new ArrayList<Musicmodel>();
	ListView playlist;
	DatabaseHandler dbHandler = new DatabaseHandler(this);

	// dj
	File filename1, filename2, filename3, filename4, filename5, filename6;
	MediaPlayer mpdj1, mpdj2, mpdj3, mpdj4, mpdj5, mpdj6;
	ImageView djdisk1, djdisk2, djdisk3, djdisk4, djdisk5, djdisk6;
	ImageView djplaypause1, djplaypause2, djplaypause3, djplaypause4,
			djplaypause5, djplaypause6;
	ImageView djloop1, djloop2, djloop3, djloop4, djloop5, djloop6;

	// track
	ImageView trackprev1, trackprev2, trackprev3, trackprev4, trackprev5,
			trackprev6;
	ImageView tracknext1, tracknext2, tracknext3, tracknext4, tracknext5,
			tracknext6;
	TextView tracktxt1, tracktxt2, tracktxt3, tracktxt4, tracktxt5, tracktxt6;
	ImageView trackplaypause1, trackplaypause2, trackplaypause3,
			trackplaypause4, trackplaypause5, trackplaypause6;
	VerticalSeekBar trackseekbar1, trackseekbar2, trackseekbar3, trackseekbar4,
			trackseekbar5, trackseekbar6;
	LinearLayout waves1, waves2, waves3, waves4, waves5, waves6;
	MediaPlayer mptrack1, mptrack2, mptrack3, mptrack4, mptrack5, mptrack6;

	ImageView addbt, newmenu;
	private RotateAnimation anim;

	boolean isDjPlaying1 = false, isDjPlaying2 = false, isDjPlaying3 = false,
			isDjPlaying4 = false, isDjPlaying5 = false, isDjPlaying6 = false;
	boolean isdatasetDj1 = false, isdatasetDj2 = false, isdatasetDj3 = false,
			isdatasetDj4 = false, isdatasetDj5 = false, isdatasetDj6 = false;
	boolean isdjloop1 = false, isdjloop2 = false, isdjloop3 = false,
			isdjloop4 = false, isdjloop5 = false, isdjloop6 = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.djmixer1);

		mpdj1 = new MediaPlayer();
		mpdj2 = new MediaPlayer();
		mpdj3 = new MediaPlayer();
		mpdj4 = new MediaPlayer();
		mpdj5 = new MediaPlayer();
		mpdj6 = new MediaPlayer();

		mptrack1 = new MediaPlayer();
		mptrack2 = new MediaPlayer();
		mptrack3 = new MediaPlayer();
		mptrack4 = new MediaPlayer();
		mptrack5 = new MediaPlayer();
		mptrack6 = new MediaPlayer();

		addbt = (ImageView) findViewById(R.id.addnew);
		newmenu = (ImageView) findViewById(R.id.newmenu);
		playlist = (ListView) findViewById(R.id.playlist);

		djdisk1 = (ImageView) findViewById(R.id.djdisk1);
		djdisk2 = (ImageView) findViewById(R.id.djdisk2);
		djdisk3 = (ImageView) findViewById(R.id.djdisk3);
		djdisk4 = (ImageView) findViewById(R.id.djdisk4);
		djdisk5 = (ImageView) findViewById(R.id.djdisk5);
		djdisk6 = (ImageView) findViewById(R.id.djdisk6);

		djplaypause1 = (ImageView) findViewById(R.id.djplaypause1);
		djplaypause2 = (ImageView) findViewById(R.id.djplaypause2);
		djplaypause3 = (ImageView) findViewById(R.id.djplaypause3);
		djplaypause4 = (ImageView) findViewById(R.id.djplaypause4);
		djplaypause5 = (ImageView) findViewById(R.id.djplaypause5);
		djplaypause6 = (ImageView) findViewById(R.id.djplaypause6);

		djloop1 = (ImageView) findViewById(R.id.djloop1);
		djloop2 = (ImageView) findViewById(R.id.djloop2);
		djloop3 = (ImageView) findViewById(R.id.djloop3);
		djloop4 = (ImageView) findViewById(R.id.djloop4);
		djloop5 = (ImageView) findViewById(R.id.djloop5);
		djloop6 = (ImageView) findViewById(R.id.djloop6);

		djloop1.setOnClickListener(this);
		djloop2.setOnClickListener(this);
		djloop3.setOnClickListener(this);
		djloop4.setOnClickListener(this);
		djloop5.setOnClickListener(this);
		djloop6.setOnClickListener(this);

		djplaypause1.setOnClickListener(this);
		djplaypause2.setOnClickListener(this);
		djplaypause3.setOnClickListener(this);
		djplaypause4.setOnClickListener(this);
		djplaypause5.setOnClickListener(this);
		djplaypause6.setOnClickListener(this);

		addbt.setOnClickListener(this);
		newmenu.setOnClickListener(this);

		trackprev1 = (ImageView) findViewById(R.id.trackprev1);
		trackprev2 = (ImageView) findViewById(R.id.trackprev2);
		trackprev3 = (ImageView) findViewById(R.id.trackprev3);
		trackprev4 = (ImageView) findViewById(R.id.trackprev4);
		trackprev5 = (ImageView) findViewById(R.id.trackprev5);
		trackprev6 = (ImageView) findViewById(R.id.trackprev6);

		tracknext1 = (ImageView) findViewById(R.id.tracknext1);
		tracknext2 = (ImageView) findViewById(R.id.tracknext2);
		tracknext3 = (ImageView) findViewById(R.id.tracknext3);
		tracknext4 = (ImageView) findViewById(R.id.tracknext4);
		tracknext5 = (ImageView) findViewById(R.id.tracknext5);
		tracknext6 = (ImageView) findViewById(R.id.tracknext6);

		trackplaypause1 = (ImageView) findViewById(R.id.trackplaypause1);
		trackplaypause2 = (ImageView) findViewById(R.id.trackplaypause2);
		trackplaypause3 = (ImageView) findViewById(R.id.trackplaypause3);
		trackplaypause4 = (ImageView) findViewById(R.id.trackplaypause4);
		trackplaypause5 = (ImageView) findViewById(R.id.trackplaypause5);
		trackplaypause6 = (ImageView) findViewById(R.id.trackplaypause6);

		tracktxt1 = (TextView) findViewById(R.id.tracktxt1);
		tracktxt2 = (TextView) findViewById(R.id.tracktxt2);
		tracktxt3 = (TextView) findViewById(R.id.tracktxt3);
		tracktxt4 = (TextView) findViewById(R.id.tracktxt4);
		tracktxt5 = (TextView) findViewById(R.id.tracktxt5);
		tracktxt6 = (TextView) findViewById(R.id.tracktxt6);

		trackseekbar1 = (VerticalSeekBar) findViewById(R.id.trackseekbar1);
		trackseekbar2 = (VerticalSeekBar) findViewById(R.id.trackseekbar2);
		trackseekbar3 = (VerticalSeekBar) findViewById(R.id.trackseekbar3);
		trackseekbar4 = (VerticalSeekBar) findViewById(R.id.trackseekbar4);
		trackseekbar5 = (VerticalSeekBar) findViewById(R.id.trackseekbar5);
		trackseekbar6 = (VerticalSeekBar) findViewById(R.id.trackseekbar6);

		waves1 = (LinearLayout) findViewById(R.id.waves1);
		waves2 = (LinearLayout) findViewById(R.id.waves2);
		waves3 = (LinearLayout) findViewById(R.id.waves3);
		waves4 = (LinearLayout) findViewById(R.id.waves4);
		waves5 = (LinearLayout) findViewById(R.id.waves5);
		waves6 = (LinearLayout) findViewById(R.id.waves6);

		trackprev1.setOnClickListener(this);
		trackprev2.setOnClickListener(this);
		trackprev3.setOnClickListener(this);
		trackprev4.setOnClickListener(this);
		trackprev5.setOnClickListener(this);
		trackprev6.setOnClickListener(this);

		tracknext1.setOnClickListener(this);
		tracknext2.setOnClickListener(this);
		tracknext3.setOnClickListener(this);
		tracknext4.setOnClickListener(this);
		tracknext5.setOnClickListener(this);
		tracknext6.setOnClickListener(this);

		trackplaypause1.setOnClickListener(this);
		trackplaypause2.setOnClickListener(this);
		trackplaypause3.setOnClickListener(this);
		trackplaypause4.setOnClickListener(this);
		trackplaypause5.setOnClickListener(this);
		trackplaypause6.setOnClickListener(this);

		playlist.setOnItemLongClickListener(new MyClickListener1());
		playlist.setOnDragListener(new DragListener1());
		findViewById(R.id.layer1).setOnDragListener(new DragListener1());
		findViewById(R.id.layer2).setOnDragListener(new DragListener2());
		findViewById(R.id.layer3).setOnDragListener(new DragListener3());
		findViewById(R.id.layer4).setOnDragListener(new DragListener4());
		findViewById(R.id.layer5).setOnDragListener(new DragListener5());
		findViewById(R.id.layer6).setOnDragListener(new DragListener6());

		musicadapter = new MusicAdapter(getactivity(), R.layout.playlistitem,
				musiclist);
		playlist.setAdapter(musicadapter);

		anim = new RotateAnimation(0f, 350f, RotateAnimation.RELATIVE_TO_SELF,
				0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.5f);
		anim.setInterpolator(new LinearInterpolator());
		anim.setRepeatCount(Animation.INFINITE);
		anim.setDuration(700);

		mpdj1.setOnCompletionListener(new OnCompletionListener() {
			@Override
			public void onCompletion(MediaPlayer mp) {
				djdisk1.setAnimation(null);
				isDjPlaying1 = false;
				isdatasetDj1 = false;
				djplaypause1.setBackgroundResource(R.drawable.playdj);
			}
		});
		mpdj2.setOnCompletionListener(new OnCompletionListener() {
			@Override
			public void onCompletion(MediaPlayer mp) {
				djdisk2.setAnimation(null);
				isDjPlaying2 = false;
				isdatasetDj2 = false;
				djplaypause2.setBackgroundResource(R.drawable.playdj);
			}
		});
		mpdj3.setOnCompletionListener(new OnCompletionListener() {
			@Override
			public void onCompletion(MediaPlayer mp) {
				djdisk3.setAnimation(null);
				isDjPlaying3 = false;
				isdatasetDj3 = false;
				djplaypause3.setBackgroundResource(R.drawable.playdj);
			}
		});
		mpdj4.setOnCompletionListener(new OnCompletionListener() {
			@Override
			public void onCompletion(MediaPlayer mp) {
				djdisk4.setAnimation(null);
				isDjPlaying4 = false;
				isdatasetDj4 = false;
				djplaypause4.setBackgroundResource(R.drawable.playdj);
			}
		});
		mpdj5.setOnCompletionListener(new OnCompletionListener() {
			@Override
			public void onCompletion(MediaPlayer mp) {
				djdisk5.setAnimation(null);
				isDjPlaying5 = false;
				isdatasetDj5 = false;
				djplaypause5.setBackgroundResource(R.drawable.playdj);
			}
		});
		mpdj6.setOnCompletionListener(new OnCompletionListener() {
			@Override
			public void onCompletion(MediaPlayer mp) {
				djdisk6.setAnimation(null);
				isDjPlaying6 = false;
				isdatasetDj6 = false;
				djplaypause6.setBackgroundResource(R.drawable.playdj);
			}
		});

		trackseekbar1.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				float vol1 = (progress) / 100.0f;
				mptrack1.setVolume(vol1, vol1);
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
			}
		});
		trackseekbar2.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				float vol1 = (progress) / 100.0f;
				mptrack2.setVolume(vol1, vol1);
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
			}
		});
		trackseekbar3.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				float vol1 = (progress) / 100.0f;
				mptrack3.setVolume(vol1, vol1);
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
			}
		});
		trackseekbar4.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				float vol1 = (progress) / 100.0f;
				mptrack4.setVolume(vol1, vol1);
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
			}
		});
		trackseekbar5.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				float vol1 = (progress) / 100.0f;
				mptrack5.setVolume(vol1, vol1);
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
			}
		});
		trackseekbar6.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				float vol1 = (progress) / 100.0f;
				mptrack6.setVolume(vol1, vol1);
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
			}
		});

	}

	private djactivity getactivity() {
		return djactivity.this;
	}

	class DragListener1 implements OnDragListener {
		@Override
		public boolean onDrag(View v, DragEvent event) {

			switch (event.getAction()) {

			case DragEvent.ACTION_DRAG_STARTED:

				break;

			case DragEvent.ACTION_DRAG_ENTERED:
				break;

			case DragEvent.ACTION_DRAG_EXITED:
				break;

			case DragEvent.ACTION_DROP:
				mpdj1.reset();
				ClipData.Item item = event.getClipData().getItemAt(0);
				File filename11 = new File(item.getText().toString());
				String name = filename11.getName();
				Log.e("name : ", name);

				filename1 = new File(item.getText().toString());

				isdatasetDj1 = true;
				try {

					djplaypause1.setBackgroundResource(R.drawable.pausedj);
					playdjmusic1(filename1.getAbsolutePath());
				} catch (IllegalStateException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();

				}
				break;

			case DragEvent.ACTION_DRAG_ENDED:
				break;
			default:
				break;
			}
			return true;
		}
	}

	class DragListener2 implements OnDragListener {
		@Override
		public boolean onDrag(View v, DragEvent event) {

			switch (event.getAction()) {

			case DragEvent.ACTION_DRAG_STARTED:

				break;

			case DragEvent.ACTION_DRAG_ENTERED:
				break;

			case DragEvent.ACTION_DRAG_EXITED:
				break;

			case DragEvent.ACTION_DROP:
				mpdj2.reset();
				ClipData.Item item = event.getClipData().getItemAt(0);
				File filename11 = new File(item.getText().toString());
				String name = filename11.getName();
				Log.e("name : ", name);

				filename2 = new File(item.getText().toString());

				isdatasetDj2 = true;
				try {

					djplaypause2.setBackgroundResource(R.drawable.pausedj);
					playdjmusic2(filename2.getAbsolutePath());
				} catch (IllegalStateException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();

				}
				break;

			case DragEvent.ACTION_DRAG_ENDED:
				break;
			default:
				break;
			}
			return true;
		}
	}

	class DragListener3 implements OnDragListener {
		@Override
		public boolean onDrag(View v, DragEvent event) {

			switch (event.getAction()) {

			case DragEvent.ACTION_DRAG_STARTED:

				break;

			case DragEvent.ACTION_DRAG_ENTERED:
				break;

			case DragEvent.ACTION_DRAG_EXITED:
				break;

			case DragEvent.ACTION_DROP:
				mpdj3.reset();
				ClipData.Item item = event.getClipData().getItemAt(0);
				File filename11 = new File(item.getText().toString());
				String name = filename11.getName();
				Log.e("name : ", name);

				filename3 = new File(item.getText().toString());

				isdatasetDj3 = true;
				try {

					djplaypause3.setBackgroundResource(R.drawable.pausedj);
					playdjmusic3(filename3.getAbsolutePath());
				} catch (IllegalStateException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();

				}
				break;

			case DragEvent.ACTION_DRAG_ENDED:
				break;
			default:
				break;
			}
			return true;
		}
	}

	class DragListener4 implements OnDragListener {
		@Override
		public boolean onDrag(View v, DragEvent event) {

			switch (event.getAction()) {

			case DragEvent.ACTION_DRAG_STARTED:

				break;

			case DragEvent.ACTION_DRAG_ENTERED:
				break;

			case DragEvent.ACTION_DRAG_EXITED:
				break;

			case DragEvent.ACTION_DROP:
				mpdj4.reset();
				ClipData.Item item = event.getClipData().getItemAt(0);
				File filename11 = new File(item.getText().toString());
				String name = filename11.getName();
				Log.e("name : ", name);

				filename4 = new File(item.getText().toString());

				isdatasetDj4 = true;
				try {

					djplaypause4.setBackgroundResource(R.drawable.pausedj);
					playdjmusic4(filename4.getAbsolutePath());
				} catch (IllegalStateException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();

				}
				break;

			case DragEvent.ACTION_DRAG_ENDED:
				break;
			default:
				break;
			}
			return true;
		}
	}

	class DragListener5 implements OnDragListener {
		@Override
		public boolean onDrag(View v, DragEvent event) {

			switch (event.getAction()) {

			case DragEvent.ACTION_DRAG_STARTED:

				break;

			case DragEvent.ACTION_DRAG_ENTERED:
				break;

			case DragEvent.ACTION_DRAG_EXITED:
				break;

			case DragEvent.ACTION_DROP:
				mpdj5.reset();
				ClipData.Item item = event.getClipData().getItemAt(0);
				File filename11 = new File(item.getText().toString());
				String name = filename11.getName();
				Log.e("name : ", name);

				filename5 = new File(item.getText().toString());

				isdatasetDj5 = true;
				try {

					djplaypause5.setBackgroundResource(R.drawable.pausedj);
					playdjmusic5(filename5.getAbsolutePath());
				} catch (IllegalStateException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();

				}
				break;

			case DragEvent.ACTION_DRAG_ENDED:
				break;
			default:
				break;
			}
			return true;
		}
	}

	class DragListener6 implements OnDragListener {
		@Override
		public boolean onDrag(View v, DragEvent event) {

			switch (event.getAction()) {

			case DragEvent.ACTION_DRAG_STARTED:

				break;

			case DragEvent.ACTION_DRAG_ENTERED:
				break;

			case DragEvent.ACTION_DRAG_EXITED:
				break;

			case DragEvent.ACTION_DROP:
				mpdj6.reset();
				ClipData.Item item = event.getClipData().getItemAt(0);
				File filename11 = new File(item.getText().toString());
				String name = filename11.getName();
				Log.e("name : ", name);

				filename6 = new File(item.getText().toString());

				isdatasetDj6 = true;
				try {

					djplaypause6.setBackgroundResource(R.drawable.pausedj);
					playdjmusic6(filename6.getAbsolutePath());
				} catch (IllegalStateException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();

				}
				break;

			case DragEvent.ACTION_DRAG_ENDED:
				break;
			default:
				break;
			}
			return true;
		}
	}

	private void playdjmusic1(String path) throws IllegalStateException,
			IOException {
		try {
			List<Musicmodel> el = dbHandler.Get_playList(false);
			if (el.size() == 0) {
				AssetFileDescriptor descriptor = getAssets().openFd(
						"sample" + path);
				mpdj1.setDataSource(descriptor.getFileDescriptor(),
						descriptor.getStartOffset(), descriptor.getLength());
				descriptor.close();
			} else {
				mpdj1.setDataSource(getApplicationContext(), Uri.parse(path));
			}
			mpdj1.prepare();
			if (isdjloop1) {
				mpdj1.setLooping(true);
			}
			mpdj1.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mpdj1.start();
					isDjPlaying1 = true;
				}
			});
			djdisk1.startAnimation(anim);
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playdjmusic2(String path) throws IllegalStateException,
			IOException {
		try {
			List<Musicmodel> el = dbHandler.Get_playList(false);
			if (el.size() == 0) {
				AssetFileDescriptor descriptor = getAssets().openFd(
						"sample" + path);
				mpdj2.setDataSource(descriptor.getFileDescriptor(),
						descriptor.getStartOffset(), descriptor.getLength());
				descriptor.close();
			} else {
				mpdj2.setDataSource(getApplicationContext(), Uri.parse(path));
			}
			mpdj2.prepare();
			if (isdjloop2) {
				mpdj2.setLooping(true);
			}
			mpdj2.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mpdj2.start();
					isDjPlaying2 = true;
				}
			});
			djdisk2.startAnimation(anim);
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playdjmusic3(String path) throws IllegalStateException,
			IOException {
		try {
			List<Musicmodel> el = dbHandler.Get_playList(false);
			if (el.size() == 0) {
				AssetFileDescriptor descriptor = getAssets().openFd(
						"sample" + path);
				mpdj3.setDataSource(descriptor.getFileDescriptor(),
						descriptor.getStartOffset(), descriptor.getLength());
				descriptor.close();
			} else {
				mpdj3.setDataSource(getApplicationContext(), Uri.parse(path));
			}
			mpdj3.prepare();
			if (isdjloop3) {
				mpdj3.setLooping(true);
			}
			mpdj3.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mpdj3.start();
					isDjPlaying3 = true;
				}
			});
			djdisk3.startAnimation(anim);
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playdjmusic4(String path) throws IllegalStateException,
			IOException {
		try {
			List<Musicmodel> el = dbHandler.Get_playList(false);
			if (el.size() == 0) {
				AssetFileDescriptor descriptor = getAssets().openFd(
						"sample" + path);
				mpdj4.setDataSource(descriptor.getFileDescriptor(),
						descriptor.getStartOffset(), descriptor.getLength());
				descriptor.close();
			} else {
				mpdj4.setDataSource(getApplicationContext(), Uri.parse(path));
			}
			mpdj4.prepare();
			if (isdjloop4) {
				mpdj4.setLooping(true);
			}
			mpdj4.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mpdj4.start();
					isDjPlaying4 = true;
				}
			});
			djdisk4.startAnimation(anim);
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playdjmusic5(String path) throws IllegalStateException,
			IOException {
		try {
			List<Musicmodel> el = dbHandler.Get_playList(false);
			if (el.size() == 0) {
				AssetFileDescriptor descriptor = getAssets().openFd(
						"sample" + path);
				mpdj5.setDataSource(descriptor.getFileDescriptor(),
						descriptor.getStartOffset(), descriptor.getLength());
				descriptor.close();
			} else {
				mpdj5.setDataSource(getApplicationContext(), Uri.parse(path));
			}
			mpdj5.prepare();
			if (isdjloop5) {
				mpdj5.setLooping(true);
			}
			mpdj5.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mpdj5.start();
					isDjPlaying5 = true;
				}
			});
			djdisk5.startAnimation(anim);
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playdjmusic6(String path) throws IllegalStateException,
			IOException {
		try {
			List<Musicmodel> el = dbHandler.Get_playList(false);
			if (el.size() == 0) {
				AssetFileDescriptor descriptor = getAssets().openFd(
						"sample" + path);
				mpdj6.setDataSource(descriptor.getFileDescriptor(),
						descriptor.getStartOffset(), descriptor.getLength());
				descriptor.close();
			} else {
				mpdj6.setDataSource(getApplicationContext(), Uri.parse(path));
			}
			mpdj6.prepare();
			if (isdjloop6) {
				mpdj6.setLooping(true);
			}
			mpdj6.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mpdj6.start();
					isDjPlaying6 = true;
				}
			});
			djdisk6.startAnimation(anim);
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private final class MyClickListener1 implements OnItemLongClickListener {

		@Override
		public boolean onItemLongClick(AdapterView<?> arg0, View view,
				int position, long arg3) {

			String selectedFromList = musiclist.get(position).path;
			ClipData.Item item = new ClipData.Item(
					((CharSequence) selectedFromList.toString()));
			String[] clipDescription = { ClipDescription.MIMETYPE_TEXT_PLAIN };
			ClipData dragData = new ClipData("item", clipDescription, item);
			DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
			view.startDrag(dragData, shadowBuilder, view, 0);
			return true;
		}

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.addnew:
			startActivity(new Intent(getactivity(), FileBrowserActivity.class));
			break;
		case R.id.newmenu:
			String[] s = getResources().getStringArray(R.array.setting_Option);

			new AlertDialog.Builder(getactivity())
					.setItems(s, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							switch (which) {
							case 0:
								dbHandler.Delete_All();
								musiclist.clear();
								musicadapter.notifyDataSetChanged();
								List<Musicmodel> ei = dbHandler
										.Get_playList(false);
								if (ei.size() == 0) {
									AssetManager assetManager = getApplicationContext()
											.getAssets();
									String[] fields;
									try {
										fields = assetManager.list("sample");
										for (int count = 0; count < fields.length; count++) {

											Musicmodel m = new Musicmodel();
											m.name = fields[count];
											m.path = fields[count];
											musiclist.add(m);
										}
									} catch (IOException e1) {
										e1.printStackTrace();
									}
								}
								break;
							case 1:
								musiclist.clear();
								musicadapter.notifyDataSetChanged();
								List<Musicmodel> el = dbHandler
										.Get_playList(true);
								for (Musicmodel e : el) {
									musiclist.add(e);
								}
								musicadapter.notifyDataSetChanged();
								break;
							case 2:
								showalert();
								break;
							case 3:
								openplaylist();
								break;
							}
						}
					}).create().show();
			break;
		case R.id.djloop1:
			isdjloop1 = !isdjloop1;
			if (isdjloop1) {
				djloop1.setBackgroundResource(R.drawable.loop);
				mpdj1.setLooping(true);
			} else {
				djloop1.setBackgroundResource(R.drawable.notloop);
				mpdj1.setLooping(false);
			}
			break;
		case R.id.djloop2:
			isdjloop2 = !isdjloop2;
			if (isdjloop2) {
				djloop2.setBackgroundResource(R.drawable.loop);
				mpdj2.setLooping(true);
			} else {
				djloop2.setBackgroundResource(R.drawable.notloop);
				mpdj2.setLooping(false);
			}
			break;
		case R.id.djloop3:
			isdjloop3 = !isdjloop3;
			if (isdjloop3) {
				djloop3.setBackgroundResource(R.drawable.loop);
				mpdj3.setLooping(true);
			} else {
				djloop3.setBackgroundResource(R.drawable.notloop);
				mpdj3.setLooping(false);
			}
			break;
		case R.id.djloop4:
			isdjloop4 = !isdjloop4;
			if (isdjloop4) {
				djloop4.setBackgroundResource(R.drawable.loop);
				mpdj4.setLooping(true);
			} else {
				djloop4.setBackgroundResource(R.drawable.notloop);
				mpdj4.setLooping(false);
			}
			break;
		case R.id.djloop5:
			isdjloop5 = !isdjloop5;
			if (isdjloop5) {
				djloop5.setBackgroundResource(R.drawable.loop);
				mpdj5.setLooping(true);
			} else {
				djloop5.setBackgroundResource(R.drawable.notloop);
				mpdj5.setLooping(false);
			}
			break;
		case R.id.djloop6:
			isdjloop6 = !isdjloop6;
			if (isdjloop6) {
				djloop6.setBackgroundResource(R.drawable.loop);
				mpdj6.setLooping(true);
			} else {
				djloop6.setBackgroundResource(R.drawable.notloop);
				mpdj6.setLooping(false);
			}
			break;
		case R.id.djplaypause1:
			if (isDjPlaying1) {
				isDjPlaying1 = false;
				mpdj1.pause();
				djdisk1.setAnimation(null);
				djplaypause1.setBackgroundResource(R.drawable.playdj);
			} else {
				if (isdatasetDj1) {
					djplaypause1.setBackgroundResource(R.drawable.pausedj);
					djdisk1.startAnimation(anim);
					mpdj1.start();
					isDjPlaying1 = true;
				}
			}
			break;
		case R.id.djplaypause2:
			if (isDjPlaying2) {
				isDjPlaying2 = false;
				mpdj2.pause();
				djdisk2.setAnimation(null);
				djplaypause2.setBackgroundResource(R.drawable.playdj);
			} else {
				if (isdatasetDj2) {
					djplaypause2.setBackgroundResource(R.drawable.pausedj);
					djdisk2.startAnimation(anim);
					mpdj2.start();
					isDjPlaying2 = true;
				}
			}
			break;
		case R.id.djplaypause3:
			if (isDjPlaying3) {
				isDjPlaying3 = false;
				mpdj3.pause();
				djdisk3.setAnimation(null);
				djplaypause3.setBackgroundResource(R.drawable.playdj);
			} else {
				if (isdatasetDj3) {
					djplaypause3.setBackgroundResource(R.drawable.pausedj);
					djdisk3.startAnimation(anim);
					mpdj3.start();
					isDjPlaying3 = true;
				}
			}
			break;
		case R.id.djplaypause4:
			if (isDjPlaying4) {
				isDjPlaying4 = false;
				mpdj4.pause();
				djdisk4.setAnimation(null);
				djplaypause4.setBackgroundResource(R.drawable.playdj);
			} else {
				if (isdatasetDj4) {
					djplaypause4.setBackgroundResource(R.drawable.pausedj);
					djdisk4.startAnimation(anim);
					mpdj4.start();
					isDjPlaying4 = true;
				}
			}
			break;
		case R.id.djplaypause5:
			if (isDjPlaying5) {
				isDjPlaying5 = false;
				mpdj5.pause();
				djdisk5.setAnimation(null);
				djplaypause5.setBackgroundResource(R.drawable.playdj);
			} else {
				if (isdatasetDj5) {
					djplaypause5.setBackgroundResource(R.drawable.pausedj);
					djdisk5.startAnimation(anim);
					mpdj5.start();
					isDjPlaying5 = true;
				}
			}
			break;
		case R.id.djplaypause6:
			if (isDjPlaying6) {
				isDjPlaying6 = false;
				mpdj6.pause();
				djdisk6.setAnimation(null);
				djplaypause6.setBackgroundResource(R.drawable.playdj);
			} else {
				if (isdatasetDj6) {
					djplaypause6.setBackgroundResource(R.drawable.pausedj);
					djdisk6.startAnimation(anim);
					mpdj6.start();
					isDjPlaying6 = true;
				}
			}
			break;
		case R.id.trackprev1:
			mptrack1.reset();
			int prev1 = Integer.valueOf(MusicLoopClass.mploo1) - 1;
			if (prev1 == 0) {
				MusicLoopClass.mploo1 = "10";
			} else {
				MusicLoopClass.mploo1 = prev1 + "";
			}
			tracktxt1.setText("Loop" + "" + MusicLoopClass.mploo1);
			playtrack1("t0s" + MusicLoopClass.mploo1 + ".ogg");
			trackplaypause1.setBackgroundResource(R.drawable.trackplay);
			break;
		case R.id.trackprev2:
			mptrack2.reset();
			int prev2 = Integer.valueOf(MusicLoopClass.mploo2) - 1;
			if (prev2 == 0) {
				MusicLoopClass.mploo2 = "10";
			} else {
				MusicLoopClass.mploo2 = prev2 + "";
			}
			tracktxt2.setText("Loop" + "" + MusicLoopClass.mploo2);
			playtrack2("t1s" + MusicLoopClass.mploo2 + ".ogg");
			trackplaypause2.setBackgroundResource(R.drawable.trackplay);
			break;
		case R.id.trackprev3:
			mptrack3.reset();
			int prev3 = Integer.valueOf(MusicLoopClass.mploo3) - 1;
			if (prev3 == 0) {
				MusicLoopClass.mploo3 = "10";
			} else {
				MusicLoopClass.mploo3 = prev3 + "";
			}
			tracktxt3.setText("Loop" + "" + MusicLoopClass.mploo3);
			playtrack3("t2s" + MusicLoopClass.mploo3 + ".ogg");
			trackplaypause3.setBackgroundResource(R.drawable.trackplay);
			break;
		case R.id.trackprev4:
			mptrack4.reset();
			int prev4 = Integer.valueOf(MusicLoopClass.mploo4) - 1;
			if (prev4 == 0) {
				MusicLoopClass.mploo4 = "10";
			} else {
				MusicLoopClass.mploo4 = prev4 + "";
			}
			tracktxt4.setText("Loop" + "" + MusicLoopClass.mploo4);
			playtrack4("t3s" + MusicLoopClass.mploo4 + ".ogg");
			trackplaypause4.setBackgroundResource(R.drawable.trackplay);
			break;
		case R.id.trackprev5:
			mptrack5.reset();
			int prev5 = Integer.valueOf(MusicLoopClass.mploo5) - 1;
			if (prev5 == 0) {
				MusicLoopClass.mploo5 = "10";
			} else {
				MusicLoopClass.mploo5 = prev5 + "";
			}
			tracktxt5.setText("Loop" + "" + MusicLoopClass.mploo5);
			playtrack5("t4s" + MusicLoopClass.mploo5 + ".ogg");
			trackplaypause5.setBackgroundResource(R.drawable.trackplay);
			break;
		case R.id.trackprev6:
			mptrack6.reset();
			int prev6 = Integer.valueOf(MusicLoopClass.mploo6) - 1;
			if (prev6 == 0) {
				MusicLoopClass.mploo6 = "10";
			} else {
				MusicLoopClass.mploo6 = prev6 + "";
			}
			tracktxt6.setText("Loop" + "" + MusicLoopClass.mploo6);
			playtrack6("t5s" + MusicLoopClass.mploo6 + ".ogg");
			trackplaypause6.setBackgroundResource(R.drawable.trackplay);
			break;
		case R.id.tracknext1:
			mptrack1.reset();
			int next1 = Integer.valueOf(MusicLoopClass.mploo1) + 1;
			if (next1 == 11) {
				MusicLoopClass.mploo1 = "1";
			} else {
				MusicLoopClass.mploo1 = next1 + "";
			}
			tracktxt1.setText("Loop" + " " + MusicLoopClass.mploo1);
			playtrack1("t0s" + MusicLoopClass.mploo1 + ".ogg");
			trackplaypause1.setBackgroundResource(R.drawable.trackplay);
			break;
		case R.id.tracknext2:
			mptrack2.reset();
			int next2 = Integer.valueOf(MusicLoopClass.mploo2) + 1;
			if (next2 == 11) {
				MusicLoopClass.mploo2 = "1";
			} else {
				MusicLoopClass.mploo2 = next2 + "";
			}
			tracktxt2.setText("Loop" + " " + MusicLoopClass.mploo2);
			playtrack2("t1s" + MusicLoopClass.mploo2 + ".ogg");
			trackplaypause2.setBackgroundResource(R.drawable.trackplay);
			break;
		case R.id.tracknext3:
			mptrack3.reset();
			int next3 = Integer.valueOf(MusicLoopClass.mploo3) + 1;
			if (next3 == 11) {
				MusicLoopClass.mploo3 = "1";
			} else {
				MusicLoopClass.mploo3 = next3 + "";
			}
			tracktxt3.setText("Loop" + " " + MusicLoopClass.mploo3);
			playtrack3("t2s" + MusicLoopClass.mploo3 + ".ogg");
			trackplaypause3.setBackgroundResource(R.drawable.trackplay);
			break;

		case R.id.tracknext4:
			mptrack4.reset();
			int next4 = Integer.valueOf(MusicLoopClass.mploo4) + 1;
			if (next4 == 11) {
				MusicLoopClass.mploo4 = "1";
			} else {
				MusicLoopClass.mploo4 = next4 + "";
			}
			tracktxt4.setText("Loop" + " " + MusicLoopClass.mploo4);
			playtrack4("t3s" + MusicLoopClass.mploo4 + ".ogg");
			trackplaypause4.setBackgroundResource(R.drawable.trackplay);
			break;

		case R.id.tracknext5:
			mptrack5.reset();
			int next5 = Integer.valueOf(MusicLoopClass.mploo5) + 1;
			if (next5 == 11) {
				MusicLoopClass.mploo5 = "1";
			} else {
				MusicLoopClass.mploo5 = next5 + "";
			}
			tracktxt5.setText("Loop" + " " + MusicLoopClass.mploo5);
			playtrack5("t4s" + MusicLoopClass.mploo5 + ".ogg");
			trackplaypause5.setBackgroundResource(R.drawable.trackplay);
			break;
		case R.id.tracknext6:
			mptrack6.reset();
			int next6 = Integer.valueOf(MusicLoopClass.mploo6) + 1;
			if (next6 == 11) {
				MusicLoopClass.mploo6 = "1";
			} else {
				MusicLoopClass.mploo6 = next6 + "";
			}
			tracktxt6.setText("Loop" + " " + MusicLoopClass.mploo6);
			playtrack6("t5s" + MusicLoopClass.mploo6 + ".ogg");
			trackplaypause6.setBackgroundResource(R.drawable.trackplay);
			break;
		case R.id.trackplaypause1:
			if (mptrack1.isPlaying()) {
				mptrack1.pause();
				trackplaypause1.setBackgroundResource(R.drawable.trackpause);
			} else {
				trackplaypause1.setBackgroundResource(R.drawable.trackplay);
				mptrack1.reset();
				playtrack1("t0s" + MusicLoopClass.mploo1 + ".ogg");
			}
			break;
		case R.id.trackplaypause2:
			if (mptrack2.isPlaying()) {
				mptrack2.pause();
				trackplaypause2.setBackgroundResource(R.drawable.trackpause);
			} else {
				trackplaypause2.setBackgroundResource(R.drawable.trackplay);
				mptrack2.reset();
				playtrack2("t1s" + MusicLoopClass.mploo2 + ".ogg");
			}
			break;
		case R.id.trackplaypause3:
			if (mptrack3.isPlaying()) {
				mptrack3.pause();
				trackplaypause3.setBackgroundResource(R.drawable.trackpause);
			} else {
				trackplaypause3.setBackgroundResource(R.drawable.trackplay);
				mptrack3.reset();
				playtrack3("t2s" + MusicLoopClass.mploo3 + ".ogg");
			}
			break;
		case R.id.trackplaypause4:
			if (mptrack4.isPlaying()) {
				mptrack4.pause();
				trackplaypause4.setBackgroundResource(R.drawable.trackpause);
			} else {
				trackplaypause4.setBackgroundResource(R.drawable.trackplay);
				mptrack4.reset();
				playtrack4("t3s" + MusicLoopClass.mploo4 + ".ogg");
			}
			break;
		case R.id.trackplaypause5:
			if (mptrack5.isPlaying()) {
				mptrack5.pause();
				trackplaypause5.setBackgroundResource(R.drawable.trackpause);
			} else {
				trackplaypause5.setBackgroundResource(R.drawable.trackplay);
				mptrack5.reset();
				playtrack5("t4s" + MusicLoopClass.mploo5 + ".ogg");
			}
			break;

		case R.id.trackplaypause6:
			if (mptrack6.isPlaying()) {
				mptrack6.pause();
				trackplaypause6.setBackgroundResource(R.drawable.trackpause);
			} else {
				trackplaypause6.setBackgroundResource(R.drawable.trackplay);
				mptrack6.reset();
				playtrack6("t5s" + MusicLoopClass.mploo6 + ".ogg");
			}
			break;
		default:
			break;
		}

	}

	private void playtrack1(String musicname) {
		try {
			AssetFileDescriptor descriptor = getAssets().openFd(
					"track/" + musicname);
			mptrack1.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mptrack1.prepare();
			mptrack1.setLooping(true);

			mptrack1.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mptrack1.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void playtrack2(String musicname) {
		try {
			AssetFileDescriptor descriptor = getAssets().openFd(
					"track/" + musicname);
			mptrack2.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mptrack2.prepare();
			mptrack2.setLooping(true);

			mptrack2.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mptrack2.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void playtrack3(String musicname) {
		try {
			AssetFileDescriptor descriptor = getAssets().openFd(
					"track/" + musicname);
			mptrack3.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mptrack3.prepare();
			mptrack3.setLooping(true);

			mptrack3.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mptrack3.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void playtrack4(String musicname) {
		try {
			AssetFileDescriptor descriptor = getAssets().openFd(
					"track/" + musicname);
			mptrack4.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mptrack4.prepare();
			mptrack4.setLooping(true);

			mptrack4.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mptrack4.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void playtrack5(String musicname) {
		try {
			AssetFileDescriptor descriptor = getAssets().openFd(
					"track/" + musicname);
			mptrack5.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mptrack5.prepare();
			mptrack5.setLooping(true);

			mptrack5.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mptrack5.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void playtrack6(String musicname) {
		try {
			AssetFileDescriptor descriptor = getAssets().openFd(
					"track/" + musicname);
			mptrack6.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mptrack6.prepare();
			mptrack6.setLooping(true);

			mptrack6.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mptrack6.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void onResume() {
		super.onResume();

		musiclist.clear();
		musicadapter.notifyDataSetChanged();
		List<Musicmodel> el = dbHandler.Get_playList(false);
		for (Musicmodel e : el) {
			musiclist.add(e);
		}
		musicadapter.notifyDataSetChanged();
		if (el.size() == 0) {
			AssetManager assetManager = getApplicationContext().getAssets();
			String[] fields;
			try {
				fields = assetManager.list("sample");
				for (int count = 0; count < fields.length; count++) {

					Musicmodel m = new Musicmodel();
					m.name = fields[count];
					m.path = fields[count];
					musiclist.add(m);

				}
			} catch (IOException e1) {
				e1.printStackTrace();
			}

		}

	}

	private void showalert() {
		AlertDialog.Builder alert = new AlertDialog.Builder(this);
		alert.setTitle("Save PlayList");
		final EditText input = new EditText(this);
		alert.setView(input);
		alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				String value = input.getText().toString();
				if (!value.equals("")) {
					dbHandler.Add_play(value.toString().trim());
					for (int i = 0; i < musiclist.size(); i++) {
						Musicmodel md = new Musicmodel();
						md.name = musiclist.get(i).name;
						md.path = musiclist.get(i).path;
						Log.e("value", value);
						dbHandler.Add_Saveplaylist(md, value.toString().trim());
					}
				}
			}
		});
		alert.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
					}
				});

		alert.show();
	}

	private void openplaylist() {
		final String[] openlist = dbHandler.openlist();
		new AlertDialog.Builder(getactivity())
				.setItems(openlist, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						musiclist.clear();
						musicadapter.notifyDataSetChanged();
						List<Musicmodel> save = dbHandler.Get_saveplayList(
								false, openlist[which].toString().trim());
						for (Musicmodel e : save) {
							musiclist.add(e);
						}
						musicadapter.notifyDataSetChanged();
					}
				}).create().show();
	}

	@Override
	public void onBackPressed() {
		showclose();
	}

	private void showclose() {
		new AlertDialog.Builder(this)
				.setTitle("Do you really want to shut down the app?")
				.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {

								setrelease();
								finish();
								return;
							}
						})
				.setNegativeButton("No", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				}).show();
	}

	private void setrelease() {
		mpdj1.release();
		mpdj2.release();
		mpdj3.release();
		mpdj4.release();
		mpdj5.release();
		mpdj6.release();
		mptrack1.release();
		mptrack2.release();
		mptrack3.release();
		mptrack4.release();
		mptrack5.release();
		mptrack6.release();
	}

}

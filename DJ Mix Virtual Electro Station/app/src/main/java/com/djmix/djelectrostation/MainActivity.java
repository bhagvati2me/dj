package com.djmix.djelectrostation;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaRecorder;
import android.media.audiofx.Visualizer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.DragEvent;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.DragShadowBuilder;
import android.view.View.OnClickListener;
import android.view.View.OnDragListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.djmix.djelectrostation.R;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.twobard.pianoview.Piano;
import com.twobard.pianoview.Piano.PianoKeyListener;

@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
public class MainActivity extends Activity implements OnClickListener {

	ImageView s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s11, s12, s13, s14, s15,
			s16, s17, s18, s19, s20, s21, s22, s23, s24, s25, s26, s27, s28,
			s29, s30, s31, s32, s33, s34, s35, s36, s37, s38, s39, s40, s42,
			s43, s44, s45;
	MediaPlayer mp1, mp2, mp3, mp4, mp5, mp6, mp7, mp8, mp9, mp10, mp11, mp12,
			mp13, mp14, mp15, mp16, mp17, mp18, mp19, mp20, mp21, mp22, mp23,
			mp24, mp25, mp26, mp27, mp28, mp29, mp30, mp31, mp32, mp33, mp34,
			mp35, mp36, mp37, mp38, mp39, mp40;

	String kit = "sA1";
	VerticalSeekBar padvolseek;

	MediaPlayer mpdisk1, mpdisk2, mpdisk3, mpdisk4, mpdisk5, mpdisk6;
	MusicAdapter musicadapter;
	ArrayList<Musicmodel> musiclist = new ArrayList<Musicmodel>();
	ListView playlist;
	DatabaseHandler dbHandler = new DatabaseHandler(this);

	// dj
	File filename1, filename2, filename3, filename4, filename5, filename6;
	MediaPlayer mpdj1, mpdj2, mpdj3, mpdj4, mpdj5, mpdj6;
	ImageView djdisk1, djdisk2, djdisk3, djdisk4, djdisk5, djdisk6;
	ImageView djplaypause1, djplaypause2, djplaypause3, djplaypause4,
			djplaypause5, djplaypause6;
	ImageView djloop1, djloop2, djloop3, djloop4, djloop5, djloop6;

	SeekBar djvol1, djvol2, djvol3, djvol4, djvol5, djvol6;

	// track
	ImageView trackprev1, trackprev2, trackprev3, trackprev4, trackprev5,
			trackprev6;
	ImageView tracknext1, tracknext2, tracknext3, tracknext4, tracknext5,
			tracknext6;
	TextView tracktxt1, tracktxt2, tracktxt3, tracktxt4, tracktxt5, tracktxt6;
	ImageView trackplaypause1, trackplaypause2, trackplaypause3,
			trackplaypause4, trackplaypause5, trackplaypause6;
	SeekBar trackseekbar1, trackseekbar2, trackseekbar3, trackseekbar4,
			trackseekbar5, trackseekbar6;
	LinearLayout waves1, waves2, waves3, waves4, waves5, waves6;
	MediaPlayer mptrack1, mptrack2, mptrack3, mptrack4, mptrack5, mptrack6;

	ImageView addbt, newmenu;
	private RotateAnimation anim;

	boolean isDjPlaying1 = false, isDjPlaying2 = false, isDjPlaying3 = false,
			isDjPlaying4 = false, isDjPlaying5 = false, isDjPlaying6 = false;
	boolean isdatasetDj1 = false, isdatasetDj2 = false, isdatasetDj3 = false,
			isdatasetDj4 = false, isdatasetDj5 = false, isdatasetDj6 = false;
	boolean isdjloop1 = false, isdjloop2 = false, isdjloop3 = false,
			isdjloop4 = false, isdjloop5 = false, isdjloop6 = false;

	Piano piano;
	ImageView rht_bt, rht_prev, rht_next, metro_bt, metro_prev, metro_next,
			sound_bt, sound_prev, sound_next;
	ImageView pad1, pad2, pad3, pad4, pad5, pad6, pad7, pad8;
	ImageView t1, t2, t3, t4;
	TextView rht_txt, metro_txt, sound_txt;
	MediaPlayer rhtmp, metromp, soundmp;
	MediaPlayer keymp1, keymp2, keymp3, keymp4, keymp5, keymp6, keymp7, keymp8,
			keymp9, keymp10, keymp11, keymp12, keymp13, keymp14, keymp15,
			keymp16, keymp17, keymp18, keymp19, keymp20, keymp21, keymp22,
			keymp23, keymp0;

	MediaPlayer padmp1, padmp2, padmp3, padmp4, padmp5, padmp6, padmp7, padmp8;
	View theme;
	SeekBar rht_seekbar, sound_seekbar;
	boolean ist1setdata = false, istrack1play = false;

	private static String audioFilePath;
	MediaRecorder mediaRecorder;
	boolean isRecording = false;
	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private InterstitialAd interstitial;
	AdRequest adRequest;
	ImageView start, pause, menu;
	AdView adView;
	ImageView screen1, screen2, screen3;
	View mscreen1, mscreen2, mscreen3;

	private String[] permissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO};

	@RequiresApi(api = Build.VERSION_CODES.M)
	private boolean arePermissionsEnabled() {
		for (String permission : permissions) {
			if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED)
				return false;
		}
		return true;
	}

	@RequiresApi(api = Build.VERSION_CODES.M)
	private void requestMultiplePermissions() {
		List<String> remainingPermissions = new ArrayList<>();
		for (String permission : permissions) {
			if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
				remainingPermissions.add(permission);
			}
		}
		requestPermissions(remainingPermissions.toArray(new String[remainingPermissions.size()]), 101);
	}

	@TargetApi(Build.VERSION_CODES.M)
	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		if (requestCode == 101) {
			for (int i = 0; i < grantResults.length; i++) {
				if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
					if (shouldShowRequestPermissionRationale(permissions[i])) {
						new AlertDialog.Builder(this)
								.setMessage("Please allow permissions to use record audio and choose local music from device")
								.setPositiveButton("Allow", new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialogInterface, int i) {
										dialogInterface.dismiss();
										requestMultiplePermissions();
									}
								})
								.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialogInterface, int i) {
										dialogInterface.dismiss();
										requestMultiplePermissions();
									}
								})
								.create()
								.show();
					}
					return;
				}
			}

			setupVisualizerFxAndUI1();
			mVisualizer1.setEnabled(true);

			setupVisualizerFxAndUI2();
			mVisualizer2.setEnabled(true);

			setupVisualizerFxAndUI3();
			mVisualizer3.setEnabled(true);
			setupVisualizerFxAndUI4();
			mVisualizer4.setEnabled(true);
			setupVisualizerFxAndUI5();
			mVisualizer5.setEnabled(true);
			setupVisualizerFxAndUI6();
			mVisualizer6.setEnabled(true);
			//====

		}
	}



	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.firstscrren);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			if(!arePermissionsEnabled()) {
				requestMultiplePermissions();
			}
		}

		mpdisk1 = new MediaPlayer();
		mpdisk2 = new MediaPlayer();
		mpdisk3 = new MediaPlayer();
		mpdisk4 = new MediaPlayer();
		mpdisk5 = new MediaPlayer();
		mpdisk6 = new MediaPlayer();

		rhtmp = new MediaPlayer();
		metromp = new MediaPlayer();
		padmp1 = new MediaPlayer();
		padmp2 = new MediaPlayer();
		padmp3 = new MediaPlayer();
		padmp4 = new MediaPlayer();
		padmp5 = new MediaPlayer();
		padmp6 = new MediaPlayer();
		padmp7 = new MediaPlayer();
		padmp8 = new MediaPlayer();

		soundmp = new MediaPlayer();
		keymp1 = new MediaPlayer();
		keymp2 = new MediaPlayer();
		keymp3 = new MediaPlayer();
		keymp4 = new MediaPlayer();
		keymp5 = new MediaPlayer();
		keymp6 = new MediaPlayer();
		keymp7 = new MediaPlayer();
		keymp8 = new MediaPlayer();
		keymp9 = new MediaPlayer();
		keymp10 = new MediaPlayer();
		keymp11 = new MediaPlayer();
		keymp12 = new MediaPlayer();
		keymp13 = new MediaPlayer();
		keymp14 = new MediaPlayer();
		keymp15 = new MediaPlayer();
		keymp16 = new MediaPlayer();
		keymp17 = new MediaPlayer();
		keymp18 = new MediaPlayer();
		keymp19 = new MediaPlayer();
		keymp20 = new MediaPlayer();
		keymp21 = new MediaPlayer();
		keymp22 = new MediaPlayer();
		keymp23 = new MediaPlayer();
		keymp0 = new MediaPlayer();
		rht_bt = (ImageView) findViewById(R.id.rht_bt);
		rht_prev = (ImageView) findViewById(R.id.rht_prev);
		rht_next = (ImageView) findViewById(R.id.rht_next);
		rht_txt = (TextView) findViewById(R.id.rht_txt);

		metro_bt = (ImageView) findViewById(R.id.metro_bt);
		metro_prev = (ImageView) findViewById(R.id.metro_prev);
		metro_next = (ImageView) findViewById(R.id.metro_next);
		metro_txt = (TextView) findViewById(R.id.metro_txt);

		sound_bt = (ImageView) findViewById(R.id.sound_bt);
		sound_prev = (ImageView) findViewById(R.id.sound_prev);
		sound_next = (ImageView) findViewById(R.id.sound_next);
		sound_txt = (TextView) findViewById(R.id.sound_txt);

		sound_txt.setText(getsound(1));

		pad1 = (ImageView) findViewById(R.id.pad1);
		pad2 = (ImageView) findViewById(R.id.pad2);
		pad3 = (ImageView) findViewById(R.id.pad3);
		pad4 = (ImageView) findViewById(R.id.pad4);
		pad5 = (ImageView) findViewById(R.id.pad5);
		pad6 = (ImageView) findViewById(R.id.pad6);
		pad7 = (ImageView) findViewById(R.id.pad7);
		pad8 = (ImageView) findViewById(R.id.pad8);

		t1 = (ImageView) findViewById(R.id.t1);
		t2 = (ImageView) findViewById(R.id.t2);
		t3 = (ImageView) findViewById(R.id.t3);
		t4 = (ImageView) findViewById(R.id.t4);

		theme = (View) findViewById(R.id.topbar);

		rht_bt.setOnClickListener(this);
		rht_prev.setOnClickListener(this);
		rht_next.setOnClickListener(this);

		metro_bt.setOnClickListener(this);
		metro_prev.setOnClickListener(this);
		metro_next.setOnClickListener(this);

		pad1.setOnClickListener(this);
		pad2.setOnClickListener(this);
		pad3.setOnClickListener(this);
		pad4.setOnClickListener(this);
		pad5.setOnClickListener(this);
		pad6.setOnClickListener(this);
		pad7.setOnClickListener(this);
		pad8.setOnClickListener(this);

		sound_bt.setOnClickListener(this);
		sound_prev.setOnClickListener(this);
		sound_next.setOnClickListener(this);

		t1.setOnClickListener(this);
		t2.setOnClickListener(this);
		t3.setOnClickListener(this);
		t4.setOnClickListener(this);

		djvol1 = (SeekBar) findViewById(R.id.djvol1);
		djvol2 = (SeekBar) findViewById(R.id.djvol2);
		djvol3 = (SeekBar) findViewById(R.id.djvol3);
		djvol4 = (SeekBar) findViewById(R.id.djvol4);
		djvol5 = (SeekBar) findViewById(R.id.djvol5);
		djvol6 = (SeekBar) findViewById(R.id.djvol6);

		djvol1.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {

			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {

			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				float soundvol = (progress) / 100.0f;
				mpdj1.setVolume(soundvol, soundvol);
			}
		});

		djvol2.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {

			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {

			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				float soundvol = (progress) / 100.0f;
				mpdj2.setVolume(soundvol, soundvol);
			}
		});
		djvol3.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {

			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {

			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				float soundvol = (progress) / 100.0f;
				mpdj3.setVolume(soundvol, soundvol);
			}
		});
		djvol4.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {

			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {

			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				float soundvol = (progress) / 100.0f;
				mpdj4.setVolume(soundvol, soundvol);
			}
		});
		djvol5.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {

			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {

			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				float soundvol = (progress) / 100.0f;
				mpdj5.setVolume(soundvol, soundvol);
			}
		});
		djvol6.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {

			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {

			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				float soundvol = (progress) / 100.0f;
				mpdj6.setVolume(soundvol, soundvol);
			}
		});
		
		
		rht_seekbar = (SeekBar) findViewById(R.id.rht_seekbar);
		sound_seekbar = (SeekBar) findViewById(R.id.sound_seekbar);

		rht_seekbar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {

			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {

			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				float soundvol = (progress) / 100.0f;
				rhtmp.setVolume(soundvol, soundvol);
			}
		});

		sound_seekbar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {

			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {

			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {

				float soundvol = (progress) / 100.0f;
				keymp1.setVolume(soundvol, soundvol);
				keymp2.setVolume(soundvol, soundvol);
				keymp3.setVolume(soundvol, soundvol);
				keymp4.setVolume(soundvol, soundvol);
				keymp5.setVolume(soundvol, soundvol);
				keymp6.setVolume(soundvol, soundvol);
				keymp7.setVolume(soundvol, soundvol);
				keymp8.setVolume(soundvol, soundvol);
				keymp9.setVolume(soundvol, soundvol);
				keymp10.setVolume(soundvol, soundvol);
				keymp11.setVolume(soundvol, soundvol);
				keymp12.setVolume(soundvol, soundvol);
				keymp13.setVolume(soundvol, soundvol);
				keymp14.setVolume(soundvol, soundvol);
				keymp15.setVolume(soundvol, soundvol);
				keymp16.setVolume(soundvol, soundvol);
				keymp17.setVolume(soundvol, soundvol);
				keymp18.setVolume(soundvol, soundvol);
				keymp19.setVolume(soundvol, soundvol);
				keymp20.setVolume(soundvol, soundvol);
				keymp21.setVolume(soundvol, soundvol);
				keymp22.setVolume(soundvol, soundvol);
				keymp23.setVolume(soundvol, soundvol);
				keymp0.setVolume(soundvol, soundvol);

			}
		});

		piano = (Piano) findViewById(R.id.pi);
		piano.setPianoKeyListener(new PianoKeyListener() {
			@Override
			public void keyPressed(int id, int action) {

				if (action == MotionEvent.ACTION_DOWN) {
					Log.e("pino", "action:" + action + " id: " + id);

					if (Integer.valueOf(MusicLoopClass.mpsound) == 1) {
						play((getsound(Integer.valueOf(MusicLoopClass.mpsound))
								+ id + ".ogg"), id);

					} else {
						play((getsound(Integer.valueOf(MusicLoopClass.mpsound))
								+ id + ".ogg"), id);

					}

				}
			}
		});

		screen1 = (ImageView) findViewById(R.id.screen1);
		screen2 = (ImageView) findViewById(R.id.screen2);
		screen3 = (ImageView) findViewById(R.id.screen3);

		mscreen1 = (View) findViewById(R.id.mscreen1);
		mscreen2 = (View) findViewById(R.id.mscreen2);
		mscreen3 = (View) findViewById(R.id.mscreen3);

		screen1.setOnClickListener(this);
		screen2.setOnClickListener(this);
		screen3.setOnClickListener(this);

		adView = (AdView) findViewById(R.id.adView);
		displayInterstitial();
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.navdrawer);
		String[] values1 = new String[] { "Rate Now", "Info", "More Apps" };
		ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, android.R.id.text1,
				values1);
		mDrawerList.setAdapter(adapter1);
		mDrawerList
				.setOnItemClickListener(new AdapterView.OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> parent, View view,
							int position, long id) {
						Intent intentweb = new Intent(getcontext(),
								WebBrowser.class);
						switch (position) {
						case 0:
							showratedialog();
							mDrawerLayout.closeDrawer(Gravity.RIGHT);
							break;
						case 1:
							intentweb
									.putExtra("URL",
											"https://play.google.com/store/apps/developer?id=DjDeve");
							startActivity(intentweb);
							mDrawerLayout.closeDrawer(Gravity.RIGHT);

							break;
						case 2:
							intentweb
									.putExtra("URL",
											"https://play.google.com/store/apps/developer?id=DjDeve");
							startActivity(intentweb);
							mDrawerLayout.closeDrawer(Gravity.RIGHT);

							break;

						}

					}
				});
		start = (ImageView) findViewById(R.id.start);
		pause = (ImageView) findViewById(R.id.pause);
		menu = (ImageView) findViewById(R.id.menubt);
		menu.setOnClickListener(this);
		start.setOnClickListener(new OnClickListener() {

			@SuppressLint("SimpleDateFormat")
			@Override
			public void onClick(View v) {

				try {

					if (!isRecording) {
						String extStorageDirectory = Environment
								.getExternalStorageDirectory()
								.getAbsolutePath();
						File folder = new File(extStorageDirectory,
								getResources().getString(R.string.app_name));
						if (!folder.exists()) {
							folder.mkdir();
						} else {
							String timeStamp = new SimpleDateFormat(
									"yyyyMMddHHmmss").format(new Date());
							audioFilePath = folder.getAbsolutePath() + "/"
									+ timeStamp + ".mp3";

						}
						Toast.makeText(getcontext(), "recording stated",
								Toast.LENGTH_SHORT).show();
						PackageManager pmanager = getPackageManager();
						if (pmanager
								.hasSystemFeature(PackageManager.FEATURE_MICROPHONE)) {
							mediaRecorder = new MediaRecorder();
							mediaRecorder
									.setAudioSource(MediaRecorder.AudioSource.DEFAULT);
							mediaRecorder
									.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
							mediaRecorder
									.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
							mediaRecorder.setOutputFile(audioFilePath);
							mediaRecorder.prepare();
							mediaRecorder.start();
							isRecording = true;
						}
					} else {
						Toast.makeText(getcontext(),
								"recording  is already stated",
								Toast.LENGTH_SHORT).show();
					}

				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});

		pause.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (isRecording) {
					isRecording = false;
					Toast.makeText(getcontext(),
							"music file saved in SD card  ", Toast.LENGTH_SHORT)
							.show();
					mediaRecorder.stop();
					mediaRecorder.reset();
					mediaRecorder.release();
				}

			}
		});
		mpdj1 = new MediaPlayer();
		mpdj2 = new MediaPlayer();
		mpdj3 = new MediaPlayer();
		mpdj4 = new MediaPlayer();
		mpdj5 = new MediaPlayer();
		mpdj6 = new MediaPlayer();

		mptrack1 = new MediaPlayer();
		mptrack2 = new MediaPlayer();
		mptrack3 = new MediaPlayer();
		mptrack4 = new MediaPlayer();
		mptrack5 = new MediaPlayer();
		mptrack6 = new MediaPlayer();

		addbt = (ImageView) findViewById(R.id.addnew);
		newmenu = (ImageView) findViewById(R.id.newmenu);
		playlist = (ListView) findViewById(R.id.playlist);

		djdisk1 = (ImageView) findViewById(R.id.djdisk1);
		djdisk2 = (ImageView) findViewById(R.id.djdisk2);
		djdisk3 = (ImageView) findViewById(R.id.djdisk3);
		djdisk4 = (ImageView) findViewById(R.id.djdisk4);
		djdisk5 = (ImageView) findViewById(R.id.djdisk5);
		djdisk6 = (ImageView) findViewById(R.id.djdisk6);

		djdisk1.setOnClickListener(this);
		djdisk2.setOnClickListener(this);
		djdisk3.setOnClickListener(this);
		djdisk4.setOnClickListener(this);
		djdisk5.setOnClickListener(this);
		djdisk6.setOnClickListener(this);

		djplaypause1 = (ImageView) findViewById(R.id.djplaypause1);
		djplaypause2 = (ImageView) findViewById(R.id.djplaypause2);
		djplaypause3 = (ImageView) findViewById(R.id.djplaypause3);
		djplaypause4 = (ImageView) findViewById(R.id.djplaypause4);
		djplaypause5 = (ImageView) findViewById(R.id.djplaypause5);
		djplaypause6 = (ImageView) findViewById(R.id.djplaypause6);

		djloop1 = (ImageView) findViewById(R.id.djloop1);
		djloop2 = (ImageView) findViewById(R.id.djloop2);
		djloop3 = (ImageView) findViewById(R.id.djloop3);
		djloop4 = (ImageView) findViewById(R.id.djloop4);
		djloop5 = (ImageView) findViewById(R.id.djloop5);
		djloop6 = (ImageView) findViewById(R.id.djloop6);

		djloop1.setOnClickListener(this);
		djloop2.setOnClickListener(this);
		djloop3.setOnClickListener(this);
		djloop4.setOnClickListener(this);
		djloop5.setOnClickListener(this);
		djloop6.setOnClickListener(this);

		djplaypause1.setOnClickListener(this);
		djplaypause2.setOnClickListener(this);
		djplaypause3.setOnClickListener(this);
		djplaypause4.setOnClickListener(this);
		djplaypause5.setOnClickListener(this);
		djplaypause6.setOnClickListener(this);

		addbt.setOnClickListener(this);
		newmenu.setOnClickListener(this);

		trackprev1 = (ImageView) findViewById(R.id.trackprev1);
		trackprev2 = (ImageView) findViewById(R.id.trackprev2);
		trackprev3 = (ImageView) findViewById(R.id.trackprev3);
		trackprev4 = (ImageView) findViewById(R.id.trackprev4);
		trackprev5 = (ImageView) findViewById(R.id.trackprev5);
		trackprev6 = (ImageView) findViewById(R.id.trackprev6);

		tracknext1 = (ImageView) findViewById(R.id.tracknext1);
		tracknext2 = (ImageView) findViewById(R.id.tracknext2);
		tracknext3 = (ImageView) findViewById(R.id.tracknext3);
		tracknext4 = (ImageView) findViewById(R.id.tracknext4);
		tracknext5 = (ImageView) findViewById(R.id.tracknext5);
		tracknext6 = (ImageView) findViewById(R.id.tracknext6);

		trackplaypause1 = (ImageView) findViewById(R.id.trackplaypause1);
		trackplaypause2 = (ImageView) findViewById(R.id.trackplaypause2);
		trackplaypause3 = (ImageView) findViewById(R.id.trackplaypause3);
		trackplaypause4 = (ImageView) findViewById(R.id.trackplaypause4);
		trackplaypause5 = (ImageView) findViewById(R.id.trackplaypause5);
		trackplaypause6 = (ImageView) findViewById(R.id.trackplaypause6);

		tracktxt1 = (TextView) findViewById(R.id.tracktxt1);
		tracktxt2 = (TextView) findViewById(R.id.tracktxt2);
		tracktxt3 = (TextView) findViewById(R.id.tracktxt3);
		tracktxt4 = (TextView) findViewById(R.id.tracktxt4);
		tracktxt5 = (TextView) findViewById(R.id.tracktxt5);
		tracktxt6 = (TextView) findViewById(R.id.tracktxt6);

		trackseekbar1 = (SeekBar) findViewById(R.id.trackseekbar1);
		trackseekbar2 = (SeekBar) findViewById(R.id.trackseekbar2);
		trackseekbar3 = (SeekBar) findViewById(R.id.trackseekbar3);
		trackseekbar4 = (SeekBar) findViewById(R.id.trackseekbar4);
		trackseekbar5 = (SeekBar) findViewById(R.id.trackseekbar5);
		trackseekbar6 = (SeekBar) findViewById(R.id.trackseekbar6);

		waves1 = (LinearLayout) findViewById(R.id.waves1);
		waves2 = (LinearLayout) findViewById(R.id.waves2);
		waves3 = (LinearLayout) findViewById(R.id.waves3);
		waves4 = (LinearLayout) findViewById(R.id.waves4);
		waves5 = (LinearLayout) findViewById(R.id.waves5);
		waves6 = (LinearLayout) findViewById(R.id.waves6);

		trackprev1.setOnClickListener(this);
		trackprev2.setOnClickListener(this);
		trackprev3.setOnClickListener(this);
		trackprev4.setOnClickListener(this);
		trackprev5.setOnClickListener(this);
		trackprev6.setOnClickListener(this);

		tracknext1.setOnClickListener(this);
		tracknext2.setOnClickListener(this);
		tracknext3.setOnClickListener(this);
		tracknext4.setOnClickListener(this);
		tracknext5.setOnClickListener(this);
		tracknext6.setOnClickListener(this);

		trackplaypause1.setOnClickListener(this);
		trackplaypause2.setOnClickListener(this);
		trackplaypause3.setOnClickListener(this);
		trackplaypause4.setOnClickListener(this);
		trackplaypause5.setOnClickListener(this);
		trackplaypause6.setOnClickListener(this);

		playlist.setOnItemLongClickListener(new MyClickListener1());
		playlist.setOnDragListener(new DragListener1());
		findViewById(R.id.layer1).setOnDragListener(new DragListener1());
		findViewById(R.id.layer2).setOnDragListener(new DragListener2());
		findViewById(R.id.layer3).setOnDragListener(new DragListener3());
		findViewById(R.id.layer4).setOnDragListener(new DragListener4());
		findViewById(R.id.layer5).setOnDragListener(new DragListener5());
		findViewById(R.id.layer6).setOnDragListener(new DragListener6());

		musicadapter = new MusicAdapter(MainActivity.this,
				R.layout.playlistitem, musiclist);
		playlist.setAdapter(musicadapter);

		anim = new RotateAnimation(0f, 350f, RotateAnimation.RELATIVE_TO_SELF,
				0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.5f);
		anim.setInterpolator(new LinearInterpolator());
		anim.setRepeatCount(Animation.INFINITE);
		anim.setDuration(700);

		mpdj1.setOnCompletionListener(new OnCompletionListener() {
			@Override
			public void onCompletion(MediaPlayer mp) {
				djdisk1.setAnimation(null);
				isDjPlaying1 = false;
				isdatasetDj1 = false;
				djplaypause1.setBackgroundResource(R.drawable.playdj);
			}
		});
		mpdj2.setOnCompletionListener(new OnCompletionListener() {
			@Override
			public void onCompletion(MediaPlayer mp) {
				djdisk2.setAnimation(null);
				isDjPlaying2 = false;
				isdatasetDj2 = false;
				djplaypause2.setBackgroundResource(R.drawable.playdj);
			}
		});
		mpdj3.setOnCompletionListener(new OnCompletionListener() {
			@Override
			public void onCompletion(MediaPlayer mp) {
				djdisk3.setAnimation(null);
				isDjPlaying3 = false;
				isdatasetDj3 = false;
				djplaypause3.setBackgroundResource(R.drawable.playdj);
			}
		});
		mpdj4.setOnCompletionListener(new OnCompletionListener() {
			@Override
			public void onCompletion(MediaPlayer mp) {
				djdisk4.setAnimation(null);
				isDjPlaying4 = false;
				isdatasetDj4 = false;
				djplaypause4.setBackgroundResource(R.drawable.playdj);
			}
		});
		mpdj5.setOnCompletionListener(new OnCompletionListener() {
			@Override
			public void onCompletion(MediaPlayer mp) {
				djdisk5.setAnimation(null);
				isDjPlaying5 = false;
				isdatasetDj5 = false;
				djplaypause5.setBackgroundResource(R.drawable.playdj);
			}
		});
		mpdj6.setOnCompletionListener(new OnCompletionListener() {
			@Override
			public void onCompletion(MediaPlayer mp) {
				djdisk6.setAnimation(null);
				isDjPlaying6 = false;
				isdatasetDj6 = false;
				djplaypause6.setBackgroundResource(R.drawable.playdj);
			}
		});

		trackseekbar1.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				float vol1 = (progress) / 100.0f;
				mptrack1.setVolume(vol1, vol1);
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
			}
		});
		trackseekbar2.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				float vol1 = (progress) / 100.0f;
				mptrack2.setVolume(vol1, vol1);
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
			}
		});
		trackseekbar3.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				float vol1 = (progress) / 100.0f;
				mptrack3.setVolume(vol1, vol1);
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
			}
		});
		trackseekbar4.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				float vol1 = (progress) / 100.0f;
				mptrack4.setVolume(vol1, vol1);
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
			}
		});
		trackseekbar5.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				float vol1 = (progress) / 100.0f;
				mptrack5.setVolume(vol1, vol1);
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
			}
		});
		trackseekbar6.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				float vol1 = (progress) / 100.0f;
				mptrack6.setVolume(vol1, vol1);
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
			}
		});

		mp1 = new MediaPlayer();
		mp2 = new MediaPlayer();
		mp3 = new MediaPlayer();
		mp4 = new MediaPlayer();
		mp5 = new MediaPlayer();
		mp6 = new MediaPlayer();
		mp7 = new MediaPlayer();
		mp8 = new MediaPlayer();
		mp9 = new MediaPlayer();
		mp10 = new MediaPlayer();
		mp11 = new MediaPlayer();
		mp12 = new MediaPlayer();
		mp13 = new MediaPlayer();
		mp14 = new MediaPlayer();
		mp15 = new MediaPlayer();
		mp16 = new MediaPlayer();
		mp17 = new MediaPlayer();
		mp18 = new MediaPlayer();
		mp19 = new MediaPlayer();
		mp20 = new MediaPlayer();
		mp21 = new MediaPlayer();
		mp22 = new MediaPlayer();
		mp23 = new MediaPlayer();
		mp24 = new MediaPlayer();
		mp25 = new MediaPlayer();
		mp26 = new MediaPlayer();
		mp27 = new MediaPlayer();
		mp28 = new MediaPlayer();
		mp29 = new MediaPlayer();
		mp30 = new MediaPlayer();
		mp31 = new MediaPlayer();
		mp32 = new MediaPlayer();
		mp33 = new MediaPlayer();
		mp34 = new MediaPlayer();
		mp35 = new MediaPlayer();
		mp36 = new MediaPlayer();
		mp37 = new MediaPlayer();
		mp38 = new MediaPlayer();
		mp39 = new MediaPlayer();
		mp40 = new MediaPlayer();

		padvolseek = (VerticalSeekBar) findViewById(R.id.padvolseek);
		padvolseek.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				final float vol1 = (progress) / 100.0f;
				MainActivity.this.runOnUiThread(new Runnable() {

					@Override
					public void run() {

						mp1.setVolume(vol1, vol1);
						mp2.setVolume(vol1, vol1);
						mp3.setVolume(vol1, vol1);
						mp4.setVolume(vol1, vol1);
						mp5.setVolume(vol1, vol1);
						mp6.setVolume(vol1, vol1);
						mp7.setVolume(vol1, vol1);
						mp8.setVolume(vol1, vol1);
						mp9.setVolume(vol1, vol1);
						mp10.setVolume(vol1, vol1);
						mp11.setVolume(vol1, vol1);
						mp12.setVolume(vol1, vol1);
						mp13.setVolume(vol1, vol1);
						mp14.setVolume(vol1, vol1);
						mp15.setVolume(vol1, vol1);
						mp16.setVolume(vol1, vol1);
						mp17.setVolume(vol1, vol1);
						mp18.setVolume(vol1, vol1);
						mp19.setVolume(vol1, vol1);
						mp20.setVolume(vol1, vol1);
						mp21.setVolume(vol1, vol1);
						mp22.setVolume(vol1, vol1);
						mp23.setVolume(vol1, vol1);
						mp24.setVolume(vol1, vol1);
						mp25.setVolume(vol1, vol1);
						mp26.setVolume(vol1, vol1);
						mp27.setVolume(vol1, vol1);
						mp28.setVolume(vol1, vol1);
						mp29.setVolume(vol1, vol1);
						mp30.setVolume(vol1, vol1);
						mp31.setVolume(vol1, vol1);
						mp32.setVolume(vol1, vol1);
						mp33.setVolume(vol1, vol1);
						mp34.setVolume(vol1, vol1);
						mp35.setVolume(vol1, vol1);
						mp36.setVolume(vol1, vol1);
						mp37.setVolume(vol1, vol1);
						mp38.setVolume(vol1, vol1);
						mp39.setVolume(vol1, vol1);
						mp40.setVolume(vol1, vol1);
					}
				});
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
			}
		});

		s1 = (ImageView) findViewById(R.id.s1);
		s2 = (ImageView) findViewById(R.id.s2);
		s3 = (ImageView) findViewById(R.id.s3);
		s4 = (ImageView) findViewById(R.id.s4);
		s5 = (ImageView) findViewById(R.id.s5);
		s6 = (ImageView) findViewById(R.id.s6);
		s7 = (ImageView) findViewById(R.id.s7);
		s8 = (ImageView) findViewById(R.id.s8);
		s9 = (ImageView) findViewById(R.id.s9);
		s10 = (ImageView) findViewById(R.id.s10);
		s11 = (ImageView) findViewById(R.id.s11);
		s12 = (ImageView) findViewById(R.id.s12);
		s13 = (ImageView) findViewById(R.id.s13);
		s14 = (ImageView) findViewById(R.id.s14);
		s15 = (ImageView) findViewById(R.id.s15);
		s16 = (ImageView) findViewById(R.id.s16);
		s17 = (ImageView) findViewById(R.id.s17);
		s18 = (ImageView) findViewById(R.id.s18);
		s19 = (ImageView) findViewById(R.id.s19);
		s20 = (ImageView) findViewById(R.id.s20);
		s21 = (ImageView) findViewById(R.id.s21);
		s22 = (ImageView) findViewById(R.id.s22);
		s23 = (ImageView) findViewById(R.id.s23);
		s24 = (ImageView) findViewById(R.id.s24);
		s25 = (ImageView) findViewById(R.id.s25);
		s26 = (ImageView) findViewById(R.id.s26);
		s27 = (ImageView) findViewById(R.id.s27);
		s28 = (ImageView) findViewById(R.id.s28);
		s29 = (ImageView) findViewById(R.id.s29);
		s30 = (ImageView) findViewById(R.id.s30);
		s31 = (ImageView) findViewById(R.id.s31);
		s32 = (ImageView) findViewById(R.id.s32);
		s33 = (ImageView) findViewById(R.id.s33);
		s34 = (ImageView) findViewById(R.id.s34);
		s35 = (ImageView) findViewById(R.id.s35);
		s36 = (ImageView) findViewById(R.id.s36);
		s37 = (ImageView) findViewById(R.id.s37);
		s38 = (ImageView) findViewById(R.id.s38);
		s39 = (ImageView) findViewById(R.id.s39);
		s40 = (ImageView) findViewById(R.id.s40);

		s1.setOnClickListener(this);
		s2.setOnClickListener(this);
		s3.setOnClickListener(this);
		s4.setOnClickListener(this);
		s5.setOnClickListener(this);
		s6.setOnClickListener(this);
		s7.setOnClickListener(this);
		s8.setOnClickListener(this);
		s9.setOnClickListener(this);
		s10.setOnClickListener(this);
		s11.setOnClickListener(this);
		s12.setOnClickListener(this);
		s13.setOnClickListener(this);
		s14.setOnClickListener(this);
		s15.setOnClickListener(this);
		s16.setOnClickListener(this);
		s17.setOnClickListener(this);
		s18.setOnClickListener(this);
		s19.setOnClickListener(this);
		s20.setOnClickListener(this);
		s21.setOnClickListener(this);
		s22.setOnClickListener(this);
		s23.setOnClickListener(this);
		s24.setOnClickListener(this);
		s25.setOnClickListener(this);
		s26.setOnClickListener(this);
		s27.setOnClickListener(this);
		s28.setOnClickListener(this);
		s29.setOnClickListener(this);
		s30.setOnClickListener(this);
		s31.setOnClickListener(this);
		s32.setOnClickListener(this);
		s33.setOnClickListener(this);
		s34.setOnClickListener(this);
		s35.setOnClickListener(this);
		s36.setOnClickListener(this);
		s37.setOnClickListener(this);
		s38.setOnClickListener(this);
		s39.setOnClickListener(this);
		s40.setOnClickListener(this);


	}

	private static final float VISUALIZER_HEIGHT_DIP = 50f;
	private Visualizer mVisualizer1, mVisualizer2, mVisualizer3, mVisualizer4,
			mVisualizer5, mVisualizer6;
	private VisualizerView mVisualizerView1, mVisualizerView2,
			mVisualizerView3, mVisualizerView4, mVisualizerView5,
			mVisualizerView6;

	private void setupVisualizerFxAndUI1() {
		mVisualizerView1 = new VisualizerView(this);
		mVisualizerView1.setLayoutParams(new ViewGroup.LayoutParams(
				ViewGroup.LayoutParams.FILL_PARENT,
				(int) (VISUALIZER_HEIGHT_DIP * getResources()
						.getDisplayMetrics().density)));
		waves1.addView(mVisualizerView1);
		mVisualizer1 = new Visualizer(mptrack1.getAudioSessionId());
		mVisualizer1.setCaptureSize(Visualizer.getCaptureSizeRange()[1]);
		mVisualizer1.setDataCaptureListener(
				new Visualizer.OnDataCaptureListener() {
					public void onWaveFormDataCapture(Visualizer visualizer,
							byte[] bytes, int samplingRate) {
						mVisualizerView1.updateVisualizer(bytes);
					}

					public void onFftDataCapture(Visualizer visualizer,
							byte[] bytes, int samplingRate) {
					}
				}, Visualizer.getMaxCaptureRate() / 2, true, false);
	}

	private void setupVisualizerFxAndUI2() {
		mVisualizerView2 = new VisualizerView(this);
		mVisualizerView2.setLayoutParams(new ViewGroup.LayoutParams(
				ViewGroup.LayoutParams.FILL_PARENT,
				(int) (VISUALIZER_HEIGHT_DIP * getResources()
						.getDisplayMetrics().density)));
		waves2.addView(mVisualizerView2);
		mVisualizer2 = new Visualizer(mptrack2.getAudioSessionId());
		mVisualizer2.setCaptureSize(Visualizer.getCaptureSizeRange()[1]);
		mVisualizer2.setDataCaptureListener(
				new Visualizer.OnDataCaptureListener() {
					public void onWaveFormDataCapture(Visualizer visualizer,
							byte[] bytes, int samplingRate) {
						mVisualizerView2.updateVisualizer(bytes);
					}

					public void onFftDataCapture(Visualizer visualizer,
							byte[] bytes, int samplingRate) {
					}
				}, Visualizer.getMaxCaptureRate() / 2, true, false);
	}

	private void setupVisualizerFxAndUI3() {
		mVisualizerView3 = new VisualizerView(this);
		mVisualizerView3.setLayoutParams(new ViewGroup.LayoutParams(
				ViewGroup.LayoutParams.FILL_PARENT,
				(int) (VISUALIZER_HEIGHT_DIP * getResources()
						.getDisplayMetrics().density)));
		waves3.addView(mVisualizerView3);
		mVisualizer3 = new Visualizer(mptrack3.getAudioSessionId());
		mVisualizer3.setCaptureSize(Visualizer.getCaptureSizeRange()[1]);
		mVisualizer3.setDataCaptureListener(
				new Visualizer.OnDataCaptureListener() {
					public void onWaveFormDataCapture(Visualizer visualizer,
							byte[] bytes, int samplingRate) {
						mVisualizerView3.updateVisualizer(bytes);
					}

					public void onFftDataCapture(Visualizer visualizer,
							byte[] bytes, int samplingRate) {
					}
				}, Visualizer.getMaxCaptureRate() / 2, true, false);
	}

	private void setupVisualizerFxAndUI4() {
		mVisualizerView4 = new VisualizerView(this);
		mVisualizerView4.setLayoutParams(new ViewGroup.LayoutParams(
				ViewGroup.LayoutParams.FILL_PARENT,
				(int) (VISUALIZER_HEIGHT_DIP * getResources()
						.getDisplayMetrics().density)));
		waves4.addView(mVisualizerView4);
		mVisualizer4 = new Visualizer(mptrack4.getAudioSessionId());
		mVisualizer4.setCaptureSize(Visualizer.getCaptureSizeRange()[1]);
		mVisualizer4.setDataCaptureListener(
				new Visualizer.OnDataCaptureListener() {
					public void onWaveFormDataCapture(Visualizer visualizer,
							byte[] bytes, int samplingRate) {
						mVisualizerView4.updateVisualizer(bytes);
					}

					public void onFftDataCapture(Visualizer visualizer,
							byte[] bytes, int samplingRate) {
					}
				}, Visualizer.getMaxCaptureRate() / 2, true, false);
	}

	private void setupVisualizerFxAndUI5() {
		mVisualizerView5 = new VisualizerView(this);
		mVisualizerView5.setLayoutParams(new ViewGroup.LayoutParams(
				ViewGroup.LayoutParams.FILL_PARENT,
				(int) (VISUALIZER_HEIGHT_DIP * getResources()
						.getDisplayMetrics().density)));
		waves5.addView(mVisualizerView5);
		mVisualizer5 = new Visualizer(mptrack5.getAudioSessionId());
		mVisualizer5.setCaptureSize(Visualizer.getCaptureSizeRange()[1]);
		mVisualizer5.setDataCaptureListener(
				new Visualizer.OnDataCaptureListener() {
					public void onWaveFormDataCapture(Visualizer visualizer,
							byte[] bytes, int samplingRate) {
						mVisualizerView5.updateVisualizer(bytes);
					}

					public void onFftDataCapture(Visualizer visualizer,
							byte[] bytes, int samplingRate) {
					}
				}, Visualizer.getMaxCaptureRate() / 2, true, false);
	}

	private void setupVisualizerFxAndUI6() {
		mVisualizerView6 = new VisualizerView(this);
		mVisualizerView6.setLayoutParams(new ViewGroup.LayoutParams(
				ViewGroup.LayoutParams.FILL_PARENT,
				(int) (VISUALIZER_HEIGHT_DIP * getResources()
						.getDisplayMetrics().density)));
		waves6.addView(mVisualizerView6);
		mVisualizer6 = new Visualizer(mptrack6.getAudioSessionId());
		mVisualizer6.setCaptureSize(Visualizer.getCaptureSizeRange()[1]);
		mVisualizer6.setDataCaptureListener(
				new Visualizer.OnDataCaptureListener() {
					public void onWaveFormDataCapture(Visualizer visualizer,
							byte[] bytes, int samplingRate) {
						mVisualizerView6.updateVisualizer(bytes);
					}

					public void onFftDataCapture(Visualizer visualizer,
							byte[] bytes, int samplingRate) {
					}
				}, Visualizer.getMaxCaptureRate() / 2, true, false);
	}

	class VisualizerView extends View {
		private byte[] mBytes;
		private float[] mPoints;
		private Rect mRect = new Rect();

		private Paint mForePaint = new Paint();

		public VisualizerView(Context context) {
			super(context);
			init();
		}

		private void init() {
			mBytes = null;

			mForePaint.setStrokeWidth(4f);
			mForePaint.setAntiAlias(true);
			mForePaint.setColor(Color.rgb(252, 193, 91));
		}

		public void updateVisualizer(byte[] bytes) {
			mBytes = bytes;
			// Log.e("bytes","by: " + bytes);
			invalidate();
		}

		@Override
		protected void onDraw(Canvas canvas) {
			super.onDraw(canvas);

			if (mBytes == null) {
				return;
			}

			if (mPoints == null || mPoints.length < mBytes.length * 4) {
				mPoints = new float[mBytes.length * 4];
			}

			mRect.set(0, 0, getWidth(), getHeight());

			for (int i = 0; i < mBytes.length - 1; i++) {
				mPoints[i * 4] = mRect.width() * i / (mBytes.length - 1);
				mPoints[i * 4 + 1] = mRect.height() / 2
						+ ((byte) (mBytes[i] + 128)) * (mRect.height() / 2)
						/ 128;
				mPoints[i * 4 + 2] = mRect.width() * (i + 1)
						/ (mBytes.length - 1);
				mPoints[i * 4 + 3] = mRect.height() / 2
						+ ((byte) (mBytes[i + 1] + 128)) * (mRect.height() / 2)
						/ 128;
			}

			float centerX = mRect.width();
			float centerY = mRect.height();
			double angle = 90;
			Matrix rotateMat = new Matrix();
			rotateMat.setRotate((float) angle, centerX, centerY);
			// rotateMat.mapPoints(mPoints);

			canvas.drawLines(mPoints, mForePaint);

		}
	}

	private Context getcontext() {

		return MainActivity.this;
	}

	public void displayInterstitial() {

		adRequest = new AdRequest.Builder().build();

		adView.loadAd(adRequest);
		interstitial = new InterstitialAd(getcontext());
		interstitial.setAdUnitId("ca-app-pub-3805784166574868/7131260340");
		interstitial.loadAd(adRequest);
		interstitial.setAdListener(new AdListener() {
			public void onAdLoaded() {
				// displayInterstitial();
				interstitial.show();
			}
		});
		// }
	}

	class DragListener1 implements OnDragListener {
		@Override
		public boolean onDrag(View v, DragEvent event) {

			switch (event.getAction()) {

			case DragEvent.ACTION_DRAG_STARTED:

				break;

			case DragEvent.ACTION_DRAG_ENTERED:
				break;

			case DragEvent.ACTION_DRAG_EXITED:
				break;

			case DragEvent.ACTION_DROP:
				mpdj1.reset();
				ClipData.Item item = event.getClipData().getItemAt(0);
				File filename11 = new File(item.getText().toString());
				String name = filename11.getName();
				Log.e("name : ", name);

				filename1 = new File(item.getText().toString());

				isdatasetDj1 = true;
				try {

					djplaypause1.setBackgroundResource(R.drawable.pausedj);
					playdjmusic1(filename1.getAbsolutePath());
				} catch (IllegalStateException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();

				}
				break;

			case DragEvent.ACTION_DRAG_ENDED:
				break;
			default:
				break;
			}
			return true;
		}
	}

	class DragListener2 implements OnDragListener {
		@Override
		public boolean onDrag(View v, DragEvent event) {

			switch (event.getAction()) {

			case DragEvent.ACTION_DRAG_STARTED:

				break;

			case DragEvent.ACTION_DRAG_ENTERED:
				break;

			case DragEvent.ACTION_DRAG_EXITED:
				break;

			case DragEvent.ACTION_DROP:
				mpdj2.reset();
				ClipData.Item item = event.getClipData().getItemAt(0);
				File filename11 = new File(item.getText().toString());
				String name = filename11.getName();
				Log.e("name : ", name);

				filename2 = new File(item.getText().toString());

				isdatasetDj2 = true;
				try {

					djplaypause2.setBackgroundResource(R.drawable.pausedj);
					playdjmusic2(filename2.getAbsolutePath());
				} catch (IllegalStateException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();

				}
				break;

			case DragEvent.ACTION_DRAG_ENDED:
				break;
			default:
				break;
			}
			return true;
		}
	}

	class DragListener3 implements OnDragListener {
		@Override
		public boolean onDrag(View v, DragEvent event) {

			switch (event.getAction()) {

			case DragEvent.ACTION_DRAG_STARTED:

				break;

			case DragEvent.ACTION_DRAG_ENTERED:
				break;

			case DragEvent.ACTION_DRAG_EXITED:
				break;

			case DragEvent.ACTION_DROP:
				mpdj3.reset();
				ClipData.Item item = event.getClipData().getItemAt(0);
				File filename11 = new File(item.getText().toString());
				String name = filename11.getName();
				Log.e("name : ", name);

				filename3 = new File(item.getText().toString());

				isdatasetDj3 = true;
				try {

					djplaypause3.setBackgroundResource(R.drawable.pausedj);
					playdjmusic3(filename3.getAbsolutePath());
				} catch (IllegalStateException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();

				}
				break;

			case DragEvent.ACTION_DRAG_ENDED:
				break;
			default:
				break;
			}
			return true;
		}
	}

	class DragListener4 implements OnDragListener {
		@Override
		public boolean onDrag(View v, DragEvent event) {

			switch (event.getAction()) {

			case DragEvent.ACTION_DRAG_STARTED:

				break;

			case DragEvent.ACTION_DRAG_ENTERED:
				break;

			case DragEvent.ACTION_DRAG_EXITED:
				break;

			case DragEvent.ACTION_DROP:
				mpdj4.reset();
				ClipData.Item item = event.getClipData().getItemAt(0);
				File filename11 = new File(item.getText().toString());
				String name = filename11.getName();
				Log.e("name : ", name);

				filename4 = new File(item.getText().toString());

				isdatasetDj4 = true;
				try {

					djplaypause4.setBackgroundResource(R.drawable.pausedj);
					playdjmusic4(filename4.getAbsolutePath());
				} catch (IllegalStateException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();

				}
				break;

			case DragEvent.ACTION_DRAG_ENDED:
				break;
			default:
				break;
			}
			return true;
		}
	}

	class DragListener5 implements OnDragListener {
		@Override
		public boolean onDrag(View v, DragEvent event) {

			switch (event.getAction()) {

			case DragEvent.ACTION_DRAG_STARTED:

				break;

			case DragEvent.ACTION_DRAG_ENTERED:
				break;

			case DragEvent.ACTION_DRAG_EXITED:
				break;

			case DragEvent.ACTION_DROP:
				mpdj5.reset();
				ClipData.Item item = event.getClipData().getItemAt(0);
				File filename11 = new File(item.getText().toString());
				String name = filename11.getName();
				Log.e("name : ", name);

				filename5 = new File(item.getText().toString());

				isdatasetDj5 = true;
				try {

					djplaypause5.setBackgroundResource(R.drawable.pausedj);
					playdjmusic5(filename5.getAbsolutePath());
				} catch (IllegalStateException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();

				}
				break;

			case DragEvent.ACTION_DRAG_ENDED:
				break;
			default:
				break;
			}
			return true;
		}
	}

	class DragListener6 implements OnDragListener {
		@Override
		public boolean onDrag(View v, DragEvent event) {

			switch (event.getAction()) {

			case DragEvent.ACTION_DRAG_STARTED:

				break;

			case DragEvent.ACTION_DRAG_ENTERED:
				break;

			case DragEvent.ACTION_DRAG_EXITED:
				break;

			case DragEvent.ACTION_DROP:
				mpdj6.reset();
				ClipData.Item item = event.getClipData().getItemAt(0);
				File filename11 = new File(item.getText().toString());
				String name = filename11.getName();
				Log.e("name : ", name);

				filename6 = new File(item.getText().toString());

				isdatasetDj6 = true;
				try {

					djplaypause6.setBackgroundResource(R.drawable.pausedj);
					playdjmusic6(filename6.getAbsolutePath());
				} catch (IllegalStateException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();

				}
				break;

			case DragEvent.ACTION_DRAG_ENDED:
				break;
			default:
				break;
			}
			return true;
		}
	}

	private void playdjmusic1(String path) throws IllegalStateException,
			IOException {
		try {
			List<Musicmodel> el = dbHandler.Get_playList(false);
			if (el.size() == 0) {
				AssetFileDescriptor descriptor = getAssets().openFd(
						"sample" + path);
				mpdj1.setDataSource(descriptor.getFileDescriptor(),
						descriptor.getStartOffset(), descriptor.getLength());
				descriptor.close();
			} else {
				mpdj1.setDataSource(getApplicationContext(), Uri.parse(path));
			}
			mpdj1.prepare();
			if (isdjloop1) {
				mpdj1.setLooping(true);
			}
			mpdj1.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mpdj1.start();
					isDjPlaying1 = true;
				}
			});
			djdisk1.startAnimation(anim);
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playdjmusic2(String path) throws IllegalStateException,
			IOException {
		try {
			List<Musicmodel> el = dbHandler.Get_playList(false);
			if (el.size() == 0) {
				AssetFileDescriptor descriptor = getAssets().openFd(
						"sample" + path);
				mpdj2.setDataSource(descriptor.getFileDescriptor(),
						descriptor.getStartOffset(), descriptor.getLength());
				descriptor.close();
			} else {
				mpdj2.setDataSource(getApplicationContext(), Uri.parse(path));
			}
			mpdj2.prepare();
			if (isdjloop2) {
				mpdj2.setLooping(true);
			}
			mpdj2.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mpdj2.start();
					isDjPlaying2 = true;
				}
			});
			djdisk2.startAnimation(anim);
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playdjmusic3(String path) throws IllegalStateException,
			IOException {
		try {
			List<Musicmodel> el = dbHandler.Get_playList(false);
			if (el.size() == 0) {
				AssetFileDescriptor descriptor = getAssets().openFd(
						"sample" + path);
				mpdj3.setDataSource(descriptor.getFileDescriptor(),
						descriptor.getStartOffset(), descriptor.getLength());
				descriptor.close();
			} else {
				mpdj3.setDataSource(getApplicationContext(), Uri.parse(path));
			}
			mpdj3.prepare();
			if (isdjloop3) {
				mpdj3.setLooping(true);
			}
			mpdj3.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mpdj3.start();
					isDjPlaying3 = true;
				}
			});
			djdisk3.startAnimation(anim);
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playdjmusic4(String path) throws IllegalStateException,
			IOException {
		try {
			List<Musicmodel> el = dbHandler.Get_playList(false);
			if (el.size() == 0) {
				AssetFileDescriptor descriptor = getAssets().openFd(
						"sample" + path);
				mpdj4.setDataSource(descriptor.getFileDescriptor(),
						descriptor.getStartOffset(), descriptor.getLength());
				descriptor.close();
			} else {
				mpdj4.setDataSource(getApplicationContext(), Uri.parse(path));
			}
			mpdj4.prepare();
			if (isdjloop4) {
				mpdj4.setLooping(true);
			}
			mpdj4.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mpdj4.start();
					isDjPlaying4 = true;
				}
			});
			djdisk4.startAnimation(anim);
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playdjmusic5(String path) throws IllegalStateException,
			IOException {
		try {
			List<Musicmodel> el = dbHandler.Get_playList(false);
			if (el.size() == 0) {
				AssetFileDescriptor descriptor = getAssets().openFd(
						"sample" + path);
				mpdj5.setDataSource(descriptor.getFileDescriptor(),
						descriptor.getStartOffset(), descriptor.getLength());
				descriptor.close();
			} else {
				mpdj5.setDataSource(getApplicationContext(), Uri.parse(path));
			}
			mpdj5.prepare();
			if (isdjloop5) {
				mpdj5.setLooping(true);
			}
			mpdj5.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mpdj5.start();
					isDjPlaying5 = true;
				}
			});
			djdisk5.startAnimation(anim);
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playdjmusic6(String path) throws IllegalStateException,
			IOException {
		try {
			List<Musicmodel> el = dbHandler.Get_playList(false);
			if (el.size() == 0) {
				AssetFileDescriptor descriptor = getAssets().openFd(
						"sample" + path);
				mpdj6.setDataSource(descriptor.getFileDescriptor(),
						descriptor.getStartOffset(), descriptor.getLength());
				descriptor.close();
			} else {
				mpdj6.setDataSource(getApplicationContext(), Uri.parse(path));
			}
			mpdj6.prepare();
			if (isdjloop6) {
				mpdj6.setLooping(true);
			}
			mpdj6.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mpdj6.start();
					isDjPlaying6 = true;
				}
			});
			djdisk6.startAnimation(anim);
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private final class MyClickListener1 implements OnItemLongClickListener {

		@Override
		public boolean onItemLongClick(AdapterView<?> arg0, View view,
				int position, long arg3) {

			String selectedFromList = musiclist.get(position).path;
			ClipData.Item item = new ClipData.Item(
					((CharSequence) selectedFromList.toString()));
			String[] clipDescription = { ClipDescription.MIMETYPE_TEXT_PLAIN };
			ClipData dragData = new ClipData("item", clipDescription, item);
			DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
			view.startDrag(dragData, shadowBuilder, view, 0);
			return true;
		}

	}

	private void play(String sd, int id) {

		switch (id) {
		case 0:
			playkeyboard0(sd);
			break;
		case 1:
			playkeyboard1(sd);
			break;
		case 2:
			playkeyboard2(sd);
			break;
		case 3:
			playkeyboard3(sd);
			break;
		case 4:
			playkeyboard4(sd);
			break;
		case 5:
			playkeyboard5(sd);
			break;
		case 6:
			playkeyboard6(sd);
			break;
		case 7:
			playkeyboard7(sd);
			break;
		case 8:
			playkeyboard8(sd);
			break;
		case 9:
			playkeyboard9(sd);
			break;
		case 10:
			playkeyboard10(sd);
			break;
		case 11:
			playkeyboard11(sd);
			break;
		case 12:
			playkeyboard12(sd);
			break;
		case 13:
			playkeyboard13(sd);
			break;
		case 14:
			playkeyboard14(sd);
			break;
		case 15:
			playkeyboard15(sd);
			break;
		case 16:
			playkeyboard16(sd);
			break;
		case 17:
			playkeyboard17(sd);
			break;
		case 18:
			playkeyboard18(sd);
			break;
		case 19:
			playkeyboard19(sd);
			break;
		case 20:
			playkeyboard20(sd);
			break;
		case 21:
			playkeyboard21(sd);
			break;
		case 22:
			playkeyboard22(sd);
			break;
		case 23:
			playkeyboard23(sd);
			break;

		default:
			break;
		}

	}

	private void playrht(String musicname) {
		try {
			ist1setdata = true;
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			rhtmp.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			rhtmp.prepare();
			rhtmp.setLooping(true);
			rhtmp.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					rhtmp.start();
					istrack1play = true;
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	boolean issounddata = false, issoundplay = false;

	private void playpad1(String musicname) {
		try {

			padmp1.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			padmp1.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			padmp1.prepare();
			padmp1.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					padmp1.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playpad2(String musicname) {
		try {

			padmp2.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			padmp2.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			padmp2.prepare();
			padmp2.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					padmp2.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playpad3(String musicname) {
		try {

			padmp3.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			padmp3.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			padmp3.prepare();
			padmp3.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					padmp3.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playpad4(String musicname) {
		try {

			padmp4.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			padmp4.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			padmp4.prepare();
			padmp4.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					padmp4.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playpad5(String musicname) {
		try {

			padmp5.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			padmp5.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			padmp5.prepare();
			padmp5.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					padmp5.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playpad6(String musicname) {
		try {

			padmp6.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			padmp6.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			padmp6.prepare();
			padmp6.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					padmp6.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playpad7(String musicname) {
		try {

			padmp7.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			padmp7.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			padmp7.prepare();
			padmp7.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					padmp7.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playpad8(String musicname) {
		try {

			padmp8.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			padmp8.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			padmp8.prepare();
			padmp8.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					padmp8.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard1(String musicname) {
		try {

			keymp1.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			keymp1.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp1.prepare();
			keymp1.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp1.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard2(String musicname) {
		try {

			keymp2.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			keymp2.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp2.prepare();
			keymp2.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp2.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard3(String musicname) {
		try {

			keymp3.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			keymp3.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp3.prepare();
			keymp3.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp3.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard4(String musicname) {
		try {

			keymp4.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			keymp4.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp4.prepare();
			keymp4.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp4.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard5(String musicname) {
		try {

			keymp5.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			keymp5.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp5.prepare();
			keymp5.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp5.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard6(String musicname) {
		try {

			keymp6.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			keymp6.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp6.prepare();
			keymp6.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp6.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard7(String musicname) {
		try {

			keymp7.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			keymp7.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp7.prepare();
			keymp7.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp7.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard8(String musicname) {
		try {

			keymp8.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			keymp8.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp8.prepare();
			keymp8.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp8.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard9(String musicname) {
		try {

			keymp9.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			keymp9.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp9.prepare();
			keymp9.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp9.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard10(String musicname) {
		try {

			keymp10.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			keymp10.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp10.prepare();
			keymp10.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp10.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard11(String musicname) {
		try {

			keymp11.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			keymp11.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp11.prepare();
			keymp11.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp11.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard12(String musicname) {
		try {

			keymp12.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			keymp12.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp12.prepare();
			keymp12.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp12.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard13(String musicname) {
		try {

			keymp13.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			keymp13.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp13.prepare();
			keymp13.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp13.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard14(String musicname) {
		try {

			keymp14.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			keymp14.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp14.prepare();
			keymp14.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp14.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard15(String musicname) {
		try {

			keymp15.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			keymp15.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp15.prepare();
			keymp15.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp15.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard16(String musicname) {
		try {

			keymp16.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			keymp16.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp16.prepare();
			keymp16.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp16.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard17(String musicname) {
		try {

			keymp17.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			keymp17.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp17.prepare();
			keymp17.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp17.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard18(String musicname) {
		try {

			keymp18.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			keymp18.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp18.prepare();
			keymp18.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp18.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard19(String musicname) {
		try {

			keymp19.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			keymp19.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp19.prepare();
			keymp19.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp19.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard20(String musicname) {
		try {

			keymp20.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			keymp20.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp20.prepare();
			keymp20.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp20.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard21(String musicname) {
		try {

			keymp21.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			keymp21.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp21.prepare();
			keymp21.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp21.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard22(String musicname) {
		try {

			keymp22.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			keymp22.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp22.prepare();
			keymp22.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp22.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard23(String musicname) {
		try {

			keymp23.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			keymp23.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp23.prepare();
			keymp23.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp23.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard0(String musicname) {
		try {

			keymp0.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			keymp0.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp0.prepare();
			keymp0.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp0.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	boolean issetmetrodata = false, ismetroplay = false;

	private void playmusicmetro(String musicname) {
		try {
			issetmetrodata = true;
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			metromp.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			metromp.prepare();
			metromp.setLooping(true);
			metromp.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					metromp.start();
					ismetroplay = true;
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void leftdjst1(String path) {
		try {
			AssetFileDescriptor descriptor = getAssets()
					.openFd("music/" + path);
			mpdisk1.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mpdisk1.prepare();
			mpdisk1.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mpdisk1.start();

				}
			});

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void leftdjst2(String path) {
		try {
			AssetFileDescriptor descriptor = getAssets()
					.openFd("music/" + path);
			mpdisk2.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mpdisk2.prepare();
			mpdisk2.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mpdisk2.start();

				}
			});

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void leftdjst3(String path) {
		try {
			AssetFileDescriptor descriptor = getAssets()
					.openFd("music/" + path);
			mpdisk3.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mpdisk3.prepare();
			mpdisk3.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mpdisk3.start();

				}
			});

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void leftdjst4(String path) {
		try {
			AssetFileDescriptor descriptor = getAssets()
					.openFd("music/" + path);
			mpdisk4.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mpdisk4.prepare();
			mpdisk4.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mpdisk4.start();

				}
			});

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void leftdjst5(String path) {
		try {
			AssetFileDescriptor descriptor = getAssets()
					.openFd("music/" + path);
			mpdisk5.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mpdisk5.prepare();
			mpdisk5.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mpdisk5.start();

				}
			});

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void leftdjst6(String path) {
		try {
			AssetFileDescriptor descriptor = getAssets()
					.openFd("music/" + path);
			mpdisk6.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mpdisk6.prepare();
			mpdisk6.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mpdisk6.start();

				}
			});

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {

		case R.id.djdisk1:
			mpdisk1.reset();
			leftdjst1("Scratch1.ogg");

			break;
		case R.id.djdisk2:
			mpdisk2.reset();
			leftdjst2("Scratch2.ogg");

			break;
		case R.id.djdisk3:
			mpdisk3.reset();
			leftdjst3("Scratch3.ogg");

			break;
		case R.id.djdisk4:
			mpdisk4.reset();
			leftdjst4("Scratch4.ogg");

			break;
		case R.id.djdisk5:
			mpdisk5.reset();
			leftdjst5("Scratch5.ogg");

			break;
		case R.id.djdisk6:
			mpdisk6.reset();
			leftdjst6("Scratch6.ogg");

			break;

		case R.id.screen1:

			screen1.setImageResource(R.drawable.dj_pressed);
			screen2.setImageResource(R.drawable.piano_normal);
			screen3.setImageResource(R.drawable.pad_normal);

			mscreen2.setVisibility(View.GONE);
			mscreen3.setVisibility(View.GONE);
			mscreen1.setVisibility(View.VISIBLE);

			break;
		case R.id.screen2:
			screen1.setImageResource(R.drawable.dj_normal);
			screen2.setImageResource(R.drawable.piano_pressed);
			screen3.setImageResource(R.drawable.pad_normal);

			mscreen1.setVisibility(View.GONE);
			mscreen3.setVisibility(View.GONE);
			mscreen2.setVisibility(View.VISIBLE);

			break;
		case R.id.screen3:

			screen1.setImageResource(R.drawable.dj_normal);
			screen2.setImageResource(R.drawable.piano_normal);
			screen3.setImageResource(R.drawable.pad_pressed);
			mscreen1.setVisibility(View.GONE);
			mscreen2.setVisibility(View.GONE);
			mscreen3.setVisibility(View.VISIBLE);

			break;

		case R.id.menubt:
			mDrawerLayout.openDrawer(Gravity.RIGHT);
			break;
		case R.id.rht_bt:
			if (rhtmp.isPlaying()) {
				rhtmp.pause();
				istrack1play = false;
				Log.e("", "pause song");
				rht_bt.setImageResource(R.drawable.play_bt);
			} else {
				if (ist1setdata) {
					istrack1play = true;
					rhtmp.start();
					Log.e("", "play song");
					rht_bt.setImageResource(R.drawable.pause_bt);
				} else {
					Log.e("", "play  first time song");
					rht_bt.setImageResource(R.drawable.pause_bt);
					playrht("t" + MusicLoopClass.mplooppiano + ".ogg");
				}
			}
			break;
		case R.id.rht_prev:

			rhtmp.reset();
			int downname1 = Integer.valueOf(MusicLoopClass.mplooppiano) - 1;

			if (downname1 == 0) {
				MusicLoopClass.mplooppiano = "15";
			} else {
				MusicLoopClass.mplooppiano = downname1 + "";
			}

			rht_bt.setImageResource(R.drawable.pause_bt);
			rht_txt.setText("Loop" + MusicLoopClass.mplooppiano);
			playrht("t" + MusicLoopClass.mplooppiano + ".ogg");
			break;

		case R.id.rht_next:
			rhtmp.reset();
			int upname1 = Integer.valueOf(MusicLoopClass.mplooppiano) + 1;
			if (upname1 == 16) {
				MusicLoopClass.mplooppiano = "1";
			} else {
				MusicLoopClass.mplooppiano = upname1 + "";
			}
			rht_bt.setImageResource(R.drawable.pause_bt);
			rht_txt.setText("Loop" + MusicLoopClass.mplooppiano);
			playrht("t" + MusicLoopClass.mplooppiano + ".ogg");
			break;

		case R.id.sound_prev:

			soundmp.reset();
			int downsound = Integer.valueOf(MusicLoopClass.mpsound) - 1;

			if (downsound == 0) {
				MusicLoopClass.mpsound = "4";
			} else {
				MusicLoopClass.mpsound = downsound + "";
			}
			sound_bt.setImageResource(R.drawable.pause_bt);
			sound_txt
					.setText(getsound(Integer.valueOf(MusicLoopClass.mpsound)));

			if (Integer.valueOf(MusicLoopClass.mpsound) == 1) {
				// playsound(getsound(Integer.valueOf(MusicLoopClass.mpsound)) +
				// ".wav");

			} else {
				// playsound(getsound(Integer.valueOf(MusicLoopClass.mpsound)) +
				// ".mp3");
			}
			break;

		case R.id.sound_next:
			soundmp.reset();
			int upsound = Integer.valueOf(MusicLoopClass.mpsound) + 1;
			if (upsound == 5) {
				MusicLoopClass.mpsound = "1";
			} else {
				MusicLoopClass.mpsound = upsound + "";
			}
			sound_bt.setImageResource(R.drawable.pause_bt);
			sound_txt
					.setText(getsound(Integer.valueOf(MusicLoopClass.mpsound)));

			if (Integer.valueOf(MusicLoopClass.mpsound) == 1) {
				// playsound(getsound(Integer.valueOf(MusicLoopClass.mpsound)) +
				// ".wav");

			} else {
				// playsound(getsound(Integer.valueOf(MusicLoopClass.mpsound)) +
				// ".mp3");
			}

			break;

		case R.id.metro_bt:
			if (metromp.isPlaying()) {
				metromp.pause();
				ismetroplay = false;
				metro_bt.setImageResource(R.drawable.play_bt);
			} else {
				if (issetmetrodata) {
					ismetroplay = true;
					metromp.start();
					metro_bt.setImageResource(R.drawable.pause_bt);
				} else {
					metro_bt.setImageResource(R.drawable.pause_bt);
					playmusicmetro("r" + MusicLoopClass.mpmetro + ".ogg");
				}
			}
			break;
		case R.id.metro_prev:
			metromp.reset();
			int downmetro = Integer.valueOf(MusicLoopClass.mpmetro) - 1;

			if (downmetro == 0) {
				MusicLoopClass.mpmetro = "10";
			} else {
				MusicLoopClass.mpmetro = downmetro + "";
			}

			metro_bt.setImageResource(R.drawable.pause_bt);
			metro_txt.setText(getbpm(Integer.valueOf(MusicLoopClass.mpmetro)));
			playmusicmetro("r" + Integer.valueOf(MusicLoopClass.mpmetro)
					+ ".ogg");
			break;

		case R.id.metro_next:
			metromp.reset();
			int upmetro = Integer.valueOf(MusicLoopClass.mpmetro) + 1;
			if (upmetro == 11) {
				MusicLoopClass.mpmetro = "1";
			} else {
				MusicLoopClass.mpmetro = upmetro + "";
			}
			metro_bt.setImageResource(R.drawable.pause_bt);
			metro_txt.setText(getbpm(Integer.valueOf(MusicLoopClass.mpmetro)));
			playmusicmetro("r" + Integer.valueOf(MusicLoopClass.mpmetro)
					+ ".ogg");
			break;

		case R.id.pad1:
			playpad1("p1.ogg");
			break;
		case R.id.pad2:
			playpad2("p2.ogg");
			break;
		case R.id.pad3:
			playpad3("p3.ogg");
			break;
		case R.id.pad4:
			playpad4("p4.ogg");
			break;
		case R.id.pad5:
			playpad5("p5.ogg");
			break;
		case R.id.pad6:
			playpad6("p6.ogg");
			break;
		case R.id.pad7:
			playpad7("p7.ogg");
			break;
		case R.id.pad8:
			playpad8("p8.ogg");
			break;

		case R.id.t1:
			setthemedefault();
			t1.setImageResource(R.drawable.a_color);
			theme.setBackground(getResources().getDrawable(R.drawable.back1));
			break;

		case R.id.t2:
			setthemedefault();
			t2.setImageResource(R.drawable.b_color);
			theme.setBackground(getResources().getDrawable(R.drawable.back2));
			break;

		case R.id.t3:
			setthemedefault();
			t3.setImageResource(R.drawable.c_color);
			theme.setBackground(getResources().getDrawable(R.drawable.back3));
			break;

		case R.id.t4:
			setthemedefault();
			t4.setImageResource(R.drawable.d_color);
			theme.setBackground(getResources().getDrawable(R.drawable.back4));
			break;
		case R.id.addnew:
			startActivity(new Intent(getactivity(), FileBrowserActivity.class));
			break;
		case R.id.newmenu:
			String[] s = getResources().getStringArray(R.array.setting_Option);

			new AlertDialog.Builder(getactivity())
					.setItems(s, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							switch (which) {
							case 0:
								dbHandler.Delete_All();
								musiclist.clear();
								musicadapter.notifyDataSetChanged();
								List<Musicmodel> ei = dbHandler
										.Get_playList(false);
								if (ei.size() == 0) {
									AssetManager assetManager = getApplicationContext()
											.getAssets();
									String[] fields;
									try {
										fields = assetManager.list("sample");
										for (int count = 0; count < fields.length; count++) {

											Musicmodel m = new Musicmodel();
											m.name = fields[count];
											m.path = fields[count];
											musiclist.add(m);
										}
									} catch (IOException e1) {
										e1.printStackTrace();
									}
								}
								break;
							case 1:
								musiclist.clear();
								musicadapter.notifyDataSetChanged();
								List<Musicmodel> el = dbHandler
										.Get_playList(true);
								for (Musicmodel e : el) {
									musiclist.add(e);
								}
								musicadapter.notifyDataSetChanged();
								break;
							case 2:
								showalert();
								break;
							case 3:
								openplaylist();
								break;
							}
						}
					}).create().show();
			break;
		case R.id.djloop1:
			isdjloop1 = !isdjloop1;
			if (isdjloop1) {
				djloop1.setBackgroundResource(R.drawable.loop);
				mpdj1.setLooping(true);
			} else {
				djloop1.setBackgroundResource(R.drawable.notloop);
				mpdj1.setLooping(false);
			}
			break;
		case R.id.djloop2:
			isdjloop2 = !isdjloop2;
			if (isdjloop2) {
				djloop2.setBackgroundResource(R.drawable.loop);
				mpdj2.setLooping(true);
			} else {
				djloop2.setBackgroundResource(R.drawable.notloop);
				mpdj2.setLooping(false);
			}
			break;
		case R.id.djloop3:
			isdjloop3 = !isdjloop3;
			if (isdjloop3) {
				djloop3.setBackgroundResource(R.drawable.loop);
				mpdj3.setLooping(true);
			} else {
				djloop3.setBackgroundResource(R.drawable.notloop);
				mpdj3.setLooping(false);
			}
			break;
		case R.id.djloop4:
			isdjloop4 = !isdjloop4;
			if (isdjloop4) {
				djloop4.setBackgroundResource(R.drawable.loop);
				mpdj4.setLooping(true);
			} else {
				djloop4.setBackgroundResource(R.drawable.notloop);
				mpdj4.setLooping(false);
			}
			break;
		case R.id.djloop5:
			isdjloop5 = !isdjloop5;
			if (isdjloop5) {
				djloop5.setBackgroundResource(R.drawable.loop);
				mpdj5.setLooping(true);
			} else {
				djloop5.setBackgroundResource(R.drawable.notloop);
				mpdj5.setLooping(false);
			}
			break;
		case R.id.djloop6:
			isdjloop6 = !isdjloop6;
			if (isdjloop6) {
				djloop6.setBackgroundResource(R.drawable.loop);
				mpdj6.setLooping(true);
			} else {
				djloop6.setBackgroundResource(R.drawable.notloop);
				mpdj6.setLooping(false);
			}
			break;
		case R.id.djplaypause1:
			if (isDjPlaying1) {
				isDjPlaying1 = false;
				mpdj1.pause();
				djdisk1.setAnimation(null);
				djplaypause1.setBackgroundResource(R.drawable.playdj);
			} else {
				if (isdatasetDj1) {
					djplaypause1.setBackgroundResource(R.drawable.pausedj);
					djdisk1.startAnimation(anim);
					mpdj1.start();
					isDjPlaying1 = true;
				}
			}
			break;
		case R.id.djplaypause2:
			if (isDjPlaying2) {
				isDjPlaying2 = false;
				mpdj2.pause();
				djdisk2.setAnimation(null);
				djplaypause2.setBackgroundResource(R.drawable.playdj);
			} else {
				if (isdatasetDj2) {
					djplaypause2.setBackgroundResource(R.drawable.pausedj);
					djdisk2.startAnimation(anim);
					mpdj2.start();
					isDjPlaying2 = true;
				}
			}
			break;
		case R.id.djplaypause3:
			if (isDjPlaying3) {
				isDjPlaying3 = false;
				mpdj3.pause();
				djdisk3.setAnimation(null);
				djplaypause3.setBackgroundResource(R.drawable.playdj);
			} else {
				if (isdatasetDj3) {
					djplaypause3.setBackgroundResource(R.drawable.pausedj);
					djdisk3.startAnimation(anim);
					mpdj3.start();
					isDjPlaying3 = true;
				}
			}
			break;
		case R.id.djplaypause4:
			if (isDjPlaying4) {
				isDjPlaying4 = false;
				mpdj4.pause();
				djdisk4.setAnimation(null);
				djplaypause4.setBackgroundResource(R.drawable.playdj);
			} else {
				if (isdatasetDj4) {
					djplaypause4.setBackgroundResource(R.drawable.pausedj);
					djdisk4.startAnimation(anim);
					mpdj4.start();
					isDjPlaying4 = true;
				}
			}
			break;
		case R.id.djplaypause5:
			if (isDjPlaying5) {
				isDjPlaying5 = false;
				mpdj5.pause();
				djdisk5.setAnimation(null);
				djplaypause5.setBackgroundResource(R.drawable.playdj);
			} else {
				if (isdatasetDj5) {
					djplaypause5.setBackgroundResource(R.drawable.pausedj);
					djdisk5.startAnimation(anim);
					mpdj5.start();
					isDjPlaying5 = true;
				}
			}
			break;
		case R.id.djplaypause6:
			if (isDjPlaying6) {
				isDjPlaying6 = false;
				mpdj6.pause();
				djdisk6.setAnimation(null);
				djplaypause6.setBackgroundResource(R.drawable.playdj);
			} else {
				if (isdatasetDj6) {
					djplaypause6.setBackgroundResource(R.drawable.pausedj);
					djdisk6.startAnimation(anim);
					mpdj6.start();
					isDjPlaying6 = true;
				}
			}
			break;
		case R.id.trackprev1:
			mptrack1.reset();
			int prev1 = Integer.valueOf(MusicLoopClass.mploo1) - 1;
			if (prev1 == 0) {
				MusicLoopClass.mploo1 = "10";
			} else {
				MusicLoopClass.mploo1 = prev1 + "";
			}
			tracktxt1.setText("Loop" + "" + MusicLoopClass.mploo1);
			playtrack1("t0s" + MusicLoopClass.mploo1 + ".ogg");
			trackplaypause1.setBackgroundResource(R.drawable.trackplay);
			break;
		case R.id.trackprev2:
			mptrack2.reset();
			int prev2 = Integer.valueOf(MusicLoopClass.mploo2) - 1;
			if (prev2 == 0) {
				MusicLoopClass.mploo2 = "10";
			} else {
				MusicLoopClass.mploo2 = prev2 + "";
			}
			tracktxt2.setText("Loop" + "" + MusicLoopClass.mploo2);
			playtrack2("t1s" + MusicLoopClass.mploo2 + ".ogg");
			trackplaypause2.setBackgroundResource(R.drawable.trackplay);
			break;
		case R.id.trackprev3:
			mptrack3.reset();
			int prev3 = Integer.valueOf(MusicLoopClass.mploo3) - 1;
			if (prev3 == 0) {
				MusicLoopClass.mploo3 = "10";
			} else {
				MusicLoopClass.mploo3 = prev3 + "";
			}
			tracktxt3.setText("Loop" + "" + MusicLoopClass.mploo3);
			playtrack3("t2s" + MusicLoopClass.mploo3 + ".ogg");
			trackplaypause3.setBackgroundResource(R.drawable.trackplay);
			break;
		case R.id.trackprev4:
			mptrack4.reset();
			int prev4 = Integer.valueOf(MusicLoopClass.mploo4) - 1;
			if (prev4 == 0) {
				MusicLoopClass.mploo4 = "10";
			} else {
				MusicLoopClass.mploo4 = prev4 + "";
			}
			tracktxt4.setText("Loop" + "" + MusicLoopClass.mploo4);
			playtrack4("t3s" + MusicLoopClass.mploo4 + ".ogg");
			trackplaypause4.setBackgroundResource(R.drawable.trackplay);
			break;
		case R.id.trackprev5:
			mptrack5.reset();
			int prev5 = Integer.valueOf(MusicLoopClass.mploo5) - 1;
			if (prev5 == 0) {
				MusicLoopClass.mploo5 = "10";
			} else {
				MusicLoopClass.mploo5 = prev5 + "";
			}
			tracktxt5.setText("Loop" + "" + MusicLoopClass.mploo5);
			playtrack5("t4s" + MusicLoopClass.mploo5 + ".ogg");
			trackplaypause5.setBackgroundResource(R.drawable.trackplay);
			break;
		case R.id.trackprev6:
			mptrack6.reset();
			int prev6 = Integer.valueOf(MusicLoopClass.mploo6) - 1;
			if (prev6 == 0) {
				MusicLoopClass.mploo6 = "10";
			} else {
				MusicLoopClass.mploo6 = prev6 + "";
			}
			tracktxt6.setText("Loop" + "" + MusicLoopClass.mploo6);
			playtrack6("t5s" + MusicLoopClass.mploo6 + ".ogg");
			trackplaypause6.setBackgroundResource(R.drawable.trackplay);
			break;
		case R.id.tracknext1:
			mptrack1.reset();
			int next1 = Integer.valueOf(MusicLoopClass.mploo1) + 1;
			if (next1 == 11) {
				MusicLoopClass.mploo1 = "1";
			} else {
				MusicLoopClass.mploo1 = next1 + "";
			}
			tracktxt1.setText("Loop" + " " + MusicLoopClass.mploo1);
			playtrack1("t0s" + MusicLoopClass.mploo1 + ".ogg");
			trackplaypause1.setBackgroundResource(R.drawable.trackplay);
			break;
		case R.id.tracknext2:
			mptrack2.reset();
			int next2 = Integer.valueOf(MusicLoopClass.mploo2) + 1;
			if (next2 == 11) {
				MusicLoopClass.mploo2 = "1";
			} else {
				MusicLoopClass.mploo2 = next2 + "";
			}
			tracktxt2.setText("Loop" + " " + MusicLoopClass.mploo2);
			playtrack2("t1s" + MusicLoopClass.mploo2 + ".ogg");
			trackplaypause2.setBackgroundResource(R.drawable.trackplay);
			break;
		case R.id.tracknext3:
			mptrack3.reset();
			int next3 = Integer.valueOf(MusicLoopClass.mploo3) + 1;
			if (next3 == 11) {
				MusicLoopClass.mploo3 = "1";
			} else {
				MusicLoopClass.mploo3 = next3 + "";
			}
			tracktxt3.setText("Loop" + " " + MusicLoopClass.mploo3);
			playtrack3("t2s" + MusicLoopClass.mploo3 + ".ogg");
			trackplaypause3.setBackgroundResource(R.drawable.trackplay);
			break;

		case R.id.tracknext4:
			mptrack4.reset();
			int next4 = Integer.valueOf(MusicLoopClass.mploo4) + 1;
			if (next4 == 11) {
				MusicLoopClass.mploo4 = "1";
			} else {
				MusicLoopClass.mploo4 = next4 + "";
			}
			tracktxt4.setText("Loop" + " " + MusicLoopClass.mploo4);
			playtrack4("t3s" + MusicLoopClass.mploo4 + ".ogg");
			trackplaypause4.setBackgroundResource(R.drawable.trackplay);
			break;

		case R.id.tracknext5:
			mptrack5.reset();
			int next5 = Integer.valueOf(MusicLoopClass.mploo5) + 1;
			if (next5 == 11) {
				MusicLoopClass.mploo5 = "1";
			} else {
				MusicLoopClass.mploo5 = next5 + "";
			}
			tracktxt5.setText("Loop" + " " + MusicLoopClass.mploo5);
			playtrack5("t4s" + MusicLoopClass.mploo5 + ".ogg");
			trackplaypause5.setBackgroundResource(R.drawable.trackplay);
			break;
		case R.id.tracknext6:
			mptrack6.reset();
			int next6 = Integer.valueOf(MusicLoopClass.mploo6) + 1;
			if (next6 == 11) {
				MusicLoopClass.mploo6 = "1";
			} else {
				MusicLoopClass.mploo6 = next6 + "";
			}
			tracktxt6.setText("Loop" + " " + MusicLoopClass.mploo6);
			playtrack6("t5s" + MusicLoopClass.mploo6 + ".ogg");
			trackplaypause6.setBackgroundResource(R.drawable.trackplay);
			break;
		case R.id.trackplaypause1:
			if (mptrack1.isPlaying()) {
				mptrack1.pause();
				trackplaypause1.setBackgroundResource(R.drawable.trackpause);
			} else {
				trackplaypause1.setBackgroundResource(R.drawable.trackplay);
				mptrack1.reset();
				playtrack1("t0s" + MusicLoopClass.mploo1 + ".ogg");
			}
			break;
		case R.id.trackplaypause2:
			if (mptrack2.isPlaying()) {
				mptrack2.pause();
				trackplaypause2.setBackgroundResource(R.drawable.trackpause);
			} else {
				trackplaypause2.setBackgroundResource(R.drawable.trackplay);
				mptrack2.reset();
				playtrack2("t1s" + MusicLoopClass.mploo2 + ".ogg");
			}
			break;
		case R.id.trackplaypause3:
			if (mptrack3.isPlaying()) {
				mptrack3.pause();
				trackplaypause3.setBackgroundResource(R.drawable.trackpause);
			} else {
				trackplaypause3.setBackgroundResource(R.drawable.trackplay);
				mptrack3.reset();
				playtrack3("t2s" + MusicLoopClass.mploo3 + ".ogg");
			}
			break;
		case R.id.trackplaypause4:
			if (mptrack4.isPlaying()) {
				mptrack4.pause();
				trackplaypause4.setBackgroundResource(R.drawable.trackpause);
			} else {
				trackplaypause4.setBackgroundResource(R.drawable.trackplay);
				mptrack4.reset();
				playtrack4("t3s" + MusicLoopClass.mploo4 + ".ogg");
			}
			break;
		case R.id.trackplaypause5:
			if (mptrack5.isPlaying()) {
				mptrack5.pause();
				trackplaypause5.setBackgroundResource(R.drawable.trackpause);
			} else {
				trackplaypause5.setBackgroundResource(R.drawable.trackplay);
				mptrack5.reset();
				playtrack5("t4s" + MusicLoopClass.mploo5 + ".ogg");
			}
			break;

		case R.id.trackplaypause6:
			if (mptrack6.isPlaying()) {
				mptrack6.pause();
				trackplaypause6.setBackgroundResource(R.drawable.trackpause);
			} else {
				trackplaypause6.setBackgroundResource(R.drawable.trackplay);
				mptrack6.reset();
				playtrack6("t5s" + MusicLoopClass.mploo6 + ".ogg");
			}
			break;
		case R.id.s1:
			if (mp1.isPlaying()) {
				mp1.stop();
				s1.setBackground(getResources().getDrawable(R.drawable.blue));
			} else {
				playsound1(kit + "1.ogg");
				s1.setBackground(getResources().getDrawable(
						R.drawable.bluelight));
			}
			break;
		case R.id.s2:
			if (mp2.isPlaying()) {
				mp2.stop();
				s2.setBackground(getResources().getDrawable(R.drawable.blue));
			} else {
				playsound2(kit + "2.ogg");
				s2.setBackground(getResources().getDrawable(
						R.drawable.bluelight));
			}
			break;
		case R.id.s3:
			if (mp3.isPlaying()) {
				mp3.stop();
				s3.setBackground(getResources().getDrawable(R.drawable.blue));
			} else {
				playsound3(kit + "3.ogg");
				s3.setBackground(getResources().getDrawable(
						R.drawable.bluelight));
			}
			break;
		case R.id.s4:
			if (mp4.isPlaying()) {
				mp4.stop();
				s4.setBackground(getResources().getDrawable(R.drawable.blue));
			} else {
				playsound4(kit + "4.ogg");
				s4.setBackground(getResources().getDrawable(
						R.drawable.bluelight));
			}
			break;
		case R.id.s5:
			if (mp5.isPlaying()) {
				mp5.stop();
				s5.setBackground(getResources().getDrawable(R.drawable.blue));
			} else {
				playsound5(kit + "5.ogg");
				s5.setBackground(getResources().getDrawable(
						R.drawable.bluelight));
			}
			break;
		case R.id.s6:
			if (mp6.isPlaying()) {
				mp6.stop();
				s6.setBackground(getResources().getDrawable(R.drawable.blue));
			} else {
				playsound6(kit + "6.ogg");
				s6.setBackground(getResources().getDrawable(
						R.drawable.bluelight));
			}
			break;
		case R.id.s7:
			if (mp7.isPlaying()) {
				mp7.stop();
				s7.setBackground(getResources().getDrawable(R.drawable.blue));
			} else {
				playsound7(kit + "7.ogg");
				s7.setBackground(getResources().getDrawable(
						R.drawable.bluelight));
			}
			break;
		case R.id.s8:
			if (mp8.isPlaying()) {
				mp8.stop();
				s8.setBackground(getResources().getDrawable(R.drawable.blue));
			} else {
				playsound8(kit + "8.ogg");
				s8.setBackground(getResources().getDrawable(
						R.drawable.bluelight));
			}
			break;
		case R.id.s9:
			if (mp9.isPlaying()) {
				mp9.stop();
				s9.setBackground(getResources().getDrawable(R.drawable.blue));
			} else {
				playsound9(kit + "9.ogg");
				s9.setBackground(getResources().getDrawable(
						R.drawable.bluelight));
			}
			break;
		case R.id.s10:
			if (mp10.isPlaying()) {
				mp10.stop();
				s10.setBackground(getResources().getDrawable(R.drawable.blue));
			} else {
				playsound10(kit + "10.ogg");
				s10.setBackground(getResources().getDrawable(
						R.drawable.bluelight));
			}
			break;
		case R.id.s11:
			if (mp11.isPlaying()) {
				mp11.stop();
				s11.setBackground(getResources().getDrawable(R.drawable.blue));
			} else {
				playsound11(kit + "11.ogg");
				s11.setBackground(getResources().getDrawable(
						R.drawable.bluelight));
			}
			break;
		case R.id.s12:
			if (mp12.isPlaying()) {
				mp12.stop();
				s12.setBackground(getResources().getDrawable(R.drawable.blue));
			} else {
				playsound12(kit + "12.ogg");
				s12.setBackground(getResources().getDrawable(
						R.drawable.bluelight));
			}
			break;
		case R.id.s13:
			if (mp13.isPlaying()) {
				mp13.stop();
				s13.setBackground(getResources().getDrawable(R.drawable.blue));
			} else {
				playsound13(kit + "13.ogg");
				s13.setBackground(getResources().getDrawable(
						R.drawable.bluelight));
			}
			break;
		case R.id.s14:
			if (mp14.isPlaying()) {
				mp14.stop();
				s14.setBackground(getResources().getDrawable(R.drawable.blue));
			} else {
				playsound14(kit + "14.ogg");
				s14.setBackground(getResources().getDrawable(
						R.drawable.bluelight));
			}
			break;
		case R.id.s15:
			if (mp15.isPlaying()) {
				mp15.stop();
				s15.setBackground(getResources().getDrawable(R.drawable.blue));
			} else {
				playsound15(kit + "15.ogg");
				s15.setBackground(getResources().getDrawable(
						R.drawable.bluelight));
			}
			break;
		case R.id.s16:
			if (mp16.isPlaying()) {
				mp16.stop();
				s16.setBackground(getResources().getDrawable(R.drawable.blue));
			} else {
				playsound16(kit + "16.ogg");
				s16.setBackground(getResources().getDrawable(
						R.drawable.bluelight));
			}
			break;
		case R.id.s17:
			if (mp17.isPlaying()) {
				mp17.stop();
				s17.setBackground(getResources().getDrawable(R.drawable.blue));
			} else {
				playsound17(kit + "17.ogg");
				s17.setBackground(getResources().getDrawable(
						R.drawable.bluelight));
			}
			break;

		case R.id.s18:
			if (mp18.isPlaying()) {
				mp18.stop();
				s18.setBackground(getResources().getDrawable(R.drawable.blue));
			} else {
				playsound18(kit + "18.ogg");
				s18.setBackground(getResources().getDrawable(
						R.drawable.bluelight));
			}
			break;
		case R.id.s19:
			if (mp19.isPlaying()) {
				mp19.stop();
				s19.setBackground(getResources().getDrawable(R.drawable.blue));
			} else {
				playsound19(kit + "19.ogg");
				s19.setBackground(getResources().getDrawable(
						R.drawable.bluelight));
			}
			break;
		case R.id.s20:
			if (mp20.isPlaying()) {
				mp20.stop();
				s20.setBackground(getResources().getDrawable(R.drawable.blue));
			} else {
				playsound20(kit + "20.ogg");
				s20.setBackground(getResources().getDrawable(
						R.drawable.bluelight));
			}
			break;
		case R.id.s21:
			if (mp21.isPlaying()) {
				mp21.stop();
			} else {
				playsound21(kit + "21.ogg");
			}
			break;
		case R.id.s22:
			if (mp22.isPlaying()) {
				mp22.stop();
			} else {
				playsound22(kit + "22.ogg");
			}
			break;
		case R.id.s23:
			if (mp23.isPlaying()) {
				mp23.stop();
			} else {
				playsound23(kit + "23.ogg");

			}
			break;
		case R.id.s24:
			if (mp24.isPlaying()) {
				mp24.stop();
			} else {
				playsound24(kit + "24.ogg");
			}
			break;
		case R.id.s25:
			if (mp25.isPlaying()) {
				mp25.stop();
			} else {
				playsound25(kit + "25.ogg");
			}
			break;

		case R.id.s26:
			if (mp26.isPlaying()) {
				mp26.stop();
			} else {
				playsound26(kit + "26.ogg");
			}
			break;
		case R.id.s27:
			if (mp27.isPlaying()) {
				mp27.stop();
			} else {
				playsound27(kit + "27.ogg");
			}
			break;
		case R.id.s28:
			if (mp28.isPlaying()) {
				mp28.stop();
			} else {
				playsound28(kit + "28.ogg");
			}
			break;
		case R.id.s29:
			if (mp29.isPlaying()) {
				mp29.stop();
			} else {
				playsound29(kit + "29.ogg");
			}
			break;
		case R.id.s30:
			if (mp30.isPlaying()) {
				mp30.stop();
			} else {
				playsound30(kit + "30.ogg");
			}
			break;
		case R.id.s31:
			if (mp31.isPlaying()) {
				mp31.stop();
			} else {
				playsound31(kit + "31.ogg");
			}
			break;
		case R.id.s32:
			if (mp32.isPlaying()) {
				mp32.stop();
			} else {
				playsound32(kit + "32.ogg");
			}
			break;
		case R.id.s33:
			if (mp33.isPlaying()) {
				mp33.stop();
			} else {
				playsound33(kit + "33.ogg");
			}
			break;
		case R.id.s34:
			if (mp34.isPlaying()) {
				mp34.stop();
			} else {
				playsound34(kit + "34.ogg");
			}
			break;
		case R.id.s35:
			if (mp35.isPlaying()) {
				mp35.stop();
			} else {
				playsound35(kit + "35.ogg");
			}
			break;
		case R.id.s36:
			if (mp36.isPlaying()) {
				mp36.stop();
			} else {
				playsound36(kit + "36.ogg");
			}
			break;
		case R.id.s37:
			if (mp37.isPlaying()) {
				mp37.stop();
			} else {
				playsound37(kit + "37.ogg");
			}
			break;
		case R.id.s38:
			if (mp38.isPlaying()) {
				mp38.stop();
			} else {
				playsound38(kit + "38.ogg");
			}
			break;
		case R.id.s39:
			if (mp39.isPlaying()) {
				mp39.stop();
			} else {
				playsound39(kit + "39.ogg");
			}
			break;
		case R.id.s40:
			if (mp40.isPlaying()) {
				mp40.stop();
			} else {
				playsound40(kit + "40.ogg");
			}
			break;

		default:
			break;
		}
	}

	private Activity getactivity() {
		// TODO Auto-generated method stub
		return MainActivity.this;
	}

	private void playtrack1(String musicname) {
		try {
			AssetFileDescriptor descriptor = getAssets().openFd(
					"track/" + musicname);
			mptrack1.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mptrack1.prepare();
			mptrack1.setLooping(true);

			mptrack1.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mptrack1.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void playtrack2(String musicname) {
		try {
			AssetFileDescriptor descriptor = getAssets().openFd(
					"track/" + musicname);
			mptrack2.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mptrack2.prepare();
			mptrack2.setLooping(true);

			mptrack2.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mptrack2.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void playtrack3(String musicname) {
		try {
			AssetFileDescriptor descriptor = getAssets().openFd(
					"track/" + musicname);
			mptrack3.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mptrack3.prepare();
			mptrack3.setLooping(true);

			mptrack3.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mptrack3.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void playtrack4(String musicname) {
		try {
			AssetFileDescriptor descriptor = getAssets().openFd(
					"track/" + musicname);
			mptrack4.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mptrack4.prepare();
			mptrack4.setLooping(true);

			mptrack4.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mptrack4.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void playtrack5(String musicname) {
		try {
			AssetFileDescriptor descriptor = getAssets().openFd(
					"track/" + musicname);
			mptrack5.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mptrack5.prepare();
			mptrack5.setLooping(true);

			mptrack5.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mptrack5.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void playtrack6(String musicname) {
		try {
			AssetFileDescriptor descriptor = getAssets().openFd(
					"track/" + musicname);
			mptrack6.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mptrack6.prepare();
			mptrack6.setLooping(true);

			mptrack6.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mptrack6.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void onResume() {
		super.onResume();

		musiclist.clear();
		musicadapter.notifyDataSetChanged();
		List<Musicmodel> el = dbHandler.Get_playList(false);
		for (Musicmodel e : el) {
			musiclist.add(e);
		}
		musicadapter.notifyDataSetChanged();
		if (el.size() == 0) {
			AssetManager assetManager = getApplicationContext().getAssets();
			String[] fields;
			try {
				fields = assetManager.list("sample");
				for (int count = 0; count < fields.length; count++) {

					Musicmodel m = new Musicmodel();
					m.name = fields[count];
					m.path = fields[count];
					musiclist.add(m);

				}
			} catch (IOException e1) {
				e1.printStackTrace();
			}

		}

	}

	private void showalert() {
		AlertDialog.Builder alert = new AlertDialog.Builder(this);
		alert.setTitle("Save PlayList");
		final EditText input = new EditText(this);
		alert.setView(input);
		alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				String value = input.getText().toString();
				if (!value.equals("")) {
					dbHandler.Add_play(value.toString().trim());
					for (int i = 0; i < musiclist.size(); i++) {
						Musicmodel md = new Musicmodel();
						md.name = musiclist.get(i).name;
						md.path = musiclist.get(i).path;
						Log.e("value", value);
						dbHandler.Add_Saveplaylist(md, value.toString().trim());
					}
				}
			}
		});
		alert.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
					}
				});

		alert.show();
	}

	private void openplaylist() {
		final String[] openlist = dbHandler.openlist();
		new AlertDialog.Builder(getactivity())
				.setItems(openlist, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						musiclist.clear();
						musicadapter.notifyDataSetChanged();
						List<Musicmodel> save = dbHandler.Get_saveplayList(
								false, openlist[which].toString().trim());
						for (Musicmodel e : save) {
							musiclist.add(e);
						}
						musicadapter.notifyDataSetChanged();
					}
				}).create().show();
	}

	private void setthemedefault() {

		t1.setImageResource(R.drawable.a);
		t2.setImageResource(R.drawable.b);
		t3.setImageResource(R.drawable.c);
		t4.setImageResource(R.drawable.d);

	}

	private String getsound(int ins) {
		String name = "";
		switch (ins) {

		case 1:
			name = "Piano";
			break;
		case 4:
			name = "Organ";
			break;
		case 2:
			name = "Flute";
			break;
		case 3:
			name = "Saxophone";
			break;
		default:
			break;
		}
		return name;
	}

	private String getbpm(int ins) {
		String name = "";
		switch (ins) {

		case 1:
			name = "Bpm100";
			break;

		case 2:
			name = "Bpm105";
			break;
		case 3:
			name = "Bpm110";
			break;
		case 4:
			name = "Bpm115";
			break;
		case 5:
			name = "Bpm120";
			break;
		case 6:
			name = "Bpm125";
			break;
		case 7:
			name = "Bpm130";
			break;
		case 8:
			name = "Bpm135";
			break;
		case 9:
			name = "Bpm140";
			break;
		case 10:
			name = "Bpm145";
			break;
		default:
			break;
		}
		return name;
	}

	public void playsound1(String path) {
		try {

			mp1.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp1.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp1.prepare();
			mp1.setLooping(true);
			mp1.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp.start();
				}
			});

		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void playsound2(String path) {
		try {
			mp2.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp2.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp2.prepare();
			mp2.setLooping(true);
			mp2.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp2.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound3(String path) {
		try {
			mp3.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp3.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp3.prepare();
			mp3.setLooping(true);
			mp3.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp3.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound4(String path) {
		try {
			mp4.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp4.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp4.prepare();
			mp4.setLooping(true);
			mp4.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp4.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound5(String path) {
		try {
			mp5.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp5.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp5.prepare();
			mp5.setLooping(true);
			mp5.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp5.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound6(String path) {
		try {
			mp6.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp6.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp6.prepare();
			mp6.setLooping(true);
			mp6.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp6.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound7(String path) {
		try {
			mp7.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp7.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp7.prepare();
			mp7.setLooping(true);
			mp7.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp7.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound8(String path) {
		try {
			mp8.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp8.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp8.prepare();
			mp8.setLooping(true);
			mp8.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp8.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound9(String path) {
		try {
			mp9.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp9.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp9.prepare();
			mp9.setLooping(true);
			mp9.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp9.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound10(String path) {
		try {
			mp10.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp10.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp10.prepare();
			mp10.setLooping(true);
			mp10.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp10.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound11(String path) {
		try {
			mp11.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp11.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp11.prepare();
			mp11.setLooping(true);
			mp11.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp11.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound12(String path) {
		try {
			mp12.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp12.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp12.prepare();
			mp12.setLooping(true);
			mp12.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp12.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound13(String path) {
		try {
			mp13.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp13.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp13.prepare();
			mp13.setLooping(true);
			mp13.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp13.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound14(String path) {
		try {
			mp14.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp14.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp14.prepare();
			mp14.setLooping(true);
			mp14.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp14.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound15(String path) {
		try {
			mp15.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp15.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp15.prepare();
			mp15.setLooping(true);
			mp15.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp15.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound16(String path) {
		try {
			mp16.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp16.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp16.prepare();
			mp16.setLooping(true);
			mp16.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp16.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound17(String path) {
		try {
			mp17.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp17.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp17.prepare();
			mp17.setLooping(true);
			mp17.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp17.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound18(String path) {
		try {
			mp18.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp18.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp18.prepare();
			mp18.setLooping(true);
			mp18.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp18.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound19(String path) {
		try {
			mp19.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp19.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp19.prepare();
			mp19.setLooping(true);
			mp19.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp19.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound20(String path) {
		try {
			mp20.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp20.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp20.prepare();
			mp20.setLooping(true);
			mp20.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp20.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound21(String path) {
		try {
			mp21.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp21.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp21.prepare();
			// mp21.setLooping(true);
			mp21.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp21.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound22(String path) {
		try {
			mp22.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp22.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp22.prepare();
			// mp22.setLooping(true);
			mp22.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp22.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound23(String path) {
		try {
			mp23.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp23.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp23.prepare();
			// mp23.setLooping(true);
			mp23.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp23.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound24(String path) {
		try {
			mp24.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp24.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp24.prepare();
			// mp24.setLooping(true);
			mp24.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp24.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound25(String path) {
		try {
			mp25.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp25.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp25.prepare();
			// mp25.setLooping(true);
			mp25.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp25.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound26(String path) {
		try {
			mp26.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp26.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp26.prepare();
			// mp26.setLooping(true);
			mp26.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp26.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound27(String path) {
		try {
			mp27.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp27.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp27.prepare();
			// mp27.setLooping(true);
			mp27.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp27.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound28(String path) {
		try {
			mp28.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp28.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp28.prepare();
			// mp28.setLooping(true);
			mp28.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp28.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound29(String path) {
		try {
			mp29.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp29.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp29.prepare();
			// mp29.setLooping(true);
			mp29.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp29.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound30(String path) {
		try {
			mp30.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp30.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp30.prepare();
			// mp30.setLooping(true);
			mp30.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp30.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound31(String path) {
		try {
			mp31.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp31.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp31.prepare();
			// mp31.setLooping(true);
			mp31.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp31.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound32(String path) {
		try {
			mp32.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp32.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp32.prepare();
			// mp32.setLooping(true);
			mp32.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp32.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound33(String path) {
		try {
			mp33.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp33.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp33.prepare();
			// mp33.setLooping(true);
			mp33.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp33.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound34(String path) {
		try {
			mp34.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp34.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp34.prepare();
			// mp34.setLooping(true);
			mp34.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp34.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound35(String path) {
		try {
			mp35.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp35.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp35.prepare();
			// mp35.setLooping(true);
			mp35.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp35.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound36(String path) {
		try {
			mp36.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp36.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp36.prepare();
			// mp36.setLooping(true);
			mp36.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp36.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound37(String path) {
		try {
			mp37.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp37.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp37.prepare();
			// mp37.setLooping(true);
			mp37.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp37.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound38(String path) {
		try {
			mp38.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp38.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp38.prepare();
			// mp38.setLooping(true);
			mp38.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp38.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound39(String path) {
		try {
			mp39.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp39.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp39.prepare();
			mp39.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp39.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound40(String path) {
		try {
			mp40.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp40.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp40.prepare();

			mp40.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp40.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void setrelese() {

		mVisualizer1.release();
		mVisualizer2.release();
		mVisualizer3.release();
		mVisualizer4.release();
		mVisualizer5.release();
		mVisualizer6.release();

		mpdisk1.release();
		mpdisk2.release();
		mpdisk3.release();
		mpdisk4.release();
		mpdisk5.release();
		mpdisk6.release();

		mp1.release();
		mp2.release();
		mp3.release();
		mp4.release();
		mp5.release();
		mp6.release();
		mp7.release();
		mp8.release();
		mp9.release();
		mp10.release();
		mp11.release();
		mp12.release();
		mp13.release();
		mp14.release();
		mp15.release();
		mp16.release();
		mp17.release();
		mp18.release();
		mp19.release();
		mp20.release();
		mp21.release();
		mp22.release();
		mp23.release();
		mp24.release();
		mp25.release();
		mp26.release();
		mp27.release();
		mp28.release();
		mp29.release();
		mp30.release();
		mp31.release();
		mp32.release();
		mp33.release();
		mp34.release();
		mp35.release();
		mp36.release();
		mp37.release();
		mp38.release();
		mp39.release();
		mp40.release();
		rhtmp.release();
		metromp.release();
		padmp1.release();
		padmp2.release();
		padmp3.release();
		padmp4.release();
		padmp5.release();
		padmp6.release();
		padmp7.release();
		padmp8.release();
		soundmp.release();
		keymp1.release();
		keymp2.release();
		keymp3.release();
		keymp4.release();
		keymp5.release();
		keymp6.release();
		keymp7.release();
		keymp8.release();
		keymp9.release();
		keymp10.release();
		keymp11.release();
		keymp12.release();
		keymp13.release();
		keymp14.release();
		keymp15.release();
		keymp16.release();
		keymp17.release();
		keymp18.release();
		keymp19.release();
		keymp20.release();
		keymp21.release();
		keymp22.release();
		keymp23.release();
		keymp0.release();
		mpdj1.release();
		mpdj2.release();
		mpdj3.release();
		mpdj4.release();
		mpdj5.release();
		mpdj6.release();
		mptrack1.release();
		mptrack2.release();
		mptrack3.release();
		mptrack4.release();
		mptrack5.release();
		mptrack6.release();
	}

	private void showratedialog() {
		new AlertDialog.Builder(this)
				.setTitle("Do you want to rate this application?")
				.setPositiveButton("Never",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								// continue with delete
								dialog.dismiss();
							}
						})
				.setNegativeButton("not now",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								// do nothing
								dialog.dismiss();
							}
						})
				.setNeutralButton("Rate Now",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								// do nothing
								startActivity(new Intent(Intent.ACTION_VIEW,
										Uri.parse("market://details?id="
												+ "com.djmix.djelectrostation")));
							}
						}).show();
	}

	@Override
	public void onBackPressed() {
		displayInterstitial();
		showclose();

	}

	private void showclose() {
		new AlertDialog.Builder(this)
				.setTitle("Do you really want to shut down the app?")
				.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {

								setrelese();
								finish();
								return;
							}
						})
				.setNegativeButton("No", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				}).show();
	}

}

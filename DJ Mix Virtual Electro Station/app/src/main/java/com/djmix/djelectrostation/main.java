package com.djmix.djelectrostation;

import java.io.IOException;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import com.djmix.djelectrostation.R;

@SuppressLint("NewApi")
public class main extends Activity implements OnClickListener {

	ImageView s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s11, s12, s13, s14, s15,
			s16, s17, s18, s19, s20, s21, s22, s23, s24, s25, s26, s27, s28,
			s29, s30, s31, s32, s33, s34, s35, s36, s37, s38, s39, s40, s42,
			s43, s44, s45;
	MediaPlayer mp1, mp2, mp3, mp4, mp5, mp6, mp7, mp8, mp9, mp10, mp11, mp12,
			mp13, mp14, mp15, mp16, mp17, mp18, mp19, mp20, mp21, mp22, mp23,
			mp24, mp25, mp26, mp27, mp28, mp29, mp30, mp31, mp32, mp33, mp34,
			mp35, mp36, mp37, mp38, mp39, mp40;

	String kit = "sA1";
	VerticalSeekBar padvolseek;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.padlayout);

		mp1 = new MediaPlayer();
		mp2 = new MediaPlayer();
		mp3 = new MediaPlayer();
		mp4 = new MediaPlayer();
		mp5 = new MediaPlayer();
		mp6 = new MediaPlayer();
		mp7 = new MediaPlayer();
		mp8 = new MediaPlayer();
		mp9 = new MediaPlayer();
		mp10 = new MediaPlayer();
		mp11 = new MediaPlayer();
		mp12 = new MediaPlayer();
		mp13 = new MediaPlayer();
		mp14 = new MediaPlayer();
		mp15 = new MediaPlayer();
		mp16 = new MediaPlayer();
		mp17 = new MediaPlayer();
		mp18 = new MediaPlayer();
		mp19 = new MediaPlayer();
		mp20 = new MediaPlayer();
		mp21 = new MediaPlayer();
		mp22 = new MediaPlayer();
		mp23 = new MediaPlayer();
		mp24 = new MediaPlayer();
		mp25 = new MediaPlayer();
		mp26 = new MediaPlayer();
		mp27 = new MediaPlayer();
		mp28 = new MediaPlayer();
		mp29 = new MediaPlayer();
		mp30 = new MediaPlayer();
		mp31 = new MediaPlayer();
		mp32 = new MediaPlayer();
		mp33 = new MediaPlayer();
		mp34 = new MediaPlayer();
		mp35 = new MediaPlayer();
		mp36 = new MediaPlayer();
		mp37 = new MediaPlayer();
		mp38 = new MediaPlayer();
		mp39 = new MediaPlayer();
		mp40 = new MediaPlayer();

		padvolseek = (VerticalSeekBar) findViewById(R.id.padvolseek);
		padvolseek.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				final float vol1 = (progress) / 100.0f;
				main.this.runOnUiThread(new Runnable() {

					@Override
					public void run() {

						mp1.setVolume(vol1, vol1);
						mp2.setVolume(vol1, vol1);
						mp3.setVolume(vol1, vol1);
						mp4.setVolume(vol1, vol1);
						mp5.setVolume(vol1, vol1);
						mp6.setVolume(vol1, vol1);
						mp7.setVolume(vol1, vol1);
						mp8.setVolume(vol1, vol1);
						mp9.setVolume(vol1, vol1);
						mp10.setVolume(vol1, vol1);
						mp11.setVolume(vol1, vol1);
						mp12.setVolume(vol1, vol1);
						mp13.setVolume(vol1, vol1);
						mp14.setVolume(vol1, vol1);
						mp15.setVolume(vol1, vol1);
						mp16.setVolume(vol1, vol1);
						mp17.setVolume(vol1, vol1);
						mp18.setVolume(vol1, vol1);
						mp19.setVolume(vol1, vol1);
						mp20.setVolume(vol1, vol1);
						mp21.setVolume(vol1, vol1);
						mp22.setVolume(vol1, vol1);
						mp23.setVolume(vol1, vol1);
						mp24.setVolume(vol1, vol1);
						mp25.setVolume(vol1, vol1);
						mp26.setVolume(vol1, vol1);
						mp27.setVolume(vol1, vol1);
						mp28.setVolume(vol1, vol1);
						mp29.setVolume(vol1, vol1);
						mp30.setVolume(vol1, vol1);
						mp31.setVolume(vol1, vol1);
						mp32.setVolume(vol1, vol1);
						mp33.setVolume(vol1, vol1);
						mp34.setVolume(vol1, vol1);
						mp35.setVolume(vol1, vol1);
						mp36.setVolume(vol1, vol1);
						mp37.setVolume(vol1, vol1);
						mp38.setVolume(vol1, vol1);
						mp39.setVolume(vol1, vol1);
						mp40.setVolume(vol1, vol1);
					}
				});
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
			}
		});

		s1 = (ImageView) findViewById(R.id.s1);
		s2 = (ImageView) findViewById(R.id.s2);
		s3 = (ImageView) findViewById(R.id.s3);
		s4 = (ImageView) findViewById(R.id.s4);
		s5 = (ImageView) findViewById(R.id.s5);
		s6 = (ImageView) findViewById(R.id.s6);
		s7 = (ImageView) findViewById(R.id.s7);
		s8 = (ImageView) findViewById(R.id.s8);
		s9 = (ImageView) findViewById(R.id.s9);
		s10 = (ImageView) findViewById(R.id.s10);
		s11 = (ImageView) findViewById(R.id.s11);
		s12 = (ImageView) findViewById(R.id.s12);
		s13 = (ImageView) findViewById(R.id.s13);
		s14 = (ImageView) findViewById(R.id.s14);
		s15 = (ImageView) findViewById(R.id.s15);
		s16 = (ImageView) findViewById(R.id.s16);
		s17 = (ImageView) findViewById(R.id.s17);
		s18 = (ImageView) findViewById(R.id.s18);
		s19 = (ImageView) findViewById(R.id.s19);
		s20 = (ImageView) findViewById(R.id.s20);
		s21 = (ImageView) findViewById(R.id.s21);
		s22 = (ImageView) findViewById(R.id.s22);
		s23 = (ImageView) findViewById(R.id.s23);
		s24 = (ImageView) findViewById(R.id.s24);
		s25 = (ImageView) findViewById(R.id.s25);
		s26 = (ImageView) findViewById(R.id.s26);
		s27 = (ImageView) findViewById(R.id.s27);
		s28 = (ImageView) findViewById(R.id.s28);
		s29 = (ImageView) findViewById(R.id.s29);
		s30 = (ImageView) findViewById(R.id.s30);
		s31 = (ImageView) findViewById(R.id.s31);
		s32 = (ImageView) findViewById(R.id.s32);
		s33 = (ImageView) findViewById(R.id.s33);
		s34 = (ImageView) findViewById(R.id.s34);
		s35 = (ImageView) findViewById(R.id.s35);
		s36 = (ImageView) findViewById(R.id.s36);
		s37 = (ImageView) findViewById(R.id.s37);
		s38 = (ImageView) findViewById(R.id.s38);
		s39 = (ImageView) findViewById(R.id.s39);
		s40 = (ImageView) findViewById(R.id.s40);

		s1.setOnClickListener(this);
		s2.setOnClickListener(this);
		s3.setOnClickListener(this);
		s4.setOnClickListener(this);
		s5.setOnClickListener(this);
		s6.setOnClickListener(this);
		s7.setOnClickListener(this);
		s8.setOnClickListener(this);
		s9.setOnClickListener(this);
		s10.setOnClickListener(this);
		s11.setOnClickListener(this);
		s12.setOnClickListener(this);
		s13.setOnClickListener(this);
		s14.setOnClickListener(this);
		s15.setOnClickListener(this);
		s16.setOnClickListener(this);
		s17.setOnClickListener(this);
		s18.setOnClickListener(this);
		s19.setOnClickListener(this);
		s20.setOnClickListener(this);
		s21.setOnClickListener(this);
		s22.setOnClickListener(this);
		s23.setOnClickListener(this);
		s24.setOnClickListener(this);
		s25.setOnClickListener(this);
		s26.setOnClickListener(this);
		s27.setOnClickListener(this);
		s28.setOnClickListener(this);
		s29.setOnClickListener(this);
		s30.setOnClickListener(this);
		s31.setOnClickListener(this);
		s32.setOnClickListener(this);
		s33.setOnClickListener(this);
		s34.setOnClickListener(this);
		s35.setOnClickListener(this);
		s36.setOnClickListener(this);
		s37.setOnClickListener(this);
		s38.setOnClickListener(this);
		s39.setOnClickListener(this);
		s40.setOnClickListener(this);

	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.s1:
			if (mp1.isPlaying()) {
				mp1.stop();
				s1.setBackground(getResources().getDrawable(R.drawable.blue));
			} else {
				playsound1(kit + "1.ogg");
				s1.setBackground(getResources().getDrawable(
						R.drawable.bluelight));
			}
			break;
		case R.id.s2:
			if (mp2.isPlaying()) {
				mp2.stop();
				s2.setBackground(getResources().getDrawable(R.drawable.blue));
			} else {
				playsound2(kit + "2.ogg");
				s2.setBackground(getResources().getDrawable(
						R.drawable.bluelight));
			}
			break;
		case R.id.s3:
			if (mp3.isPlaying()) {
				mp3.stop();
				s3.setBackground(getResources().getDrawable(R.drawable.blue));
			} else {
				playsound3(kit + "3.ogg");
				s3.setBackground(getResources().getDrawable(
						R.drawable.bluelight));
			}
			break;
		case R.id.s4:
			if (mp4.isPlaying()) {
				mp4.stop();
				s4.setBackground(getResources().getDrawable(R.drawable.blue));
			} else {
				playsound4(kit + "4.ogg");
				s4.setBackground(getResources().getDrawable(
						R.drawable.bluelight));
			}
			break;
		case R.id.s5:
			if (mp5.isPlaying()) {
				mp5.stop();
				s5.setBackground(getResources().getDrawable(R.drawable.blue));
			} else {
				playsound5(kit + "5.ogg");
				s5.setBackground(getResources().getDrawable(
						R.drawable.bluelight));
			}
			break;
		case R.id.s6:
			if (mp6.isPlaying()) {
				mp6.stop();
				s6.setBackground(getResources().getDrawable(R.drawable.blue));
			} else {
				playsound6(kit + "6.ogg");
				s6.setBackground(getResources().getDrawable(
						R.drawable.bluelight));
			}
			break;
		case R.id.s7:
			if (mp7.isPlaying()) {
				mp7.stop();
				s7.setBackground(getResources().getDrawable(R.drawable.blue));
			} else {
				playsound7(kit + "7.ogg");
				s7.setBackground(getResources().getDrawable(
						R.drawable.bluelight));
			}
			break;
		case R.id.s8:
			if (mp8.isPlaying()) {
				mp8.stop();
				s8.setBackground(getResources().getDrawable(R.drawable.blue));
			} else {
				playsound8(kit + "8.ogg");
				s8.setBackground(getResources().getDrawable(
						R.drawable.bluelight));
			}
			break;
		case R.id.s9:
			if (mp9.isPlaying()) {
				mp9.stop();
				s9.setBackground(getResources().getDrawable(R.drawable.blue));
			} else {
				playsound9(kit + "9.ogg");
				s9.setBackground(getResources().getDrawable(
						R.drawable.bluelight));
			}
			break;
		case R.id.s10:
			if (mp10.isPlaying()) {
				mp10.stop();
				s10.setBackground(getResources().getDrawable(R.drawable.blue));
			} else {
				playsound10(kit + "10.ogg");
				s10.setBackground(getResources().getDrawable(
						R.drawable.bluelight));
			}
			break;
		case R.id.s11:
			if (mp11.isPlaying()) {
				mp11.stop();
				s11.setBackground(getResources().getDrawable(R.drawable.blue));
			} else {
				playsound11(kit + "11.ogg");
				s11.setBackground(getResources().getDrawable(
						R.drawable.bluelight));
			}
			break;
		case R.id.s12:
			if (mp12.isPlaying()) {
				mp12.stop();
				s12.setBackground(getResources().getDrawable(R.drawable.blue));
			} else {
				playsound12(kit + "12.ogg");
				s12.setBackground(getResources().getDrawable(
						R.drawable.bluelight));
			}
			break;
		case R.id.s13:
			if (mp13.isPlaying()) {
				mp13.stop();
				s13.setBackground(getResources().getDrawable(R.drawable.blue));
			} else {
				playsound13(kit + "13.ogg");
				s13.setBackground(getResources().getDrawable(
						R.drawable.bluelight));
			}
			break;
		case R.id.s14:
			if (mp14.isPlaying()) {
				mp14.stop();
				s14.setBackground(getResources().getDrawable(R.drawable.blue));
			} else {
				playsound14(kit + "14.ogg");
				s14.setBackground(getResources().getDrawable(
						R.drawable.bluelight));
			}
			break;
		case R.id.s15:
			if (mp15.isPlaying()) {
				mp15.stop();
				s15.setBackground(getResources().getDrawable(R.drawable.blue));
			} else {
				playsound15(kit + "15.ogg");
				s15.setBackground(getResources().getDrawable(
						R.drawable.bluelight));
			}
			break;
		case R.id.s16:
			if (mp16.isPlaying()) {
				mp16.stop();
				s16.setBackground(getResources().getDrawable(R.drawable.blue));
			} else {
				playsound16(kit + "16.ogg");
				s16.setBackground(getResources().getDrawable(
						R.drawable.bluelight));
			}
			break;
		case R.id.s17:
			if (mp17.isPlaying()) {
				mp17.stop();
				s17.setBackground(getResources().getDrawable(R.drawable.blue));
			} else {
				playsound17(kit + "17.ogg");
				s17.setBackground(getResources().getDrawable(
						R.drawable.bluelight));
			}
			break;

		case R.id.s18:
			if (mp18.isPlaying()) {
				mp18.stop();
				s18.setBackground(getResources().getDrawable(R.drawable.blue));
			} else {
				playsound18(kit + "18.ogg");
				s18.setBackground(getResources().getDrawable(
						R.drawable.bluelight));
			}
			break;
		case R.id.s19:
			if (mp19.isPlaying()) {
				mp19.stop();
				s19.setBackground(getResources().getDrawable(R.drawable.blue));
			} else {
				playsound19(kit + "19.ogg");
				s19.setBackground(getResources().getDrawable(
						R.drawable.bluelight));
			}
			break;
		case R.id.s20:
			if (mp20.isPlaying()) {
				mp20.stop();
				s20.setBackground(getResources().getDrawable(R.drawable.blue));
			} else {
				playsound20(kit + "20.ogg");
				s20.setBackground(getResources().getDrawable(
						R.drawable.bluelight));
			}
			break;
		case R.id.s21:
			if (mp21.isPlaying()) {
				mp21.stop();
			} else {
				playsound21(kit + "21.ogg");
			}
			break;
		case R.id.s22:
			if (mp22.isPlaying()) {
				mp22.stop();
			} else {
				playsound22(kit + "22.ogg");
			}
			break;
		case R.id.s23:
			if (mp23.isPlaying()) {
				mp23.stop();
			} else {
				playsound23(kit + "23.ogg");

			}
			break;
		case R.id.s24:
			if (mp24.isPlaying()) {
				mp24.stop();
			} else {
				playsound24(kit + "24.ogg");
			}
			break;
		case R.id.s25:
			if (mp25.isPlaying()) {
				mp25.stop();
			} else {
				playsound25(kit + "25.ogg");
			}
			break;

		case R.id.s26:
			if (mp26.isPlaying()) {
				mp26.stop();
			} else {
				playsound26(kit + "26.ogg");
			}
			break;
		case R.id.s27:
			if (mp27.isPlaying()) {
				mp27.stop();
			} else {
				playsound27(kit + "27.ogg");
			}
			break;
		case R.id.s28:
			if (mp28.isPlaying()) {
				mp28.stop();
			} else {
				playsound28(kit + "28.ogg");
			}
			break;
		case R.id.s29:
			if (mp29.isPlaying()) {
				mp29.stop();
			} else {
				playsound29(kit + "29.ogg");
			}
			break;
		case R.id.s30:
			if (mp30.isPlaying()) {
				mp30.stop();
			} else {
				playsound30(kit + "30.ogg");
			}
			break;
		case R.id.s31:
			if (mp31.isPlaying()) {
				mp31.stop();
			} else {
				playsound31(kit + "31.ogg");
			}
			break;
		case R.id.s32:
			if (mp32.isPlaying()) {
				mp32.stop();
			} else {
				playsound32(kit + "32.ogg");
			}
			break;
		case R.id.s33:
			if (mp33.isPlaying()) {
				mp33.stop();
			} else {
				playsound33(kit + "33.ogg");
			}
			break;
		case R.id.s34:
			if (mp34.isPlaying()) {
				mp34.stop();
			} else {
				playsound34(kit + "34.ogg");
			}
			break;
		case R.id.s35:
			if (mp35.isPlaying()) {
				mp35.stop();
			} else {
				playsound35(kit + "35.ogg");
			}
			break;
		case R.id.s36:
			if (mp36.isPlaying()) {
				mp36.stop();
			} else {
				playsound36(kit + "36.ogg");
			}
			break;
		case R.id.s37:
			if (mp37.isPlaying()) {
				mp37.stop();
			} else {
				playsound37(kit + "37.ogg");
			}
			break;
		case R.id.s38:
			if (mp38.isPlaying()) {
				mp38.stop();
			} else {
				playsound38(kit + "38.ogg");
			}
			break;
		case R.id.s39:
			if (mp39.isPlaying()) {
				mp39.stop();
			} else {
				playsound39(kit + "39.ogg");
			}
			break;
		case R.id.s40:
			if (mp40.isPlaying()) {
				mp40.stop();
			} else {
				playsound40(kit + "40.ogg");
			}
			break;

		default:
			break;
		}

	}

	public void playsound1(String path) {
		try {

			mp1.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp1.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp1.prepare();
			mp1.setLooping(true);
			mp1.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp.start();
				}
			});

		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void playsound2(String path) {
		try {
			mp2.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp2.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp2.prepare();
			mp2.setLooping(true);
			mp2.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp2.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound3(String path) {
		try {
			mp3.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp3.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp3.prepare();
			mp3.setLooping(true);
			mp3.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp3.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound4(String path) {
		try {
			mp4.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp4.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp4.prepare();
			mp4.setLooping(true);
			mp4.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp4.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound5(String path) {
		try {
			mp5.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp5.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp5.prepare();
			mp5.setLooping(true);
			mp5.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp5.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound6(String path) {
		try {
			mp6.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp6.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp6.prepare();
			mp6.setLooping(true);
			mp6.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp6.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound7(String path) {
		try {
			mp7.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp7.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp7.prepare();
			mp7.setLooping(true);
			mp7.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp7.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound8(String path) {
		try {
			mp8.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp8.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp8.prepare();
			mp8.setLooping(true);
			mp8.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp8.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound9(String path) {
		try {
			mp9.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp9.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp9.prepare();
			mp9.setLooping(true);
			mp9.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp9.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound10(String path) {
		try {
			mp10.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp10.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp10.prepare();
			mp10.setLooping(true);
			mp10.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp10.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound11(String path) {
		try {
			mp11.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp11.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp11.prepare();
			mp11.setLooping(true);
			mp11.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp11.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound12(String path) {
		try {
			mp12.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp12.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp12.prepare();
			mp12.setLooping(true);
			mp12.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp12.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound13(String path) {
		try {
			mp13.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp13.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp13.prepare();
			mp13.setLooping(true);
			mp13.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp13.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound14(String path) {
		try {
			mp14.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp14.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp14.prepare();
			mp14.setLooping(true);
			mp14.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp14.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound15(String path) {
		try {
			mp15.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp15.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp15.prepare();
			mp15.setLooping(true);
			mp15.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp15.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound16(String path) {
		try {
			mp16.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp16.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp16.prepare();
			mp16.setLooping(true);
			mp16.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp16.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound17(String path) {
		try {
			mp17.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp17.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp17.prepare();
			mp17.setLooping(true);
			mp17.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp17.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound18(String path) {
		try {
			mp18.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp18.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp18.prepare();
			mp18.setLooping(true);
			mp18.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp18.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound19(String path) {
		try {
			mp19.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp19.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp19.prepare();
			mp19.setLooping(true);
			mp19.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp19.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound20(String path) {
		try {
			mp20.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp20.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp20.prepare();
			mp20.setLooping(true);
			mp20.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp20.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound21(String path) {
		try {
			mp21.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp21.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp21.prepare();
			// mp21.setLooping(true);
			mp21.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp21.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound22(String path) {
		try {
			mp22.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp22.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp22.prepare();
			// mp22.setLooping(true);
			mp22.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp22.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound23(String path) {
		try {
			mp23.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp23.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp23.prepare();
			// mp23.setLooping(true);
			mp23.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp23.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound24(String path) {
		try {
			mp24.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp24.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp24.prepare();
			// mp24.setLooping(true);
			mp24.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp24.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound25(String path) {
		try {
			mp25.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp25.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp25.prepare();
			// mp25.setLooping(true);
			mp25.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp25.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound26(String path) {
		try {
			mp26.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp26.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp26.prepare();
			// mp26.setLooping(true);
			mp26.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp26.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound27(String path) {
		try {
			mp27.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp27.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp27.prepare();
			// mp27.setLooping(true);
			mp27.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp27.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound28(String path) {
		try {
			mp28.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp28.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp28.prepare();
			// mp28.setLooping(true);
			mp28.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp28.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound29(String path) {
		try {
			mp29.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp29.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp29.prepare();
			// mp29.setLooping(true);
			mp29.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp29.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound30(String path) {
		try {
			mp30.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp30.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp30.prepare();
			// mp30.setLooping(true);
			mp30.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp30.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound31(String path) {
		try {
			mp31.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp31.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp31.prepare();
			// mp31.setLooping(true);
			mp31.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp31.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound32(String path) {
		try {
			mp32.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp32.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp32.prepare();
			// mp32.setLooping(true);
			mp32.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp32.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound33(String path) {
		try {
			mp33.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp33.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp33.prepare();
			// mp33.setLooping(true);
			mp33.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp33.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound34(String path) {
		try {
			mp34.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp34.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp34.prepare();
			// mp34.setLooping(true);
			mp34.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp34.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound35(String path) {
		try {
			mp35.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp35.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp35.prepare();
			// mp35.setLooping(true);
			mp35.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp35.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound36(String path) {
		try {
			mp36.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp36.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp36.prepare();
			// mp36.setLooping(true);
			mp36.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp36.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound37(String path) {
		try {
			mp37.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp37.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp37.prepare();
			// mp37.setLooping(true);
			mp37.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp37.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound38(String path) {
		try {
			mp38.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp38.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp38.prepare();
			// mp38.setLooping(true);
			mp38.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp38.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound39(String path) {
		try {
			mp39.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp39.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp39.prepare();
			mp39.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp39.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound40(String path) {
		try {
			mp40.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp40.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp40.prepare();

			mp40.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp40.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void setrelese() {

		mp1.release();
		mp2.release();
		mp3.release();
		mp4.release();
		mp5.release();
		mp6.release();
		mp7.release();
		mp8.release();
		mp9.release();
		mp10.release();
		mp11.release();
		mp12.release();
		mp13.release();
		mp14.release();
		mp15.release();
		mp16.release();
		mp17.release();
		mp18.release();
		mp19.release();
		mp20.release();
		mp21.release();
		mp22.release();
		mp23.release();
		mp24.release();
		mp25.release();
		mp26.release();
		mp27.release();
		mp28.release();
		mp29.release();
		mp30.release();
		mp31.release();
		mp32.release();
		mp33.release();
		mp34.release();
		mp35.release();
		mp36.release();
		mp37.release();
		mp38.release();
		mp39.release();
		mp40.release();

	}

	private Context getcontext() {

		return main.this;
	}

}

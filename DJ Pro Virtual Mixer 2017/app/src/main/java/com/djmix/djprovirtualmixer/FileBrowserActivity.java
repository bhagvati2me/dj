package com.djmix.djprovirtualmixer;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


import com.djmix.djprovirtualmixer.R;

//Android imports 
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.os.StatFs;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

//Import of resources file for file browser

public class FileBrowserActivity extends Activity {
	// public static final String INTENT_ACTION_SELECT_FILE =
	// "ua.com.vassiliev.androidfilebrowser.SELECT_FILE_ACTION";
	//
	// public static final String returnFileParameter =
	// "ua.com.vassiliev.androidfilebrowser.filePathRet";
	// public static final String showCannotReadParameter =
	// "ua.com.vassiliev.androidfilebrowser.showCannotRead";
	// public static final String filterExtension =
	// "ua.com.vassiliev.androidfilebrowser.filterExtension";

	ArrayList<String> pathDirsList = new ArrayList<String>();
	ArrayList<Musicmodel> musiclist = new ArrayList<Musicmodel>();
	private static final String LOGTAG = "F_PATH";

	private List<Item> fileList = new ArrayList<Item>();
	private File path = null;
	private String chosenFile;

	ArrayAdapter<Item> adapter;
	ListView playlistView;

	private boolean directoryShownIsEmpty = false;
	ImageView setting;
	// private String filterFileExtension = null;

	private static int currentAction = -1;
	private static final int SELECT_FILE = 2;
	MusicAdapter musicadapter;
	DatabaseHandler dbHandler = new DatabaseHandler(this);

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.sdcardbrowse);
		setting = (ImageView) findViewById(R.id.popup);
		setting.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String[] s = getResources().getStringArray(R.array.setting_Option);
				new AlertDialog.Builder(FileBrowserActivity.this).setItems(s, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						switch (which) {
						case 0:
							dbHandler.Delete_All();
							if (musiclist != null && musicadapter != null) {
								musiclist.clear();
								musicadapter.notifyDataSetChanged();
							}

							break;
			
						case 1:
							if (musiclist != null && musicadapter != null) {
								musiclist.clear();
								musicadapter.notifyDataSetChanged();
								List<Musicmodel> el = dbHandler.Get_playList(true);
								for (Musicmodel e : el) {
									musiclist.add(e);
								}

								musicadapter.notifyDataSetChanged();
							}
							break;
						}
					}
				}).create().show();
			}
		});
		Intent thisInt = this.getIntent();
		// if (thisInt.getAction().equalsIgnoreCase(INTENT_ACTION_SELECT_FILE))
		// {
		// Log.d(LOGTAG, "SELECT ACTION - SELECT FILE");
		// currentAction = SELECT_FILE;
		// }
		currentAction = SELECT_FILE;
		// filterFileExtension = thisInt.getStringExtra(filterExtension);

		setInitialDirectory();

		parseDirectoryPath();
		loadFileList();
		this.createFileListAdapter();
		this.initializeButtons();
		this.initializeFileListView();
		updateCurrentDirectoryTextView();
		Log.d(LOGTAG, path.getAbsolutePath());

		playlistView = (ListView) findViewById(R.id.playlist);
		musicadapter = new MusicAdapter(FileBrowserActivity.this, R.layout.playlistitem, musiclist);
		playlistView.setAdapter(musicadapter);
		ArrayList<Musicmodel> el = dbHandler.Get_playList(false);
		musiclist.addAll(el);

	}

	public static boolean isExternalStorage(Context context) {
		String state = Environment.getExternalStorageState();
		if (state.equalsIgnoreCase(Environment.MEDIA_MOUNTED))
			return true;

		return false;
	}

	private void setInitialDirectory() {

		if (this.path == null) {
			// if (Environment.getExternalStorageDirectory().isDirectory()
			// && Environment.getExternalStorageDirectory().canRead())
			// path = Environment.getExternalStorageDirectory();
			// else
			// path = new File("/");

			if (isExternalStorage(FileBrowserActivity.this)) {
				path = Environment.getExternalStorageDirectory();
				Log.e("path", path.getAbsolutePath());
			} else {
				path = getCacheDir().getParentFile();
				Log.e("path1", path.getAbsolutePath());
			}
			// Log.e("path",path.getAbsolutePath());
		}
	}

	private void parseDirectoryPath() {
		pathDirsList.clear();
		String pathString = path.getAbsolutePath();

		String[] parts = pathString.split("/");
		int i = 0;
		while (i < parts.length) {

			pathDirsList.add(parts[i]);
			i++;
		}
	}

	private void initializeButtons() {
		ImageView upDirButton = (ImageView) this.findViewById(R.id.upDirectoryButton);
		upDirButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Log.d(LOGTAG, "onclick for upDirButton");
				loadDirectoryUp();
				loadFileList();
				adapter.notifyDataSetChanged();
				updateCurrentDirectoryTextView();
			}
		});
	}

	private void loadDirectoryUp() {
		String s = pathDirsList.remove(pathDirsList.size() - 1);
		path = new File(path.toString().substring(0, path.toString().lastIndexOf(s)));
		fileList.clear();
	}

	private void updateCurrentDirectoryTextView() {
		int i = 0;
		String curDirString = "";
		while (i < pathDirsList.size()) {
			curDirString += pathDirsList.get(i) + "/";
			i++;
		}
		if (pathDirsList.size() == 0) {
			((ImageView) this.findViewById(R.id.upDirectoryButton)).setEnabled(false);
			curDirString = "/";
		} else
			((ImageView) this.findViewById(R.id.upDirectoryButton)).setEnabled(true);
		long freeSpace = getFreeSpace(curDirString);
		if (freeSpace == 0) {
			Log.d(LOGTAG, "NO FREE SPACE");
			File currentDir = new File(curDirString);
		}

		((TextView) this.findViewById(R.id.currentDirectoryTextView)).setText("Current directory: " + curDirString);
	}

	// private void showToast(String message) {
	// Toast.makeText(this, message, Toast.LENGTH_LONG).show();
	// }

	private void initializeFileListView() {
		ListView lView = (ListView) this.findViewById(R.id.fileListView);
		LinearLayout.LayoutParams lParam = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT,
				LayoutParams.FILL_PARENT);
		lParam.setMargins(15, 5, 15, 5);
		lView.setAdapter(this.adapter);
		lView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				chosenFile = fileList.get(position).file;
				File sel = new File(path + "/" + chosenFile);
				Log.d(LOGTAG, "Clicked:" + chosenFile);
				String msqPattern = ".mp3";
				if (sel.isDirectory()) {
					if (sel.canRead()) {
						pathDirsList.add(chosenFile);
						path = new File(sel + "");
						Log.d(LOGTAG, "Just reloading the list");
						loadFileList();
						adapter.notifyDataSetChanged();
						updateCurrentDirectoryTextView();
						Log.d(LOGTAG, path.getAbsolutePath());
					} else {
						// showToast("Path does not exist or cannot be read");
					}
				} else {
					Log.d(LOGTAG, "item clicked");
					if (!directoryShownIsEmpty) {

						String extsion = sel.getAbsolutePath();

						String name = "";
						int lastindex = extsion.lastIndexOf("/");
						if (lastindex != -1) {
							name = extsion.substring(lastindex + 1);
						}
						if (extsion.contains(".mp3") || extsion.contains(".MP3") || extsion.contains(".ogg")) {
							Log.d(LOGTAG, "File selected:" + chosenFile);

							Musicmodel md = new Musicmodel();
							md.name = name;
							md.path = sel.getAbsolutePath();
							musiclist.add(md);
							dbHandler.Add_playlist(md);
							musicadapter.notifyDataSetChanged();
							// showToast(extsion);
						}

						// returnFileFinishActivity(extsion);
					}
				}
			}
		});
	}

	// private void returnFileFinishActivity(String filePath) {
	// Intent retIntent = new Intent();
	// retIntent.putExtra(returnFileParameter, filePath);
	// this.setResult(RESULT_OK, retIntent);
	// this.finish();
	// }

	private void loadFileList() {
		try {
			path.mkdirs();
		} catch (SecurityException e) {
			Log.e(LOGTAG, "unable to write on the sd card ");
		}
		fileList.clear();

		if (path.exists() && path.canRead()) {
			FilenameFilter filter = new FilenameFilter() {
				public boolean accept(File dir, String filename) {
					File sel = new File(dir, filename);

					boolean showReadableFile = sel.canRead();
					if (currentAction == SELECT_FILE) {

						if (sel.isFile()) {
							Log.e("select", "file");
							return (showReadableFile);
						} else {
							Log.e("select", "not file");
							return (showReadableFile);
						}
					}
					return true;
				}
			};

			String[] fList = path.list(filter);
			this.directoryShownIsEmpty = false;
			for (int i = 0; i < fList.length; i++) {
				File sel = new File(path, fList[i]);
				Log.d(LOGTAG, "File:" + fList[i] + " readable:" + (Boolean.valueOf(sel.canRead())).toString());

				int drawableID = R.drawable.file_icon;
				boolean canRead = sel.canRead();
				if (sel.isDirectory()) {
					if (canRead) {
						drawableID = R.drawable.folder_icon;
					} else {
						drawableID = R.drawable.folder_icon_light;
					}
				}
				fileList.add(i, new Item(fList[i], drawableID, canRead));
			}
			if (fileList.size() == 0) {
				this.directoryShownIsEmpty = true;
				fileList.add(0, new Item("Directory is empty", -1, true));
			} else {
				Collections.sort(fileList, new ItemFileNameComparator());
			}
		} else {
			Log.e(LOGTAG, "path does not exist or cannot be read");
		}

	}

	private void createFileListAdapter() {
		adapter = new ArrayAdapter<Item>(this, R.layout.playlistitem, R.id.musicname, fileList) {
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				View view = super.getView(position, convertView, parent);
				TextView textView = (TextView) view.findViewById(R.id.musicname);
				int drawableID = 0;
				if (fileList.get(position).icon != -1) {
					drawableID = fileList.get(position).icon;
				}
				textView.setCompoundDrawablesWithIntrinsicBounds(drawableID, 0, 0, 0);

				textView.setEllipsize(null);

				int dp3 = (int) (3 * getResources().getDisplayMetrics().density + 0.5f);
				return view;
			}
		};
	}

	private class Item {
		public String file;
		public int icon;
		public boolean canRead;

		public Item(String file, Integer icon, boolean canRead) {
			this.file = file;
			this.icon = icon;
		}

		@Override
		public String toString() {
			return file;
		}
	}

	private class ItemFileNameComparator implements Comparator<Item> {
		public int compare(Item lhs, Item rhs) {
			return lhs.file.toLowerCase().compareTo(rhs.file.toLowerCase());
		}
	}

	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
			Log.d(LOGTAG, "ORIENTATION_LANDSCAPE");
		} else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
			Log.d(LOGTAG, "ORIENTATION_PORTRAIT");
		}

	}

	public static long getFreeSpace(String path) {
		StatFs stat = new StatFs(path);
		long availSize = (long) stat.getAvailableBlocks() * (long) stat.getBlockSize();
		return availSize;
	}

}
package com.multidj.multitrackdjelectro;

import java.io.IOException;

import android.app.Activity;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import com.multidj.multitrackdjelectro.R;

public class PadActivity extends Activity implements OnClickListener {

	ImageView cancel;
	ImageView p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18;
	MediaPlayer mptrack1, mptrack2, mptrack3, mptrack4, mptrack5, mptrack6, mptrack7, mptrack8, mptrack9, mptrack10,
			mptrack11, mptrack12, mptrack13, mptrack14, mptrack15, mptrack16, mptrack17, mptrack18;
	VerticalSeekBar t1;
	ImageView m1;
	boolean ismute = false;
	float Volume = 0.5f;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pad_layout);

		mptrack1 = new MediaPlayer();
		mptrack2 = new MediaPlayer();
		mptrack3 = new MediaPlayer();
		mptrack4 = new MediaPlayer();
		mptrack5 = new MediaPlayer();
		mptrack6 = new MediaPlayer();
		mptrack7 = new MediaPlayer();
		mptrack8 = new MediaPlayer();
		mptrack9 = new MediaPlayer();
		mptrack10 = new MediaPlayer();

		mptrack11 = new MediaPlayer();
		mptrack12 = new MediaPlayer();
		mptrack13 = new MediaPlayer();
		mptrack14 = new MediaPlayer();
		mptrack15 = new MediaPlayer();
		mptrack16 = new MediaPlayer();
		mptrack17 = new MediaPlayer();
		mptrack18 = new MediaPlayer();

		t1 = (VerticalSeekBar) findViewById(R.id.t1);
		t1.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {

			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

				Volume = (progress) / 100.0f;
				mptrack1.setVolume(Volume, Volume);
				mptrack2.setVolume(Volume, Volume);
				mptrack3.setVolume(Volume, Volume);
				mptrack4.setVolume(Volume, Volume);
				mptrack5.setVolume(Volume, Volume);
				mptrack6.setVolume(Volume, Volume);
				mptrack7.setVolume(Volume, Volume);
				mptrack8.setVolume(Volume, Volume);
				mptrack9.setVolume(Volume, Volume);
				mptrack10.setVolume(Volume, Volume);
				mptrack11.setVolume(Volume, Volume);
				mptrack12.setVolume(Volume, Volume);
				mptrack13.setVolume(Volume, Volume);
				mptrack14.setVolume(Volume, Volume);
				mptrack15.setVolume(Volume, Volume);
				mptrack16.setVolume(Volume, Volume);
				mptrack17.setVolume(Volume, Volume);
				mptrack18.setVolume(Volume, Volume);

			}
		});

		m1 = (ImageView) findViewById(R.id.m1);
		m1.setOnClickListener(this);

		cancel = (ImageView) findViewById(R.id.cancel);
		p1 = (ImageView) findViewById(R.id.p1);
		p2 = (ImageView) findViewById(R.id.p2);
		p3 = (ImageView) findViewById(R.id.p3);
		p4 = (ImageView) findViewById(R.id.p4);
		p5 = (ImageView) findViewById(R.id.p5);
		p6 = (ImageView) findViewById(R.id.p6);
		p7 = (ImageView) findViewById(R.id.p7);
		p8 = (ImageView) findViewById(R.id.p8);
		p9 = (ImageView) findViewById(R.id.p9);
		p10 = (ImageView) findViewById(R.id.p10);
		p11 = (ImageView) findViewById(R.id.p11);
		p12 = (ImageView) findViewById(R.id.p12);
		p13 = (ImageView) findViewById(R.id.p13);
		p14 = (ImageView) findViewById(R.id.p14);
		p15 = (ImageView) findViewById(R.id.p15);
		p16 = (ImageView) findViewById(R.id.p16);
		p17 = (ImageView) findViewById(R.id.p17);
		p18 = (ImageView) findViewById(R.id.p18);

		cancel.setOnClickListener(this);
		p1.setOnClickListener(this);
		p2.setOnClickListener(this);
		p3.setOnClickListener(this);
		p4.setOnClickListener(this);
		p5.setOnClickListener(this);
		p6.setOnClickListener(this);
		p7.setOnClickListener(this);
		p8.setOnClickListener(this);
		p9.setOnClickListener(this);
		p10.setOnClickListener(this);
		p11.setOnClickListener(this);
		p12.setOnClickListener(this);
		p13.setOnClickListener(this);
		p14.setOnClickListener(this);
		p15.setOnClickListener(this);
		p16.setOnClickListener(this);
		p17.setOnClickListener(this);
		p18.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.cancel:
			finishactivity();
			break;

		case R.id.m1:
			ismute = !ismute;
			if (!ismute) {
				m1.setImageResource(R.drawable.unmute);
				mptrack1.setVolume(Volume, Volume);
				mptrack2.setVolume(Volume, Volume);
				mptrack3.setVolume(Volume, Volume);
				mptrack4.setVolume(Volume, Volume);
				mptrack5.setVolume(Volume, Volume);
				mptrack6.setVolume(Volume, Volume);
				mptrack7.setVolume(Volume, Volume);
				mptrack8.setVolume(Volume, Volume);
				mptrack9.setVolume(Volume, Volume);
				mptrack10.setVolume(Volume, Volume);
				mptrack11.setVolume(Volume, Volume);
				mptrack12.setVolume(Volume, Volume);
				mptrack13.setVolume(Volume, Volume);
				mptrack14.setVolume(Volume, Volume);
				mptrack15.setVolume(Volume, Volume);
				mptrack16.setVolume(Volume, Volume);
				mptrack17.setVolume(Volume, Volume);
				mptrack18.setVolume(Volume, Volume);

			} else {
				m1.setImageResource(R.drawable.mute);
				mptrack1.setVolume(0f, 0f);
				mptrack2.setVolume(0f, 0f);
				mptrack3.setVolume(0f, 0f);
				mptrack4.setVolume(0f, 0f);
				mptrack5.setVolume(0f, 0f);
				mptrack6.setVolume(0f, 0f);
				mptrack7.setVolume(0f, 0f);
				mptrack8.setVolume(0f, 0f);
				mptrack9.setVolume(0f, 0f);
				mptrack10.setVolume(0f, 0f);
				mptrack11.setVolume(0f, 0f);
				mptrack12.setVolume(0f, 0f);
				mptrack13.setVolume(0f, 0f);
				mptrack14.setVolume(0f, 0f);
				mptrack15.setVolume(0f, 0f);
				mptrack16.setVolume(0f, 0f);
				mptrack17.setVolume(0f, 0f);
				mptrack18.setVolume(0f, 0f);

			}

			break;

		case R.id.p1:
			playtrack1("p1.ogg");
			break;
		case R.id.p2:
			playtrack2("p2.ogg");
			break;
		case R.id.p3:
			playtrack3("p3.ogg");
			break;
		case R.id.p4:
			playtrack4("p4.ogg");
			break;
		case R.id.p5:
			playtrack5("p5.ogg");
			break;
		case R.id.p6:
			playtrack6("p6.ogg");
			break;
		case R.id.p7:
			playtrack7("p7.ogg");
			break;
		case R.id.p8:
			playtrack8("p8.ogg");
			break;
		case R.id.p9:
			playtrack9("p9.ogg");
			break;
		case R.id.p10:
			playtrack10("p10.ogg");
			break;
		case R.id.p11:
			playtrack11("p11.ogg");
			break;
		case R.id.p12:
			playtrack12("p12.ogg");
			break;
		case R.id.p13:
			playtrack13("p13.ogg");
			break;
		case R.id.p14:
			playtrack14("p14.ogg");
			break;
		case R.id.p15:
			playtrack15("p15.ogg");
			break;
		case R.id.p16:
			playtrack16("p16.ogg");
			break;
		case R.id.p17:
			playtrack17("p17.ogg");
			break;
		case R.id.p18:
			playtrack18("p18.ogg");
			break;

		default:
			break;
		}
	}

	private void playtrack1(String musicname) {
		try {
			mptrack1.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			mptrack1.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mptrack1.prepare();
			mptrack1.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mptrack1.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void playtrack2(String musicname) {
		try {
			mptrack2.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			mptrack2.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mptrack2.prepare();
			mptrack2.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mptrack2.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void playtrack3(String musicname) {
		try {
			mptrack3.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			mptrack3.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mptrack3.prepare();
			mptrack3.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mptrack3.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void playtrack4(String musicname) {
		try {
			mptrack4.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			mptrack4.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mptrack4.prepare();
			mptrack4.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mptrack4.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void playtrack5(String musicname) {
		try {
			mptrack5.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			mptrack5.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mptrack5.prepare();
			mptrack5.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mptrack5.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void playtrack6(String musicname) {
		try {
			mptrack6.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			mptrack6.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mptrack6.prepare();
			mptrack6.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mptrack6.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void playtrack7(String musicname) {
		try {
			mptrack7.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			mptrack7.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mptrack7.prepare();
			mptrack7.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mptrack7.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void playtrack8(String musicname) {
		try {
			mptrack8.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			mptrack8.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mptrack8.prepare();
			mptrack8.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mptrack8.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void playtrack9(String musicname) {
		try {
			mptrack9.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			mptrack9.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mptrack9.prepare();
			mptrack9.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mptrack9.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void playtrack10(String musicname) {
		try {
			mptrack10.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			mptrack10
					.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mptrack10.prepare();
			mptrack10.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mptrack10.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void playtrack11(String musicname) {
		try {
			mptrack11.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			mptrack11
					.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mptrack11.prepare();
			mptrack11.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mptrack11.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void playtrack12(String musicname) {
		try {
			mptrack12.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			mptrack12
					.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mptrack12.prepare();
			mptrack12.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mptrack12.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void playtrack13(String musicname) {
		try {
			mptrack13.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			mptrack13
					.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mptrack13.prepare();
			mptrack13.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mptrack13.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void playtrack14(String musicname) {
		try {
			mptrack14.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			mptrack14
					.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mptrack14.prepare();
			mptrack14.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mptrack14.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void playtrack15(String musicname) {
		try {
			mptrack15.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			mptrack15
					.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mptrack15.prepare();
			mptrack15.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mptrack15.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void playtrack16(String musicname) {
		try {
			mptrack16.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			mptrack16
					.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mptrack16.prepare();
			mptrack16.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mptrack16.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void playtrack17(String musicname) {
		try {
			mptrack17.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			mptrack17
					.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mptrack17.prepare();
			mptrack17.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mptrack17.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void playtrack18(String musicname) {
		try {
			mptrack18.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			mptrack18
					.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mptrack18.prepare();
			mptrack18.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mptrack18.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onBackPressed() {
		finishactivity();
	}

	protected void finishactivity() {

		mptrack1.release();
		mptrack2.release();
		mptrack3.release();
		mptrack4.release();
		mptrack5.release();
		mptrack6.release();
		mptrack7.release();
		mptrack8.release();
		mptrack9.release();
		mptrack10.release();
		mptrack11.release();
		mptrack12.release();
		mptrack13.release();
		mptrack14.release();
		mptrack15.release();
		mptrack16.release();
		mptrack17.release();
		mptrack18.release();
		finish();
	}
}

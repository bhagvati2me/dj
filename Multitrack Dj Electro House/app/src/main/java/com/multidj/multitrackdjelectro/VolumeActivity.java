package com.multidj.multitrackdjelectro;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import com.multidj.multitrackdjelectro.R;

public class VolumeActivity extends Activity implements OnClickListener {

	ImageView cancel;
	VerticalSeekBar t1, t2, t3, t4, t5, t6;
	ImageView m1, m2, m3, m4, m5, m6;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.vol_layout);
		cancel = (ImageView) findViewById(R.id.cancel);
		t1 = (VerticalSeekBar) findViewById(R.id.t1);
		t2 = (VerticalSeekBar) findViewById(R.id.t2);
		t3 = (VerticalSeekBar) findViewById(R.id.t3);
		t4 = (VerticalSeekBar) findViewById(R.id.t4);
		t5 = (VerticalSeekBar) findViewById(R.id.t5);
		t6 = (VerticalSeekBar) findViewById(R.id.t6);

		m1 = (ImageView) findViewById(R.id.m1);
		m2 = (ImageView) findViewById(R.id.m2);
		m3 = (ImageView) findViewById(R.id.m3);
		m4 = (ImageView) findViewById(R.id.m4);
		m5 = (ImageView) findViewById(R.id.m5);
		m6 = (ImageView) findViewById(R.id.m6);

		Log.e("", "t1: " + VolumeStore.ist1mute + " vol " + VolumeStore.t1);
		Log.e("", "t2: " + VolumeStore.ist2mute + " vol " + VolumeStore.t2);
		Log.e("", "t3: " + VolumeStore.ist3mute + " vol " + VolumeStore.t3);
		Log.e("", "t4: " + VolumeStore.ist4mute + " vol " + VolumeStore.t4);
		Log.e("", "t5: " + VolumeStore.ist5mute + " vol " + VolumeStore.t5);

		if (VolumeStore.ist1mute) {
			m1.setImageResource(R.drawable.mute);
		} else {
			m1.setImageResource(R.drawable.unmute);
		}
		if (VolumeStore.ist2mute) {
			m2.setImageResource(R.drawable.mute);
		} else {
			m2.setImageResource(R.drawable.unmute);
		}
		if (VolumeStore.ist3mute) {
			m3.setImageResource(R.drawable.mute);
		} else {
			m3.setImageResource(R.drawable.unmute);
		}
		if (VolumeStore.ist4mute) {
			m4.setImageResource(R.drawable.mute);
		} else {
			m4.setImageResource(R.drawable.unmute);
		}
		if (VolumeStore.ist5mute) {
			m5.setImageResource(R.drawable.mute);
		} else {
			m5.setImageResource(R.drawable.unmute);

		}
		if (VolumeStore.ist6mute) {
			m6.setImageResource(R.drawable.mute);
		} else {
			m6.setImageResource(R.drawable.unmute);

		}

		t1.setProgress((int) VolumeStore.t1);
		t2.setProgress((int) VolumeStore.t2);
		t3.setProgress((int) VolumeStore.t3);
		t4.setProgress((int) VolumeStore.t4);
		t5.setProgress((int) VolumeStore.t5);
		t6.setProgress((int) VolumeStore.t6);

		t1.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {

			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

				VolumeStore.t1 = progress;
			}
		});
		t2.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {

			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

				VolumeStore.t2 = progress;
			}
		});
		t3.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {

			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

				VolumeStore.t3 = progress;
			}
		});
		t4.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {

			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

				VolumeStore.t4 = progress;
			}
		});
		t5.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {

			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

				VolumeStore.t5 = progress;
			}
		});
		t6.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {

			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

				VolumeStore.t6 = progress;
			}
		});

		m1.setOnClickListener(this);
		m2.setOnClickListener(this);
		m3.setOnClickListener(this);
		m4.setOnClickListener(this);
		m5.setOnClickListener(this);
		m6.setOnClickListener(this);

		cancel.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.cancel:
			finishactivity();
			break;
		case R.id.m1:
			if (VolumeStore.ist1mute) {
				m1.setImageResource(R.drawable.unmute);
			} else {
				m1.setImageResource(R.drawable.mute);
			}
			VolumeStore.ist1mute = !VolumeStore.ist1mute;
			break;
		case R.id.m2:
			if (VolumeStore.ist2mute) {
				m2.setImageResource(R.drawable.unmute);
			} else {
				m2.setImageResource(R.drawable.mute);
			}
			VolumeStore.ist2mute = !VolumeStore.ist2mute;
			break;
		case R.id.m3:
			if (VolumeStore.ist3mute) {
				m3.setImageResource(R.drawable.unmute);
			} else {
				m3.setImageResource(R.drawable.mute);
			}
			VolumeStore.ist3mute = !VolumeStore.ist3mute;
			break;
		case R.id.m4:
			if (VolumeStore.ist4mute) {
				m4.setImageResource(R.drawable.unmute);
			} else {
				m4.setImageResource(R.drawable.mute);
			}
			VolumeStore.ist4mute = !VolumeStore.ist4mute;
			break;
		case R.id.m5:
			if (VolumeStore.ist5mute) {
				m5.setImageResource(R.drawable.unmute);
			} else {
				m5.setImageResource(R.drawable.mute);
			}
			VolumeStore.ist5mute = !VolumeStore.ist5mute;
			break;
		case R.id.m6:
			if (VolumeStore.ist6mute) {
				m6.setImageResource(R.drawable.unmute);
			} else {
				m6.setImageResource(R.drawable.mute);
			}
			VolumeStore.ist6mute = !VolumeStore.ist6mute;
			break;

		default:
			break;
		}
	}

	@Override
	public void onBackPressed() {
		finishactivity();
	}

	protected void finishactivity() {

		finish();
	}

}

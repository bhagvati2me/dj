package com.multidj.multitrackdjelectro;

import java.io.IOException;

import android.app.Activity;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import com.multidj.multitrackdjelectro.R;

public class MetromomeActivity extends Activity implements OnClickListener {

	ImageView cancel;
	RadioGroup bpmradio;
	MediaPlayer mptrack0;
	Button ok;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.radio_layout);
		mptrack0 = new MediaPlayer();
		cancel = (ImageView) findViewById(R.id.cancel);
		bpmradio = (RadioGroup) findViewById(R.id.radiogrp);
		ok= (Button) findViewById(R.id.ok);
		cancel.setOnClickListener(this);
		ok.setOnClickListener(this);
		
		bpmradio.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {

				switch (group.getCheckedRadioButtonId()) {
				case R.id.r1:
					playtrack("r1.ogg");
					break;
				case R.id.r2:
					playtrack("r2.ogg");
					break;
				case R.id.r3:
					playtrack("r3.ogg");
					break;
				case R.id.r4:
					playtrack("r4.ogg");
					break;
				case R.id.r5:
					playtrack("r5.ogg");
					break;
				case R.id.r6:
					playtrack("r6.ogg");
					break;
				case R.id.r7:
					playtrack("r7.ogg");
					break;
				case R.id.r8:
					playtrack("r8.ogg");
					break;
				case R.id.r9:
					playtrack("r9.ogg");
					break;
				case R.id.r10:
					playtrack("r10.ogg");
					break;

				default:
					break;
				}

			}
		});

		
	}

	private void playtrack(String musicname) {
		try {
			mptrack0.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			mptrack0.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mptrack0.prepare();
			mptrack0.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mptrack0.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.cancel:
			finishactivity();
			break;
		case R.id.ok:
			finishactivity();
			break;

		default:
			break;
		}
	}

	@Override
	public void onBackPressed() {
		finishactivity();
	}

	protected void finishactivity() {
		mptrack0.release();
		finish();
	}

}

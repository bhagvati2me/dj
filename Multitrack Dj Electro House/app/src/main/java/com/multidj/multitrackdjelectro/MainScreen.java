package com.multidj.multitrackdjelectro;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetFileDescriptor;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.audiofx.Visualizer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainScreen extends Activity implements OnClickListener {

    String TAG = "Main";
    ImageView padbt, metronomebt, volbt, menu, soundplay, loopplay;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private InterstitialAd interstitial;
    boolean isplay = false;
    Handler handler = new Handler();
    Button play1, play2, play3, play4, play5, play6;
    Button track1, track2, track3, track4, track5, track6;
    TextView loop1, loop2, loop3, loop4, loop5, loop6;
    ImageView prev1, prev2, prev3, prev4, prev5, prev6;
    ImageView next1, next2, next3, next4, next5, next6;
    MediaPlayer mp1, mp2, mp3, mp4, mp5, mp6;
    MediaPlayer mptrack5, mptrack6, mptrack1, mptrack2, mptrack3, mptrack4;
    boolean istrack1play = false, istrack2play = false, istrack3play = false, istrack4play = false,
            istrack5play = false, istrack6play = false;
    boolean isloop1play = false, isloop2play = false, isloop3play = false, isloop4play = false, isloop5play = false,
            isloop6play = false;
    float vol1, vol2, vol3, vol4, vol5, vol6;
    private Visualizer mVisualizer1, mVisualizer2, mVisualizer3, mVisualizer4, mVisualizer5, mVisualizer6;
    private LinearLayout mLinearLayout1, mLinearLayout2, mLinearLayout3, mLinearLayout4, mLinearLayout5,
            mLinearLayout6;
    private static final float VISUALIZER_HEIGHT_DIP = 50f;
    VisualizerView mVisualizerView1, mVisualizerView2, mVisualizerView3, mVisualizerView4, mVisualizerView5,
            mVisualizerView6;


    private String[] permissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO};

    @RequiresApi(api = Build.VERSION_CODES.M)
    private boolean arePermissionsEnabled() {
        for (String permission : permissions) {
            if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED)
                return false;
        }
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void requestMultiplePermissions() {
        List<String> remainingPermissions = new ArrayList<>();
        for (String permission : permissions) {
            if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                remainingPermissions.add(permission);
            }
        }
        requestPermissions(remainingPermissions.toArray(new String[remainingPermissions.size()]), 101);
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 101) {
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                    if (shouldShowRequestPermissionRationale(permissions[i])) {
                        new AlertDialog.Builder(this)
                                .setMessage("Please allow permissions to use record audio and choose local music from device")
                                .setPositiveButton("Allow", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        requestMultiplePermissions();
                                    }
                                })
                                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        requestMultiplePermissions();
                                    }
                                })
                                .create()
                                .show();
                    }
                    return;
                }
            }

            setupVisualizerFxAndUI1();
            setupVisualizerFxAndUI2();
            setupVisualizerFxAndUI3();
            setupVisualizerFxAndUI4();
            setupVisualizerFxAndUI5();
            setupVisualizerFxAndUI6();


            mVisualizer1.setEnabled(true);
            mVisualizer2.setEnabled(true);
            mVisualizer3.setEnabled(true);
            mVisualizer4.setEnabled(true);
            mVisualizer5.setEnabled(true);
            mVisualizer6.setEnabled(true);
//====


        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if(!arePermissionsEnabled()) {
                requestMultiplePermissions();
            }
        }

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        padbt = (ImageView) findViewById(R.id.padbt);
        metronomebt = (ImageView) findViewById(R.id.metronomebt);
        volbt = (ImageView) findViewById(R.id.volbt);
        menu = (ImageView) findViewById(R.id.menubt);
        soundplay = (ImageView) findViewById(R.id.soundplay);
        loopplay = (ImageView) findViewById(R.id.loopplay);

        AdRequest adRequest = new AdRequest.Builder().build();
        AdView adView = (AdView) findViewById(R.id.adView);
        adView.loadAd(adRequest);
        interstitial = new InterstitialAd(MainScreen.this);
        interstitial.setAdUnitId("ca-app-pub-3805784166574868/4457544281");
        interstitial.loadAd(adRequest);

        interstitial.setAdListener(new AdListener() {
            public void onAdLoaded() {
                displayInterstitial();
            }
        });
        mDrawerList = (ListView) findViewById(R.id.navdrawer);
        String[] values1 = new String[]{"Rate Now", "Info", "More Apps"};
        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,
                android.R.id.text1, values1);
        mDrawerList.setAdapter(adapter1);
        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intentweb = new Intent(MainScreen.this, WebBrowser.class);
                switch (position) {
                    case 0:
                        showratedialog();
                        mDrawerLayout.closeDrawer(Gravity.START);
                        break;
                    case 1:
                        intentweb.putExtra("URL", "https://play.google.com/store/apps/developer?id=DjDeve");
                        startActivity(intentweb);
                        mDrawerLayout.closeDrawer(Gravity.START);

                        break;
                    case 2:
                        intentweb.putExtra("URL", "https://play.google.com/store/apps/developer?id=DjDeve");
                        startActivity(intentweb);
                        mDrawerLayout.closeDrawer(Gravity.START);

                        break;

                }

            }
        });

        padbt.setOnClickListener(this);
        metronomebt.setOnClickListener(this);
        volbt.setOnClickListener(this);
        menu.setOnClickListener(this);
        soundplay.setOnClickListener(this);
        loopplay.setOnClickListener(this);

        mp1 = new MediaPlayer();
        mp2 = new MediaPlayer();
        mp3 = new MediaPlayer();
        mp4 = new MediaPlayer();
        mp5 = new MediaPlayer();
        mp6 = new MediaPlayer();

        mptrack5 = new MediaPlayer();
        mptrack6 = new MediaPlayer();
        mptrack1 = new MediaPlayer();
        mptrack2 = new MediaPlayer();
        mptrack3 = new MediaPlayer();
        mptrack4 = new MediaPlayer();

        play1 = (Button) findViewById(R.id.play1);
        play2 = (Button) findViewById(R.id.play2);
        play3 = (Button) findViewById(R.id.play3);
        play4 = (Button) findViewById(R.id.play4);
        play5 = (Button) findViewById(R.id.play5);
        play6 = (Button) findViewById(R.id.play6);

        track1 = (Button) findViewById(R.id.track1);
        track2 = (Button) findViewById(R.id.track2);
        track3 = (Button) findViewById(R.id.track3);
        track4 = (Button) findViewById(R.id.track4);
        track5 = (Button) findViewById(R.id.track5);
        track6 = (Button) findViewById(R.id.track6);

        loop1 = (TextView) findViewById(R.id.loop1);
        loop2 = (TextView) findViewById(R.id.loop2);
        loop3 = (TextView) findViewById(R.id.loop3);
        loop4 = (TextView) findViewById(R.id.loop4);
        loop5 = (TextView) findViewById(R.id.loop5);
        loop6 = (TextView) findViewById(R.id.loop6);

        prev1 = (ImageView) findViewById(R.id.prev1);
        prev2 = (ImageView) findViewById(R.id.prev2);
        prev3 = (ImageView) findViewById(R.id.prev3);
        prev4 = (ImageView) findViewById(R.id.prev4);
        prev5 = (ImageView) findViewById(R.id.prev5);
        prev6 = (ImageView) findViewById(R.id.prev6);

        next1 = (ImageView) findViewById(R.id.next1);
        next2 = (ImageView) findViewById(R.id.next2);
        next3 = (ImageView) findViewById(R.id.next3);
        next4 = (ImageView) findViewById(R.id.next4);
        next5 = (ImageView) findViewById(R.id.next5);
        next6 = (ImageView) findViewById(R.id.next6);

        mLinearLayout1 = (LinearLayout) findViewById(R.id.l1);
        mLinearLayout2 = (LinearLayout) findViewById(R.id.l2);
        mLinearLayout3 = (LinearLayout) findViewById(R.id.l3);
        mLinearLayout4 = (LinearLayout) findViewById(R.id.l4);
        mLinearLayout5 = (LinearLayout) findViewById(R.id.l5);
        mLinearLayout6 = (LinearLayout) findViewById(R.id.l6);




        play1.setOnClickListener(this);
        play2.setOnClickListener(this);
        play3.setOnClickListener(this);
        play4.setOnClickListener(this);
        play5.setOnClickListener(this);
        play6.setOnClickListener(this);

        prev1.setOnClickListener(this);
        prev2.setOnClickListener(this);
        prev3.setOnClickListener(this);
        prev4.setOnClickListener(this);
        prev5.setOnClickListener(this);
        prev6.setOnClickListener(this);

        next1.setOnClickListener(this);
        next2.setOnClickListener(this);
        next3.setOnClickListener(this);
        next4.setOnClickListener(this);
        next5.setOnClickListener(this);
        next6.setOnClickListener(this);

        track1.setOnClickListener(this);
        track2.setOnClickListener(this);
        track3.setOnClickListener(this);
        track4.setOnClickListener(this);
        track5.setOnClickListener(this);
        track6.setOnClickListener(this);

    }

    public void displayInterstitial() {
        if (interstitial.isLoaded()) {
            interstitial.show();
        }
    }

    private void showratedialog() {
        new AlertDialog.Builder(this).setTitle("Do you want to rate this application?")
                .setPositiveButton("Never", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        dialog.dismiss();
                    }
                }).setNegativeButton("not now", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // do nothing
                dialog.dismiss();
            }
        }).setNeutralButton("Rate Now", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // do nothing
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id="
                        + "com.multidj.multitrackdjelectro")));
            }
        }).show();
    }

    @SuppressLint("NewApi")
    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.soundplay:
                stopsound();
                break;
            case R.id.loopplay:
                stoploop();
                break;

            case R.id.padbt:
                Intent padintent = new Intent(getcontext(), PadActivity.class);
                startActivity(padintent);

                break;
            case R.id.metronomebt:
                Intent radiointetnt = new Intent(getcontext(), MetromomeActivity.class);
                startActivity(radiointetnt);
                break;
            case R.id.volbt:
                Intent volintent = new Intent(getcontext(), VolumeActivity.class);
                startActivity(volintent);
                break;
            case R.id.menubt:
                mDrawerLayout.openDrawer(Gravity.START);
                break;

            case R.id.play1:

                if (!ist1setloop) {
                    playmusic1("t0s" + MusicLoopClass.mploo1 + ".ogg");
                    play1.setBackground(getResources().getDrawable(R.drawable.bluelight));

                } else {

                    if (mp1.isPlaying()) {
                        mp1.pause();
                        isloop1play = false;
                        play1.setBackground(getResources().getDrawable(R.drawable.blue));
                    } else {
                        if (ist1setloop) {
                            isloop1play = true;
                            mp1.start();
                            play1.setBackground(getResources().getDrawable(R.drawable.bluelight));
                        }
                    }

                }
                break;
            case R.id.play2:
                if (!ist2setloop) {
                    playmusic2("t1s" + MusicLoopClass.mploo2 + ".ogg");
                    play2.setBackground(getResources().getDrawable(R.drawable.bluelight));
                    Log.e("", "play first time p2");
                } else {

                    if (mp2.isPlaying()) {
                        mp2.pause();
                        isloop2play = false;
                        Log.e("", "pause p2");
                        play2.setBackground(getResources().getDrawable(R.drawable.blue));
                    } else {
                        if (ist2setloop) {
                            isloop2play = true;
                            mp2.start();
                            Log.e("", "play p2");
                            play2.setBackground(getResources().getDrawable(R.drawable.bluelight));
                        }
                    }

                }
                break;
            case R.id.play3:
                if (!ist3setloop) {
                    playmusic3("t2s" + MusicLoopClass.mploo3 + ".ogg");
                    play3.setBackground(getResources().getDrawable(R.drawable.bluelight));
                    Log.e("", "play first time p3");
                } else {

                    if (mp3.isPlaying()) {
                        mp3.pause();
                        isloop3play = false;
                        Log.e("", "pause p3");
                        play3.setBackground(getResources().getDrawable(R.drawable.blue));
                    } else {
                        if (ist3setloop) {
                            isloop3play = true;
                            mp3.start();
                            Log.e("", "play p3");
                            play3.setBackground(getResources().getDrawable(R.drawable.bluelight));
                        }
                    }

                }
                break;
            case R.id.play4:
                if (!ist4setloop) {
                    playmusic4("t3s" + MusicLoopClass.mploo4 + ".ogg");
                    play4.setBackground(getResources().getDrawable(R.drawable.bluelight));

                } else {

                    if (mp4.isPlaying()) {
                        mp4.pause();
                        isloop4play = false;
                        play4.setBackground(getResources().getDrawable(R.drawable.blue));
                    } else {
                        if (ist4setloop) {
                            isloop4play = true;
                            mp4.start();
                            play4.setBackground(getResources().getDrawable(R.drawable.bluelight));
                        }
                    }

                }
                break;
            case R.id.play5:
                if (!ist5setloop) {
                    playmusic5("t4s" + MusicLoopClass.mploo5 + ".ogg");
                    play5.setBackground(getResources().getDrawable(R.drawable.bluelight));

                } else {

                    if (mp5.isPlaying()) {
                        mp5.pause();
                        isloop5play = false;
                        play5.setBackground(getResources().getDrawable(R.drawable.blue));
                    } else {
                        if (ist5setloop) {
                            isloop5play = true;
                            mp5.start();
                            play5.setBackground(getResources().getDrawable(R.drawable.bluelight));
                        }
                    }

                }
                break;
            case R.id.play6:
                if (!ist6setloop) {
                    playmusic6("t5s" + MusicLoopClass.mploo6 + ".ogg");
                    play6.setBackground(getResources().getDrawable(R.drawable.bluelight));

                } else {

                    if (mp6.isPlaying()) {
                        mp6.pause();
                        isloop6play = false;
                        play6.setBackground(getResources().getDrawable(R.drawable.blue));
                    } else {
                        if (ist6setloop) {
                            isloop6play = true;
                            mp6.start();
                            play6.setBackground(getResources().getDrawable(R.drawable.bluelight));
                        }
                    }

                }
                break;
            case R.id.track1:
                Intent track1 = new Intent(MainScreen.this, FileBrowserActivity.class);
                track1.putExtra("name", 1);
                startActivityForResult(track1, 1);
                break;
            case R.id.track2:
                Intent track2 = new Intent(MainScreen.this, FileBrowserActivity.class);
                track2.putExtra("name", 2);
                startActivityForResult(track2, 1);
                break;

            case R.id.track3:
                Intent track3 = new Intent(MainScreen.this, FileBrowserActivity.class);
                track3.putExtra("name", 3);
                startActivityForResult(track3, 1);
                break;
            case R.id.track4:
                Intent track4 = new Intent(MainScreen.this, FileBrowserActivity.class);
                track4.putExtra("name", 4);
                startActivityForResult(track4, 1);
                break;
            case R.id.track5:
                Intent track5 = new Intent(MainScreen.this, FileBrowserActivity.class);
                track5.putExtra("name", 5);
                startActivityForResult(track5, 1);
                break;
            case R.id.track6:
                Intent track6 = new Intent(MainScreen.this, FileBrowserActivity.class);
                track6.putExtra("name", 6);
                startActivityForResult(track6, 1);
                break;

            case R.id.next1:
                mp1.reset();
                int upname1 = Integer.valueOf(MusicLoopClass.mploo1) + 1;
                if (upname1 == 11) {
                    MusicLoopClass.mploo1 = "1";
                } else {
                    MusicLoopClass.mploo1 = upname1 + "";
                }
                loop1.setText("L" + MusicLoopClass.mploo1);
                playmusic1("t0s" + MusicLoopClass.mploo1 + ".ogg");
                play1.setBackground(getResources().getDrawable(R.drawable.bluelight));
                break;
            case R.id.next2:
                mp2.reset();

                int upname2 = Integer.valueOf(MusicLoopClass.mploo2) + 1;

                if (upname2 == 11) {
                    MusicLoopClass.mploo2 = "1";
                } else {
                    MusicLoopClass.mploo2 = upname2 + "";
                }

                loop2.setText("L" + MusicLoopClass.mploo2);
                playmusic2("t1s" + MusicLoopClass.mploo2 + ".ogg");
                play2.setBackground(getResources().getDrawable(R.drawable.bluelight));
                break;
            case R.id.next3:
                mp3.reset();
                int upname3 = Integer.valueOf(MusicLoopClass.mploo3) + 1;
                if (upname3 == 11) {
                    MusicLoopClass.mploo3 = "1";
                } else {
                    MusicLoopClass.mploo3 = upname3 + "";
                }
                loop3.setText("L" + MusicLoopClass.mploo3);
                playmusic3("t2s" + MusicLoopClass.mploo3 + ".ogg");
                play3.setBackground(getResources().getDrawable(R.drawable.bluelight));
                break;
            case R.id.next4:
                mp4.reset();
                int upname4 = Integer.valueOf(MusicLoopClass.mploo4) + 1;
                if (upname4 == 11) {
                    MusicLoopClass.mploo4 = "1";
                } else {
                    MusicLoopClass.mploo4 = upname4 + "";
                }
                loop4.setText("L" + MusicLoopClass.mploo4);
                playmusic4("t3s" + MusicLoopClass.mploo4 + ".ogg");
                play4.setBackground(getResources().getDrawable(R.drawable.bluelight));
                break;
            case R.id.next5:
                mp5.reset();
                int upname5 = Integer.valueOf(MusicLoopClass.mploo5) + 1;
                if (upname5 == 11) {
                    MusicLoopClass.mploo5 = "1";
                } else {
                    MusicLoopClass.mploo5 = upname5 + "";
                }
                loop5.setText("L" + MusicLoopClass.mploo5);
                playmusic5("t4s" + MusicLoopClass.mploo5 + ".ogg");
                play5.setBackground(getResources().getDrawable(R.drawable.bluelight));
                break;
            case R.id.next6:
                mp6.reset();
                int upname6 = Integer.valueOf(MusicLoopClass.mploo6) + 1;
                if (upname6 == 11) {
                    MusicLoopClass.mploo6 = "1";
                } else {
                    MusicLoopClass.mploo6 = upname6 + "";
                }
                loop6.setText("L" + MusicLoopClass.mploo6);
                playmusic6("t5s" + MusicLoopClass.mploo6 + ".ogg");
                play6.setBackground(getResources().getDrawable(R.drawable.bluelight));
                break;
            case R.id.prev1:
                mp1.reset();

                int downname1 = Integer.valueOf(MusicLoopClass.mploo1) - 1;

                if (downname1 == 0) {
                    MusicLoopClass.mploo1 = "10";
                } else {
                    MusicLoopClass.mploo1 = downname1 + "";
                }

                loop1.setText("L" + MusicLoopClass.mploo1);
                playmusic1("t0s" + MusicLoopClass.mploo1 + ".ogg");
                play1.setBackground(getResources().getDrawable(R.drawable.bluelight));
                break;
            case R.id.prev2:
                mp2.reset();

                int downname2 = Integer.valueOf(MusicLoopClass.mploo2) - 1;

                if (downname2 == 0) {
                    MusicLoopClass.mploo2 = "10";
                } else {
                    MusicLoopClass.mploo2 = downname2 + "";
                }

                loop2.setText("L" + MusicLoopClass.mploo2);
                playmusic2("t1s" + MusicLoopClass.mploo2 + ".ogg");
                play2.setBackground(getResources().getDrawable(R.drawable.bluelight));
                break;
            case R.id.prev3:
                mp3.reset();

                int downname3 = Integer.valueOf(MusicLoopClass.mploo3) - 1;

                if (downname3 == 0) {
                    MusicLoopClass.mploo3 = "10";
                } else {
                    MusicLoopClass.mploo3 = downname3 + "";
                }

                loop3.setText("L" + MusicLoopClass.mploo3);
                playmusic3("t2s" + MusicLoopClass.mploo3 + ".ogg");
                play3.setBackground(getResources().getDrawable(R.drawable.bluelight));
                break;
            case R.id.prev4:
                mp4.reset();

                int downname4 = Integer.valueOf(MusicLoopClass.mploo4) - 1;

                if (downname4 == 0) {
                    MusicLoopClass.mploo4 = "10";
                } else {
                    MusicLoopClass.mploo4 = downname4 + "";
                }

                loop4.setText("L" + MusicLoopClass.mploo4);
                playmusic4("t3s" + MusicLoopClass.mploo4 + ".ogg");
                play4.setBackground(getResources().getDrawable(R.drawable.bluelight));
                break;
            case R.id.prev5:
                mp5.reset();

                int downname5 = Integer.valueOf(MusicLoopClass.mploo5) - 1;

                if (downname5 == 0) {
                    MusicLoopClass.mploo5 = "10";
                } else {
                    MusicLoopClass.mploo5 = downname5 + "";
                }

                loop5.setText("L" + MusicLoopClass.mploo5);
                playmusic5("t4s" + MusicLoopClass.mploo5 + ".ogg");
                play5.setBackground(getResources().getDrawable(R.drawable.bluelight));
                break;
            case R.id.prev6:
                mp6.reset();

                int downname6 = Integer.valueOf(MusicLoopClass.mploo6) - 1;

                if (downname6 == 0) {
                    MusicLoopClass.mploo6 = "10";
                } else {
                    MusicLoopClass.mploo6 = downname6 + "";
                }

                loop6.setText("L" + MusicLoopClass.mploo6);
                playmusic6("t5s" + MusicLoopClass.mploo6 + ".ogg");
                play6.setBackground(getResources().getDrawable(R.drawable.bluelight));
                break;

            default:
                break;
        }

    }

    boolean ist1setloop = false, ist2setloop = false, ist3setloop = false, ist4setloop = false, ist5setloop = false,
            ist6setloop = false;

    private void playmusic1(String musicname) {
        try {
            ist1setloop = true;
            mp1.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(musicname);
            mp1.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mp1.prepare();
            mp1.setLooping(true);
            if (VolumeStore.ist1mute) {
                mp1.setVolume(0, 0);
            } else {
                mp1.setVolume(vol1, vol1);
            }
            mp1.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mp1.start();
                    isloop1play = true;
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playmusic2(String musicname) {
        try {
            ist2setloop = true;
            mp2.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(musicname);
            mp2.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mp2.prepare();
            if (VolumeStore.ist2mute) {
                mp2.setVolume(0, 0);
            } else {
                mp2.setVolume(vol2, vol2);
            }
            mp2.setLooping(true);
            mp2.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mp2.start();
                    isloop2play = true;
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playmusic3(String musicname) {
        try {
            ist3setloop = true;
            mp3.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(musicname);
            mp3.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mp3.prepare();
            if (VolumeStore.ist3mute) {
                mp3.setVolume(0, 0);
            } else {
                mp3.setVolume(vol3, vol3);
            }
            mp3.setLooping(true);
            mp3.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mp3.start();
                    isloop3play = true;
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playmusic4(String musicname) {
        try {
            ist4setloop = true;
            mp4.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(musicname);
            mp4.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mp4.prepare();
            if (VolumeStore.ist4mute) {
                mp4.setVolume(0, 0);
            } else {
                mp4.setVolume(vol4, vol4);
            }
            mp4.setLooping(true);
            mp4.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mp4.start();
                    isloop4play = true;
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playmusic5(String musicname) {
        try {
            ist5setloop = true;
            mp5.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(musicname);
            mp5.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mp5.prepare();
            if (VolumeStore.ist5mute) {
                mp5.setVolume(0, 0);
            } else {
                mp5.setVolume(vol5, vol5);
            }
            mp5.setLooping(true);
            mp5.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mp5.start();
                    isloop5play = true;
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playmusic6(String musicname) {
        try {
            ist6setloop = true;
            mp6.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(musicname);
            mp6.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mp6.prepare();
            if (VolumeStore.ist6mute) {
                mp6.setVolume(0, 0);
            } else {
                mp6.setVolume(vol6, vol6);
            }
            mp6.setLooping(true);
            mp6.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mp6.start();
                    isloop6play = true;
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    boolean ist1setdata = false, ist2setdata = false, ist3setdata = false, ist4setdata = false, ist5setdata = false,
            ist6setdata = false;

    private void playtrack1(String path) throws IllegalStateException, IOException {
        try {

            ist1setdata = true;
            mptrack1.reset();
            mptrack1.setDataSource(getApplicationContext(), Uri.parse(path));
            mptrack1.prepare();
            mptrack1.setLooping(true);
            if (VolumeStore.ist1mute) {
                mptrack1.setVolume(0, 0);
            } else {
                mptrack1.setVolume(vol1, vol1);
            }
            mptrack1.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mptrack1.start();
                    istrack1play = true;

                }
            });

        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playtrack2(String path) throws IllegalStateException, IOException {
        try {
            ist2setdata = true;
            mptrack2.reset();
            mptrack2.setDataSource(getApplicationContext(), Uri.parse(path));
            mptrack2.prepare();
            mptrack2.setLooping(true);
            if (VolumeStore.ist2mute) {
                mptrack2.setVolume(0, 0);
            } else {
                mptrack2.setVolume(vol2, vol2);
            }
            mptrack2.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mptrack2.start();
                    istrack2play = true;

                }
            });

        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playtrack3(String path) throws IllegalStateException, IOException {
        try {
            ist3setdata = true;
            mptrack3.reset();
            mptrack3.setDataSource(getApplicationContext(), Uri.parse(path));
            mptrack3.prepare();
            if (VolumeStore.ist3mute) {
                mptrack3.setVolume(0, 0);
            } else {
                mptrack3.setVolume(vol3, vol3);
            }
            mptrack3.setLooping(true);
            mptrack3.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mptrack3.start();
                    istrack3play = true;

                }
            });

        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playtrack4(String path) throws IllegalStateException, IOException {
        try {
            ist4setdata = true;
            mptrack4.reset();
            mptrack4.setDataSource(getApplicationContext(), Uri.parse(path));
            mptrack4.prepare();
            mptrack4.setLooping(true);
            if (VolumeStore.ist4mute) {
                mptrack4.setVolume(0, 0);
            } else {
                mptrack4.setVolume(vol4, vol4);
            }
            mptrack4.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mptrack4.start();
                    istrack4play = true;

                }
            });

        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playtrack5(String path) throws IllegalStateException, IOException {
        try {
            ist5setdata = true;
            mptrack5.reset();
            mptrack5.setDataSource(getApplicationContext(), Uri.parse(path));
            mptrack5.prepare();
            mptrack5.setLooping(true);
            if (VolumeStore.ist5mute) {
                mptrack5.setVolume(0, 0);
            } else {
                mptrack5.setVolume(vol5, vol5);
            }
            mptrack5.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mptrack5.start();
                    istrack5play = true;

                }
            });

        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playtrack6(String path) throws IllegalStateException, IOException {
        try {
            ist6setdata = true;
            mptrack6.reset();
            mptrack6.setDataSource(getApplicationContext(), Uri.parse(path));
            mptrack6.prepare();
            mptrack6.setLooping(true);
            if (VolumeStore.ist6mute) {
                mptrack6.setVolume(0, 0);
            } else {
                mptrack6.setVolume(vol6, vol6);
            }
            mptrack6.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mptrack6.start();
                    istrack6play = true;

                }
            });

        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                switch (data.getExtras().getInt("name")) {
                    case 1:
                        try {
                            track1.setText(data.getExtras().getString("mname"));
                            playtrack1(data.getExtras().getString("path"));
                        } catch (IllegalStateException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        break;
                    case 2:
                        try {
                            track2.setText(data.getExtras().getString("mname"));
                            playtrack2(data.getExtras().getString("path"));
                        } catch (IllegalStateException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        break;
                    case 3:
                        try {
                            track3.setText(data.getExtras().getString("mname"));
                            playtrack3(data.getExtras().getString("path"));
                        } catch (IllegalStateException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        break;
                    case 4:
                        try {
                            track4.setText(data.getExtras().getString("mname"));
                            playtrack4(data.getExtras().getString("path"));
                        } catch (IllegalStateException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        break;
                    case 5:
                        try {
                            track5.setText(data.getExtras().getString("mname"));
                            playtrack5(data.getExtras().getString("path"));
                        } catch (IllegalStateException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        break;
                    case 6:
                        try {
                            track6.setText(data.getExtras().getString("mname"));
                            playtrack6(data.getExtras().getString("path"));
                        } catch (IllegalStateException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        break;
                    default:
                        break;
                }
            }
            if (resultCode == RESULT_CANCELED) {
            }
        }
    }

    @SuppressLint("NewApi")
    private void stoploop() {

        if (mp1.isPlaying()) {
            mp1.pause();
            isloop1play = false;
        } else {
            if (ist1setloop) {
                isloop1play = true;
                mp1.start();
            }
        }

        if (mp2.isPlaying()) {
            mp2.pause();
            isloop2play = false;
        } else {
            if (ist2setloop) {
                isloop2play = true;
                mp2.start();
            }
        }
        if (mp3.isPlaying()) {
            mp3.pause();
            isloop3play = false;
        } else {
            if (ist3setloop) {
                isloop3play = true;
                mp3.start();
            }
        }
        if (mp4.isPlaying()) {
            mp4.pause();
            isloop4play = false;
        } else {
            if (ist4setloop) {
                isloop4play = true;
                mp4.start();
            }
        }
        if (mp5.isPlaying()) {
            mp5.pause();
            isloop5play = false;
        } else {
            if (ist5setloop) {
                isloop5play = true;
                mp5.start();
            }
        }
        if (mp6.isPlaying()) {
            mp6.pause();
            isloop6play = false;
        } else {
            if (ist6setloop) {
                isloop6play = true;
                mp6.start();
            }
        }
        Log.e("", "loop " + isloop1play + isloop2play + isloop3play + isloop4play + isloop5play + isloop6play);
        if (isloop1play || isloop2play || isloop3play || isloop4play || isloop5play || isloop6play) {
            loopplay.setImageDrawable(getResources().getDrawable(R.drawable.playloop));
        } else {
            loopplay.setImageDrawable(getResources().getDrawable(R.drawable.pauseloop));
        }

    }

    @SuppressLint("NewApi")
    private void stopsound() {
        if (mptrack1.isPlaying()) {
            mptrack1.pause();
            istrack1play = false;
            // soundplay.setBackground(getResources().getDrawable(R.drawable.pause));
        } else {
            if (ist1setdata) {
                istrack1play = true;
                mptrack1.start();
                // soundplay.setBackground(getResources().getDrawable(R.drawable.play));
            }
        }
        if (mptrack2.isPlaying()) {
            mptrack2.pause();
            istrack2play = false;
            // soundplay.setBackground(getResources().getDrawable(R.drawable.pause));
        } else {
            if (ist2setdata) {
                istrack2play = true;
                mptrack2.start();
                // soundplay.setBackground(getResources().getDrawable(R.drawable.play));
            }
        }
        if (mptrack3.isPlaying()) {
            mptrack3.pause();
            istrack3play = false;
            // soundplay.setBackground(getResources().getDrawable(R.drawable.pause));
        } else {
            if (ist3setdata) {
                istrack3play = true;
                mptrack3.start();
                // soundplay.setBackground(getResources().getDrawable(R.drawable.play));
            }
        }
        if (mptrack4.isPlaying()) {
            mptrack4.pause();
            istrack4play = false;
            // soundplay.setBackground(getResources().getDrawable(R.drawable.pause));
        } else {
            if (ist4setdata) {
                istrack4play = true;
                mptrack4.start();
                // soundplay.setBackground(getResources().getDrawable(R.drawable.play));
            }
        }
        if (mptrack5.isPlaying()) {
            mptrack5.pause();
            istrack5play = false;
            // soundplay.setBackground(getResources().getDrawable(R.drawable.pause));
        } else {
            if (ist5setdata) {
                istrack5play = true;
                mptrack5.start();
                // soundplay.setBackground(getResources().getDrawable(R.drawable.play));
            }
        }
        if (mptrack6.isPlaying()) {
            mptrack6.pause();
            istrack6play = false;
            // soundplay.setBackground(getResources().getDrawable(R.drawable.pause));
        } else {
            if (ist6setdata) {
                istrack6play = true;
                mptrack6.start();
                // soundplay.setBackground(getResources().getDrawable(R.drawable.play));
            }
        }
        if (istrack1play || istrack2play || istrack3play || istrack4play || istrack5play || istrack6play) {
            soundplay.setImageDrawable(getResources().getDrawable(R.drawable.play));
        } else {
            soundplay.setImageDrawable(getResources().getDrawable(R.drawable.pause));
        }

    }

    private void setvolume() {

        if (VolumeStore.ist1mute) {
            mp1.setVolume(0, 0);
        } else {
            mp1.setVolume(vol1, vol1);
        }
        if (VolumeStore.ist2mute) {
            mp2.setVolume(0, 0);
        } else {
            mp2.setVolume(vol2, vol2);
        }
        if (VolumeStore.ist3mute) {
            mp3.setVolume(0, 0);
        } else {
            mp3.setVolume(vol3, vol3);
        }
        if (VolumeStore.ist4mute) {
            mp4.setVolume(0, 0);
        } else {
            mp4.setVolume(vol4, vol4);
        }
        if (VolumeStore.ist5mute) {
            mp5.setVolume(0, 0);
        } else {
            mp5.setVolume(vol5, vol5);
        }
        if (VolumeStore.ist6mute) {
            mp6.setVolume(0, 0);
        } else {
            mp6.setVolume(vol6, vol6);
        }

        if (VolumeStore.ist1mute) {
            mptrack1.setVolume(0, 0);
        } else {
            Log.e("", "mptrack1vol : " + vol1);
            mptrack1.setVolume(vol1, vol1);
        }
        if (VolumeStore.ist2mute) {
            mptrack2.setVolume(0, 0);
        } else {
            mptrack2.setVolume(vol2, vol2);
        }
        if (VolumeStore.ist3mute) {
            mptrack3.setVolume(0, 0);
        } else {
            mptrack3.setVolume(vol3, vol3);
        }
        if (VolumeStore.ist4mute) {
            mptrack4.setVolume(0, 0);
        } else {
            mptrack4.setVolume(vol4, vol4);
        }
        if (VolumeStore.ist5mute) {
            mptrack5.setVolume(0, 0);
        } else {
            mptrack5.setVolume(vol5, vol5);
        }
        if (VolumeStore.ist6mute) {
            mptrack6.setVolume(0, 0);
        } else {
            mptrack6.setVolume(vol6, vol6);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        vol1 = VolumeStore.t1 / 100;
        vol2 = VolumeStore.t2 / 100;
        vol3 = VolumeStore.t3 / 100;
        vol4 = VolumeStore.t4 / 100;
        vol5 = VolumeStore.t5 / 100;
        vol6 = VolumeStore.t6 / 100;

        setvolume();
    }

    @Override
    public void onBackPressed() {

        displayInterstitial();
        showclose();

    }

    private void resetall() {
        mp1.release();
        mp2.release();
        mp3.release();
        mp4.release();
        mp5.release();
        mp6.release();
        mptrack1.release();
        mptrack2.release();
        mptrack3.release();
        mptrack4.release();
        mptrack5.release();
        mptrack6.release();

    }

    private void showclose() {
        new AlertDialog.Builder(this).setTitle("Do you really want to shut down the app?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        resetall();
                        finish();
                        return;
                    }
                }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).show();
    }

    private Context getcontext() {
        return MainScreen.this;
    }

    private void setupVisualizerFxAndUI1() {
        mVisualizerView1 = new VisualizerView(this);

        mVisualizerView1.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.FILL_PARENT));

        mLinearLayout1.addView(mVisualizerView1);

        mVisualizer1 = new Visualizer(mp1.getAudioSessionId());

        mVisualizer1.setCaptureSize(Visualizer.getCaptureSizeRange()[1]);
        mVisualizer1.setDataCaptureListener(new Visualizer.OnDataCaptureListener() {
            public void onWaveFormDataCapture(Visualizer visualizer, byte[] bytes, int samplingRate) {
                mVisualizerView1.updateVisualizer(bytes);
            }

            public void onFftDataCapture(Visualizer visualizer, byte[] bytes, int samplingRate) {

            }
        }, Visualizer.getMaxCaptureRate() / 2, true, false);
    }

    private void setupVisualizerFxAndUI2() {
        mVisualizerView2 = new VisualizerView(this);

        mVisualizerView2.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.FILL_PARENT));

        mLinearLayout2.addView(mVisualizerView2);

        mVisualizer2 = new Visualizer(mp2.getAudioSessionId());

        mVisualizer2.setCaptureSize(Visualizer.getCaptureSizeRange()[1]);
        mVisualizer2.setDataCaptureListener(new Visualizer.OnDataCaptureListener() {
            public void onWaveFormDataCapture(Visualizer visualizer, byte[] bytes, int samplingRate) {
                mVisualizerView2.updateVisualizer(bytes);
            }

            public void onFftDataCapture(Visualizer visualizer, byte[] bytes, int samplingRate) {

            }
        }, Visualizer.getMaxCaptureRate() / 2, true, false);
    }

    private void setupVisualizerFxAndUI3() {
        mVisualizerView3 = new VisualizerView(this);

        mVisualizerView3.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.FILL_PARENT));

        mLinearLayout3.addView(mVisualizerView3);

        mVisualizer3 = new Visualizer(mp3.getAudioSessionId());

        mVisualizer3.setCaptureSize(Visualizer.getCaptureSizeRange()[1]);
        mVisualizer3.setDataCaptureListener(new Visualizer.OnDataCaptureListener() {
            public void onWaveFormDataCapture(Visualizer visualizer, byte[] bytes, int samplingRate) {
                mVisualizerView3.updateVisualizer(bytes);
            }

            public void onFftDataCapture(Visualizer visualizer, byte[] bytes, int samplingRate) {

            }
        }, Visualizer.getMaxCaptureRate() / 2, true, false);
    }

    private void setupVisualizerFxAndUI4() {
        mVisualizerView4 = new VisualizerView(this);

        mVisualizerView4.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.FILL_PARENT));

        mLinearLayout4.addView(mVisualizerView4);

        mVisualizer4 = new Visualizer(mp4.getAudioSessionId());

        mVisualizer4.setCaptureSize(Visualizer.getCaptureSizeRange()[1]);
        mVisualizer4.setDataCaptureListener(new Visualizer.OnDataCaptureListener() {
            public void onWaveFormDataCapture(Visualizer visualizer, byte[] bytes, int samplingRate) {
                mVisualizerView4.updateVisualizer(bytes);
            }

            public void onFftDataCapture(Visualizer visualizer, byte[] bytes, int samplingRate) {

            }
        }, Visualizer.getMaxCaptureRate() / 2, true, false);
    }

    private void setupVisualizerFxAndUI5() {
        mVisualizerView5 = new VisualizerView(this);

        mVisualizerView5.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.FILL_PARENT));

        mLinearLayout5.addView(mVisualizerView5);

        mVisualizer5 = new Visualizer(mp5.getAudioSessionId());

        mVisualizer5.setCaptureSize(Visualizer.getCaptureSizeRange()[1]);
        mVisualizer5.setDataCaptureListener(new Visualizer.OnDataCaptureListener() {
            public void onWaveFormDataCapture(Visualizer visualizer, byte[] bytes, int samplingRate) {
                mVisualizerView5.updateVisualizer(bytes);
            }

            public void onFftDataCapture(Visualizer visualizer, byte[] bytes, int samplingRate) {

            }
        }, Visualizer.getMaxCaptureRate() / 2, true, false);
    }

    private void setupVisualizerFxAndUI6() {
        mVisualizerView6 = new VisualizerView(this);

        mVisualizerView6.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.FILL_PARENT));

        mLinearLayout6.addView(mVisualizerView6);

        mVisualizer6 = new Visualizer(mp6.getAudioSessionId());

        mVisualizer6.setCaptureSize(Visualizer.getCaptureSizeRange()[1]);
        mVisualizer6.setDataCaptureListener(new Visualizer.OnDataCaptureListener() {
            public void onWaveFormDataCapture(Visualizer visualizer, byte[] bytes, int samplingRate) {
                mVisualizerView6.updateVisualizer(bytes);
            }

            public void onFftDataCapture(Visualizer visualizer, byte[] bytes, int samplingRate) {

            }
        }, Visualizer.getMaxCaptureRate() / 2, true, false);
    }

    class VisualizerView extends View {
        private byte[] mBytes;
        private float[] mPoints;
        private Rect mRect = new Rect();

        private Paint mForePaint = new Paint();

        public VisualizerView(Context context) {
            super(context);
            init();
        }

        private void init() {
            mBytes = null;

            mForePaint.setStrokeWidth(4f);
            mForePaint.setAntiAlias(true);
            mForePaint.setColor(Color.rgb(252, 193, 91));
        }

        public void updateVisualizer(byte[] bytes) {
            mBytes = bytes;
            // Log.e("bytes","by: " + bytes);
            invalidate();
        }

        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);

            if (mBytes == null) {
                return;
            }

            if (mPoints == null || mPoints.length < mBytes.length * 4) {
                mPoints = new float[mBytes.length * 4];
            }

            mRect.set(0, 0, getWidth(), getHeight());

            for (int i = 0; i < mBytes.length - 1; i++) {
                mPoints[i * 4] = mRect.width() * i / (mBytes.length - 1);
                mPoints[i * 4 + 1] = mRect.height() / 2 + ((byte) (mBytes[i] + 128)) * (mRect.height() / 2) / 128;
                mPoints[i * 4 + 2] = mRect.width() * (i + 1) / (mBytes.length - 1);
                mPoints[i * 4 + 3] = mRect.height() / 2 + ((byte) (mBytes[i + 1] + 128)) * (mRect.height() / 2) / 128;
            }

            float centerX = mRect.width();
            float centerY = mRect.height();
            double angle = 90;
            Matrix rotateMat = new Matrix();
            rotateMat.setRotate((float) angle, centerX, centerY);
            // rotateMat.mapPoints(mPoints);

            canvas.drawLines(mPoints, mForePaint);

        }
    }

}

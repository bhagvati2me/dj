package com.multidj.multitrackdjelectro;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.multidj.multitrackdjelectro.R;

public class MusicAdapter extends BaseAdapter {
	public LayoutInflater inflater;
	public List<Musicmodel> list;
	public Context con;

	public MusicAdapter(Activity context, int playlistitem, ArrayList<Musicmodel> musiclist) {

		con = context;
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		list = musiclist;
	}

	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	public Musicmodel getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public class ViewHolder {
		TextView music_title;

	}

	public View getView(int position, View convertview, ViewGroup parent) {
		// TODO Auto-generated method stub

		ViewHolder holder;
		if (convertview == null) {
			holder = new ViewHolder();
			convertview = inflater.inflate(R.layout.playlistitem, null);

			holder.music_title = (TextView) convertview.findViewById(R.id.musicname);

			convertview.setTag(holder);
		}

		holder = (ViewHolder) convertview.getTag();

		holder.music_title.setText(list.get(position).name);

		return convertview;
	}

}

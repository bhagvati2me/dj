package com.djdeve.megavirtualmixer;

/***********************************************************
 * Copyright 2012. All Rights Reserved
 * CreatioSoft Solutions Pvt. Ltd.
 * Noida -201301
 * India.
 *
 * Project Name :  Fruit ball roll and fall mad fun
 * Group : N/A
 * Security : Confidential
 ***********************************************************/

/**********************************************************
 * Filename : SplashScreenActivity
 * Purpose : It displays the a splash screen for 3500 ms and then control passes
 * to MainMenuActivity
 * Platform : Android 4.0
 * Author(s) : Rashmi Srivastava
 * E-mail id. : rashmi@creatiosoft.in
 * Creation date : <Sep 09, 2012>

 **********************************************************/

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Window;
import com.djdeve.megavirtualmixer.R;

public class Splass_screenActivity extends Activity {
	int flag = 0;
	boolean no;
	ProgressDialog pd;
	// MediaPlayer mp;
	public static Integer current = 0;
	public static Integer id1[] = new Integer[50];
	public static Integer id2[] = new Integer[50];
	public static int position = 0;
	protected boolean _active = true;
	protected int _splashTime = 500;

	boolean _sound;
	Thread splashTread;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.splash_scr);
		// preferences=new Preferences(getApplicationContext());
		// mp=MediaPlayer.create(this, R.raw.mainscreen);

		// _sound=preferences.getSoundThisAppPref();

		splashTread = new Thread() {
			@Override
			public void run() {
				try {

					int waited = 0;
					while (_active && (waited < _splashTime)) {
						// if(!_sound)
						// mp.start();
						sleep(1000);

						if (_active) {
							waited += 1000;
						}
					}
				} catch (InterruptedException e) {
					// do nothing
				} finally {

					finish();
					if (_active) {
						Intent info = new Intent(Splass_screenActivity.this,
								PlayActivity.class);

						startActivity(info);
					}

				}
			}
		};

		splashTread.start();

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub

		if (keyCode == KeyEvent.KEYCODE_BACK) {

			this.finish();
			_active = false;
			return true;

		}

		return super.onKeyDown(keyCode, event);
	}

}

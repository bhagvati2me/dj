package com.djdeve.megavirtualmixer;

/********************************************************* 
* Copyright 2012. All Rights Reserved
* CreatioSoft Solutions  Pvt. Ltd.
* Noida -201301
* India.
*
* Project Name          : Video Editor Application
* Group                 : N/A
* Security              : Confidential
*********************************************************/

/********************************************************
* Filename            : AudioCursorAdapter
* Purpose             : This is used to show audio list
* Platform            : Android 4.1
* Author(s)           : Jagdeep Singh
* E-mail id.          : jagdeep@creatiosoft.com
* Creation date       : <september 10, 2013> 

********************************************************/


import utils.TimeUtils;
import android.content.Context;
import android.database.Cursor;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ResourceCursorAdapter;
import android.widget.TextView;
import com.djdeve.megavirtualmixer.R;


public final class AudioCursorAdapter extends ResourceCursorAdapter {
	AudioCursorAdapter(PlayActivity playActivity, Context context,
			int layout, Cursor c) {
		super(context, layout, c);

	}

	@Override
	public void bindView(View view, Context context, Cursor cursor) {
//		ImageView videoPreview = (ImageView) view
//				.findViewById(R.id.image_preview);
//		Bitmap thumbnail = Thumbnails.getThumbnail(
//				context.getContentResolver(),
//				getInt(cursor, MediaStore.Video.Media._ID),
//				MediaStore.Video.Thumbnails.MICRO_KIND,
//				new BitmapFactory.Options());
//		videoPreview.setImageBitmap(thumbnail);

		String fileName = getString(cursor, MediaStore.Audio.Media.DISPLAY_NAME);
//		Log.i("VideoCursorAdapter", "Binding view for : " + fileName);
if(fileName.contains(".mp3") || fileName.contains(".wav"))
{
	

		TextView fileNameView = (TextView) view.findViewById(R.id.file_name);
		fileNameView.setText(fileName);

		TextView durationView = (TextView) view.findViewById(R.id.duration);
		durationView.setText(getTime(cursor, MediaStore.Audio.Media.DURATION));

		TextView addedOnView = (TextView) view.findViewById(R.id.added_date);
		addedOnView
				.setText(getString(cursor, MediaStore.Audio.Media.DATE_ADDED));
	}
	}
	private int getInt(Cursor cursor, String columnName) {
		int index = cursor.getColumnIndexOrThrow(columnName);
		return cursor.getInt(index);
	}

	private String getString(Cursor cursor, String columnName) {
		int index = cursor.getColumnIndexOrThrow(columnName);
		return cursor.getString(index);
	}

	private String getTime(Cursor cursor, String columnName) {
		int time = getInt(cursor, columnName);
		return TimeUtils.toFormattedTime(time);
	}
}
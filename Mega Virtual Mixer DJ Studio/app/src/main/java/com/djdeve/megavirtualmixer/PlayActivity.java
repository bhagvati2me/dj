package com.djdeve.megavirtualmixer;

/*Date: 04/03/2012
 * Author:Jordan Brobyn
 * Description: Used to play sounds predefined by the user
 * Multiple boards and options like loop and repeat allow for variations of playback
 * Drumkit is currently commented out due to issues with reading from compressed memory
 *
 * Complete: Start working on the application service for filebuilder etc.
 */

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.AnimationDrawable;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaRecorder;
import android.media.audiofx.BassBoost;
import android.media.audiofx.Equalizer;
import android.media.audiofx.Visualizer;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Messenger;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.djdeve.megavirtualmixer.ClipEvent.EventType;
import com.flurry.android.FlurryAgent;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.twobard.pianoview.Piano;
import com.twobard.pianoview.Piano.PianoKeyListener;

import org.apache.http.util.ByteArrayBuffer;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javazoom.jl.decoder.Bitstream;
import javazoom.jl.decoder.BitstreamException;
import javazoom.jl.decoder.Header;

//import com.startapp.android.publish.Ad;
//import com.startapp.android.publish.AdEventListener;
//import com.startapp.android.publish.StartAppAd;

public class PlayActivity extends Activity implements OnClickListener,
        OnCompletionListener {

    /********** variable initialisation ****/
    Cursor videocursor;
    private BassBoost booster1, booster2;

    TextView txt1, txt2, txt3, txt4, txt5, txt6, txt7, txt8, txt9, txt10,
            txt11, txt12, txt13, txt14, txt15;
    String[] effectName = {"sound1", "sound2", "sound3",
            "sound4", "sound5", "sound6", "sound7",
            "sound8", "sound9", "sound10", "sound11",
            "sound12", "sound13", "sound14", "sound15"};
    String[] ButtonId = {"Button10", "Button11", "Button12", "Button13",
            "Button14", "Button15", "Button16", "Button17", "Button18",
            "Button19", "Button20", "Button21", "Button22", "Button23",
            "Button24"};
    int[] soundEffectId = {R.raw.sound1, R.raw.sound2, R.raw.sound3,
            R.raw.sound4, R.raw.sound5, R.raw.sound6, R.raw.sound7,
            R.raw.sound8, R.raw.sound9, R.raw.sound10, R.raw.sound11,
            R.raw.sound12, R.raw.sound13, R.raw.sound14, R.raw.sound15};

    int[] effectImageViewId = {R.id.ImageView10, R.id.ImageView11,
            R.id.ImageView12, R.id.ImageView13, R.id.ImageView14,
            R.id.ImageView15, R.id.ImageView16, R.id.ImageView17,
            R.id.ImageView18, R.id.ImageView19, R.id.ImageView20,
            R.id.ImageView21, R.id.ImageView22, R.id.ImageView23,
            R.id.ImageView24};
    int[] effectrepeatImageViewId = {R.id.repeatImageView10,
            R.id.repeatImageView11, R.id.repeatImageView12,
            R.id.repeatImageView13, R.id.repeatImageView14,
            R.id.repeatImageView15, R.id.repeatImageView16,
            R.id.repeatImageView17, R.id.repeatImageView18,
            R.id.repeatImageView19, R.id.repeatImageView20,
            R.id.repeatImageView21, R.id.repeatImageView22,
            R.id.repeatImageView23, R.id.repeatImageView24};

    public ArrayList<String> filenamelist = new ArrayList<String>();
    public ArrayList<String> filenamelist2 = new ArrayList<String>();
    boolean loaded = false;
    ArrayList<SoundInfo> soundButtons = new ArrayList<SoundInfo>();
    SoundDataSource soundDB;
    Map<String, SoundInfo> hash;
    long startTime;
    long endTime;
    int recent_Session;
    String id;
    boolean recording = false;
    ArrayList<ClipEvent> eventLogs = new ArrayList<ClipEvent>();
    Dialog dialog;
    int loop = 0;
    int volume = 50;
    int volume2;
    int count = 0;
    SeekBar mSeekBar, firstprogressSeekbar, secprogressSeekbar;
    boolean pauser = true;
    boolean looper = false;
    boolean repeater = false;
    LinearLayout llequilizer1, llequilizer2, llBooster1, llBooster2;
    private Equalizer mEqualizer1, mEqualizer2;
    private Handler mHandler = new Handler();
    ;
    private Handler mHandlersec = new Handler();
    ;
    private ProgressDialog mProgressDialog;
    ImageView addSoundImageViewFirst, addSoundImageViewSec;

    ImageView centerBtn1, centerBtn2;
    ImageView playImageViewFirst, playImageViewSec;
    ImageView recordImageView, effectImageView;
    ImageView animatedDisk1, animatedDisk2;
    WindowManager mWinMg;
    public int width;
    public int height;
    private Handler mUIHandler;

    private Utilities utils;
    String root = Environment.getExternalStorageDirectory().toString();
    File myDir = new File(root + "/effect_sound");
    File soundsaveDir = new File(root + "/Custom SoundClips/");

    Animation rotateAnimationfirst;
    //private StartAppAd startAppAd;
    boolean adshow = true;
    ImageView chnageview1, chnageview2;

    View v1, v2;

    Piano piano;
    ImageView rht_bt, rht_prev, rht_next, metro_bt, metro_prev, metro_next,
            sound_bt, sound_prev, sound_next;
    ImageView s1, s2, s3, s4, s5, s6, s7, s8;
    ImageView t1, t2, t3, t4;
    TextView rht_txt, metro_txt, sound_txt;
    MediaPlayer rhtmp, metromp, soundmp;
    MediaPlayer keymp1, keymp2, keymp3, keymp4, keymp5, keymp6, keymp7, keymp8,
            keymp9, keymp10, keymp11, keymp12, keymp13, keymp14, keymp15,
            keymp16, keymp17, keymp0;

    MediaPlayer padmp1, padmp2, padmp3, padmp4, padmp5, padmp6, padmp7, padmp8;
    View theme;
    SeekBar rht_seekbar, sound_seekbar;
    private static String audioFilePath;
    MediaRecorder mediaRecorder;
    boolean isRecording = false;

    // ImageView start, pause, menu;

    private InterstitialAd interstitial1, interstitial2, interstitial3, interstitial4, interstitial5;
    AdRequest adRequest1, adRequest2, adRequest3, adRequest4, adRequest5;


    private String[] permissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO};

    @RequiresApi(api = Build.VERSION_CODES.M)
    private boolean arePermissionsEnabled() {
        for (String permission : permissions) {
            if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED)
                return false;
        }
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void requestMultiplePermissions() {
        List<String> remainingPermissions = new ArrayList<>();
        for (String permission : permissions) {
            if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                remainingPermissions.add(permission);
            }
        }
        requestPermissions(remainingPermissions.toArray(new String[remainingPermissions.size()]), 101);
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 101) {
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                    if (shouldShowRequestPermissionRationale(permissions[i])) {
                        new AlertDialog.Builder(this)
                                .setMessage("Please allow permissions to use record audio and choose local music from device")
                                .setPositiveButton("Allow", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        requestMultiplePermissions();
                                    }
                                })
                                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        requestMultiplePermissions();
                                    }
                                })
                                .create()
                                .show();
                    }
                    return;
                }
            }
            setupPlaylist();

//====


        }
    }


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.newlayout);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!arePermissionsEnabled()) {
                requestMultiplePermissions();
            }
        }

        interstitial1 = new InterstitialAd(getcontext());
        interstitial1.setAdUnitId("ca-app-pub-3805784166574868/4653394523");
        adRequest1 = new AdRequest.Builder().build();
        interstitial1.loadAd(adRequest1);

        interstitial2 = new InterstitialAd(getcontext());
        interstitial2.setAdUnitId("ca-app-pub-3805784166574868/4653394523");
        adRequest2 = new AdRequest.Builder().build();
        interstitial2.loadAd(adRequest2);

        interstitial3 = new InterstitialAd(getcontext());
        interstitial3.setAdUnitId("ca-app-pub-3805784166574868/4653394523");
        adRequest3 = new AdRequest.Builder().build();
        interstitial3.loadAd(adRequest3);

        interstitial4 = new InterstitialAd(getcontext());
        interstitial4.setAdUnitId("ca-app-pub-3805784166574868/4653394523");
        adRequest4 = new AdRequest.Builder().build();
        interstitial4.loadAd(adRequest4);

        AdRequest adRequest = new AdRequest.Builder().build();
        interstitial5 = new InterstitialAd(PlayActivity.this);
        interstitial5.setAdUnitId("ca-app-pub-3805784166574868/4653394523");
        adRequest5 = new AdRequest.Builder().build();
        interstitial5.loadAd(adRequest5);

        interstitial5.setAdListener(new AdListener() {
            public void onAdLoaded() {
                displayInterstitial5();
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
            }

        });

        v1 = (View) findViewById(R.id.pianolayout);
        v2 = (View) findViewById(R.id.root);
        chnageview1 = (ImageView) findViewById(R.id.changeview1);
        chnageview2 = (ImageView) findViewById(R.id.changeview2);
        waves1 = (LinearLayout) findViewById(R.id.wave1);
        waves2 = (LinearLayout) findViewById(R.id.wave2);

        mpdisk1 = new MediaPlayer();
        mpdisk2 = new MediaPlayer();

        chnageview1.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                v2.setVisibility(View.GONE);
                chnageview1.setVisibility(View.GONE);
                chnageview2.setVisibility(View.VISIBLE);

                v1.setVisibility(View.VISIBLE);

            }
        });

        chnageview2.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                v1.setVisibility(View.GONE);
                chnageview2.setVisibility(View.GONE);
                chnageview1.setVisibility(View.VISIBLE);
                v2.setVisibility(View.VISIBLE);
            }
        });

        /******** ads code start *******/
//		StartAppAd.init(this, "110824676", "200196871");
//
//		startAppAd = new StartAppAd(this);
//
//		startAppAd.loadAd(new AdEventListener() {
//			@Override
//			public void onReceiveAd(Ad ad) {
//				if (adshow)
//					startAppAd.showAd(); // show the ad
//
//			}
//
//			@Override
//			public void onFailedToReceiveAd(Ad ad) {
//			}
//		});
        /******** ads code eds *******/

        /******** check directory exist or not *******/

        /******** check directory exist or not *******/

        if (!myDir.exists())
            myDir.mkdirs();
        if (!soundsaveDir.exists())
            soundsaveDir.mkdirs();
        mUIHandler = new Handler();
        String state = Environment.getExternalStorageState();

        if (!Environment.MEDIA_MOUNTED.equals(state)) {
            mUIHandler.post(mSDCardErrorRunnable);
            return;

        }
        /******** save effect sound *******/

        saveEffectSound();

        mWinMg = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        height = mWinMg.getDefaultDisplay().getHeight();
        width = mWinMg.getDefaultDisplay().getWidth();

        utils = new Utilities();
        /******** ids of view *******/


        centerBtn1 = (ImageView) findViewById(R.id.centerBtn1);
        centerBtn2 = (ImageView) findViewById(R.id.centerBtn2);

        addSoundImageViewFirst = (ImageView) findViewById(R.id.ImageView04);
        addSoundImageViewSec = (ImageView) findViewById(R.id.ImageView05);
        playImageViewFirst = (ImageView) findViewById(R.id.ImageView01);
        playImageViewSec = (ImageView) findViewById(R.id.ImageView02);
        recordImageView = (ImageView) findViewById(R.id.recordsoundBtn);
        effectImageView = (ImageView) findViewById(R.id.imageView06);
        animatedDisk1 = (ImageView) findViewById(R.id.animBtn1);
        animatedDisk2 = (ImageView) findViewById(R.id.animBtn2);

        animatedDisk1.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                mpdisk1.reset();
                leftdjst1("Scratch1.ogg");
            }
        });
        animatedDisk2.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                mpdisk2.reset();
                leftdjst2("Scratch2.ogg");

            }
        });
//		centerBtn1.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				mpdisk1.reset();
//				leftdjst1("Scratch1.ogg");
//			}
//		});
//		centerBtn2.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				mpdisk2.reset();
//				leftdjst2("Scratch2.ogg");
//
//			}
//		});

        rotateAnimationfirst = AnimationUtils.loadAnimation(this,
                R.anim.translation);

        llequilizer1 = (LinearLayout) findViewById(R.id.EquilizerLyt);
        llequilizer2 = (LinearLayout) findViewById(R.id.EquilizerLyt2);
        mSeekBar = (SeekBar) findViewById(R.id.volumeBar);
        firstprogressSeekbar = (SeekBar) findViewById(R.id.firstprogressSeekbar);
        secprogressSeekbar = (SeekBar) findViewById(R.id.secondprogressSeekbar);
        addSoundImageViewFirst
                .setBackgroundResource(R.drawable.animation_frame);
        addSoundImageViewSec.setBackgroundResource(R.drawable.animation_frame);

        /******** create object of database *******/

        soundDB = new SoundDataSource(getApplicationContext());
        soundDB.open(); // Open db to grab data
        soundDB.deleteAll();// delte data
        editPreferences("", "Button1");
        editPreferences("", "Button2");
        // populateBoard(); //Display board with active keys and populate
        // hashmap with ids and mediaplayers

        /******** animation on song add button *******/

        AnimationDrawable frameAnimation = (AnimationDrawable) addSoundImageViewFirst
                .getBackground();

        if (frameAnimation.isRunning()) {
            frameAnimation.stop();
        } else {

            frameAnimation.start();
        }

        AnimationDrawable frameAnimation2 = (AnimationDrawable) addSoundImageViewSec
                .getBackground();

        if (frameAnimation2.isRunning()) {
            frameAnimation2.stop();
        } else {

            frameAnimation2.start();
        }
        /******** setup of layouts *******/

        equilizerSetup();
        setupPlaylist();
        setupSongSeekbar();
        setupEffectSoundLyt();

        recordImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayInterstitial3();
                // TODO Auto-generated method stub
                flurryBtnAppCount("recordBtn_clicked");
                if (recording == false) {
                    Toast.makeText(PlayActivity.this, "Start Mixing!",
                            Toast.LENGTH_SHORT).show();
                    eventLogs = new ArrayList<ClipEvent>();
                    stopAll(false);
                    recording = true;
                    startTime = TimeUnit.NANOSECONDS.toMillis(System.nanoTime());
                    recordImageView
                            .setBackgroundResource(R.drawable.microphone_pressed);

                } else {
                    recordImageView
                            .setBackgroundResource(R.drawable.microphone);

                    Toast.makeText(PlayActivity.this, "Recording Stopped!",
                            Toast.LENGTH_SHORT).show();
                    stopAll(false);
                    recording = false;
                    endTime = TimeUnit.NANOSECONDS.toMillis(System.nanoTime());
                    dialog = new Dialog(PlayActivity.this);
                    dialog.setContentView(R.layout.replay);
                    dialog.setTitle("Recorder");
                    dialog.setCancelable(true);
                    dialog.show();

                    View view = dialog.findViewById(R.id.builderView);
                    Button save = (Button) view.findViewById(R.id.replaySave);
                    save.setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            EditText text = (EditText) dialog
                                    .findViewById(R.id.recorderFileName);
                            String fileName = text.getText().toString();

                            if (eventLogs.size() <= 0) {
                                Toast.makeText(
                                        getcontext(),
                                        "ERROR: No songs were selected to mix!",
                                        Toast.LENGTH_LONG).show();
                                return;
                            }

                            int sampleRate = 44100;
                            int channels = 2;

                            CheapMP3 mp3 = new CheapMP3();
                            CheapWAV wav = new CheapWAV();

                            // Free version can only mix WAV Files, while full
                            // version can mix mp3
                            // as well.
                            for (ClipEvent event : eventLogs) {
                                /*
                                 * if(!event.fileLocation.endsWith(".wav")){
                                 * Toast.makeText(this,
                                 * "OOPS! \n Audio Mashup (Free) can only mix WAV files"
                                 * , Toast.LENGTH_LONG).show();
                                 * Toast.makeText(this,
                                 * "Support the developer.\n Buy the Full Version to mix mp3 files!"
                                 * , Toast.LENGTH_LONG).show(); return; }
                                 */
                                // Make sure the sample rate and channels are
                                // supported
                                try {
                                    if (event.fileLocation.endsWith(".wav")) {
                                        wav = new CheapWAV();
                                        wav.ReadFile(new File(
                                                event.fileLocation));
                                        if (wav.getSampleRate() != sampleRate
                                                && wav.getChannels() != channels) {
                                            Toast.makeText(
                                                    getcontext(),
                                                    "The 'SampleRate' or 'Channel' is not Supported",
                                                    Toast.LENGTH_LONG).show();
                                            Toast.makeText(
                                                    getcontext(),
                                                    "SampleRate = 44100, Channel = 2",
                                                    Toast.LENGTH_LONG).show();
                                            return;
                                        }
                                    }
                                    if (event.fileLocation.endsWith(".mp3")) {
                                        InputStream mp3Stream = new FileInputStream(
                                                event.fileLocation);
                                        Bitstream stream = new Bitstream(
                                                mp3Stream);
                                        Header header = stream.readFrame();
                                        if (header.frequency() != sampleRate) {

                                            Toast.makeText(
                                                    getcontext(),
                                                    "The 'SampleRate' or 'Channel' is not Supported",
                                                    Toast.LENGTH_LONG).show();
                                            Toast.makeText(
                                                    getcontext(),
                                                    "SampleRate = 44100, Channel = 2",
                                                    Toast.LENGTH_LONG).show();
                                            return;
                                        }
                                        stream.closeFrame();
                                        stream.close();
                                    }

                                } catch (IOException e) {
                                    e.printStackTrace();
                                } catch (BitstreamException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }
                            }

                            dialog.dismiss();
                            Intent intent = new Intent(getcontext(),
                                    FileBuilderService.class);
                            Messenger messenger = new Messenger(mHandler);
                            Bundle data = new Bundle();
                            data.putSerializable("DATA", eventLogs);
                            intent.putExtra("DURATION", (endTime - startTime));
                            intent.putExtra("FILENAME", fileName + ".wav");
                            intent.putExtra("MESSENGER", messenger);
                            intent.putExtra("DATA", data);
                            startService(intent);

                        }
                    });
                    long duration = endTime - startTime;
                    ReplayTracks builder = new ReplayTracks();
                    builder.setup(view, eventLogs, duration, hash, dialog);

                }

            }
        });

        playImageViewFirst.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                flurryBtnAppCount("playBtn_clicked");
                playSound((ImageView) v);
                repeater = false;

            }
        });

        playImageViewSec.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                flurryBtnAppCount("playBtn_clicked");
                playSound((ImageView) v);
                repeater = false;

            }
        });

        effectImageView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                flurryBtnAppCount("soundEffectBtn_clicked");
                RelativeLayout Rlyt = (RelativeLayout) findViewById(R.id.soundeffect);
                if (Rlyt.isShown()) {
                    Rlyt.setVisibility(View.GONE);
                    effectImageView
                            .setBackgroundResource(R.drawable.more_sounds);
                } else {
                    Rlyt.setVisibility(View.VISIBLE);
                    effectImageView
                            .setBackgroundResource(R.drawable.more_sounds_pressed);

                }

            }
        });
        rhtmp = new MediaPlayer();
        metromp = new MediaPlayer();
        padmp1 = new MediaPlayer();
        padmp2 = new MediaPlayer();
        padmp3 = new MediaPlayer();
        padmp4 = new MediaPlayer();
        padmp5 = new MediaPlayer();
        padmp6 = new MediaPlayer();
        padmp7 = new MediaPlayer();
        padmp8 = new MediaPlayer();

        soundmp = new MediaPlayer();
        keymp1 = new MediaPlayer();
        keymp2 = new MediaPlayer();
        keymp3 = new MediaPlayer();
        keymp4 = new MediaPlayer();
        keymp5 = new MediaPlayer();
        keymp6 = new MediaPlayer();
        keymp7 = new MediaPlayer();
        keymp8 = new MediaPlayer();
        keymp9 = new MediaPlayer();
        keymp10 = new MediaPlayer();
        keymp11 = new MediaPlayer();
        keymp12 = new MediaPlayer();
        keymp13 = new MediaPlayer();
        keymp14 = new MediaPlayer();
        keymp15 = new MediaPlayer();
        keymp16 = new MediaPlayer();
        keymp17 = new MediaPlayer();

        keymp0 = new MediaPlayer();
        rht_bt = (ImageView) findViewById(R.id.rht_bt);
        rht_prev = (ImageView) findViewById(R.id.rht_prev);
        rht_next = (ImageView) findViewById(R.id.rht_next);
        rht_txt = (TextView) findViewById(R.id.rht_txt);

        metro_bt = (ImageView) findViewById(R.id.metro_bt);
        metro_prev = (ImageView) findViewById(R.id.metro_prev);
        metro_next = (ImageView) findViewById(R.id.metro_next);
        metro_txt = (TextView) findViewById(R.id.metro_txt);

        sound_bt = (ImageView) findViewById(R.id.sound_bt);
        sound_prev = (ImageView) findViewById(R.id.sound_prev);
        sound_next = (ImageView) findViewById(R.id.sound_next);
        sound_txt = (TextView) findViewById(R.id.sound_txt);

        sound_txt.setText(getsound(1));

        s1 = (ImageView) findViewById(R.id.s1);
        s2 = (ImageView) findViewById(R.id.s2);
        s3 = (ImageView) findViewById(R.id.s3);
        s4 = (ImageView) findViewById(R.id.s4);
        s5 = (ImageView) findViewById(R.id.s5);
        s6 = (ImageView) findViewById(R.id.s6);
        s7 = (ImageView) findViewById(R.id.s7);
        s8 = (ImageView) findViewById(R.id.s8);

        t1 = (ImageView) findViewById(R.id.t1);
        t2 = (ImageView) findViewById(R.id.t2);
        t3 = (ImageView) findViewById(R.id.t3);
        t4 = (ImageView) findViewById(R.id.t4);

        theme = (View) findViewById(R.id.topbar);

        // menu.setOnClickListener(this);
        rht_bt.setOnClickListener(this);
        rht_prev.setOnClickListener(this);
        rht_next.setOnClickListener(this);

        metro_bt.setOnClickListener(this);
        metro_prev.setOnClickListener(this);
        metro_next.setOnClickListener(this);

        s1.setOnClickListener(this);
        s2.setOnClickListener(this);
        s3.setOnClickListener(this);
        s4.setOnClickListener(this);
        s5.setOnClickListener(this);
        s6.setOnClickListener(this);
        s7.setOnClickListener(this);
        s8.setOnClickListener(this);

        sound_bt.setOnClickListener(this);
        sound_prev.setOnClickListener(this);
        sound_next.setOnClickListener(this);

        t1.setOnClickListener(this);
        t2.setOnClickListener(this);
        t3.setOnClickListener(this);
        t4.setOnClickListener(this);

        rht_seekbar = (SeekBar) findViewById(R.id.rht_seekbar);
        sound_seekbar = (SeekBar) findViewById(R.id.sound_seekbar);

        rht_seekbar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                float soundvol = (progress) / 100.0f;
                rhtmp.setVolume(soundvol, soundvol);
            }
        });

        sound_seekbar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {

                float soundvol = (progress) / 100.0f;
                keymp1.setVolume(soundvol, soundvol);
                keymp2.setVolume(soundvol, soundvol);
                keymp3.setVolume(soundvol, soundvol);
                keymp4.setVolume(soundvol, soundvol);
                keymp5.setVolume(soundvol, soundvol);
                keymp6.setVolume(soundvol, soundvol);
                keymp7.setVolume(soundvol, soundvol);
                keymp8.setVolume(soundvol, soundvol);
                keymp9.setVolume(soundvol, soundvol);
                keymp10.setVolume(soundvol, soundvol);
                keymp11.setVolume(soundvol, soundvol);
                keymp12.setVolume(soundvol, soundvol);
                keymp13.setVolume(soundvol, soundvol);
                keymp14.setVolume(soundvol, soundvol);
                keymp15.setVolume(soundvol, soundvol);
                keymp16.setVolume(soundvol, soundvol);
                keymp17.setVolume(soundvol, soundvol);

                keymp0.setVolume(soundvol, soundvol);

            }
        });

        piano = (Piano) findViewById(R.id.pi);
        piano.setPianoKeyListener(new PianoKeyListener() {
            @Override
            public void keyPressed(int id, int action) {

                if (action == MotionEvent.ACTION_DOWN) {
                    Log.e("pino", "action:" + action + " id: " + id);

                    if (Integer.valueOf(MusicLoopClass.mpsound) == 1) {
                        play((getsound(Integer.valueOf(MusicLoopClass.mpsound))
                                + id + ".ogg"), id);

                    } else {
                        play((getsound(Integer.valueOf(MusicLoopClass.mpsound))
                                + id + ".ogg"), id);

                    }

                }
            }
        });

    }

    MediaPlayer mpdisk1, mpdisk2;

    private void leftdjst1(String path) {
        try {
            AssetFileDescriptor descriptor = getAssets()
                    .openFd("music/" + path);
            mpdisk1.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mpdisk1.prepare();
            mpdisk1.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mpdisk1.start();

                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void leftdjst2(String path) {
        try {
            AssetFileDescriptor descriptor = getAssets()
                    .openFd("music/" + path);
            mpdisk2.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mpdisk2.prepare();
            mpdisk2.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mpdisk2.start();

                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private Context getcontext() {
        // TODO Auto-generated method stub
        return PlayActivity.this;
    }

    private void play(String sd, int id) {

        switch (id) {
            case 0:
                playkeyboard0(sd);
                break;
            case 1:
                playkeyboard1(sd);
                break;
            case 2:
                playkeyboard2(sd);
                break;
            case 3:
                playkeyboard3(sd);
                break;
            case 4:
                playkeyboard4(sd);
                break;
            case 5:
                playkeyboard5(sd);
                break;
            case 6:
                playkeyboard6(sd);
                break;
            case 7:
                playkeyboard7(sd);
                break;
            case 8:
                playkeyboard8(sd);
                break;
            case 9:
                playkeyboard9(sd);
                break;
            case 10:
                playkeyboard10(sd);
                break;
            case 11:
                playkeyboard11(sd);
                break;
            case 12:
                playkeyboard12(sd);
                break;
            case 13:
                playkeyboard13(sd);
                break;
            case 14:
                playkeyboard14(sd);
                break;
            case 15:
                playkeyboard15(sd);
                break;
            case 16:
                playkeyboard16(sd);
                break;
            case 17:
                playkeyboard17(sd);
                break;

            default:
                break;
        }

    }

    boolean ist1setdata = false, istrack1play = false;

    private void playrht(String musicname) {
        try {
            ist1setdata = true;
            AssetFileDescriptor descriptor = getAssets().openFd(
                    "music/" + musicname);
            rhtmp.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            rhtmp.prepare();
            rhtmp.setLooping(true);
            rhtmp.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    rhtmp.start();
                    istrack1play = true;
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    boolean issounddata = false, issoundplay = false;

    private void playsound(String musicname) {
        try {
            issounddata = true;
            AssetFileDescriptor descriptor = getAssets().openFd(
                    "music/" + musicname);
            soundmp.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            soundmp.prepare();
            soundmp.setLooping(true);
            soundmp.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    soundmp.start();
                    issoundplay = true;
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playpad1(String musicname) {
        try {

            padmp1.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(
                    "music/" + musicname);
            padmp1.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            padmp1.prepare();
            padmp1.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    padmp1.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playpad2(String musicname) {
        try {

            padmp2.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(
                    "music/" + musicname);
            padmp2.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            padmp2.prepare();
            padmp2.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    padmp2.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playpad3(String musicname) {
        try {

            padmp3.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(
                    "music/" + musicname);
            padmp3.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            padmp3.prepare();
            padmp3.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    padmp3.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playpad4(String musicname) {
        try {

            padmp4.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(
                    "music/" + musicname);
            padmp4.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            padmp4.prepare();
            padmp4.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    padmp4.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playpad5(String musicname) {
        try {

            padmp5.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(
                    "music/" + musicname);
            padmp5.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            padmp5.prepare();
            padmp5.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    padmp5.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playpad6(String musicname) {
        try {

            padmp6.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(
                    "music/" + musicname);
            padmp6.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            padmp6.prepare();
            padmp6.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    padmp6.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playpad7(String musicname) {
        try {

            padmp7.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(
                    "music/" + musicname);
            padmp7.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            padmp7.prepare();
            padmp7.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    padmp7.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playpad8(String musicname) {
        try {

            padmp8.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(
                    "music/" + musicname);
            padmp8.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            padmp8.prepare();
            padmp8.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    padmp8.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playkeyboard1(String musicname) {
        try {

            keymp1.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(
                    "music/" + musicname);
            keymp1.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            keymp1.prepare();
            keymp1.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    keymp1.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playkeyboard2(String musicname) {
        try {

            keymp2.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(
                    "music/" + musicname);
            keymp2.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            keymp2.prepare();
            keymp2.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    keymp2.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playkeyboard3(String musicname) {
        try {

            keymp3.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(
                    "music/" + musicname);
            keymp3.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            keymp3.prepare();
            keymp3.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    keymp3.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playkeyboard4(String musicname) {
        try {

            keymp4.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(
                    "music/" + musicname);
            keymp4.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            keymp4.prepare();
            keymp4.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    keymp4.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playkeyboard5(String musicname) {
        try {

            keymp5.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(
                    "music/" + musicname);
            keymp5.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            keymp5.prepare();
            keymp5.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    keymp5.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playkeyboard6(String musicname) {
        try {

            keymp6.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(
                    "music/" + musicname);
            keymp6.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            keymp6.prepare();
            keymp6.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    keymp6.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playkeyboard7(String musicname) {
        try {

            keymp7.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(
                    "music/" + musicname);
            keymp7.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            keymp7.prepare();
            keymp7.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    keymp7.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playkeyboard8(String musicname) {
        try {

            keymp8.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(
                    "music/" + musicname);
            keymp8.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            keymp8.prepare();
            keymp8.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    keymp8.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playkeyboard9(String musicname) {
        try {

            keymp9.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(
                    "music/" + musicname);
            keymp9.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            keymp9.prepare();
            keymp9.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    keymp9.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playkeyboard10(String musicname) {
        try {

            keymp10.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(
                    "music/" + musicname);
            keymp10.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            keymp10.prepare();
            keymp10.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    keymp10.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playkeyboard11(String musicname) {
        try {

            keymp11.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(
                    "music/" + musicname);
            keymp11.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            keymp11.prepare();
            keymp11.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    keymp11.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playkeyboard12(String musicname) {
        try {

            keymp12.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(
                    "music/" + musicname);
            keymp12.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            keymp12.prepare();
            keymp12.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    keymp12.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playkeyboard13(String musicname) {
        try {

            keymp13.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(
                    "music/" + musicname);
            keymp13.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            keymp13.prepare();
            keymp13.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    keymp13.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playkeyboard14(String musicname) {
        try {

            keymp14.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(
                    "music/" + musicname);
            keymp14.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            keymp14.prepare();
            keymp14.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    keymp14.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playkeyboard15(String musicname) {
        try {

            keymp15.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(
                    "music/" + musicname);
            keymp15.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            keymp15.prepare();
            keymp15.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    keymp15.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playkeyboard16(String musicname) {
        try {

            keymp16.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(
                    "music/" + musicname);
            keymp16.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            keymp16.prepare();
            keymp16.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    keymp16.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playkeyboard17(String musicname) {
        try {

            keymp17.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(
                    "music/" + musicname);
            keymp17.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            keymp17.prepare();
            keymp17.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    keymp17.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playkeyboard0(String musicname) {
        try {

            Log.e("", "called");
            keymp0.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(
                    "music/" + musicname);
            keymp0.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            keymp0.prepare();
            keymp0.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    keymp0.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    boolean issetmetrodata = false, ismetroplay = false;

    private void playmusicmetro(String musicname) {
        try {
            issetmetrodata = true;
            AssetFileDescriptor descriptor = getAssets().openFd(
                    "music/" + musicname);
            metromp.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            metromp.prepare();
            metromp.setLooping(true);
            metromp.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    metromp.start();
                    ismetroplay = true;
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /******** add song method *******/

    public void editSound(View v) { // Select which files should be used with
        // each button
        // stopAll(true);
        flurryBtnAppCount("addSoundBtn_clicked");
        String s = null;
        if (v.getId() == R.id.ImageView04) {
            s = "Button1";
        } else if (v.getId() == R.id.ImageView05) {
            s = "Button2";
        }
        playListFirst(s);
    }

    /******** store song in database *******/

    public void editPreferences(String file, String id) {

        try {
            soundDB.addSound(id, file);
        } catch (Exception e) {
            // TODO: handle exception
        }
        // if (!file.equals("") && !id.equals("")) {

        // }
    }

    /******** stop and release all mediaplayer *******/

    public void stopAll(boolean leaving) {
        try {

            Collection<SoundInfo> c = hash.values();
            Iterator<SoundInfo> it = c.iterator();
            MediaPlayer mp;
            SoundInfo info;
            View view = findViewById(R.id.root);

            while (it.hasNext()) { // Remove mediaplayer data to release memory.
                // This is not always done automatically
                info = (SoundInfo) it.next();
                mp = info.returnMp();

                if (mp.isPlaying()) {
                    if (recording == true) {
                        long currTime = TimeUnit.NANOSECONDS.toMillis(System
                                .nanoTime()) - startTime;
                        ClipEvent evnt = new ClipEvent(info.id, EventType.STOP,
                                currTime, info.song, volume, 0,
                                mp.getCurrentPosition());
                        eventLogs.add(evnt);
                    }

                    ImageView v = (ImageView) view.findViewWithTag(info.id);
                    if (info.id.equals("Button1") || info.id.equals("Button2")) {
                        v.setBackgroundResource(info.play);
                    } else {
                        v.setBackgroundResource(R.drawable.fx_play);

                    }
                    v.invalidate();
                    mp.stop();
                }
                if (leaving == true)
                    mp.release();
                else
                    mp.reset();
            }
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    public void populateBoard() { // Display which buttons have a song and
        // create hashmap to use later

        Cursor cursor = soundDB.retrieveDB();
        hash = new HashMap<String, SoundInfo>();
        View view = findViewById(R.id.root);
        // prepareDrums pD = new prepareDrums();

        // hash = pD.prepareDrums(hash,getApplicationContext());//Add DrumKit

        cursor.moveToFirst(); // Point to the head of the db list

        while (!cursor.isAfterLast()) { // Log Table to verify
            SoundInfo info = new SoundInfo(cursor.getString(0),
                    cursor.getString(1), 0, 0, R.drawable.play_pressed,
                    R.drawable.pause, R.drawable.play);
            hash.put(cursor.getString(0), info);
            // ImageView iView = (ImageView)
            // view.findViewWithTag(cursor.getString(0)); //Change icon to
            // signify a song is available
            // iView.setImageResource(info.idle);
            // iView.invalidate();
            cursor.moveToNext();
        }
        cursor.close();
    }

    /******** sound setting method *******/

    public void playSound2(ImageView v) { // Play the sound associated with the
        // button
        SoundInfo index = null;
        index = hash.get(v.getTag());
        if (index != null) {
            MediaPlayer mp = index.returnMp();

            if (mp.isPlaying()) {

                mp.setVolume(((float) volume) / 100, ((float) volume) / 100);
                if (recording == true) {
                    long currTime = TimeUnit.NANOSECONDS.toMillis(System
                            .nanoTime()) - startTime;
                    ClipEvent evnt = new ClipEvent(index.id, EventType.STOP,
                            currTime, index.song, volume, 0,
                            mp.getCurrentPosition());
                    eventLogs.add(evnt);
                }
                if (recording == true) {
                    long currTime = TimeUnit.NANOSECONDS.toMillis(System
                            .nanoTime()) - startTime;
                    ClipEvent evnt = new ClipEvent(index.id, EventType.PLAY,
                            currTime, index.song, volume,
                            mp.getCurrentPosition(), 0);
                    eventLogs.add(evnt);
                }
            }

        }
    }

    /******** play song method *******/

    public void playSound(ImageView v) { // Play the sound associated with the
        // button

        SoundInfo index = null;
        try {
            index = hash.get(v.getTag());

        } catch (Exception e) {
            // TODO: handle exception
        }
        if (index != null) {
            MediaPlayer mp = index.returnMp();
            Log.e("mp", "index :: " + index.returnMp());

            if (v.getTag().toString().equals("Button1")) {
                // Updating progress bar

                updateProgressBar();
            } else if (v.getTag().toString().equals("Button2")) {
                // Updating progress bar

                updateProgressBar2();
            }
            if (mp.isPlaying()) {

                /*
                 * With multi touch one action is pressed in rapid succession
                 * Since the tabs are layered, multiple buttons are being
                 * selected The timer will prevent the buttons behind to get
                 * activated
                 */
                if (System.currentTimeMillis() - index.getTime() < 100)
                    return;

                if (index.id.equals("Button1")) {
                    mVisualizer1.setEnabled(false);
                    animatedDisk1.clearAnimation();
                } else if (index.id.equals("Button2")) {
                    mVisualizer2.setEnabled(false);
                    animatedDisk2.clearAnimation();

                }
                if (pauser) {
                    if (index.id.equals("Button1")
                            || index.id.equals("Button2")) {
                        if (recording == true) {
                            long currTime = TimeUnit.NANOSECONDS
                                    .toMillis(System.nanoTime()) - startTime;
                            ClipEvent evnt = new ClipEvent(index.id,
                                    EventType.STOP, currTime, index.song,
                                    volume, 0, mp.getCurrentPosition());
                            eventLogs.add(evnt);
                        }

                        Log.e("", "Button1 pause");
                        mp.pause();
                        v.setBackgroundResource(index.pause);
                        hash.get(v.getTag())
                                .setTime(System.currentTimeMillis());
                        hash.get(v.getTag()).setPaused(true);
                    } else {
                        // Log events for a stop
                        if (recording == true) {
                            long currTime = TimeUnit.NANOSECONDS
                                    .toMillis(System.nanoTime()) - startTime;
                            ClipEvent evnt = new ClipEvent(index.id,
                                    EventType.STOP, currTime, index.song,
                                    volume, 0, mp.getCurrentPosition());
                            eventLogs.add(evnt);
                        }

                        if (index.id.equals("Button1")) {
                            mp.stop();
                        } else if (index.id.equals("Button2")) {
                            mp.stop();
                        }

                        v.setBackgroundResource(R.drawable.fx_play);
                        v.invalidate();
                        mp.reset();
                        hash.get(v.getTag())
                                .setTime(System.currentTimeMillis());
                        hash.get(v.getTag()).setPaused(false);

                    }

                } else {

                    // Log events for a stop
                    if (recording == true) {
                        long currTime = TimeUnit.NANOSECONDS.toMillis(System
                                .nanoTime()) - startTime;
                        ClipEvent evnt = new ClipEvent(index.id,
                                EventType.STOP, currTime, index.song, volume,
                                0, mp.getCurrentPosition());
                        eventLogs.add(evnt);
                    }

                    mp.stop();
                    v.setBackgroundResource(index.idle);
                    v.invalidate();
                    mp.reset();
                    hash.get(v.getTag()).setTime(System.currentTimeMillis());
                    if (v.getTag().toString().equals("Button1")) {
                        mHandler.removeCallbacks(mUpdateTimeTask);
                    } else if (v.getTag().toString().equals("Button2")) {
                        mHandlersec.removeCallbacks(mUpdateTimeTask2);
                    }

                }

            } else {
                try {
                    // Screen overlap will hit the button mutliple times, this
                    // will prevent activation
                    if (System.currentTimeMillis() - index.getTime() < 100)
                        return;
                    if (index.id.equals("Button1")) {

                        Log.e("", "Button1 setEnabled");

                        setupVisualizerFxAndUI1(mp);
                        mVisualizer1.setEnabled(true);

                        animatedDisk1
                                .startAnimation(AnimationUtils.loadAnimation(
                                        PlayActivity.this, R.anim.rotate));
                    } else if (index.id.equals("Button2")) {

                        setupVisualizerFxAndUI2(mp);
                        mVisualizer2.setEnabled(true);

                        animatedDisk2
                                .startAnimation(AnimationUtils.loadAnimation(
                                        PlayActivity.this, R.anim.rotate));

                    }
                    // The song was previously paused. Continue from last
                    // position
                    if (index.isPaused()) {

                        // mVisualizer1.setEnabled(true);

                        mp.start();
                        if (recording == true) {
                            long currTime = TimeUnit.NANOSECONDS
                                    .toMillis(System.nanoTime()) - startTime;
                            ClipEvent evnt = new ClipEvent(index.id,
                                    EventType.PLAY, currTime, index.song,
                                    volume, mp.getCurrentPosition(), 0);
                            eventLogs.add(evnt);
                        }
                        v.setBackgroundResource(index.play);
                        hash.get(v.getTag())
                                .setTime(System.currentTimeMillis());
                        hash.get(v.getTag()).setPaused(false);
                        return;
                    }

                    mp.stop();
                    mp.reset();
                    mp.setDataSource(index.song);
                    mp.setOnCompletionListener(this);

                    // Log events for a stop
                    if (recording == true) {
                        long currTime = TimeUnit.NANOSECONDS.toMillis(System
                                .nanoTime()) - startTime;
                        ClipEvent evnt = new ClipEvent(index.id,
                                EventType.PLAY, currTime, index.song, volume,
                                mp.getCurrentPosition(), 0);
                        eventLogs.add(evnt);
                    }
                    if (index.id.equals("Button1")
                            || index.id.equals("Button2")) {
                        v.setBackgroundResource(index.play);
                    } else {
                        v.setBackgroundResource(R.drawable.fx_stop);

                    }
                    v.invalidate();
                    mp.prepare();
                    mp.start();
                    mp.setVolume(((float) volume) / 100, ((float) volume) / 100);
                    hash.get(v.getTag()).setTime(System.currentTimeMillis());
                    hash.get(v.getTag()).setRepeat(repeater);

                } catch (IllegalStateException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    mHandler.removeCallbacks(mUpdateTimeTask);
                    mHandlersec.removeCallbacks(mUpdateTimeTask2);
                    Toast.makeText(this, "Issues with this file!",
                            Toast.LENGTH_SHORT).show();
                    Toast.makeText(
                            this,
                            "Please edit this button and ensure the file exists",
                            Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                    if (index.id.equals("Button1")
                            || index.id.equals("Button2")) {
                        v.setBackgroundResource(index.play);
                    } else {
                        v.setBackgroundResource(R.drawable.fx_stop);

                    }
                }
            }
        }
    }

    /******** Listener on mediaplayer *******/

    public void onCompletion(MediaPlayer finished) {
        Collection<SoundInfo> c = hash.values();
        Iterator<SoundInfo> it = c.iterator();
        MediaPlayer mp;
        SoundInfo info;
        View view = findViewById(R.id.root);
        ImageView icon;
        finished.seekTo(0);
        while (it.hasNext()) { // Remove mediaplayer data to release memory.
            // This is not always done automatically
            info = (SoundInfo) it.next();
            mp = info.returnMp();

            if (finished == mp) {// Reset the mp object to be replayed later
                // Log events for a stop
                if (recording == true) {
                    long currTime = TimeUnit.NANOSECONDS.toMillis(System
                            .nanoTime()) - startTime;
                    ClipEvent evnt = new ClipEvent(info.id, EventType.STOP,
                            currTime, info.song, volume, 0, mp.getDuration());
                    eventLogs.add(evnt);
                }

                if (info.repeat) {
                    // mp.seekTo(0);
                    mp.start();
                    if (recording == true) {
                        long currTime = TimeUnit.NANOSECONDS.toMillis(System
                                .nanoTime()) - startTime;
                        ClipEvent evnt = new ClipEvent(info.id, EventType.PLAY,
                                currTime, info.song, volume, 0, 0);
                        eventLogs.add(evnt);
                    }

                } else {
                    if (!finished.isPlaying()) {
                        icon = (ImageView) view.findViewWithTag(info.id);
                        finished.stop();
                        finished.reset();
                        if (info.id.equals("Button1")
                                || info.id.equals("Button2")) {

                            icon.setBackgroundResource(info.idle); // Notify
                            // user that
                            // the song
                            // stopped
                            // by
                            // changing
                            // the
                            // appearance
                        } else {
                            icon.setBackgroundResource(R.drawable.fx_play);
                        }

                        if (info.id.equals("Button1")) {
                            mHandler.removeCallbacks(mUpdateTimeTask);
                            mVisualizer1.setEnabled(false);
                            animatedDisk1.clearAnimation();
                        } else if (info.id.equals("Button2")) {
                            mHandlersec.removeCallbacks(mUpdateTimeTask2);
                            mVisualizer2.setEnabled(false);
                            animatedDisk2.clearAnimation();

                        }

                    }
                }
            }
        }

    }

    /*
     * Save the recording to a file Checks for appropriate file types and
     * mixable data.
     */
    @SuppressLint("NewApi")
    public void onClick(View v) {
        // TODO Auto-generated method stub

        switch (v.getId()) {

            case R.id.rht_bt:
                if (rhtmp.isPlaying()) {
                    rhtmp.pause();
                    istrack1play = false;
                    Log.e("", "pause song");
                    rht_bt.setImageResource(R.drawable.play_bt);
                } else {
                    if (ist1setdata) {
                        istrack1play = true;
                        rhtmp.start();
                        Log.e("", "play song");
                        rht_bt.setImageResource(R.drawable.pause_bt);
                    } else {
                        Log.e("", "play  first time song");
                        rht_bt.setImageResource(R.drawable.pause_bt);
                        playrht("t" + MusicLoopClass.mploo1 + ".ogg");
                    }
                }
                break;
            case R.id.rht_prev:

                rhtmp.reset();
                int downname1 = Integer.valueOf(MusicLoopClass.mploo1) - 1;

                if (downname1 == 0) {
                    MusicLoopClass.mploo1 = "15";
                } else {
                    MusicLoopClass.mploo1 = downname1 + "";
                }

                rht_bt.setImageResource(R.drawable.pause_bt);
                rht_txt.setText("Track" + MusicLoopClass.mploo1);
                playrht("t" + MusicLoopClass.mploo1 + ".ogg");
                break;

            case R.id.rht_next:
                rhtmp.reset();
                int upname1 = Integer.valueOf(MusicLoopClass.mploo1) + 1;
                if (upname1 == 16) {
                    MusicLoopClass.mploo1 = "1";
                } else {
                    MusicLoopClass.mploo1 = upname1 + "";
                }
                rht_bt.setImageResource(R.drawable.pause_bt);
                rht_txt.setText("Track" + MusicLoopClass.mploo1);
                playrht("t" + MusicLoopClass.mploo1 + ".ogg");
                break;

            case R.id.sound_bt:
                // if (soundmp.isPlaying()) {
                // soundmp.pause();
                // issoundplay = false;
                // sound_bt.setImageResource(R.drawable.play_bt);
                // } else {
                // if (issounddata) {
                // issoundplay = true;
                // soundmp.start();
                // sound_bt.setImageResource(R.drawable.pause_bt);
                // } else {
                // sound_bt.setImageResource(R.drawable.pause_bt);
                // playsound(getsound(Integer.valueOf(MusicLoopClass.mpsound)) +
                // ".mp3");
                //
                // }
                // }
                break;
            case R.id.sound_prev:

                soundmp.reset();
                int downsound = Integer.valueOf(MusicLoopClass.mpsound) - 1;

                if (downsound == 0) {
                    MusicLoopClass.mpsound = "4";
                } else {
                    MusicLoopClass.mpsound = downsound + "";
                }
                sound_bt.setImageResource(R.drawable.pause_bt);
                sound_txt
                        .setText(getsound(Integer.valueOf(MusicLoopClass.mpsound)));

                if (Integer.valueOf(MusicLoopClass.mpsound) == 1) {
                    // playsound(getsound(Integer.valueOf(MusicLoopClass.mpsound)) +
                    // ".wav");

                } else {
                    // playsound(getsound(Integer.valueOf(MusicLoopClass.mpsound)) +
                    // ".mp3");
                }
                break;

            case R.id.sound_next:
                soundmp.reset();
                int upsound = Integer.valueOf(MusicLoopClass.mpsound) + 1;
                if (upsound == 5) {
                    MusicLoopClass.mpsound = "1";
                } else {
                    MusicLoopClass.mpsound = upsound + "";
                }
                sound_bt.setImageResource(R.drawable.pause_bt);
                sound_txt
                        .setText(getsound(Integer.valueOf(MusicLoopClass.mpsound)));

                if (Integer.valueOf(MusicLoopClass.mpsound) == 1) {
                    // playsound(getsound(Integer.valueOf(MusicLoopClass.mpsound)) +
                    // ".wav");

                } else {
                    // playsound(getsound(Integer.valueOf(MusicLoopClass.mpsound)) +
                    // ".mp3");
                }

                break;

            case R.id.metro_bt:
                if (metromp.isPlaying()) {
                    metromp.pause();
                    ismetroplay = false;
                    metro_bt.setImageResource(R.drawable.play_bt);
                } else {
                    if (issetmetrodata) {
                        ismetroplay = true;
                        metromp.start();
                        metro_bt.setImageResource(R.drawable.pause_bt);
                    } else {
                        metro_bt.setImageResource(R.drawable.pause_bt);
                        playmusicmetro("r" + MusicLoopClass.mpmetro + ".ogg");
                    }
                }
                break;
            case R.id.metro_prev:
                metromp.reset();
                int downmetro = Integer.valueOf(MusicLoopClass.mpmetro) - 1;

                if (downmetro == 0) {
                    MusicLoopClass.mpmetro = "10";
                } else {
                    MusicLoopClass.mpmetro = downmetro + "";
                }

                metro_bt.setImageResource(R.drawable.pause_bt);
                metro_txt.setText(getbpm(Integer.valueOf(MusicLoopClass.mpmetro)));
                playmusicmetro("r" + Integer.valueOf(MusicLoopClass.mpmetro)
                        + ".ogg");
                break;

            case R.id.metro_next:
                metromp.reset();
                int upmetro = Integer.valueOf(MusicLoopClass.mpmetro) + 1;
                if (upmetro == 11) {
                    MusicLoopClass.mpmetro = "1";
                } else {
                    MusicLoopClass.mpmetro = upmetro + "";
                }
                metro_bt.setImageResource(R.drawable.pause_bt);
                metro_txt.setText(getbpm(Integer.valueOf(MusicLoopClass.mpmetro)));
                playmusicmetro("r" + Integer.valueOf(MusicLoopClass.mpmetro)
                        + ".ogg");
                break;

            case R.id.s1:
                playpad1("p1.ogg");
                break;
            case R.id.s2:
                playpad2("p2.ogg");
                break;
            case R.id.s3:
                playpad3("p3.ogg");
                break;
            case R.id.s4:
                playpad4("p4.ogg");
                break;
            case R.id.s5:
                playpad5("p5.ogg");
                break;
            case R.id.s6:
                playpad6("p6.ogg");
                break;
            case R.id.s7:
                playpad7("p7.ogg");
                break;
            case R.id.s8:
                playpad8("p8.ogg");
                break;

            case R.id.t1:
                setthemedefault();
                t1.setImageResource(R.drawable.a_color);
                theme.setBackground(getResources().getDrawable(R.drawable.back1));
                break;

            case R.id.t2:
                setthemedefault();
                t2.setImageResource(R.drawable.b_color);
                theme.setBackground(getResources().getDrawable(R.drawable.back2));
                break;

            case R.id.t3:
                setthemedefault();
                t3.setImageResource(R.drawable.c_color);
                theme.setBackground(getResources().getDrawable(R.drawable.back3));
                break;

            case R.id.t4:
                setthemedefault();
                t4.setImageResource(R.drawable.d_color);
                theme.setBackground(getResources().getDrawable(R.drawable.back4));
                break;

            default:
                break;
        }

    }

    private void setthemedefault() {

        t1.setImageResource(R.drawable.a);
        t2.setImageResource(R.drawable.b);
        t3.setImageResource(R.drawable.c);
        t4.setImageResource(R.drawable.d);

    }

    private String getsound(int ins) {
        String name = "";
        switch (ins) {

            case 1:
                name = "Piano";
                break;
            case 4:
                name = "Organ";
                break;
            case 2:
                name = "Saxophone";
                break;
            case 3:
                name = "String";
                break;
            default:
                break;
        }
        return name;
    }

    private String getbpm(int ins) {
        String name = "";
        switch (ins) {

            case 1:
                name = "Bpm100";
                break;

            case 2:
                name = "Bpm105";
                break;
            case 3:
                name = "Bpm110";
                break;
            case 4:
                name = "Bpm115";
                break;
            case 5:
                name = "Bpm120";
                break;
            case 6:
                name = "Bpm125";
                break;
            case 7:
                name = "Bpm130";
                break;
            case 8:
                name = "Bpm135";
                break;
            case 9:
                name = "Bpm140";
                break;
            case 10:
                name = "Bpm145";
                break;
            default:
                break;
        }
        return name;
    }

    /******** setup equilizer for first song *******/

    public void setupEqualizer1(MediaPlayer mp, SoundInfo index) {

        LinearLayout lv = new LinearLayout(this);
        lv.setOrientation(LinearLayout.HORIZONTAL);

        mEqualizer1 = new Equalizer(0, mp.getAudioSessionId());
        mEqualizer1.setEnabled(true);
        TextView tv = new TextView(this);
        tv.setText("equalizer");
        llequilizer1.addView(tv);
        short bands = mEqualizer1.getNumberOfBands();
        final short min = mEqualizer1.getBandLevelRange()[0];
        final short max = mEqualizer1.getBandLevelRange()[1];
        for (short i = 0; i < bands; i++) {
            final short band = 1;
            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT, scaleX(25));
            VerticalSeekBar bar = new VerticalSeekBar(this);
            param.setMargins(scaleX(2), 0, scaleX(2), 0);
            bar.setLayoutParams(param);
            bar.setMax(max - min);
            bar.setProgress(max);
            bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

                public void onStopTrackingTouch(SeekBar seekBar) {
                    // TODO Auto-generated method stub

                }

                public void onStartTrackingTouch(SeekBar seekBar) {
                    // TODO Auto-generated method stub

                }

                public void onProgressChanged(SeekBar seekBar, int progress,
                                              boolean fromUser) {
                    // TODO Auto-generated method stub
                    mEqualizer1.setBandLevel(band, (short) (progress + min));

                }
            });

            lv.addView(bar);
        }
        llequilizer1.addView(lv);

        mp.attachAuxEffect(mEqualizer1.getId());
        mp.setAuxEffectSendLevel(1.0f);

        booster1 = new BassBoost(0, mp.getAudioSessionId());// -->tried with
        // sessionid also
        booster1.setEnabled(true);

        CheckBox checkBox = new CheckBox(this);
        checkBox.setText("Booster");
        llequilizer1.addView(checkBox);

        checkBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
                // TODO Auto-generated method stub

                if (isChecked) {
                    booster1.setStrength((short) 1000);

                } else {
                    booster1.setStrength((short) 0);
                }
            }
        });
        mp.attachAuxEffect(booster1.getId());
        mp.setAuxEffectSendLevel(1.0f);

        if (recording == true) {
            long currTime = TimeUnit.NANOSECONDS.toMillis(System.nanoTime())
                    - startTime;
            ClipEvent evnt = new ClipEvent(index.id, EventType.STOP, currTime,
                    index.song, volume, 0, mp.getCurrentPosition());
            eventLogs.add(evnt);
        }
        if (recording == true) {
            long currTime = TimeUnit.NANOSECONDS.toMillis(System.nanoTime())
                    - startTime;
            ClipEvent evnt = new ClipEvent(index.id, EventType.PLAY, currTime,
                    index.song, volume, mp.getCurrentPosition(), 0);
            eventLogs.add(evnt);
        }
    }

    /******** setup equilizer for second song *******/

    public void setupEqualizer2(MediaPlayer mp, SoundInfo index) {

        LinearLayout lv = new LinearLayout(this);
        lv.setOrientation(LinearLayout.HORIZONTAL);

        mEqualizer2 = new Equalizer(0, mp.getAudioSessionId());
        mEqualizer2.setEnabled(true);
        TextView tv = new TextView(this);
        tv.setText("equalizer");
        llequilizer2.addView(tv);
        short bands = mEqualizer2.getNumberOfBands();
        final short min = mEqualizer2.getBandLevelRange()[0];
        final short max = mEqualizer2.getBandLevelRange()[1];
        for (short i = 0; i < bands; i++) {
            final short band = 1;
            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT, scaleX(25));
            VerticalSeekBar bar = new VerticalSeekBar(this);
            param.setMargins(scaleX(2), 0, scaleX(2), 0);
            bar.setLayoutParams(param);
            bar.setMax(max - min);
            bar.setProgress(max);
            bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

                public void onStopTrackingTouch(SeekBar seekBar) {
                    // TODO Auto-generated method stub

                }

                public void onStartTrackingTouch(SeekBar seekBar) {
                    // TODO Auto-generated method stub

                }

                public void onProgressChanged(SeekBar seekBar, int progress,
                                              boolean fromUser) {
                    // TODO Auto-generated method stub
                    mEqualizer2.setBandLevel(band, (short) (progress + min));

                }
            });

            lv.addView(bar);
        }
        llequilizer2.addView(lv);

        mp.attachAuxEffect(mEqualizer2.getId());
        mp.setAuxEffectSendLevel(1.0f);

        booster2 = new BassBoost(0, mp.getAudioSessionId());// -->tried with
        // sessionid also
        booster2.setEnabled(true);

        CheckBox checkBox = new CheckBox(this);
        checkBox.setText("Booster");
        llequilizer2.addView(checkBox);

        checkBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
                // TODO Auto-generated method stub

                if (isChecked) {
                    booster2.setStrength((short) 1000);

                } else {
                    booster2.setStrength((short) 0);
                }
            }
        });
        mp.attachAuxEffect(booster2.getId());
        mp.setAuxEffectSendLevel(1.0f);

        if (recording == true) {
            long currTime = TimeUnit.NANOSECONDS.toMillis(System.nanoTime())
                    - startTime;
            ClipEvent evnt = new ClipEvent(index.id, EventType.STOP, currTime,
                    index.song, volume, 0, mp.getCurrentPosition());
            eventLogs.add(evnt);
        }
        if (recording == true) {
            long currTime = TimeUnit.NANOSECONDS.toMillis(System.nanoTime())
                    - startTime;
            ClipEvent evnt = new ClipEvent(index.id, EventType.PLAY, currTime,
                    index.song, volume, mp.getCurrentPosition(), 0);
            eventLogs.add(evnt);
        }
    }

    /**
     * Update timer on seekbar
     */
    public void updateProgressBar() {
        mHandler.postDelayed(mUpdateTimeTask, 100);
    }

    /**
     * Background Runnable thread
     */
    private Runnable mUpdateTimeTask = new Runnable() {
        public void run() {
            SoundInfo index = null;
            index = hash.get("Button1");
            MediaPlayer mp = index.returnMp();

            long totalDuration = mp.getDuration();
            long currentDuration = mp.getCurrentPosition();

            // Updating progress bar
            int progress = (int) (utils.getProgressPercentage(currentDuration,
                    totalDuration));
            // Log.d("Progress", ""+progress);
            firstprogressSeekbar.setProgress(progress);

            // Running this thread after 100 milliseconds
            mHandler.postDelayed(this, 100);
        }
    };

    /**
     * Update timer on seekbar
     */
    public void updateProgressBar2() {
        mHandlersec.postDelayed(mUpdateTimeTask2, 100);
    }

    /**
     * Background Runnable thread
     */
    private Runnable mUpdateTimeTask2 = new Runnable() {
        public void run() {
            SoundInfo index = null;
            index = hash.get("Button2");
            MediaPlayer mp = index.returnMp();

            long totalDuration = mp.getDuration();
            long currentDuration = mp.getCurrentPosition();

            // Updating progress bar
            int progress = (int) (utils.getProgressPercentage(currentDuration,
                    totalDuration));
            // Log.d("Progress", ""+progress);
            secprogressSeekbar.setProgress(progress);

            // Running this thread after 100 milliseconds
            mHandlersec.postDelayed(this, 100);
        }
    };

    /******** open song list method *******/

    void playListFirst(final String id) {
        // TODO Auto-generated method stub
        String[] parameters = {MediaStore.Audio.Media._ID,
                MediaStore.Audio.Media.DATA,
                MediaStore.Audio.Media.DISPLAY_NAME,
                MediaStore.Audio.Media.SIZE, MediaStore.Audio.Media.DURATION,
                MediaStore.Audio.Media.DATE_ADDED};

        videocursor = managedQuery(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                parameters, null, null, MediaStore.Audio.Media.DISPLAY_NAME
                        + " DESC");

        final Dialog dialog = new Dialog(PlayActivity.this);
        dialog.setContentView(R.layout.fontstyle_dialogbox_screen);
        dialog.setTitle("Select Songs");

        ListView textFontList = (ListView) dialog
                .findViewById(R.id.show_text_fonts_id);

        ListAdapter listAdapter = new AudioCursorAdapter(PlayActivity.this,
                PlayActivity.this, R.layout.video_preview, videocursor);

        textFontList.setAdapter(listAdapter);

        textFontList.setOnItemClickListener(new OnItemClickListener() {

            @SuppressLint("NewApi")
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1,
                                    int position, long arg3) {

                Cursor cursor = (Cursor) arg0.getItemAtPosition(position);

                int fileNameIndex = cursor
                        .getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
                final String filename = cursor.getString(fileNameIndex);

                /** Adding menu items to the popumenu */

                PopupMenu popup = new PopupMenu(getBaseContext(), arg1);
                popup.getMenu().add("Load on list 1");
                popup.getMenu().add("Load on list 2");
                popup.getMenu().add("Load on desk");

                /** Defining menu item click listener for the popup menu */
                popup.setOnMenuItemClickListener(new OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        Toast.makeText(getBaseContext(),
                                "You selected the action : " + item.getTitle(),
                                Toast.LENGTH_SHORT).show();
                        if (item.getTitle().equals("Load on list 1")) {
                            filenamelist.add(filename);
                        } else if (item.getTitle().equals("Load on list 2")) {
                            filenamelist2.add(filename);

                        } else if (item.getTitle().equals("Load on desk")) {

                            SoundInfo index = null;
                            try {
                                index = hash.get(id);

                                if (index != null) {
                                    MediaPlayer mp = index.returnMp();

                                    mp.stop();
                                    mp.reset();
                                    if (id.equals("Button1")) {
                                        playImageViewFirst
                                                .setBackgroundResource(R.drawable.play_pressed);
                                        mHandler.removeCallbacks(mUpdateTimeTask);
                                        animatedDisk1
                                                .setVisibility(View.VISIBLE);
                                        animatedDisk1
                                                .startAnimation(rotateAnimationfirst);

                                    } else if (id.equals("Button2")) {
                                        playImageViewSec
                                                .setBackgroundResource(R.drawable.play_pressed);
                                        mHandlersec
                                                .removeCallbacks(mUpdateTimeTask2);
                                        animatedDisk2
                                                .setVisibility(View.VISIBLE);
                                        animatedDisk2
                                                .startAnimation(rotateAnimationfirst);

                                    }
                                    index.song = filename;

                                } else {
                                    editPreferences(filename, id);
                                    populateBoard();
                                }
                            } catch (Exception e) {
                                // TODO: handle exception
                                editPreferences(filename, id);
                                populateBoard();
                            }

                            dialog.dismiss();

                        }

                        return true;
                    }
                });

                /** Showing the popup menu */
                popup.show();

            }
        });

        dialog.show();
    }

    void DeleteRecursive(File fileOrDirectory) {

        if (fileOrDirectory.isDirectory())
            for (File child : fileOrDirectory.listFiles())
                DeleteRecursive(child);

        fileOrDirectory.delete();

    }

    void equilizerSetup() {
        findViewById(R.id.EquilizerBtn).setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub

                        displayInterstitial1();

                        if (findViewById(R.id.EquilizerLyt).isShown()) {
                            findViewById(R.id.EquilizerLyt).setVisibility(
                                    View.GONE);
                            findViewById(R.id.EquilizerBtn)
                                    .setBackgroundResource(R.drawable.equalizer);

                        } else {
                            findViewById(R.id.EquilizerLyt).setVisibility(
                                    View.VISIBLE);
                            findViewById(R.id.EquilizerBtn)
                                    .setBackgroundResource(
                                            R.drawable.equalizer_pressed);

                        }

                    }
                });

        findViewById(R.id.EquilizerBtn2).setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub

                        displayInterstitial2();
                        if (findViewById(R.id.EquilizerLyt2).isShown()) {
                            // llequilizer1.removeAllViews();
                            findViewById(R.id.EquilizerLyt2).setVisibility(
                                    View.GONE);
                            findViewById(R.id.EquilizerBtn2)
                                    .setBackgroundResource(R.drawable.equalizer);

                        } else {
                            findViewById(R.id.EquilizerLyt2).setVisibility(
                                    View.VISIBLE);
                            findViewById(R.id.EquilizerBtn2)
                                    .setBackgroundResource(
                                            R.drawable.equalizer_pressed);

                        }
                    }
                });

    }

    class VisualizerView extends View {
        private byte[] mBytes;
        private float[] mPoints;
        private Rect mRect = new Rect();

        private Paint mForePaint = new Paint();

        public VisualizerView(Context context) {
            super(context);
            init();
        }

        private void init() {
            mBytes = null;

            mForePaint.setStrokeWidth(4f);
            mForePaint.setAntiAlias(true);
            mForePaint.setColor(Color.rgb(252, 193, 91));
        }

        public void updateVisualizer(byte[] bytes) {
            mBytes = bytes;
            // Log.e("bytes","by: " + bytes);
            invalidate();
        }

        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);

            if (mBytes == null) {
                return;
            }

            if (mPoints == null || mPoints.length < mBytes.length * 4) {
                mPoints = new float[mBytes.length * 4];
            }

            mRect.set(0, 0, getWidth(), getHeight());

            for (int i = 0; i < mBytes.length - 1; i++) {
                mPoints[i * 4] = mRect.width() * i / (mBytes.length - 1);
                mPoints[i * 4 + 1] = mRect.height() / 2
                        + ((byte) (mBytes[i] + 128)) * (mRect.height() / 2)
                        / 128;
                mPoints[i * 4 + 2] = mRect.width() * (i + 1)
                        / (mBytes.length - 1);
                mPoints[i * 4 + 3] = mRect.height() / 2
                        + ((byte) (mBytes[i + 1] + 128)) * (mRect.height() / 2)
                        / 128;
            }

            float centerX = mRect.width();
            float centerY = mRect.height();
            double angle = 90;
            Matrix rotateMat = new Matrix();
            rotateMat.setRotate((float) angle, centerX, centerY);
            // rotateMat.mapPoints(mPoints);

            canvas.drawLines(mPoints, mForePaint);

        }
    }

    LinearLayout waves1, waves2;
    private static final float VISUALIZER_HEIGHT_DIP = 50f;
    private Visualizer mVisualizer1, mVisualizer2;
    private VisualizerView mVisualizerView1, mVisualizerView2;

    private void setupVisualizerFxAndUI1(MediaPlayer mptrack1) {
        Log.e("", "setupVisualizerFxAndUI 1");

        if (mptrack1 != null) {

            Log.e("",
                    "setupVisualizerFxAndUI VisualizerView 1"
                            + Visualizer.getCaptureSizeRange()[1]);

            mVisualizerView1 = new VisualizerView(this);
            mVisualizerView1.setLayoutParams(new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.FILL_PARENT,
                    (int) (VISUALIZER_HEIGHT_DIP * getResources()
                            .getDisplayMetrics().density)));
            waves1.removeAllViews();
            waves1.addView(mVisualizerView1);
            mVisualizer1 = new Visualizer(mptrack1.getAudioSessionId());
            mVisualizer1.setCaptureSize(Visualizer.getCaptureSizeRange()[1]);
            mVisualizer1.setDataCaptureListener(
                    new Visualizer.OnDataCaptureListener() {
                        public void onWaveFormDataCapture(
                                Visualizer visualizer, byte[] bytes,
                                int samplingRate) {
                            mVisualizerView1.updateVisualizer(bytes);
                        }

                        public void onFftDataCapture(Visualizer visualizer,
                                                     byte[] bytes, int samplingRate) {
                        }
                    }, Visualizer.getMaxCaptureRate() / 2, true, false);
        }
    }

    private void setupVisualizerFxAndUI2(MediaPlayer mptrack2) {
        Log.e("", "setupVisualizerFxAndUI 2");
        if (mptrack2 != null) {
            mVisualizerView2 = new VisualizerView(this);
            mVisualizerView2.setLayoutParams(new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.FILL_PARENT,
                    (int) (VISUALIZER_HEIGHT_DIP * getResources()
                            .getDisplayMetrics().density)));
            waves2.removeAllViews();
            waves2.addView(mVisualizerView2);
            mVisualizer2 = new Visualizer(mptrack2.getAudioSessionId());
            mVisualizer2.setCaptureSize(Visualizer.getCaptureSizeRange()[1]);
            mVisualizer2.setDataCaptureListener(
                    new Visualizer.OnDataCaptureListener() {
                        public void onWaveFormDataCapture(
                                Visualizer visualizer, byte[] bytes,
                                int samplingRate) {
                            mVisualizerView2.updateVisualizer(bytes);
                        }

                        public void onFftDataCapture(Visualizer visualizer,
                                                     byte[] bytes, int samplingRate) {
                        }
                    }, Visualizer.getMaxCaptureRate() / 2, true, false);
        }
    }

    /******** setup playlist method *******/

    void setupPlaylist() {

        findViewById(R.id.playingListBtn1).setOnClickListener(
                new View.OnClickListener() {

                    @SuppressLint("NewApi")
                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        PopupMenu popup = new PopupMenu(getBaseContext(), v);
                        /** Adding menu items to the popumenu */

                        for (int i = 0; i < filenamelist.size(); i++) {

                            File name = new File(filenamelist.get(i));
                            // popup.getMenu().add(name.getName());
                            popup.getMenu().add(0, i, 0, name.getName());

                        }
                        /** Defining menu item click listener for the popup menu */
                        popup.setOnMenuItemClickListener(new OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem item) {

                                // Toast.makeText(getBaseContext(),
                                // "You selected the action : " +
                                // item.getItemId(), Toast.LENGTH_SHORT).show();
                                SoundInfo index = null;
                                index = hash.get("Button1");
                                if (index != null) {
                                    MediaPlayer mp = index.returnMp();
                                    // setupVisualizerFxAndUI1(mp);
                                    // mVisualizer1.setEnabled(true);

                                    // Log events for a stop
                                    if (recording == true) {
                                        long currTime = TimeUnit.NANOSECONDS
                                                .toMillis(System.nanoTime())
                                                - startTime;
                                        ClipEvent evnt = new ClipEvent(
                                                index.id, EventType.STOP,
                                                currTime, index.song, volume,
                                                0, mp.getCurrentPosition());
                                        eventLogs.add(evnt);
                                    }

                                    mp.stop();
                                    mp.reset();
                                    playImageViewFirst
                                            .setBackgroundResource(R.drawable.play_pressed);
                                    mHandler.removeCallbacks(mUpdateTimeTask);

                                    // index.song=item.getTitle().toString();
                                    // / Log.i("hello", id+"...");
                                    index.song = filenamelist.get(item
                                            .getItemId());

                                    if (animatedDisk1.isShown())
                                        animatedDisk1.clearAnimation();
                                    else
                                        animatedDisk1
                                                .setVisibility(View.VISIBLE);
                                    animatedDisk1
                                            .startAnimation(rotateAnimationfirst);

                                }
                                return true;
                            }
                        });

                        /** Showing the popup menu */
                        popup.show();
                    }
                });
        findViewById(R.id.playingListBtn2).setOnClickListener(
                new View.OnClickListener() {

                    @SuppressLint("NewApi")
                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        PopupMenu popup = new PopupMenu(getBaseContext(), v);
                        /** Adding menu items to the popumenu */

                        for (int i = 0; i < filenamelist2.size(); i++) {
                            File name = new File(filenamelist2.get(i));
                            // popup.getMenu().add(name.getName());
                            popup.getMenu().add(0, i, 0, name.getName());
                        }
                        /** Defining menu item click listener for the popup menu */
                        popup.setOnMenuItemClickListener(new OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem item) {
                                // Toast.makeText(getBaseContext(),
                                // "You selected the action : " +
                                // item.getTitle(), Toast.LENGTH_SHORT).show();
                                SoundInfo index = null;
                                index = hash.get("Button2");
                                if (index != null) {
                                    MediaPlayer mp = index.returnMp();
                                    // setupVisualizerFxAndUI2(mp);
                                    // mVisualizer2.setEnabled(true);
                                    // Log events for a stop
                                    if (recording == true) {
                                        long currTime = TimeUnit.NANOSECONDS
                                                .toMillis(System.nanoTime())
                                                - startTime;
                                        ClipEvent evnt = new ClipEvent(
                                                index.id, EventType.STOP,
                                                currTime, index.song, volume,
                                                0, mp.getCurrentPosition());
                                        eventLogs.add(evnt);
                                    }

                                    mp.stop();
                                    mp.reset();
                                    playImageViewSec
                                            .setBackgroundResource(R.drawable.play_pressed);
                                    mHandlersec
                                            .removeCallbacks(mUpdateTimeTask2);
                                    index.song = filenamelist2.get(item
                                            .getItemId());

                                    // index.song=item.getTitle().toString();
                                    if (animatedDisk2.isShown())
                                        animatedDisk2.clearAnimation();
                                    else
                                        animatedDisk2
                                                .setVisibility(View.VISIBLE);
                                    animatedDisk2
                                            .startAnimation(rotateAnimationfirst);

                                }
                                return true;
                            }
                        });

                        /** Showing the popup menu */
                        popup.show();
                    }
                });
    }

    /******** save effect sound in sd card *******/

    void saveEffectSound() {

        mProgressDialog = ProgressDialog.show(PlayActivity.this, "",
                "Wait......");
        mProgressDialog.setCancelable(false);

        Thread thread = new Thread() {
            public void run() {
                for (int i = 0; i < effectName.length; i++) {

                    InputStream path = getResources().openRawResource(
                            soundEffectId[i]);
                    ByteArrayBuffer baf = new ByteArrayBuffer(1024);
                    BufferedInputStream bis = new BufferedInputStream(path,
                            1024);

                    // get the bytes one by one
                    int current = 0;

                    try {
                        while ((current = bis.read()) != -1) {

                            baf.append((byte) current);
                        }
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                    byte[] bitmapdata = baf.toByteArray();
                    File file = new File(myDir, effectName[i]);
                    FileOutputStream fos;

                    try {
                        fos = new FileOutputStream(file);
                        fos.write(bitmapdata);
                        fos.flush();
                        fos.close();
                    } catch (FileNotFoundException e) {
                        // handle exception
                    } catch (IOException e) {
                        // handle exception
                    }
                    editPreferences(file.toString(), ButtonId[i]);

                }
                PlayActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        populateBoard();
                        try {

                            SoundInfo index = null;
                            index = hash.get("Button1");
                            if (index != null) {
                                MediaPlayer mp = index.returnMp();
                                setupEqualizer1(mp, index);

                            }
                        } catch (Exception e) {
                            // TODO: handle exception
                        }
                        try {
                            SoundInfo index = null;
                            index = hash.get("Button2");
                            if (index != null) {
                                MediaPlayer mp = index.returnMp();
                                setupEqualizer2(mp, index);
                            }
                        } catch (Exception e) {
                            // TODO: handle exception
                        }
                        effectTxtViewSetup();

                    }

                });
                if (mProgressDialog != null) {
                    if (mProgressDialog.isShowing())
                        mProgressDialog.dismiss();

                }

            }
        };
        thread.setDaemon(true);
        thread.start();

    }

    /******** setup song progresss seekbar *******/

    void setupSongSeekbar() {

        mSeekBar.setMax(100);
        mSeekBar.setProgress(50);
        firstprogressSeekbar.setProgress(0);
        firstprogressSeekbar.setMax(100);
        secprogressSeekbar.setProgress(0);
        secprogressSeekbar.setMax(100);

        mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {

                volume2 = progress;

            }

            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub

            }

            public void onStopTrackingTouch(SeekBar seekBar) {
                mSeekBar.setProgress(volume2);
                volume = volume2;
                playSound2((ImageView) findViewById(R.id.ImageView01));
                volume = 100 - volume2;
                playSound2((ImageView) findViewById(R.id.ImageView02));

            }

        });
        firstprogressSeekbar
                .setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

                    @Override
                    public void onProgressChanged(SeekBar seekBar,
                                                  int progress, boolean fromTouch) {

                    }

                    /**
                     * When user starts moving the progress handler
                     * */
                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {
                        // remove message Handler from updating progress bar
                        mHandler.removeCallbacks(mUpdateTimeTask);
                    }

                    /**
                     * When user stops moving the progress hanlder
                     * */
                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                        mHandler.removeCallbacks(mUpdateTimeTask);
                        SoundInfo index = null;
                        index = hash.get("Button1");
                        MediaPlayer mp = index.returnMp();
                        int totalDuration = mp.getDuration();
                        int currentPosition = utils.progressToTimer(
                                seekBar.getProgress(), totalDuration);
                        if (mp.isPlaying()) {
                            if (recording == true) {
                                long currTime = TimeUnit.NANOSECONDS
                                        .toMillis(System.nanoTime())
                                        - startTime;
                                ClipEvent evnt = new ClipEvent(index.id,
                                        EventType.STOP, currTime, index.song,
                                        volume, 0, mp.getCurrentPosition());
                                eventLogs.add(evnt);
                            }
                            // forward or backward to certain seconds
                            mp.seekTo(currentPosition);
                            if (recording == true) {
                                long currTime = TimeUnit.NANOSECONDS
                                        .toMillis(System.nanoTime())
                                        - startTime;
                                ClipEvent evnt = new ClipEvent(index.id,
                                        EventType.PLAY, currTime, index.song,
                                        volume, mp.getCurrentPosition(), 0);
                                eventLogs.add(evnt);
                            }
                            updateProgressBar();

                        } else {
                            mp.seekTo(currentPosition);

                        }

                        // update timer progress again

                    }

                });
        secprogressSeekbar
                .setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

                    @Override
                    public void onProgressChanged(SeekBar seekBar,
                                                  int progress, boolean fromTouch) {

                    }

                    /**
                     * When user starts moving the progress handler
                     * */
                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {
                        // remove message Handler from updating progress bar
                        mHandlersec.removeCallbacks(mUpdateTimeTask2);
                    }

                    /**
                     * When user stops moving the progress hanlder
                     * */
                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                        mHandlersec.removeCallbacks(mUpdateTimeTask2);
                        SoundInfo index = null;
                        index = hash.get("Button2");
                        MediaPlayer mp = index.returnMp();

                        int totalDuration = mp.getDuration();
                        int currentPosition = utils.progressToTimer(
                                seekBar.getProgress(), totalDuration);

                        if (mp.isPlaying()) {

                            if (recording == true) {
                                long currTime = TimeUnit.NANOSECONDS
                                        .toMillis(System.nanoTime())
                                        - startTime;
                                ClipEvent evnt = new ClipEvent(index.id,
                                        EventType.STOP, currTime, index.song,
                                        volume, 0, mp.getCurrentPosition());
                                eventLogs.add(evnt);
                            }
                            // forward or backward to certain seconds
                            mp.seekTo(currentPosition);

                            if (recording == true) {
                                long currTime = TimeUnit.NANOSECONDS
                                        .toMillis(System.nanoTime())
                                        - startTime;
                                ClipEvent evnt = new ClipEvent(index.id,
                                        EventType.PLAY, currTime, index.song,
                                        volume, mp.getCurrentPosition(), 0);
                                eventLogs.add(evnt);
                            }
                            updateProgressBar2();

                        } else {
                            // forward or backward to certain seconds
                            mp.seekTo(currentPosition);

                        }
                        // update timer progress again

                    }

                });

    }

    public void displayInterstitial1() {

        if (interstitial1.isLoaded()) {
            interstitial1.show();
        }
    }

    public void displayInterstitial2() {

        if (interstitial2.isLoaded()) {
            interstitial2.show();
        }
    }

    public void displayInterstitial3() {

        if (interstitial3.isLoaded()) {
            interstitial3.show();
        }
    }

    public void displayInterstitial4() {

        if (interstitial4.isLoaded()) {
            interstitial4.show();
        }
    }

    public void displayInterstitial5() {

        if (interstitial5.isLoaded()) {
            interstitial5.show();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            displayInterstitial4();
            exitDialogBox();
            setrelese();
        }
        return super.onKeyDown(keyCode, event);
    }

    public void exitDialogBox() {// public void exitDialogBox(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                PlayActivity.this);

        // set title
        alertDialogBuilder.setTitle("Done !! Why Not Try More");

        // set dialog message
        alertDialogBuilder
                .setMessage("Click yes to exit!")
                .setCancelable(false)
                .setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();

                                stopAll(true);
                                adshow = false;
                                soundDB.close();
                                mHandler.removeCallbacks(mUpdateTimeTask);
                                mHandlersec.removeCallbacks(mUpdateTimeTask2);

                                System.gc();
                                System.runFinalizersOnExit(true);
                                moveTaskToBack(false);

                                finish();
                            }
                        })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, just close
                        // the dialog box and do nothing
                        dialog.cancel();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();

        //
    }

    /******** setup sound effect Lyt *******/

    void setupEffectSoundLyt() {

        for (int i = 0; i < effectImageViewId.length; i++) {

            final int id = i;
            findViewById(effectImageViewId[i]).setOnClickListener(
                    new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            // TODO Auto-generated method stub

                            playSound((ImageView) v);
                            repeater = hash.get(ButtonId[id]).getRepeat();
                        }
                    });
            findViewById(effectrepeatImageViewId[i]).setOnClickListener(
                    new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            // TODO Auto-generated method stub
                            if (hash.get(ButtonId[id]).getRepeat()) {
                                repeater = false;
                                hash.get(ButtonId[id]).setRepeat(repeater);
                                v.setBackgroundResource(R.drawable.fx_loop_off);
                            } else {
                                repeater = true;
                                hash.get(ButtonId[id]).setRepeat(repeater);
                                v.setBackgroundResource(R.drawable.fx_loop_on);

                            }

                        }
                    });

        }
    }

    /******** setup name of sound effect *******/

    void effectTxtViewSetup() {
        txt1 = (TextView) findViewById(R.id.effecttxt1);
        txt2 = (TextView) findViewById(R.id.effecttxt2);
        txt3 = (TextView) findViewById(R.id.effecttxt3);
        txt4 = (TextView) findViewById(R.id.effecttxt4);
        txt5 = (TextView) findViewById(R.id.effecttxt5);
        txt6 = (TextView) findViewById(R.id.effecttxt6);
        txt7 = (TextView) findViewById(R.id.effecttxt7);
        txt8 = (TextView) findViewById(R.id.effecttxt8);
        txt9 = (TextView) findViewById(R.id.effecttxt9);
        txt10 = (TextView) findViewById(R.id.effecttxt10);
        txt11 = (TextView) findViewById(R.id.effecttxt11);
        txt12 = (TextView) findViewById(R.id.effecttxt12);
        txt13 = (TextView) findViewById(R.id.effecttxt13);
        txt14 = (TextView) findViewById(R.id.effecttxt14);
        txt15 = (TextView) findViewById(R.id.effecttxt15);

        txt1.setText(effectName[0]);
        txt2.setText(effectName[1]);
        txt3.setText(effectName[2]);
        txt4.setText(effectName[3]);
        txt5.setText(effectName[4]);
        txt6.setText(effectName[5]);
        txt7.setText(effectName[6]);
        txt8.setText(effectName[7]);
        txt9.setText(effectName[8]);
        txt10.setText(effectName[9]);
        txt11.setText(effectName[10]);
        txt12.setText(effectName[11]);
        txt13.setText(effectName[12]);
        txt14.setText(effectName[13]);
        txt15.setText(effectName[14]);

    }

    public int scaleX(double d) {
        return (int) (width * d) / 100;
    }

    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
      //  FlurryAgent.onEndSession(PlayActivity.this);

    }

    @Override
    protected void onStart() {
        // TODO Auto-generated method stub
        super.onStart();



       // FlurryAgent.onStartSession(PlayActivity.this, "TXYRJ9P6Y9T28V2KZXRD");
    }

    public void flurryBtnAppCount(String tag) {
        // Log.i("flurry","arv"+tag);
      //  FlurryAgent.onPageView();
      //  FlurryAgent.logEvent(tag);

    }

    private Runnable mSDCardErrorRunnable = new Runnable() {
        public void run() {
            String message = "Insert SD card.";
            Toast t = Toast.makeText(PlayActivity.this, message, Toast.LENGTH_LONG);
            t.show();
        }
    };

    private void setrelese() {
        rhtmp.release();
        metromp.release();
        padmp1.release();
        padmp2.release();
        padmp3.release();
        padmp4.release();
        padmp5.release();
        padmp6.release();
        padmp7.release();
        padmp8.release();
        soundmp.release();
        keymp1.release();
        keymp2.release();
        keymp3.release();
        keymp4.release();
        keymp5.release();
        keymp6.release();
        keymp7.release();
        keymp8.release();
        keymp9.release();
        keymp10.release();
        keymp11.release();
        keymp12.release();
        keymp13.release();
        keymp14.release();
        keymp15.release();
        keymp16.release();
        keymp17.release();

        keymp0.release();
    }

}

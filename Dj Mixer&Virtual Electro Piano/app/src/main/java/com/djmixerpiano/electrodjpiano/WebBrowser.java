package com.djmixerpiano.electrodjpiano;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.djmixerpiano.electrodjpiano.R;

public class WebBrowser extends Activity {
	WebView webView;
	ProgressDialog pbar;

	// http://streaming206.radionomy.com/Azur-BLUES
	// http://str30.creacast.com/r101_thema1
	String URL;

	@SuppressWarnings("deprecation")
	@SuppressLint({ "SetJavaScriptEnabled", "NewApi" })
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.browser);

		Bundle b = getIntent().getExtras();
		URL = b.getString("URL");
		
		
		
		webView = (WebView) findViewById(R.id.webView);

//		webView.loadDataWithBaseURL(null,  URL, "text/html", "utf-8", null);         
		WebSettings webSettings = webView.getSettings();
		webSettings.setLoadWithOverviewMode(true);
		webSettings.setUseWideViewPort(true);
		webSettings.setBuiltInZoomControls(true);
		webSettings.setJavaScriptEnabled(true);
		webSettings.setAppCacheEnabled(true);
		webSettings.setPluginState(WebSettings.PluginState.ON);
//		webSettings.setAllowFileAccessFromFileURLs(true);
//		webSettings.setMediaPlaybackRequiresUserGesture(true);
		
		webView.setWebChromeClient(new WebChromeClient());

		webView.setWebViewClient(new WebViewClient() {
			@Override
			public void onPageStarted(WebView view, String url, Bitmap favicon) {
				super.onPageStarted(view, url, favicon);
//				pbar = ProgressDialog.show(WebBrowser.this, "", "Please wait while page is loding...");
			}

			@Override
			public void onPageFinished(WebView view, String url) {
				super.onPageFinished(view, url);

//				if (pbar != null) {
//					pbar.dismiss();
//					pbar = null;
//				}

			}
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			// TODO Auto-generated method stub
			return super.shouldOverrideUrlLoading(view, url);
		}

		});

		webView.loadUrl(URL);
	}
}

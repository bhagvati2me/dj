package com.djmixerpiano.electrodjpiano;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaRecorder;
import android.media.audiofx.Visualizer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.DragEvent;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.DragShadowBuilder;
import android.view.View.OnClickListener;
import android.view.View.OnDragListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.djmixerpiano.electrodjpiano.R;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.twobard.pianoview.Piano;
import com.twobard.pianoview.Piano.PianoKeyListener;

@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
public class MainActivity extends Activity implements OnClickListener {

	Piano piano;
	ImageView rht_bt, rht_prev, rht_next, metro_bt, metro_prev, metro_next, sound_bt, sound_prev, sound_next;
	ImageView s1, s2, s3, s4, s5, s6, s7, s8;
	ImageView t1, t2, t3, t4;
	TextView rht_txt, metro_txt, sound_txt;
	MediaPlayer rhtmp, metromp, soundmp;
	MediaPlayer keymp1, keymp2, keymp3, keymp4, keymp5, keymp6, keymp7, keymp8, keymp9, keymp10, keymp11, keymp12,
			keymp13, keymp14, keymp15, keymp16, keymp17, keymp18, keymp19, keymp20, keymp21, keymp22, keymp23, keymp0;

	MediaPlayer padmp1, padmp2, padmp3, padmp4, padmp5, padmp6, padmp7, padmp8;
	View theme;
	SeekBar rht_seekbar, sound_seekbar;
	private static String audioFilePath;
	MediaRecorder mediaRecorder;
	boolean isRecording = false;
	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private InterstitialAd interstitial;
	AdRequest adRequest, adRequestdj;

	ImageView start, pause, menu;
	AdView adView, adViewdj;

	private static final float VISUALIZER_HEIGHT_DIP = 50f;
	private Visualizer mVisualizer, mVisualizer1;
	private LinearLayout mLinearLayout, mLinearLayout1;
	private VisualizerView mVisualizerView;
	private VisualizerView mVisualizerView2;
	MediaPlayer mpleft, mpright;
	MediaPlayer leftdjst, rightdjst;
	ImageView leftdjround, righgtdjround;
	ImageView addnew, newmenu;
	ImageView leftpalypause, rightpalypause;
	ListView playlist;
	SeekBar mixvolseek;
	ArrayList<Musicmodel> musiclist = new ArrayList<Musicmodel>();
	MusicAdapter musicadapter;
	private RotateAnimation anim;
	boolean isLeftPlaying = false;
	boolean isRightPlaying = false;
	VerticalSeekBar lvolseekbar, rvolseekbar;
	public float lvol = 0.5f;
	public float Rvol = 0.5f;
	public int cvol = 50;
	File rightfile, leftfile;
	DatabaseHandler dbHandler = new DatabaseHandler(this);
	TextView leftdjname, rightdjname;
	ImageView adddj, addpiano;
	View djview, pianoview;


	private String[] permissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO};

	@RequiresApi(api = Build.VERSION_CODES.M)
	private boolean arePermissionsEnabled() {
		for (String permission : permissions) {
			if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED)
				return false;
		}
		return true;
	}

	@RequiresApi(api = Build.VERSION_CODES.M)
	private void requestMultiplePermissions() {
		List<String> remainingPermissions = new ArrayList<>();
		for (String permission : permissions) {
			if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
				remainingPermissions.add(permission);
			}
		}
		requestPermissions(remainingPermissions.toArray(new String[remainingPermissions.size()]), 101);
	}

	@TargetApi(Build.VERSION_CODES.M)
	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		if (requestCode == 101) {
			for (int i = 0; i < grantResults.length; i++) {
				if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
					if (shouldShowRequestPermissionRationale(permissions[i])) {
						new AlertDialog.Builder(this)
								.setMessage("Please allow permissions to use record audio and choose local music from device")
								.setPositiveButton("Allow", new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialogInterface, int i) {
										dialogInterface.dismiss();
										requestMultiplePermissions();
									}
								})
								.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialogInterface, int i) {
										dialogInterface.dismiss();
										requestMultiplePermissions();
									}
								})
								.create()
								.show();
					}
					return;
				}
			}

			setupVisualizerFxAndUI();
			mVisualizer.setEnabled(true);
			setupVisualizerFxAndUI2();
			mVisualizer1.setEnabled(true);
			//====

		}
	}


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.firstscrren);


		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			if(!arePermissionsEnabled()) {
				requestMultiplePermissions();
			}
		}

		adView = (AdView) findViewById(R.id.adView);

		AdRequest adRequestd = new AdRequest.Builder().build();
		adViewdj = (AdView) findViewById(R.id.adViewdj);
		adViewdj.loadAd(adRequestd);

		displayInterstitial();

		mpleft = new MediaPlayer();
		mpright = new MediaPlayer();

		mLinearLayout = (LinearLayout) findViewById(R.id.l1);
		mLinearLayout1 = (LinearLayout) findViewById(R.id.l2);


		leftdjst = new MediaPlayer();
		rightdjst = new MediaPlayer();

		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.navdrawer);
		String[] values1 = new String[] { "Rate Now", "Info", "More Apps" };
		ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,
				android.R.id.text1, values1);
		mDrawerList.setAdapter(adapter1);
		mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Intent intentweb = new Intent(getcontext(), WebBrowser.class);
				switch (position) {
				case 0:
					showratedialog();
					mDrawerLayout.closeDrawer(Gravity.RIGHT);
					break;
				case 1:
					intentweb.putExtra("URL", "https://play.google.com/store/apps/developer?id=DjDeve");
					startActivity(intentweb);
					mDrawerLayout.closeDrawer(Gravity.RIGHT);

					break;
				case 2:
					intentweb.putExtra("URL", "https://play.google.com/store/apps/developer?id=DjDeve");
					startActivity(intentweb);
					mDrawerLayout.closeDrawer(Gravity.RIGHT);

					break;

				}

			}
		});
		djview = (View) findViewById(R.id.djview);
		pianoview = (View) findViewById(R.id.pianoview);
		adddj = (ImageView) findViewById(R.id.adddj);
		addpiano = (ImageView) findViewById(R.id.addpiano);

		rhtmp = new MediaPlayer();
		metromp = new MediaPlayer();
		padmp1 = new MediaPlayer();
		padmp2 = new MediaPlayer();
		padmp3 = new MediaPlayer();
		padmp4 = new MediaPlayer();
		padmp5 = new MediaPlayer();
		padmp6 = new MediaPlayer();
		padmp7 = new MediaPlayer();
		padmp8 = new MediaPlayer();

		soundmp = new MediaPlayer();
		keymp1 = new MediaPlayer();
		keymp2 = new MediaPlayer();
		keymp3 = new MediaPlayer();
		keymp4 = new MediaPlayer();
		keymp5 = new MediaPlayer();
		keymp6 = new MediaPlayer();
		keymp7 = new MediaPlayer();
		keymp8 = new MediaPlayer();
		keymp9 = new MediaPlayer();
		keymp10 = new MediaPlayer();
		keymp11 = new MediaPlayer();
		keymp12 = new MediaPlayer();
		keymp13 = new MediaPlayer();
		keymp14 = new MediaPlayer();
		keymp15 = new MediaPlayer();
		keymp16 = new MediaPlayer();
		keymp17 = new MediaPlayer();
		keymp18 = new MediaPlayer();
		keymp19 = new MediaPlayer();
		keymp20 = new MediaPlayer();
		keymp21 = new MediaPlayer();
		keymp22 = new MediaPlayer();
		keymp23 = new MediaPlayer();
		keymp0 = new MediaPlayer();
		rht_bt = (ImageView) findViewById(R.id.rht_bt);
		rht_prev = (ImageView) findViewById(R.id.rht_prev);
		rht_next = (ImageView) findViewById(R.id.rht_next);
		rht_txt = (TextView) findViewById(R.id.rht_txt);

		metro_bt = (ImageView) findViewById(R.id.metro_bt);
		metro_prev = (ImageView) findViewById(R.id.metro_prev);
		metro_next = (ImageView) findViewById(R.id.metro_next);
		metro_txt = (TextView) findViewById(R.id.metro_txt);

		sound_bt = (ImageView) findViewById(R.id.sound_bt);
		sound_prev = (ImageView) findViewById(R.id.sound_prev);
		sound_next = (ImageView) findViewById(R.id.sound_next);
		sound_txt = (TextView) findViewById(R.id.sound_txt);

		leftdjround = (ImageView) findViewById(R.id.leftdjround);
		righgtdjround = (ImageView) findViewById(R.id.righgtdjround);
		addnew = (ImageView) findViewById(R.id.addnew);
		newmenu = (ImageView) findViewById(R.id.newmenu);
		leftpalypause = (ImageView) findViewById(R.id.leftpalypause);
		rightpalypause = (ImageView) findViewById(R.id.rightpalypause);
		mixvolseek = (SeekBar) findViewById(R.id.mixvolseek);
		playlist = (ListView) findViewById(R.id.playlist);

		addpiano.setOnClickListener(this);
		adddj.setOnClickListener(this);

		leftdjround.setOnClickListener(this);
		righgtdjround.setOnClickListener(this);
		addnew.setOnClickListener(this);
		newmenu.setOnClickListener(this);
		leftpalypause.setOnClickListener(this);
		rightpalypause.setOnClickListener(this);

		sound_txt.setText(getsound(1));
		start = (ImageView) findViewById(R.id.start);
		pause = (ImageView) findViewById(R.id.pause);

		menu = (ImageView) findViewById(R.id.menubt);
		start.setOnClickListener(new OnClickListener() {

			@SuppressLint("SimpleDateFormat")
			@Override
			public void onClick(View v) {

				try {

					if (!isRecording) {
						String extStorageDirectory = Environment.getExternalStorageDirectory().getAbsolutePath();
						File folder = new File(extStorageDirectory, getResources().getString(R.string.app_name));
						if (!folder.exists()) {
							folder.mkdir();
						} else {
							String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
							audioFilePath = folder.getAbsolutePath() + "/" + timeStamp + ".mp3";

						}
						Toast.makeText(getcontext(), "recording stated", Toast.LENGTH_SHORT).show();
						PackageManager pmanager = getPackageManager();
						if (pmanager.hasSystemFeature(PackageManager.FEATURE_MICROPHONE)) {
							mediaRecorder = new MediaRecorder();
							mediaRecorder.setAudioSource(MediaRecorder.AudioSource.DEFAULT);
							mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
							mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
							mediaRecorder.setOutputFile(audioFilePath);
							mediaRecorder.prepare();
							mediaRecorder.start();
							isRecording = true;
						}
					} else {
						Toast.makeText(getcontext(), "recording  is already stated", Toast.LENGTH_SHORT).show();
					}

				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});

		pause.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (isRecording) {
					isRecording = false;
					Toast.makeText(getcontext(), "music file saved in SD card  ", Toast.LENGTH_SHORT).show();
					mediaRecorder.stop();
					mediaRecorder.reset();
					mediaRecorder.release();
				}

			}
		});

		s1 = (ImageView) findViewById(R.id.s1);
		s2 = (ImageView) findViewById(R.id.s2);
		s3 = (ImageView) findViewById(R.id.s3);
		s4 = (ImageView) findViewById(R.id.s4);
		s5 = (ImageView) findViewById(R.id.s5);
		s6 = (ImageView) findViewById(R.id.s6);
		s7 = (ImageView) findViewById(R.id.s7);
		s8 = (ImageView) findViewById(R.id.s8);

		t1 = (ImageView) findViewById(R.id.t1);
		t2 = (ImageView) findViewById(R.id.t2);
		t3 = (ImageView) findViewById(R.id.t3);
		t4 = (ImageView) findViewById(R.id.t4);

		theme = (View) findViewById(R.id.topbar);

		menu.setOnClickListener(this);
		rht_bt.setOnClickListener(this);
		rht_prev.setOnClickListener(this);
		rht_next.setOnClickListener(this);

		metro_bt.setOnClickListener(this);
		metro_prev.setOnClickListener(this);
		metro_next.setOnClickListener(this);

		s1.setOnClickListener(this);
		s2.setOnClickListener(this);
		s3.setOnClickListener(this);
		s4.setOnClickListener(this);
		s5.setOnClickListener(this);
		s6.setOnClickListener(this);
		s7.setOnClickListener(this);
		s8.setOnClickListener(this);

		sound_bt.setOnClickListener(this);
		sound_prev.setOnClickListener(this);
		sound_next.setOnClickListener(this);

		t1.setOnClickListener(this);
		t2.setOnClickListener(this);
		t3.setOnClickListener(this);
		t4.setOnClickListener(this);

		rht_seekbar = (SeekBar) findViewById(R.id.rht_seekbar);
		sound_seekbar = (SeekBar) findViewById(R.id.sound_seekbar);

		rht_seekbar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {

			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {

			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				float soundvol = (progress) / 100.0f;
				rhtmp.setVolume(soundvol, soundvol);
			}
		});

		sound_seekbar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {

			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {

			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

				float soundvol = (progress) / 100.0f;
				keymp1.setVolume(soundvol, soundvol);
				keymp2.setVolume(soundvol, soundvol);
				keymp3.setVolume(soundvol, soundvol);
				keymp4.setVolume(soundvol, soundvol);
				keymp5.setVolume(soundvol, soundvol);
				keymp6.setVolume(soundvol, soundvol);
				keymp7.setVolume(soundvol, soundvol);
				keymp8.setVolume(soundvol, soundvol);
				keymp9.setVolume(soundvol, soundvol);
				keymp10.setVolume(soundvol, soundvol);
				keymp11.setVolume(soundvol, soundvol);
				keymp12.setVolume(soundvol, soundvol);
				keymp13.setVolume(soundvol, soundvol);
				keymp14.setVolume(soundvol, soundvol);
				keymp15.setVolume(soundvol, soundvol);
				keymp16.setVolume(soundvol, soundvol);
				keymp17.setVolume(soundvol, soundvol);
				keymp18.setVolume(soundvol, soundvol);
				keymp19.setVolume(soundvol, soundvol);
				keymp20.setVolume(soundvol, soundvol);
				keymp21.setVolume(soundvol, soundvol);
				keymp22.setVolume(soundvol, soundvol);
				keymp23.setVolume(soundvol, soundvol);
				keymp0.setVolume(soundvol, soundvol);

			}
		});

		piano = (Piano) findViewById(R.id.pi);
		piano.setPianoKeyListener(new PianoKeyListener() {
			@Override
			public void keyPressed(int id, int action) {

				if (action == MotionEvent.ACTION_DOWN) {
					Log.e("pino", "action:" + action + " id: " + id);

					if (Integer.valueOf(MusicLoopClass.mpsound) == 1) {
						play((getsound(Integer.valueOf(MusicLoopClass.mpsound)) + id + ".ogg"), id);

					} else {
						play((getsound(Integer.valueOf(MusicLoopClass.mpsound)) + id + ".ogg"), id);

					}

				}
			}
		});
		leftdjname = (TextView) findViewById(R.id.leftdjname);
		rightdjname = (TextView) findViewById(R.id.rightdjname);
		musicadapter = new MusicAdapter(getcontext(), R.layout.playlistitem, musiclist);
		playlist.setAdapter(musicadapter);
		playlist.setOnItemLongClickListener(new MyClickListener());
		playlist.setOnDragListener(new LeftDragListener());
		findViewById(R.id.leftlayer).setOnDragListener(new LeftDragListener());
		findViewById(R.id.rightlayer).setOnDragListener(new RightDragListener());

		anim = new RotateAnimation(0f, 350f, RotateAnimation.RELATIVE_TO_SELF, 0.5f, RotateAnimation.RELATIVE_TO_SELF,
				0.5f);
		anim.setInterpolator(new LinearInterpolator());
		anim.setRepeatCount(Animation.INFINITE);
		anim.setDuration(700);

		mpleft.setOnCompletionListener(new OnCompletionListener() {

			@Override
			public void onCompletion(MediaPlayer mp) {

				mpleft.stop();
				mpleft.reset();
				leftdjname.setText("Drag Here");
				leftdjround.setAnimation(null);
				leftpalypause.setImageResource(R.drawable.puse);
				isLeftPlaying = false;

			}
		});
		mpright.setOnCompletionListener(new OnCompletionListener() {

			@Override
			public void onCompletion(MediaPlayer mp) {
				mpright.stop();
				mpright.reset();
				rightdjname.setText("Drag Here");
				righgtdjround.setAnimation(null);
				rightpalypause.setImageResource(R.drawable.puse);
				isRightPlaying = false;

			}
		});

		lvolseekbar = (VerticalSeekBar) findViewById(R.id.lvolseekbar);
		rvolseekbar = (VerticalSeekBar) findViewById(R.id.rvolseekbar);

		lvolseekbar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				float Rvol = (progress) / 100.0f;
				mpleft.setVolume(Rvol, Rvol);
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {

			}
		});
		rvolseekbar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				float Rvol = (progress) / 100.0f;
				mpright.setVolume(Rvol, Rvol);
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {

			}
		});
		mixvolseek.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			public void onStopTrackingTouch(SeekBar seekBar) {
			}

			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

				if (cvol == progress) {
					lvol = 0.5f;
					Rvol = 0.5f;
				}

				lvol = (100 - progress) / 100.0f;
				Rvol = (progress) / 100.0f;

				mpleft.setVolume(lvol, lvol);

				mpright.setVolume(Rvol, Rvol);

			}
		});

	}

	private final class MyClickListener implements OnItemLongClickListener {

		@Override
		public boolean onItemLongClick(AdapterView<?> arg0, View view, int position, long arg3) {

			// String selectedFromList = "trdvm";
			String selectedFromList = musiclist.get(position).path;
			ClipData.Item item = new ClipData.Item(((CharSequence) selectedFromList.toString()));
			String[] clipDescription = { ClipDescription.MIMETYPE_TEXT_PLAIN };
			ClipData dragData = new ClipData("item", clipDescription, item);
			DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
			view.startDrag(dragData, shadowBuilder, view, 0);
			return true;
		}

	}

	@SuppressLint("NewApi")
	class LeftDragListener implements OnDragListener {
		@Override
		public boolean onDrag(View v, DragEvent event) {

			switch (event.getAction()) {

			case DragEvent.ACTION_DRAG_STARTED:

				break;

			case DragEvent.ACTION_DRAG_ENTERED:
				break;

			case DragEvent.ACTION_DRAG_EXITED:
				break;

			case DragEvent.ACTION_DROP:
				// mpleft.reset();

				ClipData.Item item = event.getClipData().getItemAt(0);
				File leftfile1 = new File(item.getText().toString());
				String name = leftfile1.getName();
				Log.e("name : ", name);

				leftfile = new File(item.getText().toString());

				if (isLeftPlaying) {
					showmusicchange(0, leftfile);
				} else {
					leftdjname.setText(name);
					leftpalypause.setImageResource(R.drawable.play);
					try {
						playleftmusic(leftfile.getAbsolutePath());
					} catch (IllegalStateException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}

				}
				break;

			case DragEvent.ACTION_DRAG_ENDED:
				break;
			default:
				break;
			}
			return true;
		}
	}

	class RightDragListener implements OnDragListener {
		@Override
		public boolean onDrag(View v, DragEvent event) {

			switch (event.getAction()) {

			case DragEvent.ACTION_DRAG_STARTED:

				break;

			case DragEvent.ACTION_DRAG_ENTERED:
				break;

			case DragEvent.ACTION_DRAG_EXITED:
				break;

			case DragEvent.ACTION_DROP:
				// mpright.reset();

				ClipData.Item item = event.getClipData().getItemAt(0);
				File rightfile1 = new File(item.getText().toString());
				String name = rightfile1.getName();
				Log.e("name : ", name);
				rightfile = new File(item.getText().toString());

				if (isRightPlaying) {
					showmusicchange(1, rightfile);
				} else {
					rightdjname.setText(name);

					rightpalypause.setImageResource(R.drawable.play);
					try {
						playrightmusic(rightfile.getAbsolutePath());
					} catch (IllegalStateException e) {
						e.printStackTrace();
					}

				}
				break;

			case DragEvent.ACTION_DRAG_ENDED:

				break;
			default:
				break;
			}
			return true;
		}
	}

	private void playleftmusic(String path) throws IllegalStateException, IOException {

		if (!isLeftPlaying) {
			try {

				List<Musicmodel> el = dbHandler.Get_playList(false);
				if (el.size() == 0) {
					Log.e("leftpalying", "play " + path);
					AssetFileDescriptor descriptor = getAssets().openFd("sample/" + path);
					mpleft.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(),
							descriptor.getLength());
					descriptor.close();
				} else {
					mpleft.setDataSource(getApplicationContext(), Uri.parse(path));
				}
				mpleft.prepare();
				mpleft.setOnPreparedListener(new OnPreparedListener() {

					@Override
					public void onPrepared(MediaPlayer mp) {
						// durationLeft = mpleft.getDuration();
						// leftmpvol.setMax(durationLeft);
						// leftmpvol.postDelayed(onEverySecondLeft, 1000);
					}
				});
				mpleft.start();

				// leftmpvol.postDelayed(onEverySecondLeft, 1000);
				leftpalypause.setImageResource(R.drawable.puse);
				leftdjround.startAnimation(anim);
				isLeftPlaying = true;

			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

		} else {
			// isLeftPlaying = false;
			// mpleft.pause();
			// leftdjround.setAnimation(null);
			// leftpalypause.setImageResource(R.drawable.play);
		}
	}

	private void playrightmusic(String path) {

		if (!isRightPlaying) {
			// TODO Auto-generated method stub
			try {
				List<Musicmodel> el = dbHandler.Get_playList(false);
				if (el.size() == 0) {
					Log.e("isRightPlaying", "play " + path);

					AssetFileDescriptor descriptor = getAssets().openFd("sample/" + path);
					mpright.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(),
							descriptor.getLength());
					descriptor.close();

				} else {
					mpright.setDataSource(getApplicationContext(), Uri.parse(path));

				}
				mpright.prepare();
				mpright.setOnPreparedListener(new OnPreparedListener() {
					@Override
					public void onPrepared(MediaPlayer mp) {
						// durationRight = mpright.getDuration();
						// rightmpvol.setMax(durationRight);
						// rightmpvol.postDelayed(onEverySecondRight, 1000);

					}
				});

				mpright.start();
				// rightmpvol.postDelayed(onEverySecondRight, 1000);
				rightpalypause.setImageResource(R.drawable.puse);
				isRightPlaying = true;
				righgtdjround.startAnimation(anim);

			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

		} else {

		}
	}

	private void showmusicchange(final int mp, final File name) {
		new AlertDialog.Builder(this).setTitle("Do you want to stop current track?")
				.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {

						if (mp == 0) {
							Log.e("mpleft", "mpleft reset");
							mpleft.stop();
							mpleft.reset();
							leftdjname.setText(name.getName().toString());
							leftpalypause.setImageResource(R.drawable.play);

							try {
								isLeftPlaying = false;
								playleftmusic(name.getAbsolutePath());
							} catch (IllegalStateException e) {
								e.printStackTrace();
							} catch (IOException e) {
								e.printStackTrace();
							}

						} else {
							mpright.stop();
							mpright.reset();
							rightdjname.setText(name.getName().toString());
							rightpalypause.setImageResource(R.drawable.play);
							isRightPlaying = false;
							playrightmusic(name.getAbsolutePath());
						}

					}
				}).setNegativeButton("No", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				}).show();
	}

	@Override
	protected void onResume() {
		super.onResume();

		musiclist.clear();
		musicadapter.notifyDataSetChanged();
		List<Musicmodel> el = dbHandler.Get_playList(false);
		for (Musicmodel e : el) {
			musiclist.add(e);
		}
		musicadapter.notifyDataSetChanged();
		if (el.size() == 0) {
			// Field[] fields = R.raw.class.getFields();
			AssetManager assetManager = getApplicationContext().getAssets();
			String[] fields;
			try {
				fields = assetManager.list("sample");
				for (int count = 0; count < fields.length; count++) {

					Musicmodel m = new Musicmodel();
					m.name = fields[count];
					m.path = fields[count];
					musiclist.add(m);

				}
			} catch (IOException e1) {
				e1.printStackTrace();
			}

		}

	}

	private void setupVisualizerFxAndUI() {
		mVisualizerView = new VisualizerView(this);

		mVisualizerView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
				ViewGroup.LayoutParams.FILL_PARENT));

		mLinearLayout.addView(mVisualizerView);
		// if (mpleft.isPlaying()) {

		Log.e("mpleft", "seesion id " + mpleft.getAudioSessionId());
		mVisualizer = new Visualizer(mpleft.getAudioSessionId());

		mVisualizer.setCaptureSize(Visualizer.getCaptureSizeRange()[1]);
		mVisualizer.setDataCaptureListener(new Visualizer.OnDataCaptureListener() {
			public void onWaveFormDataCapture(Visualizer visualizer, byte[] bytes, int samplingRate) {
				mVisualizerView.updateVisualizer(bytes);
			}

			public void onFftDataCapture(Visualizer visualizer, byte[] bytes, int samplingRate) {

				Log.e("", "fftc hange");
			}
		}, Visualizer.getMaxCaptureRate() / 2, true, false);
		// }
	}

	private void setupVisualizerFxAndUI2() {
		mVisualizerView2 = new VisualizerView(this);

		mVisualizerView2.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
				ViewGroup.LayoutParams.FILL_PARENT));

		mLinearLayout1.addView(mVisualizerView2);
		// if (mpleft.isPlaying()) {

		Log.e("mpleft", "seesion id " + mpleft.getAudioSessionId());
		mVisualizer1 = new Visualizer(mpright.getAudioSessionId());

		mVisualizer1.setCaptureSize(Visualizer.getCaptureSizeRange()[1]);
		mVisualizer1.setDataCaptureListener(new Visualizer.OnDataCaptureListener() {
			public void onWaveFormDataCapture(Visualizer visualizer, byte[] bytes, int samplingRate) {
				mVisualizerView2.updateVisualizer(bytes);
			}

			public void onFftDataCapture(Visualizer visualizer, byte[] bytes, int samplingRate) {

				Log.e("", "fftc hange");
			}
		}, Visualizer.getMaxCaptureRate() / 2, true, false);
		// }
	}

	private void play(String sd, int id) {

		switch (id) {
		case 0:
			playkeyboard0(sd);
			break;
		case 1:
			playkeyboard1(sd);
			break;
		case 2:
			playkeyboard2(sd);
			break;
		case 3:
			playkeyboard3(sd);
			break;
		case 4:
			playkeyboard4(sd);
			break;
		case 5:
			playkeyboard5(sd);
			break;
		case 6:
			playkeyboard6(sd);
			break;
		case 7:
			playkeyboard7(sd);
			break;
		case 8:
			playkeyboard8(sd);
			break;
		case 9:
			playkeyboard9(sd);
			break;
		case 10:
			playkeyboard10(sd);
			break;
		case 11:
			playkeyboard11(sd);
			break;
		case 12:
			playkeyboard12(sd);
			break;
		case 13:
			playkeyboard13(sd);
			break;
		case 14:
			playkeyboard14(sd);
			break;
		case 15:
			playkeyboard15(sd);
			break;
		case 16:
			playkeyboard16(sd);
			break;
		case 17:
			playkeyboard17(sd);
			break;
		case 18:
			playkeyboard18(sd);
			break;
		case 19:
			playkeyboard19(sd);
			break;
		case 20:
			playkeyboard20(sd);
			break;
		case 21:
			playkeyboard21(sd);
			break;
		case 22:
			playkeyboard22(sd);
			break;
		case 23:
			playkeyboard23(sd);
			break;

		default:
			break;
		}

	}

	private Activity getcontext() {

		return MainActivity.this;
	}

	boolean ist1setdata = false, istrack1play = false;

	private void playrht(String musicname) {
		try {
			ist1setdata = true;
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			rhtmp.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			rhtmp.prepare();
			rhtmp.setLooping(true);
			rhtmp.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					rhtmp.start();
					istrack1play = true;
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	boolean issounddata = false, issoundplay = false;

	private void playsound(String musicname) {
		try {
			issounddata = true;
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			soundmp.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			soundmp.prepare();
			soundmp.setLooping(true);
			soundmp.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					soundmp.start();
					issoundplay = true;
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playpad1(String musicname) {
		try {

			padmp1.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			padmp1.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			padmp1.prepare();
			padmp1.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					padmp1.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playpad2(String musicname) {
		try {

			padmp2.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			padmp2.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			padmp2.prepare();
			padmp2.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					padmp2.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playpad3(String musicname) {
		try {

			padmp3.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			padmp3.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			padmp3.prepare();
			padmp3.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					padmp3.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playpad4(String musicname) {
		try {

			padmp4.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			padmp4.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			padmp4.prepare();
			padmp4.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					padmp4.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playpad5(String musicname) {
		try {

			padmp5.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			padmp5.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			padmp5.prepare();
			padmp5.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					padmp5.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playpad6(String musicname) {
		try {

			padmp6.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			padmp6.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			padmp6.prepare();
			padmp6.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					padmp6.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playpad7(String musicname) {
		try {

			padmp7.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			padmp7.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			padmp7.prepare();
			padmp7.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					padmp7.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playpad8(String musicname) {
		try {

			padmp8.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			padmp8.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			padmp8.prepare();
			padmp8.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					padmp8.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard1(String musicname) {
		try {

			keymp1.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			keymp1.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp1.prepare();
			keymp1.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp1.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard2(String musicname) {
		try {

			keymp2.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			keymp2.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp2.prepare();
			keymp2.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp2.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard3(String musicname) {
		try {

			keymp3.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			keymp3.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp3.prepare();
			keymp3.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp3.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard4(String musicname) {
		try {

			keymp4.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			keymp4.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp4.prepare();
			keymp4.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp4.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard5(String musicname) {
		try {

			keymp5.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			keymp5.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp5.prepare();
			keymp5.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp5.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard6(String musicname) {
		try {

			keymp6.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			keymp6.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp6.prepare();
			keymp6.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp6.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard7(String musicname) {
		try {

			keymp7.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			keymp7.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp7.prepare();
			keymp7.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp7.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard8(String musicname) {
		try {

			keymp8.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			keymp8.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp8.prepare();
			keymp8.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp8.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard9(String musicname) {
		try {

			keymp9.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			keymp9.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp9.prepare();
			keymp9.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp9.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard10(String musicname) {
		try {

			keymp10.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			keymp10.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp10.prepare();
			keymp10.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp10.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard11(String musicname) {
		try {

			keymp11.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			keymp11.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp11.prepare();
			keymp11.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp11.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard12(String musicname) {
		try {

			keymp12.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			keymp12.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp12.prepare();
			keymp12.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp12.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard13(String musicname) {
		try {

			keymp13.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			keymp13.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp13.prepare();
			keymp13.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp13.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard14(String musicname) {
		try {

			keymp14.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			keymp14.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp14.prepare();
			keymp14.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp14.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard15(String musicname) {
		try {

			keymp15.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			keymp15.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp15.prepare();
			keymp15.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp15.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard16(String musicname) {
		try {

			keymp16.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			keymp16.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp16.prepare();
			keymp16.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp16.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard17(String musicname) {
		try {

			keymp17.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			keymp17.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp17.prepare();
			keymp17.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp17.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard18(String musicname) {
		try {

			keymp18.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			keymp18.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp18.prepare();
			keymp18.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp18.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard19(String musicname) {
		try {

			keymp19.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			keymp19.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp19.prepare();
			keymp19.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp19.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard20(String musicname) {
		try {

			keymp20.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			keymp20.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp20.prepare();
			keymp20.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp20.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard21(String musicname) {
		try {

			keymp21.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			keymp21.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp21.prepare();
			keymp21.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp21.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard22(String musicname) {
		try {

			keymp22.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			keymp22.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp22.prepare();
			keymp22.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp22.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard23(String musicname) {
		try {

			keymp23.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			keymp23.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp23.prepare();
			keymp23.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp23.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard0(String musicname) {
		try {

			keymp0.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			keymp0.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp0.prepare();
			keymp0.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp0.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	boolean issetmetrodata = false, ismetroplay = false;

	private void playmusicmetro(String musicname) {
		try {
			issetmetrodata = true;
			AssetFileDescriptor descriptor = getAssets().openFd(musicname);
			metromp.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			metromp.prepare();
			metromp.setLooping(true);
			metromp.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					metromp.start();
					ismetroplay = true;
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void leftdjst(String path) {
		try {
			AssetFileDescriptor descriptor = getAssets().openFd("music/" + path);
			leftdjst.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			leftdjst.prepare();
			leftdjst.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					leftdjst.start();

				}
			});

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void rightdjst(String path) {
		try {
			AssetFileDescriptor descriptor = getAssets().openFd("music/" + path);
			rightdjst
					.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			rightdjst.prepare();
			rightdjst.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					rightdjst.start();
				}
			});

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void showalert() {

		AlertDialog.Builder alert = new AlertDialog.Builder(this);

		alert.setTitle("Save PlayList");

		// Set an EditText view to get user input
		final EditText input = new EditText(this);
		alert.setView(input);

		alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				String value = input.getText().toString();
				if (!value.equals("")) {
					dbHandler.Add_play(value.toString().trim());

					for (int i = 0; i < musiclist.size(); i++) {
						Musicmodel md = new Musicmodel();
						md.name = musiclist.get(i).name;
						md.path = musiclist.get(i).path;
						Log.e("value", value);
						dbHandler.Add_Saveplaylist(md, value.toString().trim());

					}

				}
			}
		});

		alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				// Canceled.
			}
		});

		alert.show();
	}

	private void openplaylist() {
		final String[] openlist = dbHandler.openlist();
		new AlertDialog.Builder(getcontext()).setItems(openlist, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {

				Log.e("list", openlist[which].toString());
				musiclist.clear();
				musicadapter.notifyDataSetChanged();
				List<Musicmodel> save = dbHandler.Get_saveplayList(false, openlist[which].toString().trim());

				for (Musicmodel e : save) {
					musiclist.add(e);
				}

				musicadapter.notifyDataSetChanged();

			}

		}).create().show();
	}

	private void displayalert() {
		// TODO Auto-generated method stub
		new AlertDialog.Builder(this).setTitle("Darg-and-drop a track onto the turntable ,then press play")
				.setPositiveButton("ok", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				}).show();
	}

	@Override
	public void onClick(View v) {
		List<Musicmodel> el = dbHandler.Get_playList(false);
		switch (v.getId()) {

		case R.id.leftdjround:
			leftdjst.reset();
			leftdjst("Scratch_Left.ogg");

			break;
		case R.id.righgtdjround:
			rightdjst.reset();
			rightdjst("Scratch_Right.ogg");
			break;

		case R.id.addnew:
			startActivity(new Intent(getcontext(), FileBrowserActivity.class));
			break;
		case R.id.newmenu:
			String[] s = getResources().getStringArray(R.array.setting_Option);

			new AlertDialog.Builder(getcontext()).setItems(s, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					switch (which) {
					case 0:
						dbHandler.Delete_All();
						musiclist.clear();
						musicadapter.notifyDataSetChanged();
						List<Musicmodel> ei = dbHandler.Get_playList(false);
						if (ei.size() == 0) {
							AssetManager assetManager = getApplicationContext().getAssets();
							String[] fields;
							try {
								fields = assetManager.list("sample");
								for (int count = 0; count < fields.length; count++) {

									Musicmodel m = new Musicmodel();
									m.name = fields[count];
									m.path = fields[count];

									musiclist.add(m);

								}
							} catch (IOException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}

						}
						break;

					case 1:
						musiclist.clear();
						musicadapter.notifyDataSetChanged();
						List<Musicmodel> el = dbHandler.Get_playList(true);
						for (Musicmodel e : el) {
							musiclist.add(e);
						}
						musicadapter.notifyDataSetChanged();

						break;

					case 2:

						showalert();

						break;

					case 3:
						openplaylist();

						break;
					}
				}

			}).create().show();

			break;
		case R.id.leftpalypause:
			if (leftdjname.getText().toString().equalsIgnoreCase("Drag Here")) {
				displayalert();
			} else {
				try {
					if (el.size() == 0) {
						if (isLeftPlaying) {
							isLeftPlaying = false;
							mpleft.pause();
							leftdjround.setAnimation(null);
							leftpalypause.setImageResource(R.drawable.play);
						} else {
							mpleft.reset();
							playleftmusic(leftfile.getName());
							// leftmpvol.postDelayed(onEverySecondLeft, 1000);
							// leftpalypause.setImageResource(R.drawable.puse);
							// leftdjround.startAnimation(anim);
							// isLeftPlaying = true;
						}

					} else {
						// playleftmusic(leftfile.getAbsolutePath());
						Log.e("isleft", " playing " + isLeftPlaying);
						if (isLeftPlaying) {
							isLeftPlaying = false;
							mpleft.pause();
							leftdjround.setAnimation(null);
							leftpalypause.setImageResource(R.drawable.play);
						} else {
							mpleft.start();
							// leftmpvol.postDelayed(onEverySecondLeft, 1000);
							leftpalypause.setImageResource(R.drawable.puse);
							leftdjround.startAnimation(anim);
							isLeftPlaying = true;
						}

					}
				} catch (IllegalStateException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			break;
		case R.id.rightpalypause:
			if (rightdjname.getText().toString().equalsIgnoreCase("Drag Here")) {
				displayalert();
			} else {

				if (el.size() == 0) {

					if (isRightPlaying) {
						isRightPlaying = false;
						mpright.pause();
						righgtdjround.setAnimation(null);
						rightpalypause.setImageResource(R.drawable.play);
					} else {
						mpright.reset();
						playrightmusic(rightfile.getName());
						// mpright.start();
						// rightmpvol.postDelayed(onEverySecondLeft, 1000);
						// rightpalypause.setImageResource(R.drawable.puse);
						// righgtdjround.startAnimation(anim);
						// isRightPlaying = true;
					}

				} else {
					// playrightmusic(rightfile.getAbsolutePath());
					if (isRightPlaying) {
						isRightPlaying = false;
						mpright.pause();
						righgtdjround.setAnimation(null);
						rightpalypause.setImageResource(R.drawable.play);
					} else {
						mpright.start();
						// rightmpvol.postDelayed(onEverySecondLeft, 1000);
						rightpalypause.setImageResource(R.drawable.puse);
						righgtdjround.startAnimation(anim);
						isRightPlaying = true;
					}
				}

			}
			break;
		case R.id.menubt:
			mDrawerLayout.openDrawer(Gravity.RIGHT);
			break;
		case R.id.rht_bt:
			if (rhtmp.isPlaying()) {
				rhtmp.pause();
				istrack1play = false;
				Log.e("", "pause song");
				rht_bt.setImageResource(R.drawable.play_bt);
			} else {
				if (ist1setdata) {
					istrack1play = true;
					rhtmp.start();
					Log.e("", "play song");
					rht_bt.setImageResource(R.drawable.pause_bt);
				} else {
					Log.e("", "play  first time song");
					rht_bt.setImageResource(R.drawable.pause_bt);
					playrht("t" + MusicLoopClass.mploo1 + ".ogg");
				}
			}
			break;
		case R.id.rht_prev:

			rhtmp.reset();
			int downname1 = Integer.valueOf(MusicLoopClass.mploo1) - 1;

			if (downname1 == 0) {
				MusicLoopClass.mploo1 = "15";
			} else {
				MusicLoopClass.mploo1 = downname1 + "";
			}

			rht_bt.setImageResource(R.drawable.pause_bt);
			rht_txt.setText("Rhythm" + MusicLoopClass.mploo1);
			playrht("t" + MusicLoopClass.mploo1 + ".ogg");
			break;

		case R.id.rht_next:
			rhtmp.reset();
			int upname1 = Integer.valueOf(MusicLoopClass.mploo1) + 1;
			if (upname1 == 16) {
				MusicLoopClass.mploo1 = "1";
			} else {
				MusicLoopClass.mploo1 = upname1 + "";
			}
			rht_bt.setImageResource(R.drawable.pause_bt);
			rht_txt.setText("Rhythm" + MusicLoopClass.mploo1);
			playrht("t" + MusicLoopClass.mploo1 + ".ogg");
			break;

		case R.id.sound_bt:
			// if (soundmp.isPlaying()) {
			// soundmp.pause();
			// issoundplay = false;
			// sound_bt.setImageResource(R.drawable.play_bt);
			// } else {
			// if (issounddata) {
			// issoundplay = true;
			// soundmp.start();
			// sound_bt.setImageResource(R.drawable.pause_bt);
			// } else {
			// sound_bt.setImageResource(R.drawable.pause_bt);
			// playsound(getsound(Integer.valueOf(MusicLoopClass.mpsound)) +
			// ".mp3");
			//
			// }
			// }
			break;
		case R.id.sound_prev:

			soundmp.reset();
			int downsound = Integer.valueOf(MusicLoopClass.mpsound) - 1;

			if (downsound == 0) {
				MusicLoopClass.mpsound = "4";
			} else {
				MusicLoopClass.mpsound = downsound + "";
			}
			sound_bt.setImageResource(R.drawable.pause_bt);
			sound_txt.setText(getsound(Integer.valueOf(MusicLoopClass.mpsound)));

			if (Integer.valueOf(MusicLoopClass.mpsound) == 1) {
				// playsound(getsound(Integer.valueOf(MusicLoopClass.mpsound)) +
				// ".wav");

			} else {
				// playsound(getsound(Integer.valueOf(MusicLoopClass.mpsound)) +
				// ".mp3");
			}
			break;

		case R.id.sound_next:
			soundmp.reset();
			int upsound = Integer.valueOf(MusicLoopClass.mpsound) + 1;
			if (upsound == 5) {
				MusicLoopClass.mpsound = "1";
			} else {
				MusicLoopClass.mpsound = upsound + "";
			}
			sound_bt.setImageResource(R.drawable.pause_bt);
			sound_txt.setText(getsound(Integer.valueOf(MusicLoopClass.mpsound)));

			if (Integer.valueOf(MusicLoopClass.mpsound) == 1) {
				// playsound(getsound(Integer.valueOf(MusicLoopClass.mpsound)) +
				// ".wav");

			} else {
				// playsound(getsound(Integer.valueOf(MusicLoopClass.mpsound)) +
				// ".mp3");
			}

			break;

		case R.id.metro_bt:
			if (metromp.isPlaying()) {
				metromp.pause();
				ismetroplay = false;
				metro_bt.setImageResource(R.drawable.play_bt);
			} else {
				if (issetmetrodata) {
					ismetroplay = true;
					metromp.start();
					metro_bt.setImageResource(R.drawable.pause_bt);
				} else {
					metro_bt.setImageResource(R.drawable.pause_bt);
					playmusicmetro("r" + MusicLoopClass.mpmetro + ".mp3");
				}
			}
			break;
		case R.id.metro_prev:
			metromp.reset();
			int downmetro = Integer.valueOf(MusicLoopClass.mpmetro) - 1;

			if (downmetro == 0) {
				MusicLoopClass.mpmetro = "10";
			} else {
				MusicLoopClass.mpmetro = downmetro + "";
			}

			metro_bt.setImageResource(R.drawable.pause_bt);
			metro_txt.setText(getbpm(Integer.valueOf(MusicLoopClass.mpmetro)));
			playmusicmetro("r" + Integer.valueOf(MusicLoopClass.mpmetro) + ".ogg");
			break;

		case R.id.metro_next:
			metromp.reset();
			int upmetro = Integer.valueOf(MusicLoopClass.mpmetro) + 1;
			if (upmetro == 11) {
				MusicLoopClass.mpmetro = "1";
			} else {
				MusicLoopClass.mpmetro = upmetro + "";
			}
			metro_bt.setImageResource(R.drawable.pause_bt);
			metro_txt.setText(getbpm(Integer.valueOf(MusicLoopClass.mpmetro)));
			playmusicmetro("r" + Integer.valueOf(MusicLoopClass.mpmetro) + ".ogg");
			break;

		case R.id.s1:
			playpad1("p1.ogg");
			break;
		case R.id.s2:
			playpad2("p2.ogg");
			break;
		case R.id.s3:
			playpad3("p3.ogg");
			break;
		case R.id.s4:
			playpad4("p4.ogg");
			break;
		case R.id.s5:
			playpad5("p5.ogg");
			break;
		case R.id.s6:
			playpad6("p6.ogg");
			break;
		case R.id.s7:
			playpad7("p7.ogg");
			break;
		case R.id.s8:
			playpad8("p8.ogg");
			break;

		case R.id.t1:
			setthemedefault();
			t1.setImageResource(R.drawable.a_color);
			theme.setBackground(getResources().getDrawable(R.drawable.back1));
			break;

		case R.id.t2:
			setthemedefault();
			t2.setImageResource(R.drawable.b_color);
			theme.setBackground(getResources().getDrawable(R.drawable.back2));
			break;

		case R.id.t3:
			setthemedefault();
			t3.setImageResource(R.drawable.c_color);
			theme.setBackground(getResources().getDrawable(R.drawable.back3));
			break;

		case R.id.t4:
			setthemedefault();
			t4.setImageResource(R.drawable.d_color);
			theme.setBackground(getResources().getDrawable(R.drawable.back4));
			break;

		case R.id.adddj:
			pianoview.setVisibility(View.GONE);
			djview.setVisibility(View.VISIBLE);
			
			addpiano.setImageResource(R.drawable.prevarrow);
			adddj.setImageResource(R.drawable.prevarrow_select);

			break;
		case R.id.addpiano:

			djview.setVisibility(View.GONE);
			pianoview.setVisibility(View.VISIBLE);
			addpiano.setImageResource(R.drawable.prevarrow_select);
			adddj.setImageResource(R.drawable.prevarrow);

			break;

		default:
			break;
		}
	}

	private void setthemedefault() {

		t1.setImageResource(R.drawable.a);
		t2.setImageResource(R.drawable.b);
		t3.setImageResource(R.drawable.c);
		t4.setImageResource(R.drawable.d);

	}

	private String getsound(int ins) {
		String name = "";
		switch (ins) {

		case 1:
			name = "Piano";
			break;
		case 4:
			name = "Steel Drums";
			break;
		case 2:
			name = "Pan Pipes";
			break;
		case 3:
			name = "String";
			break;
		default:
			break;
		}
		return name;
	}

	private String getbpm(int ins) {
		String name = "";
		switch (ins) {

		case 1:
			name = "Bpm100";
			break;

		case 2:
			name = "Bpm105";
			break;
		case 3:
			name = "Bpm110";
			break;
		case 4:
			name = "Bpm115";
			break;
		case 5:
			name = "Bpm120";
			break;
		case 6:
			name = "Bpm125";
			break;
		case 7:
			name = "Bpm130";
			break;
		case 8:
			name = "Bpm135";
			break;
		case 9:
			name = "Bpm140";
			break;
		case 10:
			name = "Bpm145";
			break;
		default:
			break;
		}
		return name;
	}

	private void showratedialog() {
		new AlertDialog.Builder(this).setTitle("Do you want to rate this application?")
				.setPositiveButton("Never", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						// continue with delete
						dialog.dismiss();
					}
				}).setNegativeButton("not now", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						// do nothing
						dialog.dismiss();
					}
				}).setNeutralButton("Rate Now", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						// do nothing
						startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id="
								+ "com.djmixerpiano.electrodjpiano")));
					}
				}).show();
	}

	@Override
	public void onBackPressed() {
		displayInterstitial();
		showclose();

	}

	private void showclose() {
		new AlertDialog.Builder(this).setTitle("Do you really want to shut down the app?")
				.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {

						setrelese();
						finish();
						return;
					}
				}).setNegativeButton("No", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				}).show();
	}

	private void setrelese() {

		mpleft.release();
		mpright.release();

		leftdjst.release();
		rightdjst.release();
		rhtmp.release();
		metromp.release();
		padmp1.release();
		padmp2.release();
		padmp3.release();
		padmp4.release();
		padmp5.release();
		padmp6.release();
		padmp7.release();
		padmp8.release();
		soundmp.release();
		keymp1.release();
		keymp2.release();
		keymp3.release();
		keymp4.release();
		keymp5.release();
		keymp6.release();
		keymp7.release();
		keymp8.release();
		keymp9.release();
		keymp10.release();
		keymp11.release();
		keymp12.release();
		keymp13.release();
		keymp14.release();
		keymp15.release();
		keymp16.release();
		keymp17.release();
		keymp18.release();
		keymp19.release();
		keymp20.release();
		keymp21.release();
		keymp22.release();
		keymp23.release();
		keymp0.release();
	}

	public void displayInterstitial() {
		// if (interstitial.isLoaded()) {

		adRequest = new AdRequest.Builder().build();
		adRequestdj = new AdRequest.Builder().build();

		adView.loadAd(adRequest);
		adViewdj.loadAd(adRequestdj);

		interstitial = new InterstitialAd(getcontext());
		interstitial.setAdUnitId("ca-app-pub-3805784166574868/6716451086");
		interstitial.loadAd(adRequest);
		interstitial.setAdListener(new AdListener() {
			public void onAdLoaded() {
				// displayInterstitial();
				interstitial.show();
			}
		});
	}

	class VisualizerView extends View {
		private byte[] mBytes;
		private float[] mPoints;
		private Rect mRect = new Rect();

		private Paint mForePaint = new Paint();

		public VisualizerView(Context context) {
			super(context);
			init();
		}

		private void init() {
			mBytes = null;

			mForePaint.setStrokeWidth(4f);
			mForePaint.setAntiAlias(true);
			mForePaint.setColor(Color.rgb(252, 193, 91));
		}

		public void updateVisualizer(byte[] bytes) {
			mBytes = bytes;
			// Log.e("bytes","by: " + bytes);
			invalidate();
		}

		@Override
		protected void onDraw(Canvas canvas) {
			super.onDraw(canvas);

			if (mBytes == null) {
				return;
			}

			if (mPoints == null || mPoints.length < mBytes.length * 4) {
				mPoints = new float[mBytes.length * 4];
			}

			mRect.set(0, 0, getWidth(), getHeight());

			for (int i = 0; i < mBytes.length - 1; i++) {
				mPoints[i * 4] = mRect.width() * i / (mBytes.length - 1);
				mPoints[i * 4 + 1] = mRect.height() / 2 + ((byte) (mBytes[i] + 128)) * (mRect.height() / 2) / 128;
				mPoints[i * 4 + 2] = mRect.width() * (i + 1) / (mBytes.length - 1);
				mPoints[i * 4 + 3] = mRect.height() / 2 + ((byte) (mBytes[i + 1] + 128)) * (mRect.height() / 2) / 128;
			}

			float centerX = mRect.width();
			float centerY = mRect.height();
			double angle = 90;
			Matrix rotateMat = new Matrix();
			rotateMat.setRotate((float) angle, centerX, centerY);
			// rotateMat.mapPoints(mPoints);

			canvas.drawLines(mPoints, mForePaint);

		}
	}
}

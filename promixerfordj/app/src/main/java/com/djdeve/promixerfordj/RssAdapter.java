package com.djdeve.promixerfordj;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.djdeve.promixerfordj.R;

public class RssAdapter extends BaseAdapter {
	public LayoutInflater inflater;
	public List<rssmodel> list;
	public Context con;

	public RssAdapter(Activity context, int playlistitem, ArrayList<rssmodel> musiclist) {

		con = context;
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		list = musiclist;
	}

	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	public rssmodel getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public class ViewHolder {
		TextView title;
		TextView date;
		TextView desc;
	}

	public View getView(int position, View convertview, ViewGroup parent) {
		// TODO Auto-generated method stub

		ViewHolder holder;
		if (convertview == null) {
			holder = new ViewHolder();
			convertview = inflater.inflate(R.layout.list_item, null);

			holder.title = (TextView) convertview.findViewById(R.id.title);
			holder.date = (TextView) convertview.findViewById(R.id.date);
			holder.desc = (TextView) convertview.findViewById(R.id.desc);
			convertview.setTag(holder);
		}

		holder = (ViewHolder) convertview.getTag();

		
		
		holder.title.setText(list.get(position).KEY_TITLE);
		holder.date.setText(list.get(position).KEY_DATE);
		holder.desc.setText(list.get(position).KEY_DESC);

		return convertview;
	}

}

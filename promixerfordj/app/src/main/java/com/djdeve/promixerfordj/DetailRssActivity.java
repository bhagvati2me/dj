package com.djdeve.promixerfordj;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.djdeve.promixerfordj.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class DetailRssActivity extends Activity {

	static final String KEY_TITLE = "title";
	static final String KEY_LINK = "link";
	static final String KEY_DESC = "description";
	static final String KEY_DATE = "pubDate";
	static final String KEY_GUID = "guid";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.single_list_item);

		AdRequest adRequest = new AdRequest.Builder().build();
		AdView adView = (AdView)findViewById(R.id.adView);
		adView.loadAd(adRequest);
		// getting intent data
		Intent in = getIntent();

		// Displaying all values on the screen
		TextView lblName = (TextView) findViewById(R.id.title);
		TextView lblCost = (TextView) findViewById(R.id.date);
		TextView lblDesc = (TextView) findViewById(R.id.desc);

		lblName.setText(in.getStringExtra(KEY_TITLE));
		lblCost.setText(in.getStringExtra(KEY_DATE));
		lblDesc.setText(in.getStringExtra(KEY_DESC));
	}
}

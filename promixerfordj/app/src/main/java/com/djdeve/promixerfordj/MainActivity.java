package com.djdeve.promixerfordj;

import java.io.IOException;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.djdeve.promixerfordj.R;

public class MainActivity extends Activity implements OnClickListener {

	ImageView up1, up2, up3, up4, up5, up6, up7, up8, dw1, dw2, dw3, dw4, dw5, dw6, dw7, dw8;
	Button fx1, fx2, fx3, fx4, fx5, fx6, fx7, fx8;
	TextView l1, l2, l3, l4, l5, l6, l7, l8;
	VerticalSeekBar vs1, vs2, vs3, vs4, vs5, vs6, vs7, vs8;
	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	ImageView b;
	private InterstitialAd interstitial;
	MediaPlayer mp1, mp2, mp3, mp4, mp5, mp6, mp7, mp8;

	boolean isfx1 = false, isfx2 = false, isfx3 = false, isfx4 = false, isfx5 = false, isfx6 = false, isfx7= false, isfx8= false;

	boolean ismp1play = false, ismp1mute = false;
	boolean ismp2play = false, ismp2mute = false;
	boolean ismp3play = false, ismp3mute = false;
	boolean ismp4play = false, ismp4mute = false;
	boolean ismp5play = false, ismp5mute = false;
	boolean ismp6play = false, ismp6mute = false;
	boolean ismp7play = false, ismp7mute = false;
	boolean ismp8play = false, ismp8mute = false;	
		

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		AdRequest adRequest = new AdRequest.Builder().build();
		AdView adView = (AdView) findViewById(R.id.adView);
		adView.loadAd(adRequest);
		interstitial = new InterstitialAd(MainActivity.this);
		interstitial.setAdUnitId("ca-app-pub-3805784166574868/5499572333");
		interstitial.loadAd(adRequest);

		interstitial.setAdListener(new AdListener() {
			public void onAdLoaded() {

				displayInterstitial();
			}

			@Override
			public void onAdOpened() {
				super.onAdOpened();

			}

		});

		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.navdrawer);
		b = (ImageView) findViewById(R.id.menubt);
		b.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				mDrawerLayout.openDrawer(Gravity.START);

			}
		});
		String[] values = new String[] { "Rate ", "Info- More Apps" };
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.menulisttext, android.R.id.text1, values);
		mDrawerList.setAdapter(adapter);
		mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Intent intent = new Intent(MainActivity.this, RssActivity.class);
				Intent intentweb = new Intent(MainActivity.this, WebBrowser.class);
				switch (position) {
				case 0:
					showratedialog();
					break;
				case 1:
					intentweb.putExtra("URL", "https://play.google.com/store/apps/details?id=com.djdeve.promixerfordj");
					startActivity(intentweb);
					mDrawerLayout.closeDrawer(Gravity.START);
					break;

				}

			}

		});

		up1 = (ImageView) findViewById(R.id.up1);
		up2 = (ImageView) findViewById(R.id.up2);
		up3 = (ImageView) findViewById(R.id.up3);
		up4 = (ImageView) findViewById(R.id.up4);
		up5 = (ImageView) findViewById(R.id.up5);
		up6 = (ImageView) findViewById(R.id.up6);
		up7 = (ImageView) findViewById(R.id.up7);
		up8 = (ImageView) findViewById(R.id.up8);
		
		dw1 = (ImageView) findViewById(R.id.dw1);
		dw2 = (ImageView) findViewById(R.id.dw2);
		dw3 = (ImageView) findViewById(R.id.dw3);
		dw4 = (ImageView) findViewById(R.id.dw4);
		dw5 = (ImageView) findViewById(R.id.dw5);
		dw6 = (ImageView) findViewById(R.id.dw6);
		dw7 = (ImageView) findViewById(R.id.dw7);
		dw8 = (ImageView) findViewById(R.id.dw8);

		fx1 = (Button) findViewById(R.id.fx1);
		fx2 = (Button) findViewById(R.id.fx2);
		fx3 = (Button) findViewById(R.id.fx3);
		fx4 = (Button) findViewById(R.id.fx4);
		fx5 = (Button) findViewById(R.id.fx5);
		fx6 = (Button) findViewById(R.id.fx6);
		fx7 = (Button) findViewById(R.id.fx7);
		fx8 = (Button) findViewById(R.id.fx8);

		l1 = (TextView) findViewById(R.id.l1);
		l2 = (TextView) findViewById(R.id.l2);
		l3 = (TextView) findViewById(R.id.l3);
		l4 = (TextView) findViewById(R.id.l4);
		l5 = (TextView) findViewById(R.id.l5);
		l6 = (TextView) findViewById(R.id.l6);
		l7 = (TextView) findViewById(R.id.l7);
		l8 = (TextView) findViewById(R.id.l8);

		vs1 = (VerticalSeekBar) findViewById(R.id.vs1);
		vs2 = (VerticalSeekBar) findViewById(R.id.vs2);
		vs3 = (VerticalSeekBar) findViewById(R.id.vs3);
		vs4 = (VerticalSeekBar) findViewById(R.id.vs4);
		vs5 = (VerticalSeekBar) findViewById(R.id.vs5);
		vs6 = (VerticalSeekBar) findViewById(R.id.vs6);
		vs7 = (VerticalSeekBar) findViewById(R.id.vs7);
		vs8 = (VerticalSeekBar) findViewById(R.id.vs8);

		up1.setOnClickListener(this);
		up2.setOnClickListener(this);
		up3.setOnClickListener(this);
		up4.setOnClickListener(this);
		up5.setOnClickListener(this);
		up6.setOnClickListener(this);
		up7.setOnClickListener(this);
		up8.setOnClickListener(this);

		dw1.setOnClickListener(this);
		dw2.setOnClickListener(this);
		dw3.setOnClickListener(this);
		dw4.setOnClickListener(this);
		dw5.setOnClickListener(this);
		dw6.setOnClickListener(this);
		dw7.setOnClickListener(this);
		dw8.setOnClickListener(this);

		fx1.setOnClickListener(this);
		fx2.setOnClickListener(this);
		fx3.setOnClickListener(this);
		fx4.setOnClickListener(this);
		fx5.setOnClickListener(this);
		fx6.setOnClickListener(this);
		fx7.setOnClickListener(this);
		fx8.setOnClickListener(this);

		mp1 = new MediaPlayer();
		mp2 = new MediaPlayer();
		mp3 = new MediaPlayer();
		mp4 = new MediaPlayer();
		mp5 = new MediaPlayer();
		mp6 = new MediaPlayer();
		mp7 = new MediaPlayer();
		mp8 = new MediaPlayer();

		vs1.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				float Rvol = (progress) / 100.0f;
				mp1.setVolume(Rvol, Rvol);
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {

			}
		});
		vs2.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				float Rvol = (progress) / 100.0f;
				mp2.setVolume(Rvol, Rvol);
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {

			}
		});
		vs3.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				float Rvol = (progress) / 100.0f;
				mp3.setVolume(Rvol, Rvol);
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {

			}
		});
		vs4.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				float Rvol = (progress) / 100.0f;
				mp4.setVolume(Rvol, Rvol);
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {

			}
		});
		vs5.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				float Rvol = (progress) / 100.0f;
				mp5.setVolume(Rvol, Rvol);
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {

			}
		});
		vs6.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				float Rvol = (progress) / 100.0f;
				mp6.setVolume(Rvol, Rvol);
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {

			}
		});
		vs7.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				float Rvol = (progress) / 100.0f;
				mp7.setVolume(Rvol, Rvol);
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {

			}
		});
		vs8.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				float Rvol = (progress) / 100.0f;
				mp8.setVolume(Rvol, Rvol);
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {

			}
		});

	}

	@SuppressLint("ResourceAsColor")
	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.up1:
			mp1.reset();
			int upname1 = Integer.valueOf(MusicLoopClass.mploo1) + 1;
			if (upname1 == 11) {
				MusicLoopClass.mploo1 = "1";
			} else {
				MusicLoopClass.mploo1 = upname1 + "";
			}
			ismp1play = false;
			l1.setText("FX" + MusicLoopClass.mploo1);
			playmusic1(MusicLoopClass.mploo1 + ".ogg");
			break;
		case R.id.up2:
			mp2.reset();

			int upname2 = Integer.valueOf(MusicLoopClass.mploo2) + 1;

			if (upname2 == 11) {
				MusicLoopClass.mploo2 = "1";
			} else {
				MusicLoopClass.mploo2 = upname2 + "";
			}

			ismp2play = false;
			l2.setText("FX" + MusicLoopClass.mploo2);
			playmusic2(MusicLoopClass.mploo2 + ".ogg");

			break;
		case R.id.up3:
			mp3.reset();
			int upname3 = Integer.valueOf(MusicLoopClass.mploo3) + 1;
			if (upname3 == 11) {
				MusicLoopClass.mploo3 = "1";
			} else {
				MusicLoopClass.mploo3 = upname3 + "";
			}
			ismp3play = false;
			l3.setText("FX" + MusicLoopClass.mploo3);
			playmusic3(MusicLoopClass.mploo3 + ".ogg");
			break;
		case R.id.up4:
			mp4.reset();
			int upname4 = Integer.valueOf(MusicLoopClass.mploo4) + 1;
			if (upname4 == 11) {
				MusicLoopClass.mploo4 = "1";
			} else {
				MusicLoopClass.mploo4 = upname4 + "";
			}
			ismp4play = false;
			l4.setText("FX" + MusicLoopClass.mploo4);
			playmusic4(MusicLoopClass.mploo4 + ".ogg");
			break;
		case R.id.up5:
			mp5.reset();
			int upname5 = Integer.valueOf(MusicLoopClass.mploo5) + 1;
			if (upname5 == 11) {
				MusicLoopClass.mploo5 = "1";
			} else {
				MusicLoopClass.mploo5 = upname5 + "";
			}
			ismp5play = false;
			l5.setText("FX" + MusicLoopClass.mploo5);
			playmusic5(MusicLoopClass.mploo5 + ".ogg");
			break;
		case R.id.up6:
			mp6.reset();
			int upname6 = Integer.valueOf(MusicLoopClass.mploo6) + 1;
			if (upname6 == 11) {
				MusicLoopClass.mploo6 = "1";
			} else {
				MusicLoopClass.mploo6 = upname6 + "";
			}
			ismp6play = false;
			l6.setText("FX" + MusicLoopClass.mploo6);
			playmusic6(MusicLoopClass.mploo6 + ".ogg");
			break;
		case R.id.up7:
			mp7.reset();
			int upname7 = Integer.valueOf(MusicLoopClass.mploo7) + 1;
			if (upname7 == 11) {
				MusicLoopClass.mploo7 = "1";
			} else {
				MusicLoopClass.mploo7 = upname7 + "";
			}
			ismp7play = false;
			l7.setText("FX" + MusicLoopClass.mploo7);
			playmusic7(MusicLoopClass.mploo7 + ".ogg");
			break;
		case R.id.up8:
			mp8.reset();
			int upname8 = Integer.valueOf(MusicLoopClass.mploo8) + 1;
			if (upname8 == 11) {
				MusicLoopClass.mploo8 = "1";
			} else {
				MusicLoopClass.mploo8 = upname8 + "";
			}
			ismp8play = false;
			l8.setText("FX" + MusicLoopClass.mploo8);
			playmusic8(MusicLoopClass.mploo8 + ".ogg");
			break;
		case R.id.dw1:
			mp1.reset();

			int downname1 = Integer.valueOf(MusicLoopClass.mploo1) - 1;

			if (downname1 == 0) {
				MusicLoopClass.mploo1 = "10";
			} else {
				MusicLoopClass.mploo1 = downname1 + "";
			}

			ismp1play = false;
			l1.setText("FX" + MusicLoopClass.mploo1);
			playmusic1(MusicLoopClass.mploo1 + ".ogg");
			break;
		case R.id.dw2:
			mp2.reset();

			int downname2 = Integer.valueOf(MusicLoopClass.mploo2) - 1;

			if (downname2 == 0) {
				MusicLoopClass.mploo2 = "10";
			} else {
				MusicLoopClass.mploo2 = downname2 + "";
			}

			ismp2play = false;
			l2.setText("FX" + MusicLoopClass.mploo2);
			playmusic2(MusicLoopClass.mploo2 + ".ogg");
			break;
		case R.id.dw3:
			mp3.reset();

			int downname3 = Integer.valueOf(MusicLoopClass.mploo3) - 1;

			if (downname3 == 0) {
				MusicLoopClass.mploo3 = "10";
			} else {
				MusicLoopClass.mploo3 = downname3 + "";
			}

			ismp3play = false;
			l3.setText("FX" + MusicLoopClass.mploo3);
			playmusic3(MusicLoopClass.mploo3 + ".ogg");
			break;
		case R.id.dw4:
			mp4.reset();

			int downname4 = Integer.valueOf(MusicLoopClass.mploo4) - 1;

			if (downname4 == 0) {
				MusicLoopClass.mploo4 = "10";
			} else {
				MusicLoopClass.mploo4 = downname4 + "";
			}

			ismp4play = false;
			l4.setText("FX" + MusicLoopClass.mploo4);
			playmusic4(MusicLoopClass.mploo4 + ".ogg");
			break;
		case R.id.dw5:
			mp5.reset();

			int downname5 = Integer.valueOf(MusicLoopClass.mploo5) - 1;

			if (downname5 == 0) {
				MusicLoopClass.mploo5 = "10";
			} else {
				MusicLoopClass.mploo5 = downname5 + "";
			}

			ismp5play = false;
			l5.setText("FX" + MusicLoopClass.mploo5);
			playmusic5(MusicLoopClass.mploo5 + ".ogg");
			break;
		case R.id.dw6:
			mp6.reset();

			int downname6 = Integer.valueOf(MusicLoopClass.mploo6) - 1;

			if (downname6 == 0) {
				MusicLoopClass.mploo6 = "10";
			} else {
				MusicLoopClass.mploo6 = downname6 + "";
			}

			ismp6play = false;
			l6.setText("FX" + MusicLoopClass.mploo6);
			playmusic6(MusicLoopClass.mploo6 + ".ogg");
			break;
		case R.id.dw7:
			mp7.reset();

			int downname7 = Integer.valueOf(MusicLoopClass.mploo7) - 1;

			if (downname7 == 0) {
				MusicLoopClass.mploo7 = "10";
			} else {
				MusicLoopClass.mploo7 = downname7 + "";
			}

			ismp7play = false;
			l7.setText("FX" + MusicLoopClass.mploo7);
			playmusic7(MusicLoopClass.mploo7 + ".ogg");
			break;			
		case R.id.dw8:
			mp8.reset();

			int downname8 = Integer.valueOf(MusicLoopClass.mploo8) - 1;

			if (downname8 == 0) {
				MusicLoopClass.mploo8 = "10";
			} else {
				MusicLoopClass.mploo8 = downname8 + "";
			}

			ismp8play = false;
			l8.setText("FX" + MusicLoopClass.mploo8);
			playmusic8(MusicLoopClass.mploo8 + ".ogg");
			break;

		case R.id.fx1:

			isfx1 = !isfx1;
			if (MusicLoopClass.mploo1.equals("1")) {
				playmusic1("1.ogg");
			}

			if (!mp1.isPlaying()) {
				mp1.start();
				fx1.setBackgroundColor(Color.rgb(163, 163, 163));
			} else {
				mp1.pause();
				fx1.setBackgroundColor(Color.rgb(0, 0, 0));
			}

			break;
		case R.id.fx2:
			isfx2 = !isfx2;
			if (MusicLoopClass.mploo2.equals("2")) {
				playmusic2("2.ogg");
			}

			if (!mp2.isPlaying()) {
				fx2.setBackgroundColor(Color.rgb(163, 163, 163));
				mp2.start();
			} else {
				mp2.pause();
				fx2.setBackgroundColor(Color.rgb(0, 0, 0));
			}

			break;

		case R.id.fx3:

			isfx3 = !isfx3;
			if (MusicLoopClass.mploo3.equals("3")) {
				playmusic3("3.ogg");
			}

			if (!mp3.isPlaying()) {
				fx3.setBackgroundColor(Color.rgb(163, 163, 163));
				mp3.start();
			} else {
				mp3.pause();
				fx3.setBackgroundColor(Color.rgb(0, 0, 0));
			}

			break;

		case R.id.fx4:
			isfx4 = !isfx4;
			if (MusicLoopClass.mploo4.equals("4")) {
				playmusic4("4.ogg");
			}

			if (!mp4.isPlaying()) {
				fx4.setBackgroundColor(Color.rgb(163, 163, 163));
				mp4.start();
			} else {
				mp4.pause();
				fx4.setBackgroundColor(Color.rgb(0, 0, 0));
			}

			break;

		case R.id.fx5:
			isfx5 = !isfx5;
			if (MusicLoopClass.mploo5.equals("5")) {
				playmusic5("5.ogg");
			}

			if (!mp5.isPlaying()) {
				fx5.setBackgroundColor(Color.rgb(163, 163, 163));
				mp5.start();
			} else {
				mp5.pause();
				fx5.setBackgroundColor(Color.rgb(0, 0, 0));
			}

			break;

		case R.id.fx6:
			isfx6 = !isfx6;
			if (MusicLoopClass.mploo6.equals("6")) {
				playmusic6("6.ogg");
			}

			if (!mp6.isPlaying()) {
				fx6.setBackgroundColor(Color.rgb(163, 163, 163));
				mp6.start();
			} else {
				mp6.pause();
				fx6.setBackgroundColor(Color.rgb(0, 0, 0));
			}

			break;
			
		case R.id.fx7:
			isfx7 = !isfx7;
			if (MusicLoopClass.mploo7.equals("7")) {
				playmusic7("7.ogg");
			}

			if (!mp7.isPlaying()) {
				fx7.setBackgroundColor(Color.rgb(163, 163, 163));
				mp7.start();
			} else {
				mp7.pause();
				fx7.setBackgroundColor(Color.rgb(0, 0, 0));
			}

			break;
			
		case R.id.fx8:
			isfx8 = !isfx8;
			if (MusicLoopClass.mploo8.equals("8")) {
				playmusic8("8.ogg");
			}

			if (!mp8.isPlaying()) {
				fx8.setBackgroundColor(Color.rgb(163, 163, 163));
				mp8.start();
			} else {
				mp8.pause();
				fx8.setBackgroundColor(Color.rgb(0, 0, 0));
			}

			break;

		default:
			break;
		}

	}

	public void playmusic1(String path) {

		// if (isfx1) {
		if (!ismp1play) {
			// TODO Auto-generated method stub
			try {

				AssetFileDescriptor descriptor = getAssets().openFd("music/" + path);
				mp1.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
				descriptor.close();

				mp1.prepare();
				mp1.setOnPreparedListener(new OnPreparedListener() {
					@Override
					public void onPrepared(MediaPlayer mp) {
						if (isfx1) {
							mp1.start();
						}
						mp1.setLooping(true);
						ismp1play = true;
					}
				});

			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} else {
			ismp1play = false;
			mp1.pause();

		}
		// }

	}

	public void playmusic2(String path) {
		if (!ismp2play) {
			// TODO Auto-generated method stub
			try {

				AssetFileDescriptor descriptor = getAssets().openFd("music/" + path);
				mp2.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
				descriptor.close();

				mp2.prepare();
				mp2.setOnPreparedListener(new OnPreparedListener() {
					@Override
					public void onPrepared(MediaPlayer mp) {

						if (isfx2) {
							mp2.start();
						}
						mp2.setLooping(true);
						ismp2play = true;
					}
				});

			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} else {
			ismp2play = false;
			mp2.pause();

		}
	}

	public void playmusic3(String path) {
		if (!ismp3play) {
			// TODO Auto-generated method stub
			try {

				AssetFileDescriptor descriptor = getAssets().openFd("music/" + path);
				mp3.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
				descriptor.close();

				mp3.prepare();
				mp3.setOnPreparedListener(new OnPreparedListener() {
					@Override
					public void onPrepared(MediaPlayer mp) {
						if (isfx3) {
							mp3.start();
						}
						mp3.setLooping(true);
						ismp3play = true;
					}
				});

			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} else {
			ismp3play = false;
			mp3.pause();

		}
	}

	public void playmusic4(String path) {
		if (!ismp4play) {
			// TODO Auto-generated method stub
			try {

				AssetFileDescriptor descriptor = getAssets().openFd("music/" + path);
				mp4.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
				descriptor.close();

				mp4.prepare();
				mp4.setOnPreparedListener(new OnPreparedListener() {
					@Override
					public void onPrepared(MediaPlayer mp) {
						if (isfx4) {
							mp4.start();
						}

						mp4.setLooping(true);
						ismp4play = true;
					}
				});

			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} else {
			ismp4play = false;
			mp4.pause();

		}
	}

	public void playmusic5(String path) {
		if (!ismp5play) {
			// TODO Auto-generated method stub
			try {

				AssetFileDescriptor descriptor = getAssets().openFd("music/" + path);
				mp5.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
				descriptor.close();

				mp5.prepare();
				mp5.setOnPreparedListener(new OnPreparedListener() {
					@Override
					public void onPrepared(MediaPlayer mp) {

						if (isfx5) {
							mp5.start();
						}
						mp5.setLooping(true);
						ismp5play = true;
					}
				});

			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} else {
			ismp5play = false;
			mp5.pause();

		}
	}

	public void playmusic6(String path) {
		if (!ismp6play) {
			// TODO Auto-generated method stub
			try {

				AssetFileDescriptor descriptor = getAssets().openFd("music/" + path);
				mp6.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
				descriptor.close();

				mp6.prepare();
				mp6.setOnPreparedListener(new OnPreparedListener() {
					@Override
					public void onPrepared(MediaPlayer mp) {
						if (isfx6) {
							mp6.start();
						}
						mp6.setLooping(true);
						ismp6play = true;
					}
				});

			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} else {
			ismp6play = false;
			mp6.pause();

		}
	}
	
	public void playmusic7(String path) {
		if (!ismp7play) {
			// TODO Auto-generated method stub
			try {

				AssetFileDescriptor descriptor = getAssets().openFd("music/" + path);
				mp7.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
				descriptor.close();

				mp7.prepare();
				mp7.setOnPreparedListener(new OnPreparedListener() {
					@Override
					public void onPrepared(MediaPlayer mp) {
						if (isfx7) {
							mp7.start();
						}
						mp7.setLooping(true);
						ismp7play = true;
					}
				});

			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} else {
			ismp7play = false;
			mp7.pause();

		}
	}
	
	public void playmusic8(String path) {
		if (!ismp8play) {
			// TODO Auto-generated method stub
			try {

				AssetFileDescriptor descriptor = getAssets().openFd("music/" + path);
				mp8.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
				descriptor.close();

				mp8.prepare();
				mp8.setOnPreparedListener(new OnPreparedListener() {
					@Override
					public void onPrepared(MediaPlayer mp) {
						if (isfx8) {
							mp8.start();
						}
						mp8.setLooping(true);
						ismp8play = true;
					}
				});

			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} else {
			ismp8play = false;
			mp8.pause();

		}
	}

	private void showratedialog() {
		new AlertDialog.Builder(this).setTitle("Do you want to rate this application?")
				.setPositiveButton("Never", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						// continue with delete
						dialog.dismiss();
					}
				}).setNegativeButton("not now", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						// do nothing
						dialog.dismiss();
					}
				}).setNeutralButton("Rate Now", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						// do nothing
						startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id="
								+ "com.djdeve.promixerfordj")));
					}
				}).show();
	}

	public void displayInterstitial() {
		if (interstitial.isLoaded()) {
			interstitial.show();
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub

		displayInterstitial();
		showclose();

	}

	private void showclose() {
		new AlertDialog.Builder(this).setTitle("Do you really want to shut down the app?")
				.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {

						mp1.release();
						mp2.release();
						mp3.release();
						mp4.release();
						mp5.release();
						mp6.release();
						mp7.release();
						mp8.release();
						
						
						finish();
						return;
					}
				}).setNegativeButton("No", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				}).show();
	}

}

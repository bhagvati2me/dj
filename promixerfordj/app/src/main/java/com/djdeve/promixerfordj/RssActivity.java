package com.djdeve.promixerfordj;

import java.util.ArrayList;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.djdeve.promixerfordj.R;

//<item>
//<title>Pioneer presents the XDJ-1000 player with touch screen</title>
//<link>http://www.gearjunkies.com/news/9547</link>
//<description>Meet the XDJ-1000: The digitally focused player with a large touch screen, a familiar club layout, and performace features inherited from Pioneer DJ\'s professional set-up.</description>
//<pubDate>Thu, 13 Nov 2014 17:51:20 GMT</pubDate>
//<guid>http://www.gearjunkies.com/news/9547</guid>
//</item>
public class RssActivity extends Activity {

	// All static variables
	// static final String URL =
	// "http://api.androidhive.info/pizza/?format=xml";
	static String URL ;
	// XML node keys
	static final String KEY_ITEM = "item"; // parent node

	static final String KEY_TITLE = "title";
	static final String KEY_LINK = "link";
	static final String KEY_DESC = "description";
	static final String KEY_DATE = "pubDate";
	static final String KEY_GUID = "guid";
	ListView listview;
	ArrayList<rssmodel> menuItems = new ArrayList<rssmodel>();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.rsslist);
		AdRequest adRequest = new AdRequest.Builder().build();
		AdView adView = (AdView)findViewById(R.id.adView);
		adView.loadAd(adRequest);
		Bundle b = getIntent().getExtras();
		String id = b.getString("URL");
		URL  = id;
		
		new RetrieveFeedTask().execute();

	}

	class RetrieveFeedTask extends AsyncTask<String, Void, NodeList> {

		XMLParser parser;
		ProgressDialog pbar;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pbar = ProgressDialog.show(RssActivity.this, "", "Please wait ...");
			pbar.show();
		}

		protected NodeList doInBackground(String... urls) {
			try {

				parser = new XMLParser();
				String xml = parser.getXmlFromUrl(URL);
				Document doc = parser.getDomElement(xml);

				NodeList nl = doc.getElementsByTagName(KEY_ITEM);

				return nl;
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}

		protected void onPostExecute(NodeList nl) {
			if (pbar != null) {
				pbar.dismiss();
				pbar = null;
			}
			rssmodel rs;
			for (int i = 0; i < nl.getLength(); i++) {
				Element e = (Element) nl.item(i);

				rs = new rssmodel();

				rs.KEY_TITLE = parser.getValue(e, KEY_TITLE);
				
				rs.KEY_LINK = parser.getValue(e, KEY_LINK);
				rs.KEY_DESC = parser.getValue(e, KEY_DESC);
				rs.KEY_DATE = parser.getValue(e, KEY_DATE);
				rs.KEY_GUID = parser.getValue(e, KEY_GUID);

				menuItems.add(rs);
			}
			showlist();
		}
	}

	private void showlist() {
		listview = (ListView) findViewById(R.id.listview);
		RssAdapter adapter = new RssAdapter(RssActivity.this, R.layout.list_item, menuItems);
		listview.setAdapter(adapter);
		
		listview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Intent in = new Intent(getApplicationContext(), DetailRssActivity.class);
				in.putExtra(KEY_TITLE, menuItems.get(position).KEY_TITLE);
				in.putExtra(KEY_DATE, menuItems.get(position).KEY_DATE);
				in.putExtra(KEY_DESC, menuItems.get(position).KEY_DESC);
				startActivity(in);

			}
		});

	}
}
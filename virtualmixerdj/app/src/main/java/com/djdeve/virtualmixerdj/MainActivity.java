package com.djdeve.virtualmixerdj;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaRecorder;
import android.media.audiofx.Equalizer;
import android.media.audiofx.Visualizer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.DragEvent;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.DragShadowBuilder;
import android.view.View.OnClickListener;
import android.view.View.OnDragListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.twobard.pianoview.Piano;
import com.twobard.pianoview.Piano.PianoKeyListener;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MainActivity extends Activity implements OnClickListener {

    ImageView lbutton1, lbutton2, lbutton3, lbutton4, lbutton5, lbutton6;
    // ImageView rbutton1, rbutton2, rbutton3, rbutton4, rbutton5, rbutton6;
    ImageView leftdjround, righgtdjround;
    ImageView addnew, newmenu;
    ImageView leftpalypause, rightpalypause, menu;
    // TextView leftmptxt1, leftmptxt2, rightmptxt1, rightmptxt2;

    VerticalSeekBar lvolseekbar, rvolseekbar;
    SeekBar leftmpvol, rightmpvol;
    TextView leftdjname, rightdjname;
    SeekBar lefteqlow, lefteqmedium, lefteqhigh, righteqlow, righteqmedium,
            righteqhigh;
    ListView playlist;
    // SeekBar lefttempo, righttempo;
    SeekBar mixvolseek;

    ArrayList<Musicmodel> musiclist = new ArrayList<Musicmodel>();
    MusicAdapter musicadapter;
    DatabaseHandler dbHandler = new DatabaseHandler(this);
    MediaPlayer mpleft, mpright;
    MediaPlayer mplbutton1, mplbutton2, mplbutton3, mplbutton4, mplbutton5,
            mplbutton6;
    MediaPlayer mprbutton1, mprbutton2, mprbutton3, mprbutton4, mprbutton5,
            mprbutton6;
    MediaPlayer leftdjst, rightdjst;

    File rightfile, leftfile;
    public float lvol = 0.5f;
    public float Rvol = 0.5f;
    public int cvol = 50;
    private RotateAnimation anim;
    private Equalizer mEqualizer1, mEqualizer2;
    final short band1 = 0;
    final short band2 = 0;
    final short band3 = 0;
    final short band4 = 0;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;

    private InterstitialAd interstitial;
    private VisualizerView mVisualizerView;
    private VisualizerView mVisualizerView2;

    ImageView screen1, screen2, screen3;
    View mscreen1, mscreen2, mscreen3;
    AdRequest adRequest;
    MediaRecorder mediaRecorder;
    ImageView start, pause;
    boolean isRecording = false;
    private static String audioFilePath;

    ImageView sapad1, sapad2, sapad3, sapad4, sapad5, sapad6, sapad7, sapad8,
            sapad9, sapad10, sapad11, sapad12, sapad13, sapad14, sapad15,
            sapad16, sapad17, sapad18, sapad19, sapad20, sapad21, sapad22,
            sapad23, sapad24, sapad25, sapad26, sapad27, sapad28, sapad29,
            sapad30, sapad31, sapad32, sapad33, sapad34, sapad35, sapad36,
            sapad37, sapad38, sapad39, sapad40, sapad42, sapad43, sapad44,
            sapad45;
    MediaPlayer mppad1, mppad2, mppad3, mppad4, mppad5, mppad6, mppad7, mppad8,
            mppad9, mppad10, mppad11, mppad12, mppad13, mppad14, mppad15,
            mppad16, mppad17, mppad18, mppad19, mppad20, mppad21, mppad22,
            mppad23, mppad24, mppad25, mppad26, mppad27, mppad28, mppad29,
            mppad30, mppad31, mppad32, mppad33, mppad34, mppad35, mppad36,
            mppad37, mppad38, mppad39, mppad40;

    String kitname = "sA1";

    Piano piano;
    ImageView rht_bt, rht_prev, rht_next, metro_bt, metro_prev, metro_next,
            sound_bt, sound_prev, sound_next;
    ImageView s1, s2, s3, s4, s5, s6, s7, s8;
    ImageView t1, t2, t3, t4;
    TextView rht_txt, metro_txt, sound_txt;
    MediaPlayer rhtmp, metromp, soundmp;
    MediaPlayer keymp1, keymp2, keymp3, keymp4, keymp5, keymp6, keymp7, keymp8,
            keymp9, keymp10, keymp11, keymp12, keymp13, keymp14, keymp15,
            keymp16, keymp17, keymp18, keymp19, keymp20, keymp21, keymp22,
            keymp23, keymp0;

    MediaPlayer padmp1, padmp2, padmp3, padmp4, padmp5, padmp6, padmp7, padmp8;
    View theme;
    SeekBar rht_seekbar, sound_seekbar;

    private String[] permissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO};

    @RequiresApi(api = Build.VERSION_CODES.M)
    private boolean arePermissionsEnabled() {
        for (String permission : permissions) {
            if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED)
                return false;
        }
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void requestMultiplePermissions() {
        List<String> remainingPermissions = new ArrayList<>();
        for (String permission : permissions) {
            if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                remainingPermissions.add(permission);
            }
        }
        requestPermissions(remainingPermissions.toArray(new String[remainingPermissions.size()]), 101);
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 101) {
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                    if (shouldShowRequestPermissionRationale(permissions[i])) {
                        new AlertDialog.Builder(this)
                                .setMessage("Please allow permissions to use record audio and choose local music from device")
                                .setPositiveButton("Allow", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        requestMultiplePermissions();
                                    }
                                })
                                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        requestMultiplePermissions();
                                    }
                                })
                                .create()
                                .show();
                    }
                    return;
                }
            }

            setupVisualizerFxAndUI();
            mVisualizer.setEnabled(true);
            setupVisualizerFxAndUI2();
            mVisualizer1.setEnabled(true);
            //====

        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.firstscrren);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if(!arePermissionsEnabled()) {
                requestMultiplePermissions();
            }
        }
        setuppap();
        setuppiano();
        screen1 = (ImageView) findViewById(R.id.screen1);
        screen2 = (ImageView) findViewById(R.id.screen2);
        screen3 = (ImageView) findViewById(R.id.screen3);

        mscreen1 = (View) findViewById(R.id.mscreen1);
        mscreen2 = (View) findViewById(R.id.mscreen2);
        mscreen3 = (View) findViewById(R.id.mscreen3);

        screen1.setOnClickListener(this);
        screen2.setOnClickListener(this);
        screen3.setOnClickListener(this);

        start = (ImageView) findViewById(R.id.start);
        pause = (ImageView) findViewById(R.id.pause);
        start.setOnClickListener(new OnClickListener() {

            @SuppressLint("SimpleDateFormat")
            @Override
            public void onClick(View v) {

                try {

                    if (!isRecording) {
                        String extStorageDirectory = Environment
                                .getExternalStorageDirectory()
                                .getAbsolutePath();
                        File folder = new File(extStorageDirectory,
                                getResources().getString(R.string.app_name));
                        if (!folder.exists()) {
                            folder.mkdir();
                            String timeStamp = new SimpleDateFormat(
                                    "yyyyMMddHHmmss").format(new Date());
                            audioFilePath = folder.getAbsolutePath() + "/"
                                    + timeStamp + ".ogg";
                        } else {
                            String timeStamp = new SimpleDateFormat(
                                    "yyyyMMddHHmmss").format(new Date());
                            audioFilePath = folder.getAbsolutePath() + "/"
                                    + timeStamp + ".ogg";

                        }
                        Toast.makeText(getactivity(), "recording stated",
                                Toast.LENGTH_SHORT).show();
                        PackageManager pmanager = getPackageManager();
                        if (pmanager
                                .hasSystemFeature(PackageManager.FEATURE_MICROPHONE)) {
                            mediaRecorder = new MediaRecorder();
                            mediaRecorder
                                    .setAudioSource(MediaRecorder.AudioSource.DEFAULT);
                            mediaRecorder
                                    .setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
                            mediaRecorder
                                    .setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
                            mediaRecorder.setOutputFile(audioFilePath);
                            mediaRecorder.prepare();
                            mediaRecorder.start();
                            isRecording = true;
                        }
                    } else {
                        Toast.makeText(getactivity(),
                                "recording  is already stated",
                                Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        pause.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                if (isRecording) {
                    isRecording = false;
                    Toast.makeText(getactivity(),
                            "music file saved in SD card  ", Toast.LENGTH_SHORT)
                            .show();
                    mediaRecorder.stop();
                    mediaRecorder.reset();
                    mediaRecorder.release();
                }

            }
        });

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mLinearLayout = (LinearLayout) findViewById(R.id.l1);
        mLinearLayout1 = (LinearLayout) findViewById(R.id.l2);

        mpleft = new MediaPlayer();
        mpright = new MediaPlayer();


        adRequest = new AdRequest.Builder().build();
        AdView adView = (AdView) findViewById(R.id.adView);
        adView.loadAd(adRequest);
        interstitial = new InterstitialAd(MainActivity.this);
        interstitial.setAdUnitId("ca-app-pub-3805784166574868/5798955232");
        interstitial.loadAd(adRequest);

        interstitial.setAdListener(new AdListener() {
            public void onAdLoaded() {
                displayInterstitial();
            }
        });
        mDrawerList = (ListView) findViewById(R.id.navdrawer);
        String[] values1 = new String[]{"Rate Now", "Info", "More Apps"};
        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1,
                values1);
        mDrawerList.setAdapter(adapter1);
        mDrawerList
                .setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {
                        Intent intentweb = new Intent(MainActivity.this,
                                WebBrowser.class);
                        switch (position) {
                            case 0:
                                showratedialog();
                                mDrawerLayout.closeDrawer(Gravity.START);
                                break;
                            case 1:
                                intentweb
                                        .putExtra("URL",
                                                "https://play.google.com/store/apps/details?id=com.djdeve.virtualmixerdj");
                                startActivity(intentweb);
                                mDrawerLayout.closeDrawer(Gravity.START);

                                break;
                            case 2:
                                intentweb
                                        .putExtra("URL",
                                                "https://play.google.com/store/apps/details?id=com.djdeve.virtualmixerdj");
                                startActivity(intentweb);
                                mDrawerLayout.closeDrawer(Gravity.START);

                                break;

                        }

                    }
                });

        mplbutton1 = new MediaPlayer();
        mplbutton2 = new MediaPlayer();
        mplbutton3 = new MediaPlayer();
        mplbutton4 = new MediaPlayer();
        mplbutton5 = new MediaPlayer();
        mplbutton6 = new MediaPlayer();
        mprbutton1 = new MediaPlayer();
        mprbutton2 = new MediaPlayer();
        mprbutton3 = new MediaPlayer();
        mprbutton4 = new MediaPlayer();
        mprbutton5 = new MediaPlayer();
        mprbutton6 = new MediaPlayer();
        leftdjst = new MediaPlayer();
        rightdjst = new MediaPlayer();

        lbutton1 = (ImageView) findViewById(R.id.lbutton1);
        lbutton2 = (ImageView) findViewById(R.id.lbutton2);
        lbutton3 = (ImageView) findViewById(R.id.lbutton3);
        lbutton4 = (ImageView) findViewById(R.id.lbutton4);
        lbutton5 = (ImageView) findViewById(R.id.lbutton5);
        lbutton6 = (ImageView) findViewById(R.id.lbutton6);
        // rbutton1 = (ImageView) findViewById(R.id.rbutton1);
        // rbutton2 = (ImageView) findViewById(R.id.rbutton2);
        // rbutton3 = (ImageView) findViewById(R.id.rbutton3);
        // rbutton4 = (ImageView) findViewById(R.id.rbutton4);
        // rbutton5 = (ImageView) findViewById(R.id.rbutton5);
        // rbutton6 = (ImageView) findViewById(R.id.rbutton6);
        leftdjround = (ImageView) findViewById(R.id.leftdjround);
        righgtdjround = (ImageView) findViewById(R.id.righgtdjround);
        addnew = (ImageView) findViewById(R.id.addnew);
        newmenu = (ImageView) findViewById(R.id.newmenu);
        leftpalypause = (ImageView) findViewById(R.id.leftpalypause);
        rightpalypause = (ImageView) findViewById(R.id.rightpalypause);

        // leftmptxt1 = (TextView) findViewById(R.id.leftmptxt1);
        // leftmptxt2 = (TextView) findViewById(R.id.leftmptxt2);
        // rightmptxt1 = (TextView) findViewById(R.id.rightmptxt1);
        // rightmptxt2 = (TextView) findViewById(R.id.rightmptxt2);

        lvolseekbar = (VerticalSeekBar) findViewById(R.id.lvolseekbar);
        rvolseekbar = (VerticalSeekBar) findViewById(R.id.rvolseekbar);

        leftmpvol = (SeekBar) findViewById(R.id.leftmpvol);
        rightmpvol = (SeekBar) findViewById(R.id.rightmpvol);
        leftdjname = (TextView) findViewById(R.id.leftdjname);
        rightdjname = (TextView) findViewById(R.id.rightdjname);

        lefteqlow = (SeekBar) findViewById(R.id.lefteqlow);
        lefteqmedium = (SeekBar) findViewById(R.id.lefteqmedium);
        lefteqhigh = (SeekBar) findViewById(R.id.lefteqhigh);
        righteqlow = (SeekBar) findViewById(R.id.righteqlow);
        righteqmedium = (SeekBar) findViewById(R.id.righteqmedium);
        righteqhigh = (SeekBar) findViewById(R.id.righteqhigh);

        // lefttempo = (SeekBar) findViewById(R.id.lefttempo);
        // righttempo = (SeekBar) findViewById(R.id.righttempo);
        mixvolseek = (SeekBar) findViewById(R.id.mixvolseek);

        playlist = (ListView) findViewById(R.id.playlist);
        menu = (ImageView) findViewById(R.id.menubt);
        lbutton1.setOnClickListener(this);
        lbutton2.setOnClickListener(this);
        lbutton3.setOnClickListener(this);
        lbutton4.setOnClickListener(this);
        lbutton5.setOnClickListener(this);
        lbutton6.setOnClickListener(this);
        // rbutton1.setOnClickListener(this);
        // rbutton2.setOnClickListener(this);
        // rbutton3.setOnClickListener(this);
        // rbutton4.setOnClickListener(this);
        // rbutton5.setOnClickListener(this);
        // rbutton6.setOnClickListener(this);
        leftdjround.setOnClickListener(this);
        righgtdjround.setOnClickListener(this);
        addnew.setOnClickListener(this);
        newmenu.setOnClickListener(this);
        leftpalypause.setOnClickListener(this);
        rightpalypause.setOnClickListener(this);
        menu.setOnClickListener(this);

        musicadapter = new MusicAdapter(getactivity(), R.layout.playlistitem,
                musiclist);
        playlist.setAdapter(musicadapter);
        playlist.setOnItemLongClickListener(new MyClickListener());
        playlist.setOnDragListener(new LeftDragListener());
        // playlist.setOnDragListener(new RightDragListener());
        findViewById(R.id.leftlayer).setOnDragListener(new LeftDragListener());
        findViewById(R.id.rightlayer)
                .setOnDragListener(new RightDragListener());

        anim = new RotateAnimation(0f, 350f, RotateAnimation.RELATIVE_TO_SELF,
                0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.5f);
        anim.setInterpolator(new LinearInterpolator());
        anim.setRepeatCount(Animation.INFINITE);
        anim.setDuration(700);

        mpleft.setOnCompletionListener(new OnCompletionListener() {

            @Override
            public void onCompletion(MediaPlayer mp) {

                mpleft.stop();
                mpleft.reset();
                // leftvol1.setText("0.00");
                // leftvol2.setText("0.00");
                leftdjname.setText("Drag Here");
                leftdjround.setAnimation(null);
                leftpalypause.setImageResource(R.drawable.puse);
                isLeftPlaying = false;

            }
        });
        mpright.setOnCompletionListener(new OnCompletionListener() {

            @Override
            public void onCompletion(MediaPlayer mp) {
                mpright.stop();
                mpright.reset();
                // rightvol1.setText("0.00");
                // rightvol2.setText("0.00");
                rightdjname.setText("Drag Here");
                righgtdjround.setAnimation(null);
                rightpalypause.setImageResource(R.drawable.puse);
                isRightPlaying = false;

            }
        });
        leftmpvol.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                if (isLeftPlaying) {
                    if (fromUser) {
                        mpleft.seekTo(progress);
                        updateLeftTime();
                    }
                }
            }
        });
        rightmpvol.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                if (isRightPlaying) {

                    if (fromUser) {
                        mpright.seekTo(progress);
                        updateRightTime();
                    }
                }

            }
        });
        lvolseekbar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                float Rvol = (progress) / 100.0f;
                mpleft.setVolume(Rvol, Rvol);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        rvolseekbar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                float Rvol = (progress) / 100.0f;
                mpright.setVolume(Rvol, Rvol);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        mixvolseek.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {

                if (cvol == progress) {
                    lvol = 0.5f;
                    Rvol = 0.5f;
                }

                lvol = (100 - progress) / 100.0f;
                Rvol = (progress) / 100.0f;

                mpleft.setVolume(lvol, lvol);

                mpright.setVolume(Rvol, Rvol);

            }
        });

        mEqualizer1 = new Equalizer(0, mpleft.getAudioSessionId());
        mEqualizer1.setEnabled(true);
        final short minEQLevel = mEqualizer1.getBandLevelRange()[0];
        final short maxEQLevel = mEqualizer1.getBandLevelRange()[1];

        lefteqlow.setMax(maxEQLevel - minEQLevel);
        // lefteqlow.setProgress((maxEQLevel - minEQLevel) / 2);

        lefteqlow
                .setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
                    public void onProgressChanged(SeekBar seekBar,
                                                  int progress, boolean fromUser) {
                        mEqualizer1.setBandLevel(band1,
                                (short) (progress + minEQLevel));

                    }

                    public void onStartTrackingTouch(SeekBar seekBar) {
                    }

                    public void onStopTrackingTouch(SeekBar seekBar) {
                    }
                });
        lefteqmedium.setMax(maxEQLevel - minEQLevel);
        // lefteqmedium.setProgress((maxEQLevel - minEQLevel) / 2);

        lefteqmedium
                .setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
                    public void onProgressChanged(SeekBar seekBar,
                                                  int progress, boolean fromUser) {
                        mEqualizer1.setBandLevel(band2,
                                (short) (progress + minEQLevel));

                    }

                    public void onStartTrackingTouch(SeekBar seekBar) {
                    }

                    public void onStopTrackingTouch(SeekBar seekBar) {
                    }
                });
        lefteqhigh.setMax(maxEQLevel - minEQLevel);
        // lefteqhigh.setProgress((maxEQLevel - minEQLevel) / 2);

        lefteqhigh
                .setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
                    public void onProgressChanged(SeekBar seekBar,
                                                  int progress, boolean fromUser) {
                        mEqualizer1.setBandLevel(band3,
                                (short) (progress + minEQLevel));

                    }

                    public void onStartTrackingTouch(SeekBar seekBar) {
                    }

                    public void onStopTrackingTouch(SeekBar seekBar) {
                    }
                });

        mEqualizer2 = new Equalizer(0, mpright.getAudioSessionId());
        mEqualizer2.setEnabled(true);
        final short minEQLevel2 = mEqualizer2.getBandLevelRange()[0];
        final short maxEQLevel2 = mEqualizer2.getBandLevelRange()[1];

        righteqlow.setMax(maxEQLevel2 - minEQLevel2);
        // righteqlow.setProgress((maxEQLevel2 - minEQLevel2) / 2);

        righteqlow
                .setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
                    public void onProgressChanged(SeekBar seekBar,
                                                  int progress, boolean fromUser) {
                        mEqualizer2.setBandLevel(band1,
                                (short) (progress + minEQLevel2));

                    }

                    public void onStartTrackingTouch(SeekBar seekBar) {
                    }

                    public void onStopTrackingTouch(SeekBar seekBar) {
                    }
                });
        righteqmedium.setMax(maxEQLevel2 - minEQLevel2);
        // righteqmedium.setProgress((maxEQLevel2 - minEQLevel2) / 2);

        righteqmedium
                .setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
                    public void onProgressChanged(SeekBar seekBar,
                                                  int progress, boolean fromUser) {
                        mEqualizer2.setBandLevel(band2,
                                (short) (progress + minEQLevel2));

                    }

                    public void onStartTrackingTouch(SeekBar seekBar) {
                    }

                    public void onStopTrackingTouch(SeekBar seekBar) {
                    }
                });
        righteqhigh.setMax(maxEQLevel2 - minEQLevel2);
        // righteqhigh.setProgress((maxEQLevel2 - minEQLevel2) / 2);

        righteqhigh
                .setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
                    public void onProgressChanged(SeekBar seekBar,
                                                  int progress, boolean fromUser) {
                        mEqualizer2.setBandLevel(band3,
                                (short) (progress + minEQLevel2));

                    }

                    public void onStartTrackingTouch(SeekBar seekBar) {
                    }

                    public void onStopTrackingTouch(SeekBar seekBar) {
                    }
                });
    }

    private void setuppiano() {

        rhtmp = new MediaPlayer();
        metromp = new MediaPlayer();
        padmp1 = new MediaPlayer();
        padmp2 = new MediaPlayer();
        padmp3 = new MediaPlayer();
        padmp4 = new MediaPlayer();
        padmp5 = new MediaPlayer();
        padmp6 = new MediaPlayer();
        padmp7 = new MediaPlayer();
        padmp8 = new MediaPlayer();

        soundmp = new MediaPlayer();
        keymp1 = new MediaPlayer();
        keymp2 = new MediaPlayer();
        keymp3 = new MediaPlayer();
        keymp4 = new MediaPlayer();
        keymp5 = new MediaPlayer();
        keymp6 = new MediaPlayer();
        keymp7 = new MediaPlayer();
        keymp8 = new MediaPlayer();
        keymp9 = new MediaPlayer();
        keymp10 = new MediaPlayer();
        keymp11 = new MediaPlayer();
        keymp12 = new MediaPlayer();
        keymp13 = new MediaPlayer();
        keymp14 = new MediaPlayer();
        keymp15 = new MediaPlayer();
        keymp16 = new MediaPlayer();
        keymp17 = new MediaPlayer();
        keymp18 = new MediaPlayer();
        keymp19 = new MediaPlayer();
        keymp20 = new MediaPlayer();
        keymp21 = new MediaPlayer();
        keymp22 = new MediaPlayer();
        keymp23 = new MediaPlayer();
        keymp0 = new MediaPlayer();
        rht_bt = (ImageView) findViewById(R.id.rht_bt);
        rht_prev = (ImageView) findViewById(R.id.rht_prev);
        rht_next = (ImageView) findViewById(R.id.rht_next);
        rht_txt = (TextView) findViewById(R.id.rht_txt);

        metro_bt = (ImageView) findViewById(R.id.metro_bt);
        metro_prev = (ImageView) findViewById(R.id.metro_prev);
        metro_next = (ImageView) findViewById(R.id.metro_next);
        metro_txt = (TextView) findViewById(R.id.metro_txt);

        sound_bt = (ImageView) findViewById(R.id.sound_bt);
        sound_prev = (ImageView) findViewById(R.id.sound_prev);
        sound_next = (ImageView) findViewById(R.id.sound_next);
        sound_txt = (TextView) findViewById(R.id.sound_txt);

        sound_txt.setText(getsound(1));
        s1 = (ImageView) findViewById(R.id.s1);
        s2 = (ImageView) findViewById(R.id.s2);
        s3 = (ImageView) findViewById(R.id.s3);
        s4 = (ImageView) findViewById(R.id.s4);
        s5 = (ImageView) findViewById(R.id.s5);
        s6 = (ImageView) findViewById(R.id.s6);
        s7 = (ImageView) findViewById(R.id.s7);
        s8 = (ImageView) findViewById(R.id.s8);

        t1 = (ImageView) findViewById(R.id.t1);
        t2 = (ImageView) findViewById(R.id.t2);
        t3 = (ImageView) findViewById(R.id.t3);
        t4 = (ImageView) findViewById(R.id.t4);

        theme = (View) findViewById(R.id.topbar);

        rht_bt.setOnClickListener(this);
        rht_prev.setOnClickListener(this);
        rht_next.setOnClickListener(this);

        metro_bt.setOnClickListener(this);
        metro_prev.setOnClickListener(this);
        metro_next.setOnClickListener(this);

        s1.setOnClickListener(this);
        s2.setOnClickListener(this);
        s3.setOnClickListener(this);
        s4.setOnClickListener(this);
        s5.setOnClickListener(this);
        s6.setOnClickListener(this);
        s7.setOnClickListener(this);
        s8.setOnClickListener(this);

        sound_bt.setOnClickListener(this);
        sound_prev.setOnClickListener(this);
        sound_next.setOnClickListener(this);

        t1.setOnClickListener(this);
        t2.setOnClickListener(this);
        t3.setOnClickListener(this);
        t4.setOnClickListener(this);

        rht_seekbar = (SeekBar) findViewById(R.id.rht_seekbar);
        sound_seekbar = (SeekBar) findViewById(R.id.sound_seekbar);

        rht_seekbar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                float soundvol = (progress) / 100.0f;
                rhtmp.setVolume(soundvol, soundvol);
            }
        });

        sound_seekbar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {

                float soundvol = (progress) / 100.0f;
                keymp1.setVolume(soundvol, soundvol);
                keymp2.setVolume(soundvol, soundvol);
                keymp3.setVolume(soundvol, soundvol);
                keymp4.setVolume(soundvol, soundvol);
                keymp5.setVolume(soundvol, soundvol);
                keymp6.setVolume(soundvol, soundvol);
                keymp7.setVolume(soundvol, soundvol);
                keymp8.setVolume(soundvol, soundvol);
                keymp9.setVolume(soundvol, soundvol);
                keymp10.setVolume(soundvol, soundvol);
                keymp11.setVolume(soundvol, soundvol);
                keymp12.setVolume(soundvol, soundvol);
                keymp13.setVolume(soundvol, soundvol);
                keymp14.setVolume(soundvol, soundvol);
                keymp15.setVolume(soundvol, soundvol);
                keymp16.setVolume(soundvol, soundvol);
                keymp17.setVolume(soundvol, soundvol);
                keymp18.setVolume(soundvol, soundvol);
                keymp19.setVolume(soundvol, soundvol);
                keymp20.setVolume(soundvol, soundvol);
                keymp21.setVolume(soundvol, soundvol);
                keymp22.setVolume(soundvol, soundvol);
                keymp23.setVolume(soundvol, soundvol);
                keymp0.setVolume(soundvol, soundvol);

            }
        });

        piano = (Piano) findViewById(R.id.pi);
        piano.setPianoKeyListener(new PianoKeyListener() {
            @Override
            public void keyPressed(int id, int action) {

                if (action == MotionEvent.ACTION_DOWN) {
                    Log.e("pino", "action:" + action + " id: " + id);

                    if (Integer.valueOf(MusicLoopClass.mpsound) == 1) {
                        play((getsound(Integer.valueOf(MusicLoopClass.mpsound))
                                + id + ".ogg"), id);

                    } else {
                        play((getsound(Integer.valueOf(MusicLoopClass.mpsound))
                                + id + ".ogg"), id);

                    }

                }
            }
        });

    }

    private void play(String sd, int id) {

        switch (id) {
            case 0:
                playkeyboard0(sd);
                break;
            case 1:
                playkeyboard1(sd);
                break;
            case 2:
                playkeyboard2(sd);
                break;
            case 3:
                playkeyboard3(sd);
                break;
            case 4:
                playkeyboard4(sd);
                break;
            case 5:
                playkeyboard5(sd);
                break;
            case 6:
                playkeyboard6(sd);
                break;
            case 7:
                playkeyboard7(sd);
                break;
            case 8:
                playkeyboard8(sd);
                break;
            case 9:
                playkeyboard9(sd);
                break;
            case 10:
                playkeyboard10(sd);
                break;
            case 11:
                playkeyboard11(sd);
                break;
            case 12:
                playkeyboard12(sd);
                break;
            case 13:
                playkeyboard13(sd);
                break;
            case 14:
                playkeyboard14(sd);
                break;
            case 15:
                playkeyboard15(sd);
                break;
            case 16:
                playkeyboard16(sd);
                break;
            case 17:
                playkeyboard17(sd);
                break;
            case 18:
                playkeyboard18(sd);
                break;
            case 19:
                playkeyboard19(sd);
                break;
            case 20:
                playkeyboard20(sd);
                break;
            case 21:
                playkeyboard21(sd);
                break;
            case 22:
                playkeyboard22(sd);
                break;
            case 23:
                playkeyboard23(sd);
                break;

            default:
                break;
        }

    }

    boolean ist1setdata = false, istrack1play = false;

    boolean issetmetrodata = false, ismetroplay = false;

    private void playmusicmetro(String musicname) {
        try {
            issetmetrodata = true;
            AssetFileDescriptor descriptor = getAssets().openFd(
                    "piano/" + musicname);
            metromp.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            metromp.prepare();
            metromp.setLooping(true);
            metromp.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    metromp.start();
                    ismetroplay = true;
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void setthemedefault() {

        t1.setImageResource(R.drawable.a);
        t2.setImageResource(R.drawable.b);
        t3.setImageResource(R.drawable.c);
        t4.setImageResource(R.drawable.d);

    }

    private String getsound(int ins) {
        String name = "";
        switch (ins) {

            case 1:
                name = "Piano";
                break;
            case 4:
                name = "Flute";
                break;
            case 2:
                name = "Organ";
                break;
            case 3:
                name = "Saxophone";
                break;
            default:
                break;
        }
        return name;
    }

    private String getbpm(int ins) {
        String name = "";
        switch (ins) {

            case 1:
                name = "bpm100";
                break;

            case 2:
                name = "bpm105";
                break;
            case 3:
                name = "bpm110";
                break;
            case 4:
                name = "bpm115";
                break;
            case 5:
                name = "bpm120";
                break;
            case 6:
                name = "bpm125";
                break;
            case 7:
                name = "bpm130";
                break;
            case 8:
                name = "bpm135";
                break;
            case 9:
                name = "bpm140";
                break;
            case 10:
                name = "bpm145";
                break;
            default:
                break;
        }
        return name;
    }

    private void playpad1(String musicname) {
        try {

            padmp1.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(
                    "piano/" + musicname);
            padmp1.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            padmp1.prepare();
            padmp1.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    padmp1.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playpad2(String musicname) {
        try {

            padmp2.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(
                    "piano/" + musicname);
            padmp2.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            padmp2.prepare();
            padmp2.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    padmp2.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playpad3(String musicname) {
        try {

            padmp3.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(
                    "piano/" + musicname);
            padmp3.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            padmp3.prepare();
            padmp3.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    padmp3.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playpad4(String musicname) {
        try {

            padmp4.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(
                    "piano/" + musicname);
            padmp4.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            padmp4.prepare();
            padmp4.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    padmp4.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playpad5(String musicname) {
        try {

            padmp5.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(
                    "piano/" + musicname);
            padmp5.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            padmp5.prepare();
            padmp5.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    padmp5.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playpad6(String musicname) {
        try {

            padmp6.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(
                    "piano/" + musicname);
            padmp6.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            padmp6.prepare();
            padmp6.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    padmp6.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playpad7(String musicname) {
        try {

            padmp7.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(
                    "piano/" + musicname);
            padmp7.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            padmp7.prepare();
            padmp7.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    padmp7.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playpad8(String musicname) {
        try {

            padmp8.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(
                    "piano/" + musicname);
            padmp8.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            padmp8.prepare();
            padmp8.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    padmp8.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playrht(String musicname) {
        try {
            ist1setdata = true;
            AssetFileDescriptor descriptor = getAssets().openFd(
                    "piano/" + musicname);
            rhtmp.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            rhtmp.prepare();
            rhtmp.setLooping(true);
            rhtmp.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    rhtmp.start();
                    istrack1play = true;
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playkeyboard1(String musicname) {
        try {

            keymp1.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(
                    "piano/" + musicname);
            keymp1.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            keymp1.prepare();
            keymp1.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    keymp1.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playkeyboard2(String musicname) {
        try {

            keymp2.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(
                    "piano/" + musicname);
            keymp2.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            keymp2.prepare();
            keymp2.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    keymp2.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playkeyboard3(String musicname) {
        try {

            keymp3.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(
                    "piano/" + musicname);
            keymp3.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            keymp3.prepare();
            keymp3.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    keymp3.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playkeyboard4(String musicname) {
        try {

            keymp4.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(
                    "piano/" + musicname);
            keymp4.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            keymp4.prepare();
            keymp4.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    keymp4.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playkeyboard5(String musicname) {
        try {

            keymp5.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(
                    "piano/" + musicname);
            keymp5.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            keymp5.prepare();
            keymp5.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    keymp5.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playkeyboard6(String musicname) {
        try {

            keymp6.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(
                    "piano/" + musicname);
            keymp6.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            keymp6.prepare();
            keymp6.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    keymp6.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playkeyboard7(String musicname) {
        try {

            keymp7.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(
                    "piano/" + musicname);
            keymp7.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            keymp7.prepare();
            keymp7.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    keymp7.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playkeyboard8(String musicname) {
        try {

            keymp8.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(
                    "piano/" + musicname);
            keymp8.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            keymp8.prepare();
            keymp8.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    keymp8.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playkeyboard9(String musicname) {
        try {

            keymp9.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(
                    "piano/" + musicname);
            keymp9.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            keymp9.prepare();
            keymp9.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    keymp9.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playkeyboard10(String musicname) {
        try {

            keymp10.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(
                    "piano/" + musicname);
            keymp10.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            keymp10.prepare();
            keymp10.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    keymp10.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playkeyboard11(String musicname) {
        try {

            keymp11.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(
                    "piano/" + musicname);
            keymp11.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            keymp11.prepare();
            keymp11.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    keymp11.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playkeyboard12(String musicname) {
        try {

            keymp12.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(
                    "piano/" + musicname);
            keymp12.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            keymp12.prepare();
            keymp12.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    keymp12.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playkeyboard13(String musicname) {
        try {

            keymp13.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(
                    "piano/" + musicname);
            keymp13.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            keymp13.prepare();
            keymp13.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    keymp13.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playkeyboard14(String musicname) {
        try {

            keymp14.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(
                    "piano/" + musicname);
            keymp14.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            keymp14.prepare();
            keymp14.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    keymp14.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playkeyboard15(String musicname) {
        try {

            keymp15.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(
                    "piano/" + musicname);
            keymp15.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            keymp15.prepare();
            keymp15.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    keymp15.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playkeyboard16(String musicname) {
        try {

            keymp16.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(
                    "piano/" + musicname);
            keymp16.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            keymp16.prepare();
            keymp16.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    keymp16.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playkeyboard17(String musicname) {
        try {

            keymp17.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(
                    "piano/" + musicname);
            keymp17.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            keymp17.prepare();
            keymp17.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    keymp17.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playkeyboard18(String musicname) {
        try {

            keymp18.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(
                    "piano/" + musicname);
            keymp18.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            keymp18.prepare();
            keymp18.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    keymp18.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playkeyboard19(String musicname) {
        try {

            keymp19.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(
                    "piano/" + musicname);
            keymp19.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            keymp19.prepare();
            keymp19.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    keymp19.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playkeyboard20(String musicname) {
        try {

            keymp20.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(
                    "piano/" + musicname);
            keymp20.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            keymp20.prepare();
            keymp20.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    keymp20.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playkeyboard21(String musicname) {
        try {

            keymp21.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(
                    "piano/" + musicname);
            keymp21.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            keymp21.prepare();
            keymp21.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    keymp21.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playkeyboard22(String musicname) {
        try {

            keymp22.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(
                    "piano/" + musicname);
            keymp22.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            keymp22.prepare();
            keymp22.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    keymp22.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playkeyboard23(String musicname) {
        try {

            keymp23.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(
                    "piano/" + musicname);
            keymp23.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            keymp23.prepare();
            keymp23.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    keymp23.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playkeyboard0(String musicname) {
        try {

            keymp0.reset();
            AssetFileDescriptor descriptor = getAssets().openFd(
                    "piano/" + musicname);

            keymp0.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            keymp0.prepare();
            keymp0.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    keymp0.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void setuppap() {

        mppad1 = new MediaPlayer();
        mppad2 = new MediaPlayer();
        mppad3 = new MediaPlayer();
        mppad4 = new MediaPlayer();
        mppad5 = new MediaPlayer();
        mppad6 = new MediaPlayer();
        mppad7 = new MediaPlayer();
        mppad8 = new MediaPlayer();
        mppad9 = new MediaPlayer();
        mppad10 = new MediaPlayer();
        mppad11 = new MediaPlayer();
        mppad12 = new MediaPlayer();
        mppad13 = new MediaPlayer();
        mppad14 = new MediaPlayer();
        mppad15 = new MediaPlayer();
        mppad16 = new MediaPlayer();
        mppad17 = new MediaPlayer();
        mppad18 = new MediaPlayer();
        mppad19 = new MediaPlayer();
        mppad20 = new MediaPlayer();
        mppad21 = new MediaPlayer();
        mppad22 = new MediaPlayer();
        mppad23 = new MediaPlayer();
        mppad24 = new MediaPlayer();
        mppad25 = new MediaPlayer();
        mppad26 = new MediaPlayer();
        mppad27 = new MediaPlayer();
        mppad28 = new MediaPlayer();
        mppad29 = new MediaPlayer();
        mppad30 = new MediaPlayer();
        mppad31 = new MediaPlayer();
        mppad32 = new MediaPlayer();
        mppad33 = new MediaPlayer();
        mppad34 = new MediaPlayer();
        mppad35 = new MediaPlayer();
        mppad36 = new MediaPlayer();
        mppad37 = new MediaPlayer();
        mppad38 = new MediaPlayer();
        mppad39 = new MediaPlayer();
        mppad40 = new MediaPlayer();
        sapad1 = (ImageView) findViewById(R.id.sapad1);
        sapad2 = (ImageView) findViewById(R.id.sapad2);
        sapad3 = (ImageView) findViewById(R.id.sapad3);
        sapad4 = (ImageView) findViewById(R.id.sapad4);
        sapad5 = (ImageView) findViewById(R.id.sapad5);
        sapad6 = (ImageView) findViewById(R.id.sapad6);
        sapad7 = (ImageView) findViewById(R.id.sapad7);
        sapad8 = (ImageView) findViewById(R.id.sapad8);
        sapad9 = (ImageView) findViewById(R.id.sapad9);
        sapad10 = (ImageView) findViewById(R.id.sapad10);
        sapad11 = (ImageView) findViewById(R.id.sapad11);
        sapad12 = (ImageView) findViewById(R.id.sapad12);
        sapad13 = (ImageView) findViewById(R.id.sapad13);
        sapad14 = (ImageView) findViewById(R.id.sapad14);
        sapad15 = (ImageView) findViewById(R.id.sapad15);
        sapad16 = (ImageView) findViewById(R.id.sapad16);
        sapad17 = (ImageView) findViewById(R.id.sapad17);
        sapad18 = (ImageView) findViewById(R.id.sapad18);
        sapad19 = (ImageView) findViewById(R.id.sapad19);
        sapad20 = (ImageView) findViewById(R.id.sapad20);
        sapad21 = (ImageView) findViewById(R.id.sapad21);
        sapad22 = (ImageView) findViewById(R.id.sapad22);
        sapad23 = (ImageView) findViewById(R.id.sapad23);
        sapad24 = (ImageView) findViewById(R.id.sapad24);
        sapad25 = (ImageView) findViewById(R.id.sapad25);
        sapad26 = (ImageView) findViewById(R.id.sapad26);
        sapad27 = (ImageView) findViewById(R.id.sapad27);
        sapad28 = (ImageView) findViewById(R.id.sapad28);
        sapad29 = (ImageView) findViewById(R.id.sapad29);
        sapad30 = (ImageView) findViewById(R.id.sapad30);
        sapad31 = (ImageView) findViewById(R.id.sapad31);
        sapad32 = (ImageView) findViewById(R.id.sapad32);
        sapad33 = (ImageView) findViewById(R.id.sapad33);
        sapad34 = (ImageView) findViewById(R.id.sapad34);
        sapad35 = (ImageView) findViewById(R.id.sapad35);
        sapad36 = (ImageView) findViewById(R.id.sapad36);
        sapad37 = (ImageView) findViewById(R.id.sapad37);
        sapad38 = (ImageView) findViewById(R.id.sapad38);
        sapad39 = (ImageView) findViewById(R.id.sapad39);
        sapad40 = (ImageView) findViewById(R.id.sapad40);

        sapad1.setOnClickListener(this);
        sapad2.setOnClickListener(this);
        sapad3.setOnClickListener(this);
        sapad4.setOnClickListener(this);
        sapad5.setOnClickListener(this);
        sapad6.setOnClickListener(this);
        sapad7.setOnClickListener(this);
        sapad8.setOnClickListener(this);
        sapad9.setOnClickListener(this);
        sapad10.setOnClickListener(this);
        sapad11.setOnClickListener(this);
        sapad12.setOnClickListener(this);
        sapad13.setOnClickListener(this);
        sapad14.setOnClickListener(this);
        sapad15.setOnClickListener(this);
        sapad16.setOnClickListener(this);
        sapad17.setOnClickListener(this);
        sapad18.setOnClickListener(this);
        sapad19.setOnClickListener(this);
        sapad20.setOnClickListener(this);
        sapad21.setOnClickListener(this);
        sapad22.setOnClickListener(this);
        sapad23.setOnClickListener(this);
        sapad24.setOnClickListener(this);
        sapad25.setOnClickListener(this);
        sapad26.setOnClickListener(this);
        sapad27.setOnClickListener(this);
        sapad28.setOnClickListener(this);
        sapad29.setOnClickListener(this);
        sapad30.setOnClickListener(this);
        sapad31.setOnClickListener(this);
        sapad32.setOnClickListener(this);
        sapad33.setOnClickListener(this);
        sapad34.setOnClickListener(this);
        sapad35.setOnClickListener(this);
        sapad36.setOnClickListener(this);
        sapad37.setOnClickListener(this);
        sapad38.setOnClickListener(this);
        sapad39.setOnClickListener(this);
        sapad40.setOnClickListener(this);
    }

    private void showratedialog() {
        new AlertDialog.Builder(this)
                .setTitle("Do you want to rate this application?")
                .setPositiveButton("Never",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // continue with delete
                                dialog.dismiss();
                            }
                        })
                .setNegativeButton("not now",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // do nothing
                                dialog.dismiss();
                            }
                        })
                .setNeutralButton("Rate Now",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // do nothing
                                startActivity(new Intent(Intent.ACTION_VIEW,
                                        Uri.parse("market://details?id="
                                                + "com.djdeve.virtualmixerdj")));
                            }
                        }).show();
    }

    public void displayInterstitial() {
        if (interstitial.isLoaded()) {
            interstitial.show();
        }
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub

        displayInterstitial();
        showclose();

    }

    private static final float VISUALIZER_HEIGHT_DIP = 50f;
    private Visualizer mVisualizer, mVisualizer1;
    private LinearLayout mLinearLayout, mLinearLayout1;

    private void setupVisualizerFxAndUI() {
        mVisualizerView = new VisualizerView(this);

        mVisualizerView.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.FILL_PARENT));

        mLinearLayout.addView(mVisualizerView);
        // if (mpleft.isPlaying()) {

        Log.e("mpleft", "seesion id " + mpleft.getAudioSessionId());
        mVisualizer = new Visualizer(mpleft.getAudioSessionId());

        mVisualizer.setCaptureSize(Visualizer.getCaptureSizeRange()[1]);
        mVisualizer.setDataCaptureListener(
                new Visualizer.OnDataCaptureListener() {
                    public void onWaveFormDataCapture(Visualizer visualizer,
                                                      byte[] bytes, int samplingRate) {
                        mVisualizerView.updateVisualizer(bytes);
                    }

                    public void onFftDataCapture(Visualizer visualizer,
                                                 byte[] bytes, int samplingRate) {

                        Log.e("", "fftc hange");
                    }
                }, Visualizer.getMaxCaptureRate() / 2, true, false);
        // }
    }

    private void setupVisualizerFxAndUI2() {
        mVisualizerView2 = new VisualizerView(this);

        mVisualizerView2.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.FILL_PARENT));

        mLinearLayout1.addView(mVisualizerView2);
        // if (mpleft.isPlaying()) {

        Log.e("mpleft", "seesion id " + mpleft.getAudioSessionId());
        mVisualizer1 = new Visualizer(mpright.getAudioSessionId());

        mVisualizer1.setCaptureSize(Visualizer.getCaptureSizeRange()[1]);
        mVisualizer1.setDataCaptureListener(
                new Visualizer.OnDataCaptureListener() {
                    public void onWaveFormDataCapture(Visualizer visualizer,
                                                      byte[] bytes, int samplingRate) {
                        mVisualizerView2.updateVisualizer(bytes);
                    }

                    public void onFftDataCapture(Visualizer visualizer,
                                                 byte[] bytes, int samplingRate) {

                        Log.e("", "fftc hange");
                    }
                }, Visualizer.getMaxCaptureRate() / 2, true, false);
        // }
    }

    private void setrelease() {
        mpleft.release();
        mpright.release();
        mplbutton1.release();
        mplbutton2.release();
        mplbutton3.release();
        mplbutton4.release();
        mplbutton5.release();
        mplbutton6.release();
        mprbutton1.release();
        mprbutton2.release();
        mprbutton3.release();
        mprbutton4.release();
        mprbutton5.release();
        mprbutton6.release();
        leftdjst.release();
        rightdjst.release();
        mppad1.release();
        mppad2.release();
        mppad3.release();
        mppad4.release();
        mppad5.release();
        mppad6.release();
        mppad7.release();
        mppad8.release();
        mppad9.release();
        mppad10.release();
        mppad11.release();
        mppad12.release();
        mppad13.release();
        mppad14.release();
        mppad15.release();
        mppad16.release();
        mppad17.release();
        mppad18.release();
        mppad19.release();
        mppad20.release();
        mppad21.release();
        mppad22.release();
        mppad23.release();
        mppad24.release();
        mppad25.release();
        mppad26.release();
        mppad27.release();
        mppad28.release();
        mppad29.release();
        mppad30.release();
        mppad31.release();
        mppad32.release();
        mppad33.release();
        mppad34.release();
        mppad35.release();
        mppad36.release();
        mppad37.release();
        mppad38.release();
        mppad39.release();
        mppad40.release();
        keymp1.release();
        keymp2.release();
        keymp3.release();
        keymp4.release();
        keymp5.release();
        keymp6.release();
        keymp7.release();
        keymp8.release();
        keymp9.release();
        keymp10.release();
        keymp11.release();
        keymp12.release();
        keymp13.release();
        keymp14.release();
        keymp15.release();
        keymp16.release();
        keymp17.release();
        keymp18.release();
        keymp19.release();
        keymp20.release();
        keymp21.release();
        keymp22.release();
        keymp23.release();
        keymp0.release();
    }

    public void playsoundpad1(String path) {
        try {

            mppad1.reset();
            AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
            mppad1.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mppad1.prepare();

            mppad1.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mppad1.start();
                }
            });

        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void playsoundpad2(String path) {
        try {
            mppad2.reset();
            AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
            mppad2.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mppad2.prepare();
            mppad2.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mppad) {
                    mppad2.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void playsoundpad3(String path) {
        try {
            mppad3.reset();
            AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
            mppad3.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mppad3.prepare();
            mppad3.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mppad) {
                    mppad3.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void playsoundpad4(String path) {
        try {
            mppad4.reset();
            AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
            mppad4.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mppad4.prepare();
            mppad4.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mppad) {
                    mppad4.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void playsoundpad5(String path) {
        try {
            mppad5.reset();
            AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
            mppad5.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mppad5.prepare();
            mppad5.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mppad) {
                    mppad5.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void playsoundpad6(String path) {
        try {
            mppad6.reset();
            AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
            mppad6.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mppad6.prepare();
            mppad6.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mppad) {
                    mppad6.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void playsoundpad7(String path) {
        try {
            mppad7.reset();
            AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
            mppad7.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mppad7.prepare();
            mppad7.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mppad) {
                    mppad7.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void playsoundpad8(String path) {
        try {
            mppad8.reset();
            AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
            mppad8.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mppad8.prepare();
            mppad8.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mppad) {
                    mppad8.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void playsoundpad9(String path) {
        try {
            mppad9.reset();
            AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
            mppad9.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mppad9.prepare();
            mppad9.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mppad) {
                    mppad9.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void playsoundpad10(String path) {
        try {
            mppad10.reset();
            AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
            mppad10.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mppad10.prepare();
            mppad10.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mppad) {
                    mppad10.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void playsoundpad11(String path) {
        try {
            mppad11.reset();
            AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
            mppad11.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mppad11.prepare();
            mppad11.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mppad) {
                    mppad11.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void playsoundpad12(String path) {
        try {
            mppad12.reset();
            AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
            mppad12.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mppad12.prepare();
            mppad12.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mppad) {
                    mppad12.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void playsoundpad13(String path) {
        try {
            mppad13.reset();
            AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
            mppad13.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mppad13.prepare();
            mppad13.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mppad) {
                    mppad13.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void playsoundpad14(String path) {
        try {
            mppad14.reset();
            AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
            mppad14.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mppad14.prepare();
            mppad14.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mppad) {
                    mppad14.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void playsoundpad15(String path) {
        try {
            mppad15.reset();
            AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
            mppad15.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mppad15.prepare();
            mppad15.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mppad) {
                    mppad15.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void playsoundpad16(String path) {
        try {
            mppad16.reset();
            AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
            mppad16.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mppad16.prepare();
            mppad16.setLooping(true);
            mppad16.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mppad) {
                    mppad16.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void playsoundpad17(String path) {
        try {
            mppad17.reset();
            AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
            mppad17.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mppad17.prepare();
            mppad17.setLooping(true);
            mppad17.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mppad) {
                    mppad17.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void playsoundpad18(String path) {
        try {
            mppad18.reset();
            AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
            mppad18.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mppad18.prepare();
            mppad18.setLooping(true);
            mppad18.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mppad) {
                    mppad18.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void playsoundpad19(String path) {
        try {
            mppad19.reset();
            AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
            mppad19.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mppad19.prepare();
            mppad19.setLooping(true);
            mppad19.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mppad) {
                    mppad19.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void playsoundpad20(String path) {
        try {
            mppad20.reset();
            AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
            mppad20.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mppad20.prepare();
            mppad20.setLooping(true);
            mppad20.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mppad) {
                    mppad20.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void playsoundpad21(String path) {
        try {
            mppad21.reset();
            AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
            mppad21.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mppad21.prepare();
            mppad21.setLooping(true);
            mppad21.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mppad) {
                    mppad21.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void playsoundpad22(String path) {
        try {
            mppad22.reset();
            AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
            mppad22.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mppad22.prepare();
            mppad22.setLooping(true);
            mppad22.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mppad) {
                    mppad22.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void playsoundpad23(String path) {
        try {
            mppad23.reset();
            AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
            mppad23.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mppad23.prepare();
            mppad23.setLooping(true);
            mppad23.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mppad) {
                    mppad23.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void playsoundpad24(String path) {
        try {
            mppad24.reset();
            AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
            mppad24.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mppad24.prepare();
            mppad24.setLooping(true);
            mppad24.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mppad) {
                    mppad24.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void playsoundpad25(String path) {
        try {
            mppad25.reset();
            AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
            mppad25.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mppad25.prepare();
            mppad25.setLooping(true);
            mppad25.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mppad) {
                    mppad25.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void playsoundpad26(String path) {
        try {
            mppad26.reset();
            AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
            mppad26.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mppad26.prepare();
            mppad26.setLooping(true);
            mppad26.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mppad) {
                    mppad26.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void playsoundpad27(String path) {
        try {
            mppad27.reset();
            AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
            mppad27.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mppad27.prepare();
            mppad27.setLooping(true);
            mppad27.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mppad) {
                    mppad27.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void playsoundpad28(String path) {
        try {
            mppad28.reset();
            AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
            mppad28.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mppad28.prepare();
            mppad28.setLooping(true);
            mppad28.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mppad) {
                    mppad28.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void playsoundpad29(String path) {
        try {
            mppad29.reset();
            AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
            mppad29.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mppad29.prepare();
            mppad29.setLooping(true);
            mppad29.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mppad) {
                    mppad29.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void playsoundpad30(String path) {
        try {
            mppad30.reset();
            AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
            mppad30.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mppad30.prepare();
            mppad30.setLooping(true);
            mppad30.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mppad) {
                    mppad30.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void playsoundpad31(String path) {
        try {
            mppad31.reset();
            AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
            mppad31.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mppad31.prepare();
            mppad31.setLooping(true);
            mppad31.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mppad) {
                    mppad31.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void playsoundpad32(String path) {
        try {
            mppad32.reset();
            AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
            mppad32.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mppad32.prepare();
            mppad32.setLooping(true);
            mppad32.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mppad) {
                    mppad32.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void playsoundpad33(String path) {
        try {
            mppad33.reset();
            AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
            mppad33.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mppad33.prepare();
            mppad33.setLooping(true);
            mppad33.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mppad) {
                    mppad33.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void playsoundpad34(String path) {
        try {
            mppad34.reset();
            AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
            mppad34.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mppad34.prepare();
            mppad34.setLooping(true);
            mppad34.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mppad) {
                    mppad34.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void playsoundpad35(String path) {
        try {
            mppad35.reset();
            AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
            mppad35.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mppad35.prepare();
            mppad35.setLooping(true);
            mppad35.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mppad) {
                    mppad35.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void playsoundpad36(String path) {
        try {
            mppad36.reset();
            AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
            mppad36.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mppad36.prepare();
            mppad36.setLooping(true);
            mppad36.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mppad) {
                    mppad36.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void playsoundpad37(String path) {
        try {
            mppad37.reset();
            AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
            mppad37.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mppad37.prepare();
            mppad37.setLooping(true);
            mppad37.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mppad) {
                    mppad37.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void playsoundpad38(String path) {
        try {
            mppad38.reset();
            AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
            mppad38.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mppad38.prepare();
            mppad38.setLooping(true);
            mppad38.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mppad) {
                    mppad38.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void playsoundpad39(String path) {
        try {
            mppad39.reset();
            AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
            mppad39.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mppad39.prepare();
            mppad39.setLooping(true);
            mppad39.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mppad) {
                    mppad39.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void playsoundpad40(String path) {
        try {
            mppad40.reset();
            AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
            mppad40.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mppad40.prepare();
            mppad40.setLooping(true);

            mppad40.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mppad) {
                    mppad40.start();
                }
            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void showclose() {
        new AlertDialog.Builder(this)
                .setTitle("Do you really want to shut down the app?")
                .setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {

                                setrelease();
                                finish();
                                return;
                            }
                        })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    @SuppressLint("NewApi")
    @Override
    public void onClick(View v) {
        List<Musicmodel> el = dbHandler.Get_playList(false);
        switch (v.getId()) {

            case R.id.screen1:
                adRequest = new AdRequest.Builder().build();
                interstitial = new InterstitialAd(MainActivity.this);
                interstitial.setAdUnitId("ca-app-pub-3805784166574868/5798955232");
                interstitial.loadAd(adRequest);

                interstitial.setAdListener(new AdListener() {
                    public void onAdLoaded() {
                        displayInterstitial();
                    }
                });
                mscreen2.setVisibility(View.GONE);
                mscreen3.setVisibility(View.GONE);
                mscreen1.setVisibility(View.VISIBLE);

                break;
            case R.id.screen2:
                adRequest = new AdRequest.Builder().build();
                interstitial = new InterstitialAd(MainActivity.this);
                interstitial.setAdUnitId("ca-app-pub-3805784166574868/5798955232");
                interstitial.loadAd(adRequest);

                interstitial.setAdListener(new AdListener() {
                    public void onAdLoaded() {
                        displayInterstitial();
                    }
                });
                mscreen1.setVisibility(View.GONE);
                mscreen3.setVisibility(View.GONE);
                mscreen2.setVisibility(View.VISIBLE);

                break;
            case R.id.screen3:
                adRequest = new AdRequest.Builder().build();
                interstitial = new InterstitialAd(MainActivity.this);
                interstitial.setAdUnitId("ca-app-pub-3805784166574868/5798955232");
                interstitial.loadAd(adRequest);

                interstitial.setAdListener(new AdListener() {
                    public void onAdLoaded() {
                        displayInterstitial();
                    }
                });
                mscreen1.setVisibility(View.GONE);
                mscreen2.setVisibility(View.GONE);
                mscreen3.setVisibility(View.VISIBLE);

                break;


            case R.id.rht_bt:
                if (rhtmp.isPlaying()) {
                    rhtmp.pause();
                    istrack1play = false;
                    Log.e("", "pause song");
                    rht_bt.setImageResource(R.drawable.play_bt);
                } else {
                    if (ist1setdata) {
                        istrack1play = true;
                        rhtmp.start();
                        Log.e("", "play song");
                        rht_bt.setImageResource(R.drawable.pause_bt);
                    } else {
                        Log.e("", "play  first time song");
                        rht_bt.setImageResource(R.drawable.pause_bt);
                        playrht("t" + MusicLoopClass.mploo1 + ".ogg");
                    }
                }
                break;
            case R.id.rht_prev:

                rhtmp.reset();
                int downname1 = Integer.valueOf(MusicLoopClass.mploo1) - 1;

                if (downname1 == 0) {
                    MusicLoopClass.mploo1 = "15";
                } else {
                    MusicLoopClass.mploo1 = downname1 + "";
                }

                rht_bt.setImageResource(R.drawable.pause_bt);
                rht_txt.setText("Track" + MusicLoopClass.mploo1);
                playrht("t" + MusicLoopClass.mploo1 + ".ogg");
                break;

            case R.id.rht_next:
                rhtmp.reset();
                int upname1 = Integer.valueOf(MusicLoopClass.mploo1) + 1;
                if (upname1 == 16) {
                    MusicLoopClass.mploo1 = "1";
                } else {
                    MusicLoopClass.mploo1 = upname1 + "";
                }
                rht_bt.setImageResource(R.drawable.pause_bt);
                rht_txt.setText("Track" + MusicLoopClass.mploo1);
                playrht("t" + MusicLoopClass.mploo1 + ".ogg");
                break;

            case R.id.sound_bt:
                // if (soundmp.isPlaying()) {
                // soundmp.pause();
                // issoundplay = false;
                // sound_bt.setImageResource(R.drawable.play_bt);
                // } else {
                // if (issounddata) {
                // issoundplay = true;
                // soundmp.start();
                // sound_bt.setImageResource(R.drawable.pause_bt);
                // } else {
                // sound_bt.setImageResource(R.drawable.pause_bt);
                // playsound(getsound(Integer.valueOf(MusicLoopClass.mpsound)) +
                // ".mp3");
                //
                // }
                // }
                break;
            case R.id.sound_prev:

                soundmp.reset();
                int downsound = Integer.valueOf(MusicLoopClass.mpsound) - 1;

                if (downsound == 0) {
                    MusicLoopClass.mpsound = "4";
                } else {
                    MusicLoopClass.mpsound = downsound + "";
                }
                sound_bt.setImageResource(R.drawable.pause_bt);
                sound_txt.setText(getsound(Integer.valueOf(MusicLoopClass.mpsound)));

                if (Integer.valueOf(MusicLoopClass.mpsound) == 1) {
                    // playsound(getsound(Integer.valueOf(MusicLoopClass.mpsound)) +
                    // ".wav");

                } else {
                    // playsound(getsound(Integer.valueOf(MusicLoopClass.mpsound)) +
                    // ".mp3");
                }
                break;

            case R.id.sound_next:
                soundmp.reset();
                int upsound = Integer.valueOf(MusicLoopClass.mpsound) + 1;
                if (upsound == 5) {
                    MusicLoopClass.mpsound = "1";
                } else {
                    MusicLoopClass.mpsound = upsound + "";
                }
                sound_bt.setImageResource(R.drawable.pause_bt);
                sound_txt.setText(getsound(Integer.valueOf(MusicLoopClass.mpsound)));

                if (Integer.valueOf(MusicLoopClass.mpsound) == 1) {
                    // playsound(getsound(Integer.valueOf(MusicLoopClass.mpsound)) +
                    // ".wav");

                } else {
                    // playsound(getsound(Integer.valueOf(MusicLoopClass.mpsound)) +
                    // ".mp3");
                }

                break;

            case R.id.metro_bt:
                if (metromp.isPlaying()) {
                    metromp.pause();
                    ismetroplay = false;
                    metro_bt.setImageResource(R.drawable.play_bt);
                } else {
                    if (issetmetrodata) {
                        ismetroplay = true;
                        metromp.start();
                        metro_bt.setImageResource(R.drawable.pause_bt);
                    } else {
                        metro_bt.setImageResource(R.drawable.pause_bt);
                        playmusicmetro("r" + MusicLoopClass.mpmetro + ".ogg");
                    }
                }
                break;
            case R.id.metro_prev:
                metromp.reset();
                int downmetro = Integer.valueOf(MusicLoopClass.mpmetro) - 1;

                if (downmetro == 0) {
                    MusicLoopClass.mpmetro = "10";
                } else {
                    MusicLoopClass.mpmetro = downmetro + "";
                }

                metro_bt.setImageResource(R.drawable.pause_bt);
                metro_txt.setText(getbpm(Integer.valueOf(MusicLoopClass.mpmetro)));
                playmusicmetro("r" + Integer.valueOf(MusicLoopClass.mpmetro) + ".ogg");
                break;

            case R.id.metro_next:
                metromp.reset();
                int upmetro = Integer.valueOf(MusicLoopClass.mpmetro) + 1;
                if (upmetro == 11) {
                    MusicLoopClass.mpmetro = "1";
                } else {
                    MusicLoopClass.mpmetro = upmetro + "";
                }
                metro_bt.setImageResource(R.drawable.pause_bt);
                metro_txt.setText(getbpm(Integer.valueOf(MusicLoopClass.mpmetro)));
                playmusicmetro("r" + Integer.valueOf(MusicLoopClass.mpmetro) + ".ogg");
                break;

            case R.id.s1:
                playpad1("p1.ogg");
                break;
            case R.id.s2:
                playpad2("p2.ogg");
                break;
            case R.id.s3:
                playpad3("p3.ogg");
                break;
            case R.id.s4:
                playpad4("p4.ogg");
                break;
            case R.id.s5:
                playpad5("p5.ogg");
                break;
            case R.id.s6:
                playpad6("p6.ogg");
                break;
            case R.id.s7:
                playpad7("p7.ogg");
                break;
            case R.id.s8:
                playpad8("p8.ogg");
                break;

            case R.id.t1:
                setthemedefault();
                t1.setImageResource(R.drawable.a_color);
                theme.setBackground(getResources().getDrawable(R.drawable.back1));
                break;

            case R.id.t2:
                setthemedefault();
                t2.setImageResource(R.drawable.b_color);
                theme.setBackground(getResources().getDrawable(R.drawable.back2));
                break;

            case R.id.t3:
                setthemedefault();
                t3.setImageResource(R.drawable.c_color);
                theme.setBackground(getResources().getDrawable(R.drawable.back3));
                break;

            case R.id.t4:
                setthemedefault();
                t4.setImageResource(R.drawable.d_color);
                theme.setBackground(getResources().getDrawable(R.drawable.back4));
                break;
            case R.id.sapad1:
                if (mppad1.isPlaying()) {
                    mppad1.stop();
                } else {
                    playsoundpad1(kitname + "1.ogg");
                }
                break;
            case R.id.sapad2:
                if (mppad2.isPlaying()) {
                    mppad2.stop();
                } else {
                    playsoundpad2(kitname + "2.ogg");
                }
                break;
            case R.id.sapad3:
                if (mppad3.isPlaying()) {
                    mppad3.stop();
                } else {
                    playsoundpad3(kitname + "3.ogg");
                }
                break;
            case R.id.sapad4:
                if (mppad4.isPlaying()) {
                    mppad4.stop();
                } else {
                    playsoundpad4(kitname + "4.ogg");
                }
                break;
            case R.id.sapad5:
                if (mppad5.isPlaying()) {
                    mppad5.stop();
                } else {
                    playsoundpad5(kitname + "5.ogg");
                }
                break;
            case R.id.sapad6:
                if (mppad6.isPlaying()) {
                    mppad6.stop();
                } else {
                    playsoundpad6(kitname + "6.ogg");
                }
                break;
            case R.id.sapad7:
                if (mppad7.isPlaying()) {
                    mppad7.stop();
                } else {
                    playsoundpad7(kitname + "7.ogg");
                }
                break;
            case R.id.sapad8:
                if (mppad8.isPlaying()) {
                    mppad8.stop();
                } else {
                    playsoundpad8(kitname + "8.ogg");
                }
                break;
            case R.id.sapad9:
                if (mppad9.isPlaying()) {
                    mppad9.stop();
                } else {
                    playsoundpad9(kitname + "9.ogg");
                }
                break;
            case R.id.sapad10:
                if (mppad10.isPlaying()) {
                    mppad10.stop();
                } else {
                    playsoundpad10(kitname + "10.ogg");
                }
                break;
            case R.id.sapad11:
                if (mppad11.isPlaying()) {
                    mppad11.stop();
                    sapad11.setBackground(getResources().getDrawable(
                            R.drawable.blue));
                } else {
                    playsoundpad11(kitname + "11.ogg");
                }
                break;
            case R.id.sapad12:
                if (mppad12.isPlaying()) {
                    mppad12.stop();
                } else {
                    playsoundpad12(kitname + "12.ogg");
                }
                break;
            case R.id.sapad13:
                if (mppad13.isPlaying()) {
                    mppad13.stop();
                } else {
                    playsoundpad13(kitname + "13.ogg");
                    sapad13.setBackground(getResources().getDrawable(
                            R.drawable.bluelight));
                }
                break;
            case R.id.sapad14:
                if (mppad14.isPlaying()) {
                    mppad14.stop();
                } else {
                    playsoundpad14(kitname + "14.ogg");

                }
                break;
            case R.id.sapad15:
                if (mppad15.isPlaying()) {
                    mppad15.stop();
                } else {
                    playsoundpad15(kitname + "15.ogg");

                }
                break;

            case R.id.sapad16:
                if (mppad16.isPlaying()) {
                    mppad16.stop();
                    sapad16.setBackground(getResources().getDrawable(
                            R.drawable.green));
                } else {
                    playsoundpad16(kitname + "16.ogg");
                    sapad16.setBackground(getResources().getDrawable(
                            R.drawable.greenlight));
                }
                break;
            case R.id.sapad17:
                if (mppad17.isPlaying()) {
                    mppad17.stop();
                    sapad17.setBackground(getResources().getDrawable(
                            R.drawable.green));
                } else {
                    playsoundpad17(kitname + "17.ogg");
                    sapad17.setBackground(getResources().getDrawable(
                            R.drawable.greenlight));
                }
                break;

            case R.id.sapad18:
                if (mppad18.isPlaying()) {
                    mppad18.stop();
                    sapad18.setBackground(getResources().getDrawable(
                            R.drawable.green));
                } else {
                    playsoundpad18(kitname + "18.ogg");
                    sapad18.setBackground(getResources().getDrawable(
                            R.drawable.greenlight));
                }
                break;
            case R.id.sapad19:
                if (mppad19.isPlaying()) {
                    mppad19.stop();
                    sapad19.setBackground(getResources().getDrawable(
                            R.drawable.green));
                } else {
                    playsoundpad19(kitname + "19.ogg");
                    sapad19.setBackground(getResources().getDrawable(
                            R.drawable.greenlight));
                }
                break;
            case R.id.sapad20:
                if (mppad20.isPlaying()) {
                    mppad20.stop();
                    sapad20.setBackground(getResources().getDrawable(
                            R.drawable.green));
                } else {
                    playsoundpad20(kitname + "20.ogg");
                    sapad20.setBackground(getResources().getDrawable(
                            R.drawable.greenlight));
                }
                break;
            case R.id.sapad21:
                if (mppad21.isPlaying()) {
                    mppad21.stop();
                    sapad21.setBackground(getResources().getDrawable(
                            R.drawable.green));
                } else {
                    playsoundpad21(kitname + "21.ogg");
                    sapad21.setBackground(getResources().getDrawable(
                            R.drawable.greenlight));
                }
                break;
            case R.id.sapad22:
                if (mppad22.isPlaying()) {
                    mppad22.stop();
                    sapad22.setBackground(getResources().getDrawable(
                            R.drawable.green));
                } else {
                    playsoundpad22(kitname + "22.ogg");
                    sapad22.setBackground(getResources().getDrawable(
                            R.drawable.greenlight));
                }
                break;
            case R.id.sapad23:
                if (mppad23.isPlaying()) {
                    mppad23.stop();
                    sapad23.setBackground(getResources().getDrawable(
                            R.drawable.green));
                } else {
                    playsoundpad23(kitname + "23.ogg");
                    sapad23.setBackground(getResources().getDrawable(
                            R.drawable.greenlight));

                }
                break;
            case R.id.sapad24:
                if (mppad24.isPlaying()) {
                    mppad24.stop();
                    sapad24.setBackground(getResources().getDrawable(
                            R.drawable.green));
                } else {
                    playsoundpad24(kitname + "24.ogg");
                    sapad24.setBackground(getResources().getDrawable(
                            R.drawable.greenlight));
                }
                break;
            case R.id.sapad25:
                if (mppad25.isPlaying()) {
                    mppad25.stop();
                    sapad25.setBackground(getResources().getDrawable(
                            R.drawable.green));
                } else {
                    playsoundpad25(kitname + "25.ogg");
                    sapad25.setBackground(getResources().getDrawable(
                            R.drawable.greenlight));
                }
                break;

            case R.id.sapad26:
                if (mppad26.isPlaying()) {
                    mppad26.stop();
                    sapad26.setBackground(getResources().getDrawable(
                            R.drawable.green));
                } else {
                    playsoundpad26(kitname + "26.ogg");
                    sapad26.setBackground(getResources().getDrawable(
                            R.drawable.greenlight));
                }
                break;
            case R.id.sapad27:
                if (mppad27.isPlaying()) {
                    mppad27.stop();
                    sapad27.setBackground(getResources().getDrawable(
                            R.drawable.green));
                } else {
                    playsoundpad27(kitname + "27.ogg");
                    sapad27.setBackground(getResources().getDrawable(
                            R.drawable.greenlight));
                }
                break;
            case R.id.sapad28:
                if (mppad28.isPlaying()) {
                    mppad28.stop();
                    sapad28.setBackground(getResources().getDrawable(
                            R.drawable.green));
                } else {
                    playsoundpad28(kitname + "28.ogg");
                    sapad28.setBackground(getResources().getDrawable(
                            R.drawable.greenlight));
                }
                break;
            case R.id.sapad29:
                if (mppad29.isPlaying()) {
                    mppad29.stop();
                    sapad29.setBackground(getResources().getDrawable(
                            R.drawable.green));
                } else {
                    playsoundpad29(kitname + "29.ogg");
                    sapad29.setBackground(getResources().getDrawable(
                            R.drawable.greenlight));
                }
                break;
            case R.id.sapad30:
                if (mppad30.isPlaying()) {
                    mppad30.stop();
                    sapad30.setBackground(getResources().getDrawable(
                            R.drawable.green));
                } else {
                    playsoundpad30(kitname + "30.ogg");
                    sapad30.setBackground(getResources().getDrawable(
                            R.drawable.greenlight));
                }
                break;
            case R.id.sapad31:
                if (mppad31.isPlaying()) {
                    mppad31.stop();
                    sapad31.setBackground(getResources().getDrawable(
                            R.drawable.green));
                } else {
                    playsoundpad31(kitname + "31.ogg");
                    sapad31.setBackground(getResources().getDrawable(
                            R.drawable.greenlight));
                }
                break;
            case R.id.sapad32:
                if (mppad32.isPlaying()) {
                    mppad32.stop();
                    sapad32.setBackground(getResources().getDrawable(
                            R.drawable.green));
                } else {
                    playsoundpad32(kitname + "32.ogg");
                    sapad32.setBackground(getResources().getDrawable(
                            R.drawable.greenlight));
                }
                break;
            case R.id.sapad33:
                if (mppad33.isPlaying()) {
                    mppad33.stop();
                    sapad33.setBackground(getResources().getDrawable(
                            R.drawable.green));
                } else {
                    playsoundpad33(kitname + "33.ogg");
                    sapad33.setBackground(getResources().getDrawable(
                            R.drawable.greenlight));
                }
                break;
            case R.id.sapad34:
                if (mppad34.isPlaying()) {
                    mppad34.stop();
                    sapad34.setBackground(getResources().getDrawable(
                            R.drawable.green));
                } else {
                    playsoundpad34(kitname + "34.ogg");
                    sapad34.setBackground(getResources().getDrawable(
                            R.drawable.greenlight));
                }
                break;
            case R.id.sapad35:
                if (mppad35.isPlaying()) {
                    mppad35.stop();
                    sapad35.setBackground(getResources().getDrawable(
                            R.drawable.green));
                } else {
                    playsoundpad35(kitname + "35.ogg");
                    sapad35.setBackground(getResources().getDrawable(
                            R.drawable.greenlight));
                }
                break;
            case R.id.sapad36:
                if (mppad36.isPlaying()) {
                    mppad36.stop();
                    sapad36.setBackground(getResources().getDrawable(
                            R.drawable.green));
                } else {
                    playsoundpad36(kitname + "36.ogg");
                    sapad36.setBackground(getResources().getDrawable(
                            R.drawable.greenlight));
                }
                break;
            case R.id.sapad37:
                if (mppad37.isPlaying()) {
                    mppad37.stop();
                    sapad37.setBackground(getResources().getDrawable(
                            R.drawable.green));
                } else {
                    playsoundpad37(kitname + "37.ogg");
                    sapad37.setBackground(getResources().getDrawable(
                            R.drawable.greenlight));
                }
                break;
            case R.id.sapad38:
                if (mppad38.isPlaying()) {
                    mppad38.stop();
                    sapad38.setBackground(getResources().getDrawable(
                            R.drawable.green));
                } else {
                    playsoundpad38(kitname + "38.ogg");
                    sapad38.setBackground(getResources().getDrawable(
                            R.drawable.greenlight));
                }
                break;
            case R.id.sapad39:
                if (mppad39.isPlaying()) {
                    mppad39.stop();
                    sapad39.setBackground(getResources().getDrawable(
                            R.drawable.green));
                } else {
                    playsoundpad39(kitname + "39.ogg");
                    sapad39.setBackground(getResources().getDrawable(
                            R.drawable.greenlight));
                }
                break;
            case R.id.sapad40:
                if (mppad40.isPlaying()) {
                    mppad40.stop();
                    sapad40.setBackground(getResources().getDrawable(
                            R.drawable.green));
                } else {
                    playsoundpad40(kitname + "40.ogg");
                    sapad40.setBackground(getResources().getDrawable(
                            R.drawable.greenlight));
                }
                break;

            case R.id.lbutton1:
                mplbutton1.reset();
                leftbutton1("leftpad1.ogg");
                break;
            case R.id.lbutton2:
                mplbutton2.reset();
                leftbutton2("leftpad2.ogg");
                break;
            case R.id.lbutton3:
                mplbutton3.reset();
                leftbutton3("leftpad3.ogg");
                break;
            case R.id.lbutton4:
                mplbutton4.reset();
                leftbutton4("leftpad4.ogg");
                break;
            case R.id.lbutton5:
                mplbutton5.reset();
                leftbutton5("leftpad5.ogg");
                break;
            case R.id.lbutton6:
                mplbutton6.reset();
                leftbutton6("leftpad6.ogg");
                break;
            // case R.id.rbutton1:
            // mprbutton1.reset();
            // rightbutton1("rightpad1.ogg");
            // break;
            // case R.id.rbutton2:
            // mprbutton2.reset();
            // rightbutton2("rightpad2.ogg");
            // break;
            // case R.id.rbutton3:
            // mprbutton3.reset();
            // rightbutton3("rightpad3.ogg");
            // break;
            // case R.id.rbutton4:
            // mprbutton4.reset();
            // rightbutton4("rightpad4.ogg");
            // break;
            // case R.id.rbutton5:
            // mprbutton5.reset();
            // rightbutton5("rightpad5.ogg");
            // break;
            // case R.id.rbutton6:
            // mprbutton6.reset();
            // rightbutton6("rightpad6.ogg");
            // break;
            case R.id.leftdjround:
                leftdjst.reset();
                leftdjst("Scratch_Left.ogg");

                break;
            case R.id.righgtdjround:
                rightdjst.reset();
                rightdjst("Scratch_Right.ogg");
                break;
            case R.id.addnew:
                startActivity(new Intent(getactivity(), FileBrowserActivity.class));
                break;
            case R.id.newmenu:
                String[] s = getResources().getStringArray(R.array.setting_Option);

                new AlertDialog.Builder(getactivity())
                        .setItems(s, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                switch (which) {
                                    case 0:
                                        dbHandler.Delete_All();
                                        musiclist.clear();
                                        musicadapter.notifyDataSetChanged();
                                        List<Musicmodel> ei = dbHandler
                                                .Get_playList(false);
                                        if (ei.size() == 0) {
                                            AssetManager assetManager = getApplicationContext()
                                                    .getAssets();
                                            String[] fields;
                                            try {
                                                fields = assetManager.list("sample");
                                                for (int count = 0; count < fields.length; count++) {

                                                    Musicmodel m = new Musicmodel();
                                                    m.name = fields[count];
                                                    m.path = fields[count];

                                                    musiclist.add(m);

                                                }
                                            } catch (IOException e1) {
                                                // TODO Auto-generated catch block
                                                e1.printStackTrace();
                                            }

                                        }
                                        break;

                                    case 1:
                                        musiclist.clear();
                                        musicadapter.notifyDataSetChanged();
                                        List<Musicmodel> el = dbHandler
                                                .Get_playList(true);
                                        for (Musicmodel e : el) {
                                            musiclist.add(e);
                                        }
                                        musicadapter.notifyDataSetChanged();

                                        break;

                                    case 2:

                                        showalert();

                                        break;

                                    case 3:
                                        openplaylist();

                                        break;
                                }
                            }

                        }).create().show();

                break;
            case R.id.leftpalypause:
                if (leftdjname.getText().toString().equalsIgnoreCase("Drag Here")) {
                    displayalert();
                } else {
                    try {
                        if (el.size() == 0) {
                            if (isLeftPlaying) {
                                isLeftPlaying = false;
                                mpleft.pause();
                                leftdjround.setAnimation(null);
                                leftpalypause.setImageResource(R.drawable.play);
                            } else {
                                mpleft.reset();
                                playleftmusic(leftfile.getName());
                                // leftmpvol.postDelayed(onEverySecondLeft, 1000);
                                // leftpalypause.setImageResource(R.drawable.puse);
                                // leftdjround.startAnimation(anim);
                                // isLeftPlaying = true;
                            }

                        } else {
                            // playleftmusic(leftfile.getAbsolutePath());
                            Log.e("isleft", " playing " + isLeftPlaying);
                            if (isLeftPlaying) {
                                isLeftPlaying = false;
                                mpleft.pause();
                                leftdjround.setAnimation(null);
                                leftpalypause.setImageResource(R.drawable.play);
                            } else {
                                mpleft.start();
                                leftmpvol.postDelayed(onEverySecondLeft, 1000);
                                leftpalypause.setImageResource(R.drawable.puse);
                                leftdjround.startAnimation(anim);
                                isLeftPlaying = true;
                            }

                        }
                    } catch (IllegalStateException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                break;
            case R.id.rightpalypause:
                if (rightdjname.getText().toString().equalsIgnoreCase("Drag Here")) {
                    displayalert();
                } else {

                    if (el.size() == 0) {

                        if (isRightPlaying) {
                            isRightPlaying = false;
                            mpright.pause();
                            righgtdjround.setAnimation(null);
                            rightpalypause.setImageResource(R.drawable.play);
                        } else {
                            mpright.reset();
                            playrightmusic(rightfile.getName());
                            // mpright.start();
                            // rightmpvol.postDelayed(onEverySecondLeft, 1000);
                            // rightpalypause.setImageResource(R.drawable.puse);
                            // righgtdjround.startAnimation(anim);
                            // isRightPlaying = true;
                        }

                    } else {
                        // playrightmusic(rightfile.getAbsolutePath());
                        if (isRightPlaying) {
                            isRightPlaying = false;
                            mpright.pause();
                            righgtdjround.setAnimation(null);
                            rightpalypause.setImageResource(R.drawable.play);
                        } else {
                            mpright.start();
                            rightmpvol.postDelayed(onEverySecondLeft, 1000);
                            rightpalypause.setImageResource(R.drawable.puse);
                            righgtdjround.startAnimation(anim);
                            isRightPlaying = true;
                        }
                    }

                }
                break;
            case R.id.menubt:
                mDrawerLayout.openDrawer(Gravity.START);
                break;

            default:
                break;
        }
    }

    private void openplaylist() {
        final String[] openlist = dbHandler.openlist();
        new AlertDialog.Builder(getactivity())
                .setItems(openlist, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        Log.e("list", openlist[which].toString());
                        musiclist.clear();
                        musicadapter.notifyDataSetChanged();
                        List<Musicmodel> save = dbHandler.Get_saveplayList(
                                false, openlist[which].toString().trim());

                        for (Musicmodel e : save) {
                            musiclist.add(e);
                        }

                        musicadapter.notifyDataSetChanged();

                    }

                }).create().show();

    }

    private void displayalert() {
        // TODO Auto-generated method stub
        new AlertDialog.Builder(this)
                .setTitle(
                        "Darg-and-drop a track onto the turntable ,then press play")
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    @SuppressLint("NewApi")
    class LeftDragListener implements OnDragListener {
        @Override
        public boolean onDrag(View v, DragEvent event) {

            switch (event.getAction()) {

                case DragEvent.ACTION_DRAG_STARTED:

                    break;

                case DragEvent.ACTION_DRAG_ENTERED:
                    break;

                case DragEvent.ACTION_DRAG_EXITED:
                    break;

                case DragEvent.ACTION_DROP:
                    // mpleft.reset();

                    ClipData.Item item = event.getClipData().getItemAt(0);
                    File leftfile1 = new File(item.getText().toString());
                    String name = leftfile1.getName();
                    Log.e("name : ", name);

                    leftfile = new File(item.getText().toString());

                    if (isLeftPlaying) {
                        showmusicchange(0, leftfile);
                    } else {
                        leftdjname.setText(name);
                        leftpalypause.setImageResource(R.drawable.play);
                        try {
                            playleftmusic(leftfile.getAbsolutePath());
                        } catch (IllegalStateException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }
                    break;

                case DragEvent.ACTION_DRAG_ENDED:
                    break;
                default:
                    break;
            }
            return true;
        }
    }

    class RightDragListener implements OnDragListener {
        @Override
        public boolean onDrag(View v, DragEvent event) {

            switch (event.getAction()) {

                case DragEvent.ACTION_DRAG_STARTED:

                    break;

                case DragEvent.ACTION_DRAG_ENTERED:
                    break;

                case DragEvent.ACTION_DRAG_EXITED:
                    break;

                case DragEvent.ACTION_DROP:
                    // mpright.reset();

                    ClipData.Item item = event.getClipData().getItemAt(0);
                    File rightfile1 = new File(item.getText().toString());
                    String name = rightfile1.getName();
                    Log.e("name : ", name);
                    rightfile = new File(item.getText().toString());

                    if (isRightPlaying) {
                        showmusicchange(1, rightfile);
                    } else {
                        rightdjname.setText(name);

                        rightpalypause.setImageResource(R.drawable.play);
                        try {
                            playrightmusic(rightfile.getAbsolutePath());
                        } catch (IllegalStateException e) {
                            e.printStackTrace();
                        }

                    }
                    break;

                case DragEvent.ACTION_DRAG_ENDED:

                    break;
                default:
                    break;
            }
            return true;
        }
    }

    private final class MyClickListener implements OnItemLongClickListener {

        @Override
        public boolean onItemLongClick(AdapterView<?> arg0, View view,
                                       int position, long arg3) {

            // String selectedFromList = "trdvm";
            String selectedFromList = musiclist.get(position).path;
            ClipData.Item item = new ClipData.Item(
                    ((CharSequence) selectedFromList.toString()));
            String[] clipDescription = {ClipDescription.MIMETYPE_TEXT_PLAIN};
            ClipData dragData = new ClipData("item", clipDescription, item);
            DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
            view.startDrag(dragData, shadowBuilder, view, 0);
            return true;
        }

    }

    private void showalert() {

        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle("Save PlayList");

        // Set an EditText view to get user input
        final EditText input = new EditText(this);
        alert.setView(input);

        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String value = input.getText().toString();
                if (!value.equals("")) {
                    dbHandler.Add_play(value.toString().trim());

                    for (int i = 0; i < musiclist.size(); i++) {
                        Musicmodel md = new Musicmodel();
                        md.name = musiclist.get(i).name;
                        md.path = musiclist.get(i).path;
                        Log.e("value", value);
                        dbHandler.Add_Saveplaylist(md, value.toString().trim());

                    }

                }
            }
        });

        alert.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        // Canceled.
                    }
                });

        alert.show();
    }

    private void showmusicchange(final int mp, final File name) {
        new AlertDialog.Builder(this)
                .setTitle("Do you want to stop current track?")
                .setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {

                                if (mp == 0) {
                                    Log.e("mpleft", "mpleft reset");
                                    mpleft.stop();
                                    mpleft.reset();
                                    leftdjname.setText(name.getName()
                                            .toString());
                                    leftpalypause
                                            .setImageResource(R.drawable.play);

                                    try {
                                        isLeftPlaying = false;
                                        playleftmusic(name.getAbsolutePath());
                                    } catch (IllegalStateException e) {
                                        e.printStackTrace();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }

                                } else {
                                    mpright.stop();
                                    mpright.reset();
                                    rightdjname.setText(name.getName()
                                            .toString());
                                    rightpalypause
                                            .setImageResource(R.drawable.play);
                                    isRightPlaying = false;
                                    playrightmusic(name.getAbsolutePath());
                                }

                            }
                        })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    private Runnable onEverySecondLeft = new Runnable() {
        @Override
        public void run() {
            if (true == runningLeft) {
                if (leftmpvol != null) {
                    leftmpvol.setProgress(mpleft.getCurrentPosition());
                }

                if (mpleft.isPlaying()) {
                    leftmpvol.postDelayed(onEverySecondLeft, 1000);
                    updateLeftTime();
                }
            }
        }
    };
    private Runnable onEverySecondRight = new Runnable() {
        @Override
        public void run() {
            if (true == runningRight) {
                if (rightmpvol != null) {
                    rightmpvol.setProgress(mpright.getCurrentPosition());
                }

                if (mpright.isPlaying()) {
                    rightmpvol.postDelayed(onEverySecondRight, 1000);
                    updateRightTime();
                }
            }
        }
    };

    private void updateLeftTime() {
        do {
            currentLeft = mpleft.getCurrentPosition();
            System.out.println("duration - " + durationLeft + " current- "
                    + currentLeft);
            int dSeconds = (int) (durationLeft / 1000) % 60;
            int dMinutes = (int) ((durationLeft / (1000 * 60)) % 60);
            int dHours = (int) ((durationLeft / (1000 * 60 * 60)) % 24);

            int cSeconds = (int) (currentLeft / 1000) % 60;
            int cMinutes = (int) ((currentLeft / (1000 * 60)) % 60);
            int cHours = (int) ((currentLeft / (1000 * 60 * 60)) % 24);

            if (dHours == 0) {
                // leftvol1.setText(String.format("%02d:%02d ", cMinutes,
                // cSeconds));
                // leftvol2.setText(String.format("%02d:%02d", dMinutes,
                // dSeconds));
            } else {
                // leftvol1.setText(String.format("%02d:%02d:%02d ", cHours,
                // cMinutes, cSeconds));
                // leftvol2.setText(String.format("%02d:%02d:%02d", dHours,
                // dMinutes, dSeconds));
            }
            try {
                Log.d("Value: ", String
                        .valueOf((int) (currentLeft * 100 / durationLeft)));
                if (leftmpvol.getProgress() >= 100) {
                    break;
                }
            } catch (Exception e) {
            }
        } while (leftmpvol.getProgress() <= 100);
    }

    private void updateRightTime() {
        do {
            currentRight = mpright.getCurrentPosition();
            int dSeconds = (int) (durationRight / 1000) % 60;
            int dMinutes = (int) ((durationRight / (1000 * 60)) % 60);
            int dHours = (int) ((durationRight / (1000 * 60 * 60)) % 24);

            int cSeconds = (int) (currentRight / 1000) % 60;
            int cMinutes = (int) ((currentRight / (1000 * 60)) % 60);
            int cHours = (int) ((currentRight / (1000 * 60 * 60)) % 24);

            if (dHours == 0) {
                // rightvol1.setText(String.format("%02d:%02d ", cMinutes,
                // cSeconds));
                // rightvol2.setText(String.format("%02d:%02d", dMinutes,
                // dSeconds));
            } else {
                // rightvol1.setText(String.format("%02d:%02d:%02d ", cHours,
                // cMinutes, cSeconds));
                // rightvol2.setText(String.format("%02d:%02d:%02d", dHours,
                // dMinutes, dSeconds));
            }

            try {
                if (rightmpvol.getProgress() >= 100) {
                    break;
                }
            } catch (Exception e) {
            }
        } while (rightmpvol.getProgress() <= 100);
    }

    boolean isLeftPlaying = false;
    boolean isRightPlaying = false;
    private int currentLeft = 0;
    private int currentRight = 0;
    private boolean runningLeft = true;
    private boolean runningRight = true;
    private int durationLeft = 0;
    private int durationRight = 0;

    // private void addLineRenderer() {
    // Paint linePaint = new Paint();
    // linePaint.setStrokeWidth(1f);
    // linePaint.setAntiAlias(true);
    // linePaint.setColor(Color.argb(88, 0, 128, 255));
    //
    // Paint lineFlashPaint = new Paint();
    // // lineFlashPaint.setStrokeWidth(1f);
    // // lineFlashPaint.setAntiAlias(true);
    // // lineFlashPaint.setColor(Color.argb(188, 255, 255, 255));
    // LineRenderer lineRenderer = new LineRenderer(linePaint, lineFlashPaint,
    // true);
    // mVisualizerView.addRenderer(lineRenderer);
    // }

    private void leftdjst(String path) {
        try {
            AssetFileDescriptor descriptor = getAssets()
                    .openFd("music/" + path);
            leftdjst.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            leftdjst.prepare();
            leftdjst.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    leftdjst.start();

                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void rightdjst(String path) {
        try {
            AssetFileDescriptor descriptor = getAssets()
                    .openFd("music/" + path);
            rightdjst.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            rightdjst.prepare();
            rightdjst.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    rightdjst.start();
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void leftbutton1(String path) {
        try {
            AssetFileDescriptor descriptor = getAssets()
                    .openFd("music/" + path);
            mplbutton1.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mplbutton1.prepare();
            mplbutton1.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mplbutton1.start();
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void leftbutton2(String path) {
        try {
            AssetFileDescriptor descriptor = getAssets()
                    .openFd("music/" + path);
            mplbutton2.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mplbutton2.prepare();
            mplbutton2.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mplbutton2.start();
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void leftbutton3(String path) {
        try {
            AssetFileDescriptor descriptor = getAssets()
                    .openFd("music/" + path);
            mplbutton3.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mplbutton3.prepare();
            mplbutton3.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mplbutton3.start();
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void leftbutton4(String path) {
        try {
            AssetFileDescriptor descriptor = getAssets()
                    .openFd("music/" + path);
            mplbutton4.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mplbutton4.prepare();
            mplbutton4.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mplbutton4.start();
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void leftbutton5(String path) {
        try {
            AssetFileDescriptor descriptor = getAssets()
                    .openFd("music/" + path);
            mplbutton5.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mplbutton5.prepare();
            mplbutton5.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mplbutton5.start();
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void leftbutton6(String path) {
        try {
            AssetFileDescriptor descriptor = getAssets()
                    .openFd("music/" + path);
            mplbutton6.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mplbutton6.prepare();
            mplbutton6.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mplbutton6.start();
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void rightbutton1(String path) {
        try {
            AssetFileDescriptor descriptor = getAssets()
                    .openFd("music/" + path);
            mprbutton1.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mprbutton1.prepare();
            mprbutton1.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mprbutton1.start();
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void rightbutton2(String path) {
        try {
            AssetFileDescriptor descriptor = getAssets()
                    .openFd("music/" + path);
            mprbutton2.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mprbutton2.prepare();
            mprbutton2.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mprbutton2.start();
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void rightbutton3(String path) {
        try {
            AssetFileDescriptor descriptor = getAssets()
                    .openFd("music/" + path);
            mprbutton3.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mprbutton3.prepare();
            mprbutton3.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mprbutton3.start();
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void rightbutton4(String path) {
        try {
            AssetFileDescriptor descriptor = getAssets()
                    .openFd("music/" + path);
            mprbutton4.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mprbutton4.prepare();
            mprbutton4.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mprbutton4.start();
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void rightbutton5(String path) {
        try {
            AssetFileDescriptor descriptor = getAssets()
                    .openFd("music/" + path);
            mprbutton5.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mprbutton5.prepare();
            mprbutton5.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mprbutton5.start();
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void rightbutton6(String path) {
        try {
            AssetFileDescriptor descriptor = getAssets()
                    .openFd("music/" + path);
            mprbutton6.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mprbutton6.prepare();
            mprbutton6.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mprbutton6.start();
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playleftmusic(String path) throws IllegalStateException,
            IOException {

        if (!isLeftPlaying) {
            try {

                List<Musicmodel> el = dbHandler.Get_playList(false);
                if (el.size() == 0) {
                    Log.e("leftpalying", "play " + path);
                    AssetFileDescriptor descriptor = getAssets().openFd(
                            "sample/" + path);
                    mpleft.setDataSource(descriptor.getFileDescriptor(),
                            descriptor.getStartOffset(), descriptor.getLength());
                    descriptor.close();
                } else {
                    mpleft.setDataSource(getApplicationContext(),
                            Uri.parse(path));
                }
                mpleft.prepare();
                mpleft.setOnPreparedListener(new OnPreparedListener() {

                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        durationLeft = mpleft.getDuration();
                        leftmpvol.setMax(durationLeft);
                        leftmpvol.postDelayed(onEverySecondLeft, 1000);
                    }
                });
                mpleft.start();

                leftmpvol.postDelayed(onEverySecondLeft, 1000);
                leftpalypause.setImageResource(R.drawable.puse);
                leftdjround.startAnimation(anim);
                isLeftPlaying = true;

            } catch (IllegalStateException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        } else {
            // isLeftPlaying = false;
            // mpleft.pause();
            // leftdjround.setAnimation(null);
            // leftpalypause.setImageResource(R.drawable.play);
        }
    }

    private void playrightmusic(String path) {

        if (!isRightPlaying) {
            // TODO Auto-generated method stub
            try {
                List<Musicmodel> el = dbHandler.Get_playList(false);
                if (el.size() == 0) {
                    Log.e("isRightPlaying", "play " + path);

                    AssetFileDescriptor descriptor = getAssets().openFd(
                            "sample/" + path);
                    mpright.setDataSource(descriptor.getFileDescriptor(),
                            descriptor.getStartOffset(), descriptor.getLength());
                    descriptor.close();

                } else {
                    mpright.setDataSource(getApplicationContext(),
                            Uri.parse(path));

                }
                mpright.prepare();
                mpright.setOnPreparedListener(new OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        durationRight = mpright.getDuration();
                        rightmpvol.setMax(durationRight);
                        rightmpvol.postDelayed(onEverySecondRight, 1000);

                    }
                });

                mpright.start();
                rightmpvol.postDelayed(onEverySecondRight, 1000);
                rightpalypause.setImageResource(R.drawable.puse);
                isRightPlaying = true;
                righgtdjround.startAnimation(anim);

            } catch (IllegalStateException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        } else {
            // isRightPlaying = false;
            // mpright.pause();
            // righgtdjround.setAnimation(null);
            // rightpalypause.setImageResource(R.drawable.play);

        }
    }

    private MainActivity getactivity() {
        return MainActivity.this;
    }

    @Override
    protected void onResume() {
        super.onResume();

        musiclist.clear();
        musicadapter.notifyDataSetChanged();
        List<Musicmodel> el = dbHandler.Get_playList(false);
        for (Musicmodel e : el) {
            musiclist.add(e);
        }
        musicadapter.notifyDataSetChanged();
        if (el.size() == 0) {
            // Field[] fields = R.raw.class.getFields();
            AssetManager assetManager = getApplicationContext().getAssets();
            String[] fields;
            try {
                fields = assetManager.list("sample");
                for (int count = 0; count < fields.length; count++) {

                    Musicmodel m = new Musicmodel();
                    m.name = fields[count];
                    m.path = fields[count];
                    musiclist.add(m);

                }
            } catch (IOException e1) {
                e1.printStackTrace();
            }

        }

    }

    class VisualizerView extends View {
        private byte[] mBytes;
        private float[] mPoints;
        private Rect mRect = new Rect();

        private Paint mForePaint = new Paint();

        public VisualizerView(Context context) {
            super(context);
            init();
        }

        private void init() {
            mBytes = null;

            mForePaint.setStrokeWidth(4f);
            mForePaint.setAntiAlias(true);
            mForePaint.setColor(Color.rgb(252, 193, 91));
        }

        public void updateVisualizer(byte[] bytes) {
            mBytes = bytes;
            // Log.e("bytes","by: " + bytes);
            invalidate();
        }

        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);

            if (mBytes == null) {
                return;
            }

            if (mPoints == null || mPoints.length < mBytes.length * 4) {
                mPoints = new float[mBytes.length * 4];
            }

            mRect.set(0, 0, getWidth(), getHeight());

            for (int i = 0; i < mBytes.length - 1; i++) {
                mPoints[i * 4] = mRect.width() * i / (mBytes.length - 1);
                mPoints[i * 4 + 1] = mRect.height() / 2
                        + ((byte) (mBytes[i] + 128)) * (mRect.height() / 2)
                        / 128;
                mPoints[i * 4 + 2] = mRect.width() * (i + 1)
                        / (mBytes.length - 1);
                mPoints[i * 4 + 3] = mRect.height() / 2
                        + ((byte) (mBytes[i + 1] + 128)) * (mRect.height() / 2)
                        / 128;
            }

            float centerX = mRect.width();
            float centerY = mRect.height();
            double angle = 90;
            Matrix rotateMat = new Matrix();
            rotateMat.setRotate((float) angle, centerX, centerY);
            // rotateMat.mapPoints(mPoints);

            canvas.drawLines(mPoints, mForePaint);

        }
    }
}

package com.djmix.djmixingpartynight.version1A;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHandler extends SQLiteOpenHelper {
	private static final int DATABASE_VERSION = 2;
	private static final String DATABASE_NAME = "music.sqlite";
	private static final String TABLE_PLAYLISt = "playlist";
	private static final String TABLE_SAVEPLAYLISt = "saveplaylist";
	private static final String TABLE_Play = "play";

	private static final String KEY_ID = "id";
	private static final String KEY_NAME = "name";
	private static final String KEY_PATH = "path";
	private static final String KEY_PLAYLISTNAME = "playlistname";
	private ArrayList<Musicmodel> play_list;

	public DatabaseHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		String CREATE_Play_TABLE = "CREATE TABLE " + TABLE_PLAYLISt + "("
				+ KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT,"
				+ KEY_PATH + " TEXT" + ")";
		db.execSQL(CREATE_Play_TABLE);

		String CREATE_SAVEPLAY_TABLE = "CREATE TABLE " + TABLE_SAVEPLAYLISt
				+ "(" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT,"
				+ KEY_PATH + " TEXT," + KEY_PLAYLISTNAME + " TEXT" + ")";
		db.execSQL(CREATE_SAVEPLAY_TABLE);

		String CREATE_play = "CREATE TABLE " + TABLE_Play + "(" + KEY_ID
				+ " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT" + ")";
		db.execSQL(CREATE_play);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_PLAYLISt);
		onCreate(db);
	}

	public void Add_playlist(Musicmodel mobj) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(KEY_NAME, mobj.name); // Contact email
		values.put(KEY_PATH, mobj.path); // Contact comment

		db.insert(TABLE_PLAYLISt, null, values);
		db.close();
	}

	public void Add_play(String name) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(KEY_NAME, name); // Contact email

		db.insert(TABLE_Play, null, values);
		db.close();
	}

	public void Add_Saveplaylist(Musicmodel mobj, String name) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(KEY_NAME, mobj.name); // Contact email
		values.put(KEY_PATH, mobj.path); // Contact comment
		values.put(KEY_PLAYLISTNAME, name);

		db.insert(TABLE_SAVEPLAYLISt, null, values);
		db.close();
	}

	public ArrayList<Musicmodel> Get_playList(boolean asc) {
		try {
			play_list = new ArrayList<Musicmodel>();
			String selectQuery;
			if (asc) {
				selectQuery = "SELECT  * FROM " + TABLE_PLAYLISt + " ORDER BY "
						+ KEY_NAME + " ASC ";
			} else {
				selectQuery = "SELECT  * FROM " + TABLE_PLAYLISt;
			}

			SQLiteDatabase db = this.getWritableDatabase();
			Cursor cursor = db.rawQuery(selectQuery, null);
			if (cursor.moveToFirst()) {
				do {
					Musicmodel feedobj = new Musicmodel();
					feedobj.id = cursor
							.getString(cursor.getColumnIndex(KEY_ID));
					feedobj.name = cursor.getString(cursor
							.getColumnIndex(KEY_NAME));
					feedobj.path = cursor.getString(cursor
							.getColumnIndex(KEY_PATH));
					play_list.add(feedobj);
				} while (cursor.moveToNext());
			}

			cursor.close();
			db.close();
			return play_list;
		} catch (Exception e) {
			Log.e("all_contact", "" + e);
		}

		return play_list;
	}

	public String[] openlist() {
		try {
			String selectQuery;
			selectQuery = "SELECT  * FROM " + TABLE_Play;

			SQLiteDatabase db = this.getWritableDatabase();
			Cursor cursor = db.rawQuery(selectQuery, null);
			String arraylist[] = new String[cursor.getCount()];
			if (cursor.moveToFirst()) {
				do {
					arraylist[cursor.getPosition()] = cursor.getString(cursor
							.getColumnIndex(KEY_NAME));
				} while (cursor.moveToNext());
			}

			cursor.close();
			db.close();
			return arraylist;
		} catch (Exception e) {
			Log.e("all_contact", "" + e);
		}
		return null;

	}

	public ArrayList<Musicmodel> Get_saveplayList(boolean asc, String name) {
		try {
			play_list = new ArrayList<Musicmodel>();
			String selectQuery;
			// if (asc) {
			// selectQuery = "SELECT  * FROM " + TABLE_SAVEPLAYLISt
			// + " where " + KEY_PLAYLISTNAME + " = " + name
			// + " ORDER BY " + KEY_NAME + " ASC ";
			// } else {
			// selectQuery = "SELECT  * FROM " + TABLE_SAVEPLAYLISt
			// + " where " + KEY_PLAYLISTNAME + " = " + name;
			// }
			selectQuery = "SELECT  * FROM " + TABLE_SAVEPLAYLISt;

			SQLiteDatabase db = this.getWritableDatabase();
			Cursor cursor = db.rawQuery(selectQuery, null);
			if (cursor.moveToFirst()) {
				do {
					Musicmodel feedobj = new Musicmodel();
					String playname = cursor.getString(cursor
							.getColumnIndex(KEY_PLAYLISTNAME));
					if (playname.equals(name)) {
						feedobj.id = cursor.getString(cursor
								.getColumnIndex(KEY_ID));
						feedobj.name = cursor.getString(cursor
								.getColumnIndex(KEY_NAME));
						feedobj.path = cursor.getString(cursor
								.getColumnIndex(KEY_PATH));
						Log.e("keyname", cursor.getString(cursor
								.getColumnIndex(KEY_PLAYLISTNAME)));
						play_list.add(feedobj);
					}
				} while (cursor.moveToNext());
			}

			cursor.close();
			db.close();
			return play_list;
		} catch (Exception e) {
			Log.e("all_contact", "" + e);
		}

		return play_list;
	}

	public void Delete_All() {
		SQLiteDatabase db = this.getWritableDatabase();

		db.delete(TABLE_PLAYLISt, null, null);
		db.close();
	}

}

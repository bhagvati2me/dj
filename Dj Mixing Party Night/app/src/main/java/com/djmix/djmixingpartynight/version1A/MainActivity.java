package com.djmix.djmixingpartynight.version1A;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetFileDescriptor;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.audiofx.Equalizer;
import android.media.audiofx.Visualizer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.DragShadowBuilder;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.djmix.djmixingpartynight.R;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity implements OnClickListener {

    ImageView lbutton1, lbutton2, lbutton3, lbutton4, lbutton5, lbutton6,
            lbutton7, lbutton8;
    ImageView rbutton1, rbutton2, rbutton3, rbutton4, rbutton5, rbutton6,
            rbutton7, rbutton8;
    ImageView leftdjround, righgtdjround;
    ImageView addnewleft, addnewright;
    Button leftpaly, leftpause, rightpaly, rightpause;
    ImageView menu;
    // TextView leftmptxt1, leftmptxt2, rightmptxt1, rightmptxt2;

    VerticalSeekBar lvolseekbar, rvolseekbar;
    SeekBar leftmpvol, rightmpvol;
    TextView leftdjname, rightdjname;
    SeekBar lefteqlow, lefteqmedium, lefteqhigh, righteqlow, righteqmedium,
            righteqhigh;
    // ListView playlist;
    // SeekBar lefttempo, righttempo;
    SeekBar mixvolseek;

    ArrayList<Musicmodel> musiclist = new ArrayList<Musicmodel>();
    MusicAdapter musicadapter;
    DatabaseHandler dbHandler = new DatabaseHandler(this);
    MediaPlayer mpleft, mpright;
    MediaPlayer mplbutton1, mplbutton2, mplbutton3, mplbutton4, mplbutton5,
            mplbutton6, mplbutton7, mplbutton8;
    MediaPlayer mprbutton1, mprbutton2, mprbutton3, mprbutton4, mprbutton5,
            mprbutton6, mprbutton7, mprbutton8;
    MediaPlayer leftdjst, rightdjst;
    View pad1layer, Eq1layer, discalayer, pad2layer, Eq2layer, discblayer;
    Button discA, Eq1, pad1, discB, Eq2, pad2;

    File rightfile, leftfile;
    public float lvol = 0.5f;
    public float Rvol = 0.5f;
    public int cvol = 50;
    private RotateAnimation anim;
    private Equalizer mEqualizer1, mEqualizer2;
    final short band1 = 0;
    final short band2 = 0;
    final short band3 = 0;
    final short band4 = 0;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;

    private InterstitialAd interstitial;
    private VisualizerView mVisualizerView;
    private VisualizerView mVisualizerView2;


    private String[] permissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO};

    @RequiresApi(api = Build.VERSION_CODES.M)
    private boolean arePermissionsEnabled() {
        for (String permission : permissions) {
            if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED)
                return false;
        }
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void requestMultiplePermissions() {
        List<String> remainingPermissions = new ArrayList<>();
        for (String permission : permissions) {
            if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                remainingPermissions.add(permission);
            }
        }
        requestPermissions(remainingPermissions.toArray(new String[remainingPermissions.size()]), 101);
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 101) {
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                    if (shouldShowRequestPermissionRationale(permissions[i])) {
                        new AlertDialog.Builder(this)
                                .setMessage("Please allow permissions to use record audio and choose local music from device")
                                .setPositiveButton("Allow", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        requestMultiplePermissions();
                                    }
                                })
                                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        requestMultiplePermissions();
                                    }
                                })
                                .create()
                                .show();
                    }
                    return;
                }
            }

            setupVisualizerFxAndUI();
            mVisualizer.setEnabled(true);
            setupVisualizerFxAndUI2();
            mVisualizer1.setEnabled(true);

//====


        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!arePermissionsEnabled()) {
                requestMultiplePermissions();
            }
        }


        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mLinearLayout = (LinearLayout) findViewById(R.id.l1);
        mLinearLayout1 = (LinearLayout) findViewById(R.id.l2);

        mpleft = new MediaPlayer();
        mpright = new MediaPlayer();


        AdRequest adRequest = new AdRequest.Builder().build();
        AdView adView = (AdView) findViewById(R.id.adView);
        adView.loadAd(adRequest);
        interstitial = new InterstitialAd(MainActivity.this);
        interstitial.setAdUnitId("ca-app-pub-3805784166574868/1268683287");
        interstitial.loadAd(adRequest);

        interstitial.setAdListener(new AdListener() {
            public void onAdLoaded() {
                displayInterstitial();
            }
        });
        mDrawerList = (ListView) findViewById(R.id.navdrawer);
        String[] values1 = new String[]{"Rate Now", "Info", "More Apps"};
        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1,
                values1);
        mDrawerList.setAdapter(adapter1);
        mDrawerList
                .setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {
                        Intent intentweb = new Intent(MainActivity.this,
                                WebBrowser.class);
                        switch (position) {
                            case 0:
                                showratedialog();
                                mDrawerLayout.closeDrawer(Gravity.START);
                                break;
                            case 1:
                                intentweb
                                        .putExtra("URL",
                                                "https://play.google.com/store/apps/details?id=com.punsoftware.mixer");
                                startActivity(intentweb);
                                mDrawerLayout.closeDrawer(Gravity.START);

                                break;
                            case 2:
                                intentweb
                                        .putExtra("URL",
                                                "https://play.google.com/store/apps/details?id=com.punsoftware.mixer");
                                startActivity(intentweb);
                                mDrawerLayout.closeDrawer(Gravity.START);

                                break;

                        }

                    }
                });

        mplbutton1 = new MediaPlayer();
        mplbutton2 = new MediaPlayer();
        mplbutton3 = new MediaPlayer();
        mplbutton4 = new MediaPlayer();
        mplbutton5 = new MediaPlayer();
        mplbutton6 = new MediaPlayer();
        mplbutton7 = new MediaPlayer();
        mplbutton8 = new MediaPlayer();

        mprbutton1 = new MediaPlayer();
        mprbutton2 = new MediaPlayer();
        mprbutton3 = new MediaPlayer();
        mprbutton4 = new MediaPlayer();
        mprbutton5 = new MediaPlayer();
        mprbutton6 = new MediaPlayer();
        mprbutton7 = new MediaPlayer();
        mprbutton8 = new MediaPlayer();
        leftdjst = new MediaPlayer();
        rightdjst = new MediaPlayer();

        lbutton1 = (ImageView) findViewById(R.id.lbutton1);
        lbutton2 = (ImageView) findViewById(R.id.lbutton2);
        lbutton3 = (ImageView) findViewById(R.id.lbutton3);
        lbutton4 = (ImageView) findViewById(R.id.lbutton4);
        lbutton5 = (ImageView) findViewById(R.id.lbutton5);
        lbutton6 = (ImageView) findViewById(R.id.lbutton6);
        lbutton7 = (ImageView) findViewById(R.id.lbutton7);
        lbutton8 = (ImageView) findViewById(R.id.lbutton8);

        rbutton1 = (ImageView) findViewById(R.id.rbutton1);
        rbutton2 = (ImageView) findViewById(R.id.rbutton2);
        rbutton3 = (ImageView) findViewById(R.id.rbutton3);
        rbutton4 = (ImageView) findViewById(R.id.rbutton4);
        rbutton5 = (ImageView) findViewById(R.id.rbutton5);
        rbutton6 = (ImageView) findViewById(R.id.rbutton6);
        rbutton7 = (ImageView) findViewById(R.id.rbutton7);
        rbutton8 = (ImageView) findViewById(R.id.rbutton8);

        leftdjround = (ImageView) findViewById(R.id.leftdjround);
        righgtdjround = (ImageView) findViewById(R.id.righgtdjround);
        addnewleft = (ImageView) findViewById(R.id.addleft);
        addnewright = (ImageView) findViewById(R.id.addright);
        leftpaly = (Button) findViewById(R.id.leftpaly);
        rightpaly = (Button) findViewById(R.id.rightpaly);
        leftpause = (Button) findViewById(R.id.leftpause);
        rightpause = (Button) findViewById(R.id.rightpause);

        pad1layer = (View) findViewById(R.id.pad1layer);
        Eq1layer = (View) findViewById(R.id.Eq1layer);
        discalayer = (View) findViewById(R.id.discalayer);

        pad2layer = (View) findViewById(R.id.pad2layer);
        Eq2layer = (View) findViewById(R.id.Eq2layer);
        discblayer = (View) findViewById(R.id.discblayer);

        discA = (Button) findViewById(R.id.discA);
        Eq1 = (Button) findViewById(R.id.Eq1);
        pad1 = (Button) findViewById(R.id.pad1);
        discB = (Button) findViewById(R.id.discB);
        Eq2 = (Button) findViewById(R.id.Eq2);
        pad2 = (Button) findViewById(R.id.pad2);

        lvolseekbar = (VerticalSeekBar) findViewById(R.id.lvolseekbar);
        rvolseekbar = (VerticalSeekBar) findViewById(R.id.rvolseekbar);

        leftmpvol = (SeekBar) findViewById(R.id.leftmpvol);
        rightmpvol = (SeekBar) findViewById(R.id.rightmpvol);
        leftdjname = (TextView) findViewById(R.id.leftdjname);
        rightdjname = (TextView) findViewById(R.id.rightdjname);

        lefteqlow = (SeekBar) findViewById(R.id.lefteqlow);
        lefteqmedium = (SeekBar) findViewById(R.id.lefteqmedium);
        lefteqhigh = (SeekBar) findViewById(R.id.lefteqhigh);
        righteqlow = (SeekBar) findViewById(R.id.righteqlow);
        righteqmedium = (SeekBar) findViewById(R.id.righteqmedium);
        righteqhigh = (SeekBar) findViewById(R.id.righteqhigh);

        // lefttempo = (SeekBar) findViewById(R.id.lefttempo);
        // righttempo = (SeekBar) findViewById(R.id.righttempo);
        mixvolseek = (SeekBar) findViewById(R.id.mixvolseek);

        // playlist = (ListView) findViewById(R.id.playlist);
        menu = (ImageView) findViewById(R.id.menubt);
        lbutton1.setOnClickListener(this);
        lbutton2.setOnClickListener(this);
        lbutton3.setOnClickListener(this);
        lbutton4.setOnClickListener(this);
        lbutton5.setOnClickListener(this);
        lbutton6.setOnClickListener(this);
        lbutton7.setOnClickListener(this);
        lbutton8.setOnClickListener(this);
        rbutton1.setOnClickListener(this);
        rbutton2.setOnClickListener(this);
        rbutton3.setOnClickListener(this);
        rbutton4.setOnClickListener(this);
        rbutton5.setOnClickListener(this);
        rbutton6.setOnClickListener(this);
        rbutton7.setOnClickListener(this);
        rbutton8.setOnClickListener(this);
        leftdjround.setOnClickListener(this);
        righgtdjround.setOnClickListener(this);
        addnewleft.setOnClickListener(this);
        addnewright.setOnClickListener(this);
        leftpaly.setOnClickListener(this);
        rightpaly.setOnClickListener(this);
        leftpause.setOnClickListener(this);
        rightpause.setOnClickListener(this);
        menu.setOnClickListener(this);

        discA.setOnClickListener(this);
        Eq1.setOnClickListener(this);
        pad1.setOnClickListener(this);
        discB.setOnClickListener(this);
        Eq2.setOnClickListener(this);
        pad2.setOnClickListener(this);

        anim = new RotateAnimation(0f, 350f, RotateAnimation.RELATIVE_TO_SELF,
                0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.5f);
        anim.setInterpolator(new LinearInterpolator());
        anim.setRepeatCount(Animation.INFINITE);
        anim.setDuration(700);

        mpleft.setOnCompletionListener(new OnCompletionListener() {

            @Override
            public void onCompletion(MediaPlayer mp) {

                leftdjname.setText("Drag Here");
                leftdjround.setAnimation(null);
                isLeftPlaying = false;

            }
        });
        mpright.setOnCompletionListener(new OnCompletionListener() {

            @Override
            public void onCompletion(MediaPlayer mp) {
                // mpright.stop();
                // mpright.reset();
                // rightvol1.setText("0.00");
                // rightvol2.setText("0.00");
                rightdjname.setText("Drag Here");
                righgtdjround.setAnimation(null);
                // rightpaly.setClickable(true);
                // rightpause.setClickable(false);
                isRightPlaying = false;

            }
        });
        leftmpvol.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                if (isLeftPlaying) {
                    if (fromUser) {
                        mpleft.seekTo(progress);
                        updateLeftTime();
                    }
                }
            }
        });
        rightmpvol.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                if (isRightPlaying) {

                    if (fromUser) {
                        mpright.seekTo(progress);
                        updateRightTime();
                    }
                }

            }
        });
        lvolseekbar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                float Rvol = (progress) / 100.0f;
                mpleft.setVolume(Rvol, Rvol);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        rvolseekbar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                float Rvol = (progress) / 100.0f;
                mpright.setVolume(Rvol, Rvol);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        mixvolseek.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {

                if (cvol == progress) {
                    lvol = 0.5f;
                    Rvol = 0.5f;
                }

                lvol = (100 - progress) / 100.0f;
                Rvol = (progress) / 100.0f;

                mpleft.setVolume(lvol, lvol);

                mpright.setVolume(Rvol, Rvol);

            }
        });

        mEqualizer1 = new Equalizer(0, mpleft.getAudioSessionId());
        mEqualizer1.setEnabled(true);
        final short minEQLevel = mEqualizer1.getBandLevelRange()[0];
        final short maxEQLevel = mEqualizer1.getBandLevelRange()[1];

        lefteqlow.setMax(maxEQLevel - minEQLevel);
        // lefteqlow.setProgress((maxEQLevel - minEQLevel) / 2);

        lefteqlow
                .setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    public void onProgressChanged(SeekBar seekBar,
                                                  int progress, boolean fromUser) {
                        mEqualizer1.setBandLevel(band1,
                                (short) (progress + minEQLevel));

                    }

                    public void onStartTrackingTouch(SeekBar seekBar) {
                    }

                    public void onStopTrackingTouch(SeekBar seekBar) {
                    }
                });
        lefteqmedium.setMax(maxEQLevel - minEQLevel);
        // lefteqmedium.setProgress((maxEQLevel - minEQLevel) / 2);

        lefteqmedium
                .setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    public void onProgressChanged(SeekBar seekBar,
                                                  int progress, boolean fromUser) {
                        mEqualizer1.setBandLevel(band2,
                                (short) (progress + minEQLevel));

                    }

                    public void onStartTrackingTouch(SeekBar seekBar) {
                    }

                    public void onStopTrackingTouch(SeekBar seekBar) {
                    }
                });
        lefteqhigh.setMax(maxEQLevel - minEQLevel);
        // lefteqhigh.setProgress((maxEQLevel - minEQLevel) / 2);

        lefteqhigh
                .setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    public void onProgressChanged(SeekBar seekBar,
                                                  int progress, boolean fromUser) {
                        mEqualizer1.setBandLevel(band3,
                                (short) (progress + minEQLevel));

                    }

                    public void onStartTrackingTouch(SeekBar seekBar) {
                    }

                    public void onStopTrackingTouch(SeekBar seekBar) {
                    }
                });

        mEqualizer2 = new Equalizer(0, mpright.getAudioSessionId());
        mEqualizer2.setEnabled(true);
        final short minEQLevel2 = mEqualizer2.getBandLevelRange()[0];
        final short maxEQLevel2 = mEqualizer2.getBandLevelRange()[1];

        righteqlow.setMax(maxEQLevel2 - minEQLevel2);
        // righteqlow.setProgress((maxEQLevel2 - minEQLevel2) / 2);

        righteqlow
                .setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    public void onProgressChanged(SeekBar seekBar,
                                                  int progress, boolean fromUser) {
                        mEqualizer2.setBandLevel(band1,
                                (short) (progress + minEQLevel2));

                    }

                    public void onStartTrackingTouch(SeekBar seekBar) {
                    }

                    public void onStopTrackingTouch(SeekBar seekBar) {
                    }
                });
        righteqmedium.setMax(maxEQLevel2 - minEQLevel2);
        // righteqmedium.setProgress((maxEQLevel2 - minEQLevel2) / 2);

        righteqmedium
                .setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    public void onProgressChanged(SeekBar seekBar,
                                                  int progress, boolean fromUser) {
                        mEqualizer2.setBandLevel(band2,
                                (short) (progress + minEQLevel2));

                    }

                    public void onStartTrackingTouch(SeekBar seekBar) {
                    }

                    public void onStopTrackingTouch(SeekBar seekBar) {
                    }
                });
        righteqhigh.setMax(maxEQLevel2 - minEQLevel2);
        // righteqhigh.setProgress((maxEQLevel2 - minEQLevel2) / 2);

        righteqhigh
                .setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    public void onProgressChanged(SeekBar seekBar,
                                                  int progress, boolean fromUser) {
                        mEqualizer2.setBandLevel(band3,
                                (short) (progress + minEQLevel2));

                    }

                    public void onStartTrackingTouch(SeekBar seekBar) {
                    }

                    public void onStopTrackingTouch(SeekBar seekBar) {
                    }
                });
    }

    private void showratedialog() {
        new AlertDialog.Builder(this)
                .setTitle("Do you want to rate this application?")
                .setPositiveButton("Never",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // continue with delete
                                dialog.dismiss();
                            }
                        })
                .setNegativeButton("not now",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // do nothing
                                dialog.dismiss();
                            }
                        })
                .setNeutralButton("Rate Now",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // do nothing
                                startActivity(new Intent(Intent.ACTION_VIEW,
                                        Uri.parse("market://details?id="
                                                + "com.punsoftware.mixer")));
                            }
                        }).show();
    }

    public void displayInterstitial() {
        if (interstitial.isLoaded()) {
            interstitial.show();
        }
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub

        displayInterstitial();
        showclose();

    }

    private static final float VISUALIZER_HEIGHT_DIP = 50f;
    private Visualizer mVisualizer, mVisualizer1;
    private LinearLayout mLinearLayout, mLinearLayout1;

    private void setupVisualizerFxAndUI() {
        mVisualizerView = new VisualizerView(this);

        mVisualizerView.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.FILL_PARENT));

        mLinearLayout.addView(mVisualizerView);
        // if (mpleft.isPlaying()) {

        Log.e("mpleft", "seesion id " + mpleft.getAudioSessionId());
        mVisualizer = new Visualizer(mpleft.getAudioSessionId());

        mVisualizer.setCaptureSize(Visualizer.getCaptureSizeRange()[1]);
        mVisualizer.setDataCaptureListener(
                new Visualizer.OnDataCaptureListener() {
                    public void onWaveFormDataCapture(Visualizer visualizer,
                                                      byte[] bytes, int samplingRate) {
                        mVisualizerView.updateVisualizer(bytes);
                    }

                    public void onFftDataCapture(Visualizer visualizer,
                                                 byte[] bytes, int samplingRate) {

                        Log.e("", "fftc hange");
                    }
                }, Visualizer.getMaxCaptureRate() / 2, true, false);
        // }
    }

    private void setupVisualizerFxAndUI2() {
        mVisualizerView2 = new VisualizerView(this);

        mVisualizerView2.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.FILL_PARENT));

        mLinearLayout1.addView(mVisualizerView2);
        // if (mpleft.isPlaying()) {

        Log.e("mpleft", "seesion id " + mpleft.getAudioSessionId());
        mVisualizer1 = new Visualizer(mpright.getAudioSessionId());

        mVisualizer1.setCaptureSize(Visualizer.getCaptureSizeRange()[1]);
        mVisualizer1.setDataCaptureListener(
                new Visualizer.OnDataCaptureListener() {
                    public void onWaveFormDataCapture(Visualizer visualizer,
                                                      byte[] bytes, int samplingRate) {
                        mVisualizerView2.updateVisualizer(bytes);
                    }

                    public void onFftDataCapture(Visualizer visualizer,
                                                 byte[] bytes, int samplingRate) {

                        Log.e("", "fftc hange");
                    }
                }, Visualizer.getMaxCaptureRate() / 2, true, false);
        // }
    }

    private void setrelease() {
        // leftmpvol =null;
        // rightmpvol =null;
        if (mpleft.isPlaying())
            mpleft.stop();
        if (mpright.isPlaying())
            mpright.stop();
        // mpleft.reset();
        // mpright.reset();
        // mpleft.release();
        // mpright.release();
        mplbutton1.release();
        mplbutton2.release();
        mplbutton3.release();
        mplbutton4.release();
        mplbutton5.release();
        mplbutton6.release();
        mplbutton7.release();
        mplbutton8.release();

        mprbutton1.release();
        mprbutton2.release();
        mprbutton3.release();
        mprbutton4.release();
        mprbutton5.release();
        mprbutton6.release();
        mprbutton7.release();
        mprbutton8.release();
        leftdjst.release();
        rightdjst.release();
    }

    private void showclose() {
        new AlertDialog.Builder(this)
                .setTitle("Do you really want to shut down the app?")
                .setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {

                                setrelease();
                                finish();
                                return;
                            }
                        })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                Log.e("list",
                        "clcik: on result" + data.getExtras().getInt("name")
                                + " " + data.getExtras().getString("path"));

                switch (data.getExtras().getInt("name")) {
                    case 0:
                        if (mpleft.isPlaying()) {
                            mpleft.stop();
                        }
                        mpleft.reset();
                        try {
                            isLeftPlaying = false;
                            leftdjname.setText(data.getExtras().getString("mname"));
                            playleftmusic(data.getExtras().getString("path"));
                        } catch (IllegalStateException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        break;
                    case 1:
                        if (mpright.isPlaying()) {
                            mpright.stop();
                        }
                        mpright.reset();
                        try {
                            isRightPlaying = false;
                            rightdjname
                                    .setText(data.getExtras().getString("mname"));
                            // rightpaly.setClickable(false);
                            // rightpause.setClickable(true);

                            playrightmusic(data.getExtras().getString("path"));
                        } catch (IllegalStateException e) {
                            e.printStackTrace();
                        }

                        break;

                    default:
                        break;
                }
            }
            if (resultCode == RESULT_CANCELED) {
            }
        }
    }

    @Override
    public void onClick(View v) {
        List<Musicmodel> el = dbHandler.Get_playList(false);
        switch (v.getId()) {

            case R.id.discA:
                Eq1layer.setVisibility(View.GONE);
                pad1layer.setVisibility(View.GONE);
                discalayer.setVisibility(View.VISIBLE);
                break;
            case R.id.Eq1:

                pad1layer.setVisibility(View.GONE);
                discalayer.setVisibility(View.GONE);
                Eq1layer.setVisibility(View.VISIBLE);

                break;
            case R.id.pad1:

                discalayer.setVisibility(View.GONE);
                Eq1layer.setVisibility(View.GONE);
                pad1layer.setVisibility(View.VISIBLE);
                break;
            case R.id.discB:
                Eq2layer.setVisibility(View.GONE);
                pad2layer.setVisibility(View.GONE);
                discblayer.setVisibility(View.VISIBLE);
                break;
            case R.id.Eq2:
                pad2layer.setVisibility(View.GONE);
                discblayer.setVisibility(View.GONE);
                Eq2layer.setVisibility(View.VISIBLE);
                break;
            case R.id.pad2:
                discblayer.setVisibility(View.GONE);
                Eq2layer.setVisibility(View.GONE);
                pad2layer.setVisibility(View.VISIBLE);
                break;

            case R.id.addleft:
                Intent i = new Intent(MainActivity.this, FileBrowserActivity.class);
                i.putExtra("name", 0);
                startActivityForResult(i, 1);
                break;

            case R.id.addright:
                Intent ir = new Intent(MainActivity.this, FileBrowserActivity.class);
                ir.putExtra("name", 1);
                startActivityForResult(ir, 1);
                break;

            case R.id.lbutton1:
                mplbutton1.reset();
                leftbutton1("leftpad1.ogg");
                break;
            case R.id.lbutton2:
                mplbutton2.reset();
                leftbutton2("leftpad2.ogg");
                break;
            case R.id.lbutton3:
                mplbutton3.reset();
                leftbutton3("leftpad3.ogg");
                break;
            case R.id.lbutton4:
                mplbutton4.reset();
                leftbutton4("leftpad4.ogg");
                break;
            case R.id.lbutton5:
                mplbutton5.reset();
                leftbutton5("leftpad5.ogg");
                break;
            case R.id.lbutton6:
                mplbutton6.reset();
                leftbutton6("leftpad6.ogg");
                break;
            case R.id.lbutton7:
                mplbutton7.reset();
                leftbutton7("leftpad7.ogg");
                break;
            case R.id.lbutton8:
                mplbutton8.reset();
                leftbutton8("leftpad8.ogg");
                break;
            case R.id.rbutton1:
                mprbutton1.reset();
                rightbutton1("rightpad1.ogg");
                break;
            case R.id.rbutton2:
                mprbutton2.reset();
                rightbutton2("rightpad2.ogg");
                break;
            case R.id.rbutton3:
                mprbutton3.reset();
                rightbutton3("rightpad3.ogg");
                break;
            case R.id.rbutton4:
                mprbutton4.reset();
                rightbutton4("rightpad4.ogg");
                break;
            case R.id.rbutton5:
                mprbutton5.reset();
                rightbutton5("rightpad5.ogg");
                break;
            case R.id.rbutton6:
                mprbutton6.reset();
                rightbutton6("rightpad6.ogg");
                break;
            case R.id.rbutton7:
                mprbutton7.reset();
                rightbutton7("rightpad7.ogg");
                break;
            case R.id.rbutton8:
                mprbutton8.reset();
                rightbutton8("rightpad8.ogg");
                break;
            case R.id.leftdjround:
                leftdjst.reset();
                leftdjst("Scratch_Left.ogg");

                break;
            case R.id.righgtdjround:
                rightdjst.reset();
                rightdjst("Scratch_Right.ogg");
                break;

            case R.id.leftpaly:
                if (!isLeftPlaying) {
                    if (leftdjname.getText().toString()
                            .equalsIgnoreCase("Drag Here")) {
                        displayalert();
                    } else {
                        mpleft.start();
                        leftmpvol.postDelayed(onEverySecondLeft, 1000);
                        leftdjround.startAnimation(anim);
                        isLeftPlaying = true;
                    }
                }

                break;
            case R.id.rightpaly:
                if (!isRightPlaying) {
                    if (rightdjname.getText().toString()
                            .equalsIgnoreCase("Drag Here")) {
                        displayalert();
                    } else {
                        mpright.start();
                        rightmpvol.postDelayed(onEverySecondLeft, 1000);
                        righgtdjround.startAnimation(anim);
                        isRightPlaying = true;
                    }
                }

                break;
            case R.id.leftpause:
                Log.e("", "isLeftPlaying leftpause " + isLeftPlaying);
                isLeftPlaying = false;
                mpleft.pause();
                leftdjround.setAnimation(null);

                break;
            case R.id.rightpause:
                isRightPlaying = false;
                mpright.pause();
                righgtdjround.setAnimation(null);
                break;
            case R.id.menubt:
                mDrawerLayout.openDrawer(Gravity.START);
                break;

            default:
                break;
        }
    }

    private void openplaylist() {
        final String[] openlist = dbHandler.openlist();
        new AlertDialog.Builder(getactivity())
                .setItems(openlist, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        Log.e("list", openlist[which].toString());
                        musiclist.clear();
                        musicadapter.notifyDataSetChanged();
                        List<Musicmodel> save = dbHandler.Get_saveplayList(
                                false, openlist[which].toString().trim());

                        for (Musicmodel e : save) {
                            musiclist.add(e);
                        }

                        musicadapter.notifyDataSetChanged();

                    }

                }).create().show();

    }

    private void displayalert() {
        // TODO Auto-generated method stub
        new AlertDialog.Builder(this)
                .setTitle(
                        "Darg-and-drop a track onto the turntable ,then press play")
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    private final class MyClickListener implements OnItemLongClickListener {

        @Override
        public boolean onItemLongClick(AdapterView<?> arg0, View view,
                                       int position, long arg3) {

            // String selectedFromList = "trdvm";
            String selectedFromList = musiclist.get(position).path;
            ClipData.Item item = new ClipData.Item(
                    ((CharSequence) selectedFromList.toString()));
            String[] clipDescription = {ClipDescription.MIMETYPE_TEXT_PLAIN};
            ClipData dragData = new ClipData("item", clipDescription, item);
            DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
            view.startDrag(dragData, shadowBuilder, view, 0);
            return true;
        }

    }

    private void showalert() {

        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle("Save PlayList");

        // Set an EditText view to get user input
        final EditText input = new EditText(this);
        alert.setView(input);

        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String value = input.getText().toString();
                if (!value.equals("")) {
                    dbHandler.Add_play(value.toString().trim());

                    for (int i = 0; i < musiclist.size(); i++) {
                        Musicmodel md = new Musicmodel();
                        md.name = musiclist.get(i).name;
                        md.path = musiclist.get(i).path;
                        Log.e("value", value);
                        dbHandler.Add_Saveplaylist(md, value.toString().trim());

                    }

                }
            }
        });

        alert.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        // Canceled.
                    }
                });

        alert.show();
    }

    private Runnable onEverySecondLeft = new Runnable() {
        @Override
        public void run() {
            if (true == runningLeft) {
                if (leftmpvol != null) {
                    leftmpvol.setProgress(mpleft.getCurrentPosition());
                }

                if (mpleft.isPlaying()) {
                    leftmpvol.postDelayed(onEverySecondLeft, 1000);
                    updateLeftTime();
                }
            }
        }
    };
    private Runnable onEverySecondRight = new Runnable() {
        @Override
        public void run() {
            if (true == runningRight) {
                if (rightmpvol != null) {
                    rightmpvol.setProgress(mpright.getCurrentPosition());
                }

                if (mpright.isPlaying()) {
                    rightmpvol.postDelayed(onEverySecondRight, 1000);
                    updateRightTime();
                }
            }
        }
    };

    private void updateLeftTime() {
        do {
            currentLeft = mpleft.getCurrentPosition();
            System.out.println("duration - " + durationLeft + " current- "
                    + currentLeft);
            int dSeconds = (int) (durationLeft / 1000) % 60;
            int dMinutes = (int) ((durationLeft / (1000 * 60)) % 60);
            int dHours = (int) ((durationLeft / (1000 * 60 * 60)) % 24);

            int cSeconds = (int) (currentLeft / 1000) % 60;
            int cMinutes = (int) ((currentLeft / (1000 * 60)) % 60);
            int cHours = (int) ((currentLeft / (1000 * 60 * 60)) % 24);

            if (dHours == 0) {
                // leftvol1.setText(String.format("%02d:%02d ", cMinutes,
                // cSeconds));
                // leftvol2.setText(String.format("%02d:%02d", dMinutes,
                // dSeconds));
            } else {
                // leftvol1.setText(String.format("%02d:%02d:%02d ", cHours,
                // cMinutes, cSeconds));
                // leftvol2.setText(String.format("%02d:%02d:%02d", dHours,
                // dMinutes, dSeconds));
            }
            try {
                Log.d("Value: ", String
                        .valueOf((int) (currentLeft * 100 / durationLeft)));
                if (leftmpvol.getProgress() >= 100) {
                    break;
                }
            } catch (Exception e) {
            }
        } while (leftmpvol.getProgress() <= 100);
    }

    private void updateRightTime() {
        do {
            currentRight = mpright.getCurrentPosition();
            int dSeconds = (int) (durationRight / 1000) % 60;
            int dMinutes = (int) ((durationRight / (1000 * 60)) % 60);
            int dHours = (int) ((durationRight / (1000 * 60 * 60)) % 24);

            int cSeconds = (int) (currentRight / 1000) % 60;
            int cMinutes = (int) ((currentRight / (1000 * 60)) % 60);
            int cHours = (int) ((currentRight / (1000 * 60 * 60)) % 24);

            if (dHours == 0) {
                // rightvol1.setText(String.format("%02d:%02d ", cMinutes,
                // cSeconds));
                // rightvol2.setText(String.format("%02d:%02d", dMinutes,
                // dSeconds));
            } else {
                // rightvol1.setText(String.format("%02d:%02d:%02d ", cHours,
                // cMinutes, cSeconds));
                // rightvol2.setText(String.format("%02d:%02d:%02d", dHours,
                // dMinutes, dSeconds));
            }

            try {
                if (rightmpvol.getProgress() >= 100) {
                    break;
                }
            } catch (Exception e) {
            }
        } while (rightmpvol.getProgress() <= 100);
    }

    boolean isLeftPlaying = false;
    boolean isRightPlaying = false;
    private int currentLeft = 0;
    private int currentRight = 0;
    private boolean runningLeft = true;
    private boolean runningRight = true;
    private int durationLeft = 0;
    private int durationRight = 0;

    // private void addLineRenderer() {
    // Paint linePaint = new Paint();
    // linePaint.setStrokeWidth(1f);
    // linePaint.setAntiAlias(true);
    // linePaint.setColor(Color.argb(88, 0, 128, 255));
    //
    // Paint lineFlashPaint = new Paint();
    // // lineFlashPaint.setStrokeWidth(1f);
    // // lineFlashPaint.setAntiAlias(true);
    // // lineFlashPaint.setColor(Color.argb(188, 255, 255, 255));
    // LineRenderer lineRenderer = new LineRenderer(linePaint, lineFlashPaint,
    // true);
    // mVisualizerView.addRenderer(lineRenderer);
    // }

    private void leftdjst(String path) {
        try {
            AssetFileDescriptor descriptor = getAssets()
                    .openFd("music/" + path);
            leftdjst.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            leftdjst.prepare();
            leftdjst.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    leftdjst.start();

                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void rightdjst(String path) {
        try {
            AssetFileDescriptor descriptor = getAssets()
                    .openFd("music/" + path);
            rightdjst.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            rightdjst.prepare();
            rightdjst.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    rightdjst.start();
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void leftbutton1(String path) {
        try {
            AssetFileDescriptor descriptor = getAssets()
                    .openFd("music/" + path);
            mplbutton1.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mplbutton1.prepare();
            mplbutton1.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mplbutton1.start();
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void leftbutton2(String path) {
        try {
            AssetFileDescriptor descriptor = getAssets()
                    .openFd("music/" + path);
            mplbutton2.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mplbutton2.prepare();
            mplbutton2.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mplbutton2.start();
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void leftbutton3(String path) {
        try {
            AssetFileDescriptor descriptor = getAssets()
                    .openFd("music/" + path);
            mplbutton3.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mplbutton3.prepare();
            mplbutton3.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mplbutton3.start();
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void leftbutton4(String path) {
        try {
            AssetFileDescriptor descriptor = getAssets()
                    .openFd("music/" + path);
            mplbutton4.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mplbutton4.prepare();
            mplbutton4.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mplbutton4.start();
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void leftbutton5(String path) {
        try {
            AssetFileDescriptor descriptor = getAssets()
                    .openFd("music/" + path);
            mplbutton5.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mplbutton5.prepare();
            mplbutton5.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mplbutton5.start();
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void leftbutton7(String path) {
        try {
            AssetFileDescriptor descriptor = getAssets()
                    .openFd("music/" + path);
            mplbutton7.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mplbutton7.prepare();
            mplbutton7.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mplbutton7.start();
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void leftbutton8(String path) {
        try {
            AssetFileDescriptor descriptor = getAssets()
                    .openFd("music/" + path);
            mplbutton8.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mplbutton8.prepare();
            mplbutton8.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mplbutton8.start();
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void leftbutton6(String path) {
        try {
            AssetFileDescriptor descriptor = getAssets()
                    .openFd("music/" + path);
            mplbutton6.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mplbutton6.prepare();
            mplbutton6.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mplbutton6.start();
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void rightbutton1(String path) {
        try {
            AssetFileDescriptor descriptor = getAssets()
                    .openFd("music/" + path);
            mprbutton1.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mprbutton1.prepare();
            mprbutton1.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mprbutton1.start();
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void rightbutton2(String path) {
        try {
            AssetFileDescriptor descriptor = getAssets()
                    .openFd("music/" + path);
            mprbutton2.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mprbutton2.prepare();
            mprbutton2.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mprbutton2.start();
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void rightbutton3(String path) {
        try {
            AssetFileDescriptor descriptor = getAssets()
                    .openFd("music/" + path);
            mprbutton3.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mprbutton3.prepare();
            mprbutton3.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mprbutton3.start();
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void rightbutton4(String path) {
        try {
            AssetFileDescriptor descriptor = getAssets()
                    .openFd("music/" + path);
            mprbutton4.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mprbutton4.prepare();
            mprbutton4.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mprbutton4.start();
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void rightbutton5(String path) {
        try {
            AssetFileDescriptor descriptor = getAssets()
                    .openFd("music/" + path);
            mprbutton5.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mprbutton5.prepare();
            mprbutton5.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mprbutton5.start();
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void rightbutton6(String path) {
        try {
            AssetFileDescriptor descriptor = getAssets()
                    .openFd("music/" + path);
            mprbutton6.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mprbutton6.prepare();
            mprbutton6.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mprbutton6.start();
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void rightbutton7(String path) {
        try {
            AssetFileDescriptor descriptor = getAssets()
                    .openFd("music/" + path);
            mprbutton7.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mprbutton7.prepare();
            mprbutton7.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mprbutton7.start();
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void rightbutton8(String path) {
        try {
            AssetFileDescriptor descriptor = getAssets()
                    .openFd("music/" + path);
            mprbutton8.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mprbutton8.prepare();
            mprbutton8.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mprbutton8.start();
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void playleftmusic(String path) throws IllegalStateException,
            IOException {

        if (!isLeftPlaying) {
            try {

                mpleft.setDataSource(getApplicationContext(), Uri.parse(path));

                mpleft.prepare();
                mpleft.setOnPreparedListener(new OnPreparedListener() {

                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        durationLeft = mpleft.getDuration();
                        leftmpvol.setMax(durationLeft);
                        leftmpvol.postDelayed(onEverySecondLeft, 1000);
                    }
                });
                mpleft.start();

                leftmpvol.postDelayed(onEverySecondLeft, 1000);
                leftdjround.startAnimation(anim);
                isLeftPlaying = true;

            } catch (IllegalStateException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        } else {
        }
    }

    private void playrightmusic(String path) {

        if (!isRightPlaying) {
            // TODO Auto-generated method stub
            try {

                mpright.setDataSource(getApplicationContext(), Uri.parse(path));

                mpright.prepare();
                mpright.setOnPreparedListener(new OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        durationRight = mpright.getDuration();
                        rightmpvol.setMax(durationRight);
                        rightmpvol.postDelayed(onEverySecondRight, 1000);

                    }
                });

                mpright.start();
                rightmpvol.postDelayed(onEverySecondRight, 1000);
                isRightPlaying = true;
                righgtdjround.startAnimation(anim);

            } catch (IllegalStateException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        } else {

        }
    }

    private MainActivity getactivity() {
        return MainActivity.this;
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    class VisualizerView extends View {
        private byte[] mBytes;
        private float[] mPoints;
        private Rect mRect = new Rect();

        private Paint mForePaint = new Paint();

        public VisualizerView(Context context) {
            super(context);
            init();
        }

        private void init() {
            mBytes = null;

            mForePaint.setStrokeWidth(4f);
            mForePaint.setAntiAlias(true);
            mForePaint.setColor(Color.rgb(252, 193, 91));
        }

        public void updateVisualizer(byte[] bytes) {
            mBytes = bytes;
            // Log.e("bytes","by: " + bytes);
            invalidate();
        }

        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);

            if (mBytes == null) {
                return;
            }

            if (mPoints == null || mPoints.length < mBytes.length * 4) {
                mPoints = new float[mBytes.length * 4];
            }

            mRect.set(0, 0, getWidth(), getHeight());

            for (int i = 0; i < mBytes.length - 1; i++) {
                mPoints[i * 4] = mRect.width() * i / (mBytes.length - 1);
                mPoints[i * 4 + 1] = mRect.height() / 2
                        + ((byte) (mBytes[i] + 128)) * (mRect.height() / 2)
                        / 128;
                mPoints[i * 4 + 2] = mRect.width() * (i + 1)
                        / (mBytes.length - 1);
                mPoints[i * 4 + 3] = mRect.height() / 2
                        + ((byte) (mBytes[i + 1] + 128)) * (mRect.height() / 2)
                        / 128;
            }

            float centerX = mRect.width();
            float centerY = mRect.height();
            double angle = 90;
            Matrix rotateMat = new Matrix();
            rotateMat.setRotate((float) angle, centerX, centerY);
            // rotateMat.mapPoints(mPoints);

            canvas.drawLines(mPoints, mForePaint);

        }
    }
}

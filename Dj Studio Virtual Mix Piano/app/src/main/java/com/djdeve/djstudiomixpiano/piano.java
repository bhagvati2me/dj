package com.djdeve.djstudiomixpiano;

import java.io.IOException;

import android.app.Activity;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

import com.djdeve.djstudiomixpiano.R;
import com.twobard.pianoview.Piano;
import com.twobard.pianoview.Piano.PianoKeyListener;

public class piano extends Activity implements OnClickListener {

	Piano piano1, piano2;
	MediaPlayer keymp1, keymp2, keymp3, keymp4, keymp5, keymp6, keymp7, keymp8,
			keymp9, keymp10, keymp11, keymp12, keymp13, keymp14, keymp15,
			keymp16, keymp17, keymp18, keymp19, keymp20, keymp21, keymp22,
			keymp23, keymp0;

	ImageView s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s11, s12, s13, s14, s15,
			s16;
	MediaPlayer mp1, mp2, mp3, mp4, mp5, mp6, mp7, mp8, mp9, mp10, mp11, mp12,
			mp13, mp14, mp15, mp16;
	VerticalSeekBar padvol1;

	String kit = "sA1";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pad1);

		mp1 = new MediaPlayer();
		mp2 = new MediaPlayer();
		mp3 = new MediaPlayer();
		mp4 = new MediaPlayer();
		mp5 = new MediaPlayer();
		mp6 = new MediaPlayer();
		mp7 = new MediaPlayer();
		mp8 = new MediaPlayer();
		mp9 = new MediaPlayer();
		mp10 = new MediaPlayer();
		mp11 = new MediaPlayer();
		mp12 = new MediaPlayer();
		mp13 = new MediaPlayer();
		mp14 = new MediaPlayer();
		mp15 = new MediaPlayer();
		mp16 = new MediaPlayer();
		keymp0 = new MediaPlayer();
		keymp1 = new MediaPlayer();
		keymp2 = new MediaPlayer();
		keymp3 = new MediaPlayer();
		keymp4 = new MediaPlayer();
		keymp5 = new MediaPlayer();
		keymp6 = new MediaPlayer();
		keymp7 = new MediaPlayer();
		keymp8 = new MediaPlayer();
		keymp9 = new MediaPlayer();
		keymp10 = new MediaPlayer();
		keymp11 = new MediaPlayer();
		keymp12 = new MediaPlayer();
		keymp13 = new MediaPlayer();
		keymp14 = new MediaPlayer();
		keymp15 = new MediaPlayer();
		keymp16 = new MediaPlayer();
		keymp17 = new MediaPlayer();

		s1 = (ImageView) findViewById(R.id.s1);
		s2 = (ImageView) findViewById(R.id.s2);
		s3 = (ImageView) findViewById(R.id.s3);
		s4 = (ImageView) findViewById(R.id.s4);
		s5 = (ImageView) findViewById(R.id.s5);
		s6 = (ImageView) findViewById(R.id.s6);
		s7 = (ImageView) findViewById(R.id.s7);
		s8 = (ImageView) findViewById(R.id.s8);
		s9 = (ImageView) findViewById(R.id.s9);
		s10 = (ImageView) findViewById(R.id.s10);
		s11 = (ImageView) findViewById(R.id.s11);
		s12 = (ImageView) findViewById(R.id.s12);
		s13 = (ImageView) findViewById(R.id.s13);
		s14 = (ImageView) findViewById(R.id.s14);
		s15 = (ImageView) findViewById(R.id.s15);
		s16 = (ImageView) findViewById(R.id.s16);
		s1.setOnClickListener(this);
		s2.setOnClickListener(this);
		s3.setOnClickListener(this);
		s4.setOnClickListener(this);
		s5.setOnClickListener(this);
		s6.setOnClickListener(this);
		s7.setOnClickListener(this);
		s8.setOnClickListener(this);
		s9.setOnClickListener(this);
		s10.setOnClickListener(this);
		s11.setOnClickListener(this);
		s12.setOnClickListener(this);
		s13.setOnClickListener(this);
		s14.setOnClickListener(this);
		s15.setOnClickListener(this);
		s16.setOnClickListener(this);

		piano1 = (Piano) findViewById(R.id.piano1);
		piano1.setPianoKeyListener(new PianoKeyListener() {
			@Override
			public void keyPressed(int id, int action) {

				if (action == MotionEvent.ACTION_DOWN) {

					play(("piano" + id + ".ogg"), id);

				}
			}
		});
		padvol1 = (VerticalSeekBar) findViewById(R.id.padvol1);
		padvol1.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				// TODO Auto-generated method stub
				final float vol1 = (progress) / 100.0f;
				piano.this.runOnUiThread(new Runnable() {

					@Override
					public void run() {

						keymp0.setVolume(vol1, vol1);
						keymp1.setVolume(vol1, vol1);
						keymp2.setVolume(vol1, vol1);
						keymp3.setVolume(vol1, vol1);
						keymp4.setVolume(vol1, vol1);
						keymp5.setVolume(vol1, vol1);
						keymp6.setVolume(vol1, vol1);
						keymp7.setVolume(vol1, vol1);
						keymp8.setVolume(vol1, vol1);
						keymp9.setVolume(vol1, vol1);
						keymp10.setVolume(vol1, vol1);
						keymp11.setVolume(vol1, vol1);
						keymp12.setVolume(vol1, vol1);
						keymp13.setVolume(vol1, vol1);
						keymp14.setVolume(vol1, vol1);
						keymp15.setVolume(vol1, vol1);
						keymp16.setVolume(vol1, vol1);
						keymp17.setVolume(vol1, vol1);

					}
				});
			}
		});

	}

	private void play(String sd, int id) {

		switch (id) {
		case 0:
			playkeyboard0(sd);
			break;
		case 1:
			playkeyboard1(sd);
			break;
		case 2:
			playkeyboard2(sd);
			break;
		case 3:
			playkeyboard3(sd);
			break;
		case 4:
			playkeyboard4(sd);
			break;
		case 5:
			playkeyboard5(sd);
			break;
		case 6:
			playkeyboard6(sd);
			break;
		case 7:
			playkeyboard7(sd);
			break;
		case 8:
			playkeyboard8(sd);
			break;
		case 9:
			playkeyboard9(sd);
			break;
		case 10:
			playkeyboard10(sd);
			break;
		case 11:
			playkeyboard11(sd);
			break;
		case 12:
			playkeyboard12(sd);
			break;
		case 13:
			playkeyboard13(sd);
			break;
		case 14:
			playkeyboard14(sd);
			break;
		case 15:
			playkeyboard15(sd);
			break;
		case 16:
			playkeyboard16(sd);
			break;
		case 17:
			playkeyboard17(sd);
			break;

		default:
			break;
		}

	}

	private void playkeyboard1(String musicname) {
		try {

			keymp1.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			keymp1.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp1.prepare();
			keymp1.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp1.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard2(String musicname) {
		try {

			keymp2.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			keymp2.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp2.prepare();
			keymp2.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp2.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard3(String musicname) {
		try {

			keymp3.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			keymp3.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp3.prepare();
			keymp3.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp3.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard4(String musicname) {
		try {

			keymp4.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			keymp4.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp4.prepare();
			keymp4.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp4.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard5(String musicname) {
		try {

			keymp5.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			keymp5.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp5.prepare();
			keymp5.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp5.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard6(String musicname) {
		try {

			keymp6.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			keymp6.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp6.prepare();
			keymp6.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp6.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard7(String musicname) {
		try {

			keymp7.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			keymp7.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp7.prepare();
			keymp7.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp7.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard8(String musicname) {
		try {

			keymp8.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			keymp8.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp8.prepare();
			keymp8.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp8.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard9(String musicname) {
		try {

			keymp9.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			keymp9.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp9.prepare();
			keymp9.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp9.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard10(String musicname) {
		try {

			keymp10.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			keymp10.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp10.prepare();
			keymp10.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp10.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard11(String musicname) {
		try {

			keymp11.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			keymp11.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp11.prepare();
			keymp11.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp11.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard12(String musicname) {
		try {

			keymp12.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			keymp12.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp12.prepare();
			keymp12.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp12.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard13(String musicname) {
		try {

			keymp13.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			keymp13.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp13.prepare();
			keymp13.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp13.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard14(String musicname) {
		try {

			keymp14.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			keymp14.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp14.prepare();
			keymp14.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp14.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard15(String musicname) {
		try {

			keymp15.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			keymp15.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp15.prepare();
			keymp15.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp15.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard16(String musicname) {
		try {

			keymp16.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			keymp16.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp16.prepare();
			keymp16.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp16.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard17(String musicname) {
		try {

			keymp17.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			keymp17.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp17.prepare();
			keymp17.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp17.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard0(String musicname) {
		try {

			keymp0.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);

			keymp0.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp0.prepare();
			keymp0.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp0.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.s1:
			if (mp1.isPlaying()) {
				mp1.stop();
			} else {
				playsound1(kit + "1.ogg");
			}
			break;
		case R.id.s2:
			if (mp2.isPlaying()) {
				mp2.stop();
			} else {
				playsound2(kit + "2.ogg");
			}
			break;
		case R.id.s3:
			if (mp3.isPlaying()) {
				mp3.stop();
			} else {
				playsound3(kit + "3.ogg");
			}
			break;
		case R.id.s4:
			if (mp4.isPlaying()) {
				mp4.stop();
			} else {
				playsound4(kit + "4.ogg");
			}
			break;
		case R.id.s5:
			if (mp5.isPlaying()) {
				mp5.stop();
			} else {
				playsound5(kit + "5.ogg");
			}
			break;
		case R.id.s6:
			if (mp6.isPlaying()) {
				mp6.stop();
			} else {
				playsound6(kit + "6.ogg");
			}
			break;
		case R.id.s7:
			if (mp7.isPlaying()) {
				mp7.stop();
			} else {
				playsound7(kit + "7.ogg");
			}
			break;
		case R.id.s8:
			if (mp8.isPlaying()) {
				mp8.stop();
			} else {
				playsound8(kit + "8.ogg");
			}
			break;
		case R.id.s9:
			if (mp9.isPlaying()) {
				mp9.stop();
			} else {
				playsound9(kit + "9.ogg");
			}
			break;
		case R.id.s10:
			if (mp10.isPlaying()) {
				mp10.stop();
			} else {
				playsound10(kit + "10.ogg");
			}
			break;
		case R.id.s11:
			if (mp11.isPlaying()) {
				mp11.stop();
			} else {
				playsound11(kit + "11.ogg");
			}
			break;
		case R.id.s12:
			if (mp12.isPlaying()) {
				mp12.stop();
			} else {
				playsound12(kit + "12.ogg");
			}
			break;
		case R.id.s13:
			if (mp13.isPlaying()) {
				mp13.stop();
			} else {
				playsound13(kit + "13.ogg");
			}
			break;
		case R.id.s14:
			if (mp14.isPlaying()) {
				mp14.stop();
			} else {
				playsound14(kit + "14.ogg");
			}
			break;
		case R.id.s15:
			if (mp15.isPlaying()) {
				mp15.stop();
			} else {
				playsound15(kit + "15.ogg");
			}
			break;
		case R.id.s16:
			if (mp16.isPlaying()) {
				mp16.stop();
			} else {
				playsound16(kit + "16.ogg");
			}
			break;

		default:
			break;
		}

	}

	public void playsound1(String path) {
		try {

			mp1.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp1.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp1.prepare();
			mp1.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp.start();
				}
			});

		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void playsound2(String path) {
		try {
			mp2.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp2.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp2.prepare();
			mp2.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp2.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound3(String path) {
		try {
			mp3.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp3.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp3.prepare();
			mp3.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp3.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound4(String path) {
		try {
			mp4.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp4.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp4.prepare();
			mp4.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp4.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound5(String path) {
		try {
			mp5.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp5.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp5.prepare();
			mp5.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp5.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound6(String path) {
		try {
			mp6.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp6.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp6.prepare();
			mp6.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp6.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound7(String path) {
		try {
			mp7.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp7.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp7.prepare();
			mp7.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp7.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound8(String path) {
		try {
			mp8.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp8.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp8.prepare();
			mp8.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp8.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound9(String path) {
		try {
			mp9.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp9.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp9.prepare();
			mp9.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp9.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound10(String path) {
		try {
			mp10.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp10.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp10.prepare();
			mp10.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp10.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound11(String path) {
		try {
			mp11.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp11.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp11.prepare();
			mp11.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp11.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound12(String path) {
		try {
			mp12.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp12.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp12.prepare();
			mp12.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp12.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound13(String path) {
		try {
			mp13.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp13.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp13.prepare();
			mp13.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp13.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound14(String path) {
		try {
			mp14.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp14.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp14.prepare();
			mp14.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp14.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound15(String path) {
		try {
			mp15.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp15.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp15.prepare();
			mp15.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp15.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound16(String path) {
		try {
			mp16.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp16.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp16.prepare();
			mp16.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp16.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}

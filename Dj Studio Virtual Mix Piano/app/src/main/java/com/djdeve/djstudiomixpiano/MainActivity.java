package com.djdeve.djstudiomixpiano;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaRecorder;
import android.media.audiofx.Visualizer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.DragEvent;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.DragShadowBuilder;
import android.view.View.OnClickListener;
import android.view.View.OnDragListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.djdeve.djstudiomixpiano.R;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.twobard.pianoview.Piano;
import com.twobard.pianoview.Piano.PianoKeyListener;

public class MainActivity extends Activity implements OnClickListener {
	ImageView start, pause, menubt;
	private static String audioFilePath;
	MediaRecorder mediaRecorder;
	boolean isRecording = false;
	ImageView screen1, screen2;
	View mscreen1, mscreen2;
	private InterstitialAd interstitial;
	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;

	// =============================================
	MusicAdapter musicadapter;
	ArrayList<Musicmodel> musiclist = new ArrayList<Musicmodel>();
	ListView playlist;
	DatabaseHandler dbHandler = new DatabaseHandler(this);

	File filename1;
	MediaPlayer mpdj1, mpdj2, leftdiskmp, rightdiskmp;
	MediaPlayer leftmp1, leftmp2, leftmp3, rightmp1, rightmp2, rightmp3,
			leftmp12, leftmp22, leftmp32, rightmp12, rightmp22, rightmp32;
	ImageView lr1, lr2, lr3, rr1, rr2, rr3, lr12, lr22, lr32, rr12, rr22, rr32;
	ImageView djdisk1, djdisk2;
	ImageView djplaypause1, djplaypause2;

	ImageView addbt, newmenu, lsync, rsync, lplay, rplay, lplayarrow1,
			lplayarrow2, rplayarrow1, rplayarrow2;
	private RotateAnimation anim;
	MediaPlayer leftdjst, rightdjst;

	boolean isDjPlaying1 = false;
	boolean isdatasetDj1 = false;
	boolean isDjPlaying2 = false;
	boolean isdatasetDj2 = false;

	LinearLayout l1, l2;
	SeekBar dj1vol, dj2vol, volseekbar;
	public float lvol = 0.5f;
	public float Rvol = 0.5f;
	public int cvol = 50;
	boolean isleftsync = false, isrightsync = false;
	TextView lplaytext, rplaytext;
	VerticalSeekBar leftseek, rightseek;

	// ==========================================
	Piano piano1, piano2;
	MediaPlayer keymp1, keymp2, keymp3, keymp4, keymp5, keymp6, keymp7, keymp8,
			keymp9, keymp10, keymp11, keymp12, keymp13, keymp14, keymp15,
			keymp16, keymp17, keymp18, keymp19, keymp20, keymp21, keymp22,
			keymp23, keymp0;

	ImageView s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s11, s12, s13, s14, s15,
			s16;
	MediaPlayer mp1, mp2, mp3, mp4, mp5, mp6, mp7, mp8, mp9, mp10, mp11, mp12,
			mp13, mp14, mp15, mp16;
	VerticalSeekBar padvol1;

	String kit = "sA1";


	private String[] permissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO};

	@RequiresApi(api = Build.VERSION_CODES.M)
	private boolean arePermissionsEnabled() {
		for (String permission : permissions) {
			if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED)
				return false;
		}
		return true;
	}

	@RequiresApi(api = Build.VERSION_CODES.M)
	private void requestMultiplePermissions() {
		List<String> remainingPermissions = new ArrayList<>();
		for (String permission : permissions) {
			if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
				remainingPermissions.add(permission);
			}
		}
		requestPermissions(remainingPermissions.toArray(new String[remainingPermissions.size()]), 101);
	}

	@TargetApi(Build.VERSION_CODES.M)
	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		if (requestCode == 101) {
			for (int i = 0; i < grantResults.length; i++) {
				if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
					if (shouldShowRequestPermissionRationale(permissions[i])) {
						new AlertDialog.Builder(this)
								.setMessage("Please allow permissions to use record audio and choose local music from device")
								.setPositiveButton("Allow", new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialogInterface, int i) {
										dialogInterface.dismiss();
										requestMultiplePermissions();
									}
								})
								.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialogInterface, int i) {
										dialogInterface.dismiss();
										requestMultiplePermissions();
									}
								})
								.create()
								.show();
					}
					return;
				}
			}

			setupVisualizerFxAndUI1();
			mVisualizer1.setEnabled(true);
			setupVisualizerFxAndUI2();
			mVisualizer2.setEnabled(true);


//====


		}
	}



	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.firstscrren);


		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			if(!arePermissionsEnabled()) {
				requestMultiplePermissions();
			}
		}


		AdRequest adRequest = new AdRequest.Builder().build();
		AdView adView = (AdView) findViewById(R.id.adView);
		adView.loadAd(adRequest);
		interstitial = new InterstitialAd(MainActivity.this);
		interstitial.setAdUnitId("ca-app-pub-3805784166574868/7458563404");
		interstitial.loadAd(adRequest);

		interstitial.setAdListener(new AdListener() {
			public void onAdLoaded() {
				displayInterstitial();
			}

			@Override
			public void onAdOpened() {
				super.onAdOpened();
			}

		});

		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.navdrawer);
		menubt = (ImageView) findViewById(R.id.menubt);
		menubt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				mDrawerLayout.openDrawer(Gravity.START);

			}
		});
		String[] values = new String[] { "More Apps", "Info", "Rate Now " };
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				R.layout.menulisttext, android.R.id.text1, values);
		mDrawerList.setAdapter(adapter);
		mDrawerList
				.setOnItemClickListener(new AdapterView.OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> parent, View view,
							int position, long id) {

						Intent intentweb = new Intent(MainActivity.this,
								WebBrowser.class);
						switch (position) {
						case 0:
							intentweb
									.putExtra("URL",
											"https://play.google.com/store/apps/developer?id=DjDeve");
							startActivity(intentweb);
							mDrawerLayout.closeDrawer(Gravity.START);
							break;
						case 1:
							intentweb
									.putExtra("URL",
											"https://play.google.com/store/apps/developer?id=DjDeve");
							startActivity(intentweb);
							mDrawerLayout.closeDrawer(Gravity.START);
							break;
						case 2:

							showratedialog();
							mDrawerLayout.closeDrawer(Gravity.START);
							break;

						}

					}

				});

		screen1 = (ImageView) findViewById(R.id.screen1);
		screen2 = (ImageView) findViewById(R.id.screen2);

		mscreen1 = (View) findViewById(R.id.mscreen1);
		mscreen2 = (View) findViewById(R.id.mscreen2);

		screen1.setOnClickListener(this);
		screen2.setOnClickListener(this);
		start = (ImageView) findViewById(R.id.start);
		pause = (ImageView) findViewById(R.id.pause);

		start.setOnClickListener(new OnClickListener() {

			@SuppressLint("SimpleDateFormat")
			@Override
			public void onClick(View v) {

				try {

					if (!isRecording) {
						String extStorageDirectory = Environment
								.getExternalStorageDirectory()
								.getAbsolutePath();
						File folder = new File(extStorageDirectory,
								getResources().getString(R.string.app_name));
						if (!folder.exists()) {
							folder.mkdir();
							String timeStamp = new SimpleDateFormat(
									"yyyyMMddHHmmss").format(new Date());
							audioFilePath = folder.getAbsolutePath() + "/"
									+ timeStamp + ".ogg";

						} else {
							String timeStamp = new SimpleDateFormat(
									"yyyyMMddHHmmss").format(new Date());
							audioFilePath = folder.getAbsolutePath() + "/"
									+ timeStamp + ".ogg";

						}
						Toast.makeText(getcontext(), "recording stated",
								Toast.LENGTH_SHORT).show();
						PackageManager pmanager = getPackageManager();
						if (pmanager
								.hasSystemFeature(PackageManager.FEATURE_MICROPHONE)) {
							mediaRecorder = new MediaRecorder();
							mediaRecorder
									.setAudioSource(MediaRecorder.AudioSource.DEFAULT);
							mediaRecorder
									.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
							mediaRecorder
									.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
							mediaRecorder.setOutputFile(audioFilePath);
							mediaRecorder.prepare();
							mediaRecorder.start();
							isRecording = true;
						}
					} else {
						Toast.makeText(getcontext(),
								"recording  is already stated",
								Toast.LENGTH_SHORT).show();
					}

				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});

		pause.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (isRecording) {
					isRecording = false;
					Toast.makeText(getcontext(),
							"music file saved in SD card  ", Toast.LENGTH_SHORT)
							.show();
					mediaRecorder.stop();
					mediaRecorder.reset();
					mediaRecorder.release();
				}

			}
		});

		// ==========================================
		leftdiskmp = new MediaPlayer();
		rightdiskmp = new MediaPlayer();
		mpdj1 = new MediaPlayer();
		mpdj2 = new MediaPlayer();
		leftmp1 = new MediaPlayer();
		leftmp2 = new MediaPlayer();
		leftmp3 = new MediaPlayer();
		leftmp12 = new MediaPlayer();
		leftmp22 = new MediaPlayer();
		leftmp32 = new MediaPlayer();
		rightmp1 = new MediaPlayer();
		rightmp2 = new MediaPlayer();
		rightmp3 = new MediaPlayer();
		rightmp12 = new MediaPlayer();
		rightmp22 = new MediaPlayer();
		rightmp32 = new MediaPlayer();

		leftseek = (VerticalSeekBar) findViewById(R.id.leftseek);
		rightseek = (VerticalSeekBar) findViewById(R.id.rightseek);

		lplaytext = (TextView) findViewById(R.id.lplaytext);
		rplaytext = (TextView) findViewById(R.id.rplaytext);

		addbt = (ImageView) findViewById(R.id.addnew1);
		newmenu = (ImageView) findViewById(R.id.newmenu1);
		playlist = (ListView) findViewById(R.id.playlist1);
		djdisk1 = (ImageView) findViewById(R.id.leftdisk);
		djdisk2 = (ImageView) findViewById(R.id.rightdisk);
		djplaypause1 = (ImageView) findViewById(R.id.djplay1);
		djplaypause2 = (ImageView) findViewById(R.id.djplay2);
		djplaypause1.setOnClickListener(this);
		djplaypause2.setOnClickListener(this);
		addbt.setOnClickListener(this);
		newmenu.setOnClickListener(this);
		dj1vol = (SeekBar) findViewById(R.id.djseekbar1);
		dj2vol = (SeekBar) findViewById(R.id.djseekbar2);
		volseekbar = (SeekBar) findViewById(R.id.volseekbar);

		dj1vol.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				float vol1 = (progress) / 100.0f;
				mpdj1.setVolume(vol1, vol1);
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
			}
		});

		dj2vol.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				float vol1 = (progress) / 100.0f;
				mpdj2.setVolume(vol1, vol1);
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
			}
		});
		volseekbar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				if (cvol == progress) {
					lvol = 0.5f;
					Rvol = 0.5f;
				}
				lvol = (100 - progress) / 100.0f;
				Rvol = (progress) / 100.0f;
				mpdj1.setVolume(lvol, lvol);
				mpdj2.setVolume(Rvol, Rvol);
			}
		});

		l1 = (LinearLayout) findViewById(R.id.l1);
		l2 = (LinearLayout) findViewById(R.id.l2);

		playlist.setOnItemLongClickListener(new MyClickListener1());
		playlist.setOnDragListener(new DragListener1());
		findViewById(R.id.layer1).setOnDragListener(new DragListener1());
		findViewById(R.id.layer2).setOnDragListener(new DragListener2());

		musicadapter = new MusicAdapter(MainActivity.this,
				R.layout.playlistitem, musiclist);
		playlist.setAdapter(musicadapter);

		anim = new RotateAnimation(0f, 350f, RotateAnimation.RELATIVE_TO_SELF,
				0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.5f);
		anim.setInterpolator(new LinearInterpolator());
		anim.setRepeatCount(Animation.INFINITE);
		anim.setDuration(700);

		leftdjst = new MediaPlayer();
		rightdjst = new MediaPlayer();

		djdisk1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				leftdjst.reset();
				leftdjst("Scratch_Left.ogg");
			}
		});
		djdisk2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				rightdjst.reset();
				rightdjst("Scratch_Right.ogg");
			}
		});

		mpdj1.setOnCompletionListener(new OnCompletionListener() {
			@Override
			public void onCompletion(MediaPlayer mp) {
				djdisk1.setAnimation(null);
				isDjPlaying1 = false;
				isdatasetDj1 = false;
				djplaypause1.setImageResource(R.drawable.ic_action_play);
			}
		});

		mpdj2.setOnCompletionListener(new OnCompletionListener() {
			@Override
			public void onCompletion(MediaPlayer mp) {
				djdisk2.setAnimation(null);
				isDjPlaying2 = false;
				isdatasetDj2 = false;
				djplaypause2.setImageResource(R.drawable.ic_action_play);
			}
		});


		lplay = (ImageView) findViewById(R.id.lplay);
		lsync = (ImageView) findViewById(R.id.lsync);
		lplayarrow1 = (ImageView) findViewById(R.id.lplayarrow1);
		lplayarrow2 = (ImageView) findViewById(R.id.lplayarrow2);
		rplay = (ImageView) findViewById(R.id.rplay);
		rsync = (ImageView) findViewById(R.id.rsync);
		rplayarrow1 = (ImageView) findViewById(R.id.rplayarrow1);
		rplayarrow2 = (ImageView) findViewById(R.id.rplayarrow2);

		lplay.setOnClickListener(this);
		lsync.setOnClickListener(this);
		lplayarrow1.setOnClickListener(this);
		lplayarrow2.setOnClickListener(this);
		rplay.setOnClickListener(this);
		rsync.setOnClickListener(this);
		rplayarrow1.setOnClickListener(this);
		rplayarrow2.setOnClickListener(this);

		leftseek.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {

			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				float Rvol = (progress) / 100.0f;
				leftmp1.setVolume(Rvol, Rvol);
				leftmp2.setVolume(Rvol, Rvol);
				leftmp3.setVolume(Rvol, Rvol);
				leftmp12.setVolume(Rvol, Rvol);
				leftmp22.setVolume(Rvol, Rvol);
				leftmp32.setVolume(Rvol, Rvol);

			}
		});
		rightseek.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {

			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				float Rvol = (progress) / 100.0f;

				rightmp1.setVolume(Rvol, Rvol);
				rightmp2.setVolume(Rvol, Rvol);
				rightmp3.setVolume(Rvol, Rvol);
				rightmp12.setVolume(Rvol, Rvol);
				rightmp22.setVolume(Rvol, Rvol);
				rightmp32.setVolume(Rvol, Rvol);
			}
		});

		leftdiskmp.setOnCompletionListener(new OnCompletionListener() {

			@Override
			public void onCompletion(MediaPlayer mp) {

				if (!isleftsync) {
					leftdiskmp.reset();
					int upname1 = Integer.valueOf(MusicLoopClass.mploo1) + 1;
					if (upname1 == 5) {
						MusicLoopClass.mploo1 = "1";
					} else {
						MusicLoopClass.mploo1 = upname1 + "";
					}
					lplaytext.setText("LOOP A" + MusicLoopClass.mploo1);
					playleftdisk("drums" + MusicLoopClass.mploo1 + ".ogg");

				}

			}
		});

		rightdiskmp.setOnCompletionListener(new OnCompletionListener() {

			@Override
			public void onCompletion(MediaPlayer mp) {

				if (!isrightsync) {
					rightdiskmp.reset();
					int upname1 = Integer.valueOf(MusicLoopClass.mploo2) + 1;
					if (upname1 == 5) {
						MusicLoopClass.mploo2 = "1";
					} else {
						MusicLoopClass.mploo2 = upname1 + "";
					}
					rplaytext.setText("LOOP B" + MusicLoopClass.mploo2);
					playrightdisk("music" + MusicLoopClass.mploo2 + ".ogg");

				}

			}
		});
		lr1 = (ImageView) findViewById(R.id.lr1);
		lr2 = (ImageView) findViewById(R.id.lr2);
		lr3 = (ImageView) findViewById(R.id.lr3);
		rr1 = (ImageView) findViewById(R.id.rr1);
		rr2 = (ImageView) findViewById(R.id.rr2);
		rr3 = (ImageView) findViewById(R.id.rr3);
		lr12 = (ImageView) findViewById(R.id.lr12);
		lr22 = (ImageView) findViewById(R.id.lr22);
		lr32 = (ImageView) findViewById(R.id.lr32);
		rr12 = (ImageView) findViewById(R.id.rr12);
		rr22 = (ImageView) findViewById(R.id.rr22);
		rr32 = (ImageView) findViewById(R.id.rr32);
		lr1.setOnClickListener(this);
		lr2.setOnClickListener(this);
		lr3.setOnClickListener(this);
		rr1.setOnClickListener(this);
		rr2.setOnClickListener(this);
		rr3.setOnClickListener(this);
		lr12.setOnClickListener(this);
		lr22.setOnClickListener(this);
		lr32.setOnClickListener(this);
		rr12.setOnClickListener(this);
		rr22.setOnClickListener(this);
		rr32.setOnClickListener(this);

		// ==================================================
		mp1 = new MediaPlayer();
		mp2 = new MediaPlayer();
		mp3 = new MediaPlayer();
		mp4 = new MediaPlayer();
		mp5 = new MediaPlayer();
		mp6 = new MediaPlayer();
		mp7 = new MediaPlayer();
		mp8 = new MediaPlayer();
		mp9 = new MediaPlayer();
		mp10 = new MediaPlayer();
		mp11 = new MediaPlayer();
		mp12 = new MediaPlayer();
		mp13 = new MediaPlayer();
		mp14 = new MediaPlayer();
		mp15 = new MediaPlayer();
		mp16 = new MediaPlayer();
		keymp0 = new MediaPlayer();
		keymp1 = new MediaPlayer();
		keymp2 = new MediaPlayer();
		keymp3 = new MediaPlayer();
		keymp4 = new MediaPlayer();
		keymp5 = new MediaPlayer();
		keymp6 = new MediaPlayer();
		keymp7 = new MediaPlayer();
		keymp8 = new MediaPlayer();
		keymp9 = new MediaPlayer();
		keymp10 = new MediaPlayer();
		keymp11 = new MediaPlayer();
		keymp12 = new MediaPlayer();
		keymp13 = new MediaPlayer();
		keymp14 = new MediaPlayer();
		keymp15 = new MediaPlayer();
		keymp16 = new MediaPlayer();
		keymp17 = new MediaPlayer();

		s1 = (ImageView) findViewById(R.id.s1);
		s2 = (ImageView) findViewById(R.id.s2);
		s3 = (ImageView) findViewById(R.id.s3);
		s4 = (ImageView) findViewById(R.id.s4);
		s5 = (ImageView) findViewById(R.id.s5);
		s6 = (ImageView) findViewById(R.id.s6);
		s7 = (ImageView) findViewById(R.id.s7);
		s8 = (ImageView) findViewById(R.id.s8);
		s9 = (ImageView) findViewById(R.id.s9);
		s10 = (ImageView) findViewById(R.id.s10);
		s11 = (ImageView) findViewById(R.id.s11);
		s12 = (ImageView) findViewById(R.id.s12);
		s13 = (ImageView) findViewById(R.id.s13);
		s14 = (ImageView) findViewById(R.id.s14);
		s15 = (ImageView) findViewById(R.id.s15);
		s16 = (ImageView) findViewById(R.id.s16);
		s1.setOnClickListener(this);
		s2.setOnClickListener(this);
		s3.setOnClickListener(this);
		s4.setOnClickListener(this);
		s5.setOnClickListener(this);
		s6.setOnClickListener(this);
		s7.setOnClickListener(this);
		s8.setOnClickListener(this);
		s9.setOnClickListener(this);
		s10.setOnClickListener(this);
		s11.setOnClickListener(this);
		s12.setOnClickListener(this);
		s13.setOnClickListener(this);
		s14.setOnClickListener(this);
		s15.setOnClickListener(this);
		s16.setOnClickListener(this);

		piano1 = (Piano) findViewById(R.id.piano1);
		piano1.setPianoKeyListener(new PianoKeyListener() {
			@Override
			public void keyPressed(int id, int action) {

				if (action == MotionEvent.ACTION_DOWN) {

					if (pianoname1.equals("piano")) {
						play((pianoname1 + id + ".ogg"), id);

					} else {
						play((pianoname1 + id + ".ogg"), id);

					}
				}
			}
		});
		padvol1 = (VerticalSeekBar) findViewById(R.id.padvol1);
		padvol1.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				// TODO Auto-generated method stub
				final float vol1 = (progress) / 100.0f;
				MainActivity.this.runOnUiThread(new Runnable() {

					@Override
					public void run() {

						keymp0.setVolume(vol1, vol1);
						keymp1.setVolume(vol1, vol1);
						keymp2.setVolume(vol1, vol1);
						keymp3.setVolume(vol1, vol1);
						keymp4.setVolume(vol1, vol1);
						keymp5.setVolume(vol1, vol1);
						keymp6.setVolume(vol1, vol1);
						keymp7.setVolume(vol1, vol1);
						keymp8.setVolume(vol1, vol1);
						keymp9.setVolume(vol1, vol1);
						keymp10.setVolume(vol1, vol1);
						keymp11.setVolume(vol1, vol1);
						keymp12.setVolume(vol1, vol1);
						keymp13.setVolume(vol1, vol1);
						keymp14.setVolume(vol1, vol1);
						keymp15.setVolume(vol1, vol1);
						keymp16.setVolume(vol1, vol1);
						keymp17.setVolume(vol1, vol1);

					}
				});
			}
		});

	}

	private static final float VISUALIZER_HEIGHT_DIP = 50f;
	private Visualizer mVisualizer1, mVisualizer2;
	private VisualizerView mVisualizerView1, mVisualizerView2;

	private void setupVisualizerFxAndUI1() {
		mVisualizerView1 = new VisualizerView(this);
		mVisualizerView1.setLayoutParams(new ViewGroup.LayoutParams(
				ViewGroup.LayoutParams.FILL_PARENT,
				(int) (VISUALIZER_HEIGHT_DIP * getResources()
						.getDisplayMetrics().density)));
		l1.addView(mVisualizerView1);
		mVisualizer1 = new Visualizer(mpdj1.getAudioSessionId());
		mVisualizer1.setCaptureSize(Visualizer.getCaptureSizeRange()[1]);
		mVisualizer1.setDataCaptureListener(
				new Visualizer.OnDataCaptureListener() {
					public void onWaveFormDataCapture(Visualizer visualizer,
							byte[] bytes, int samplingRate) {
						mVisualizerView1.updateVisualizer(bytes);
					}

					public void onFftDataCapture(Visualizer visualizer,
							byte[] bytes, int samplingRate) {
					}
				}, Visualizer.getMaxCaptureRate() / 2, true, false);
	}

	private void setupVisualizerFxAndUI2() {
		mVisualizerView2 = new VisualizerView(this);
		mVisualizerView2.setLayoutParams(new ViewGroup.LayoutParams(
				ViewGroup.LayoutParams.FILL_PARENT,
				(int) (VISUALIZER_HEIGHT_DIP * getResources()
						.getDisplayMetrics().density)));
		l2.addView(mVisualizerView2);
		mVisualizer2 = new Visualizer(mpdj2.getAudioSessionId());
		mVisualizer2.setCaptureSize(Visualizer.getCaptureSizeRange()[1]);
		mVisualizer2.setDataCaptureListener(
				new Visualizer.OnDataCaptureListener() {
					public void onWaveFormDataCapture(Visualizer visualizer,
							byte[] bytes, int samplingRate) {
						mVisualizerView2.updateVisualizer(bytes);
					}

					public void onFftDataCapture(Visualizer visualizer,
							byte[] bytes, int samplingRate) {
					}
				}, Visualizer.getMaxCaptureRate() / 2, true, false);
	}

	class DragListener1 implements OnDragListener {
		@Override
		public boolean onDrag(View v, DragEvent event) {

			switch (event.getAction()) {

			case DragEvent.ACTION_DRAG_STARTED:

				break;

			case DragEvent.ACTION_DRAG_ENTERED:
				break;

			case DragEvent.ACTION_DRAG_EXITED:
				break;

			case DragEvent.ACTION_DROP:
				mpdj1.reset();
				ClipData.Item item = event.getClipData().getItemAt(0);
				File filename11 = new File(item.getText().toString());
				String name = filename11.getName();
				Log.e("name : ", name);

				filename1 = new File(item.getText().toString());

				isdatasetDj1 = true;
				try {

					djplaypause1.setImageResource(R.drawable.ic_action_pause);
					playdjmusic1(filename1.getAbsolutePath());
				} catch (IllegalStateException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();

				}
				break;

			case DragEvent.ACTION_DRAG_ENDED:
				break;
			default:
				break;
			}
			return true;
		}
	}

	class DragListener2 implements OnDragListener {
		@Override
		public boolean onDrag(View v, DragEvent event) {

			switch (event.getAction()) {

			case DragEvent.ACTION_DRAG_STARTED:

				break;

			case DragEvent.ACTION_DRAG_ENTERED:
				break;

			case DragEvent.ACTION_DRAG_EXITED:
				break;

			case DragEvent.ACTION_DROP:
				mpdj2.reset();
				ClipData.Item item = event.getClipData().getItemAt(0);
				File filename11 = new File(item.getText().toString());
				String name = filename11.getName();
				Log.e("name : ", name);

				isdatasetDj2 = true;
				try {

					djplaypause2.setImageResource(R.drawable.ic_action_pause);
					playdjmusic2(filename11.getAbsolutePath());
				} catch (IllegalStateException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();

				}
				break;

			case DragEvent.ACTION_DRAG_ENDED:
				break;
			default:
				break;
			}
			return true;
		}
	}

	private void playdjmusic1(String path) throws IllegalStateException,
			IOException {
		try {
			List<Musicmodel> el = dbHandler.Get_playList(false);
			if (el.size() == 0) {
				AssetFileDescriptor descriptor = getAssets().openFd(
						"sample" + path);
				mpdj1.setDataSource(descriptor.getFileDescriptor(),
						descriptor.getStartOffset(), descriptor.getLength());
				descriptor.close();
			} else {
				mpdj1.setDataSource(getApplicationContext(), Uri.parse(path));
			}
			mpdj1.prepare();

			mpdj1.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mpdj1.start();
					isDjPlaying1 = true;
				}
			});
			djdisk1.startAnimation(anim);
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playdjmusic2(String path) throws IllegalStateException,
			IOException {
		try {
			List<Musicmodel> el = dbHandler.Get_playList(false);
			if (el.size() == 0) {
				AssetFileDescriptor descriptor = getAssets().openFd(
						"sample" + path);
				mpdj2.setDataSource(descriptor.getFileDescriptor(),
						descriptor.getStartOffset(), descriptor.getLength());
				descriptor.close();
			} else {
				mpdj2.setDataSource(getApplicationContext(), Uri.parse(path));
			}
			mpdj2.prepare();

			mpdj2.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mpdj2.start();
					isDjPlaying2 = true;
				}
			});
			djdisk2.startAnimation(anim);
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private final class MyClickListener1 implements OnItemLongClickListener {

		@Override
		public boolean onItemLongClick(AdapterView<?> arg0, View view,
				int position, long arg3) {

			String selectedFromList = musiclist.get(position).path;
			ClipData.Item item = new ClipData.Item(
					((CharSequence) selectedFromList.toString()));
			String[] clipDescription = { ClipDescription.MIMETYPE_TEXT_PLAIN };
			ClipData dragData = new ClipData("item", clipDescription, item);
			DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
			view.startDrag(dragData, shadowBuilder, view, 0);
			return true;
		}

	}

	private void playleftdisk(String path) {
		try {

			Log.e("tag", isleftsync + "");
			AssetFileDescriptor descriptor = getAssets()
					.openFd("music/" + path);
			leftdiskmp.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			leftdiskmp.prepare();
			if (isleftsync) {
				leftdiskmp.setLooping(true);
			}
			leftdiskmp.setOnPreparedListener(new OnPreparedListener() {

				@Override
				public void onPrepared(MediaPlayer mp) {
					leftdiskmp.start();
				}
			});

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playrightdisk(String path) {
		try {

			Log.e("tag", isrightsync + "");
			AssetFileDescriptor descriptor = getAssets()
					.openFd("music/" + path);
			rightdiskmp.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			rightdiskmp.prepare();
			if (isrightsync) {
				rightdiskmp.setLooping(true);
			}
			rightdiskmp.setOnPreparedListener(new OnPreparedListener() {

				@Override
				public void onPrepared(MediaPlayer mp) {
					rightdiskmp.start();
				}
			});

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private Context getcontext() {

		return MainActivity.this;
	}

	public void displayInterstitial() {
		if (interstitial.isLoaded()) {
			interstitial.show();
		}
	}

	private void showratedialog() {
		new AlertDialog.Builder(this)
				.setTitle("Do you want to rate this application?")
				.setPositiveButton("Never",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								// continue with delete
								dialog.dismiss();
							}
						})
				.setNegativeButton("not now",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								// do nothing
								dialog.dismiss();
							}
						})
				.setNeutralButton("Rate Now",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								// do nothing
								startActivity(new Intent(Intent.ACTION_VIEW,
										Uri.parse("market://details?id="
												+ "com.djdeve.djstudiomixpiano")));
							}
						}).show();
	}
	private void setke1() {
		s1.setBackgroundResource(R.drawable.pianoo);
		s2.setBackgroundResource(R.drawable.violin);
		s3.setBackgroundResource(R.drawable.guitar);
		s4.setBackgroundResource(R.drawable.oboe);

	}
	String pianoname1 = "piano";

	@SuppressLint("NewApi")
	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.s1:
			setke1();
			s1.setBackgroundResource(R.drawable.piano_s);
			pianoname1 = "Piano";
			break;
		case R.id.s2:

			setke1();
			s2.setBackgroundResource(R.drawable.violin_s);
			pianoname1 = "Flute";
			break;
		case R.id.s3:
			setke1();
			s3.setBackgroundResource(R.drawable.guitar_s);
			pianoname1 = "Organ";
			break;

		case R.id.s4:
			setke1();
			s4.setBackgroundResource(R.drawable.oboe_s);
			pianoname1 = "Saxophone";
			break;
		case R.id.s5:
			if (mp5.isPlaying()) {
				mp5.stop();
			} else {
				playsound5(kit + "5.ogg");
			}
			break;
		case R.id.s6:
			if (mp6.isPlaying()) {
				mp6.stop();
			} else {
				playsound6(kit + "6.ogg");
			}
			break;
		case R.id.s7:
			if (mp7.isPlaying()) {
				mp7.stop();
			} else {
				playsound7(kit + "7.ogg");
			}
			break;
		case R.id.s8:
			if (mp8.isPlaying()) {
				mp8.stop();
			} else {
				playsound8(kit + "8.ogg");
			}
			
			break;
		case R.id.s9:
			if (mp9.isPlaying()) {
				mp9.stop();
				s9.setBackground(getResources().getDrawable(
						R.drawable.round1));
			} else {
				playsound9(kit + "9.ogg");
				s9.setBackground(getResources().getDrawable(
						R.drawable.roundgray2));
			}
			break;
		case R.id.s10:
			if (mp10.isPlaying()) {
				mp10.stop();
				s10.setBackground(getResources().getDrawable(
						R.drawable.round1));
			} else {
				playsound10(kit + "10.ogg");
				s10.setBackground(getResources().getDrawable(
						R.drawable.roundgray2));
			}
			break;
		case R.id.s11:
			if (mp11.isPlaying()) {
				mp11.stop();
				s11.setBackground(getResources().getDrawable(
						R.drawable.round1));
			} else {
				playsound11(kit + "11.ogg");
				s11.setBackground(getResources().getDrawable(
						R.drawable.roundgray2));
			}
			break;
		case R.id.s12:
			if (mp12.isPlaying()) {
				mp12.stop();
				s12.setBackground(getResources().getDrawable(
						R.drawable.round1));
			} else {
				playsound12(kit + "12.ogg");
				s12.setBackground(getResources().getDrawable(
						R.drawable.roundgray2));
			}
			break;
		case R.id.s13:
			if (mp13.isPlaying()) {
				mp13.stop();
				s13.setBackground(getResources().getDrawable(
						R.drawable.round1));
			} else {
				playsound13(kit + "13.ogg");
				s13.setBackground(getResources().getDrawable(
						R.drawable.roundgray2));
			}
			break;
		case R.id.s14:
			if (mp14.isPlaying()) {
				mp14.stop();
				s14.setBackground(getResources().getDrawable(
						R.drawable.round1));
			} else {
				playsound14(kit + "14.ogg");
				s14.setBackground(getResources().getDrawable(
						R.drawable.roundgray2));
			}
			break;
		case R.id.s15:
			if (mp15.isPlaying()) {
				mp15.stop();
				s15.setBackground(getResources().getDrawable(
						R.drawable.round1));
			} else {
				playsound15(kit + "15.ogg");
				s15.setBackground(getResources().getDrawable(
						R.drawable.roundgray2));
			}
			break;
		case R.id.s16:
			if (mp16.isPlaying()) {
				mp16.stop();
				s16.setBackground(getResources().getDrawable(
						R.drawable.round1));
			} else {
				playsound16(kit + "16.ogg");
				s16.setBackground(getResources().getDrawable(
						R.drawable.roundgray2));
			}
			break;

		case R.id.screen1:

			screen1.setImageResource(R.drawable.a_color);
			screen2.setImageResource(R.drawable.b);

			mscreen2.setVisibility(View.GONE);
			mscreen1.setVisibility(View.VISIBLE);

			break;
		case R.id.screen2:
			screen1.setImageResource(R.drawable.a);
			screen2.setImageResource(R.drawable.b_color);

			mscreen1.setVisibility(View.GONE);
			mscreen2.setVisibility(View.VISIBLE);

			break;

		case R.id.lr1:
			if (!leftmp1.isPlaying()) {
				leftmp1.reset();
				playroundmusic1();
				lr1.setImageDrawable(getResources().getDrawable(
						R.drawable.roundgray));
			} else {
				leftmp1.pause();
				lr1.setImageDrawable(getResources().getDrawable(
						R.drawable.round));
			}

			break;
		case R.id.lr2:

			if (!leftmp2.isPlaying()) {
				leftmp2.reset();
				playroundmusic2();
				lr2.setImageDrawable(getResources().getDrawable(
						R.drawable.roundgray));
			} else {
				leftmp2.pause();
				lr2.setImageDrawable(getResources().getDrawable(
						R.drawable.round));
			}

			break;
		case R.id.lr3:

			if (!leftmp3.isPlaying()) {
				leftmp3.reset();
				playroundmusic3();
				lr3.setImageDrawable(getResources().getDrawable(
						R.drawable.roundgray));
			} else {
				leftmp3.pause();
				lr3.setImageDrawable(getResources().getDrawable(
						R.drawable.round));
			}

			break;
		case R.id.rr1:
			if (!rightmp1.isPlaying()) {
				rightmp1.reset();
				playroundmusicright1();
				rr1.setImageDrawable(getResources().getDrawable(
						R.drawable.roundgray));
			} else {
				rightmp1.pause();
				rr1.setImageDrawable(getResources().getDrawable(
						R.drawable.round));

			}

			break;
		case R.id.rr2:
			if (!rightmp2.isPlaying()) {
				rightmp2.reset();
				playroundmusicright2();
				rr2.setImageDrawable(getResources().getDrawable(
						R.drawable.roundgray));

			} else {
				rightmp2.pause();
				rr2.setImageDrawable(getResources().getDrawable(
						R.drawable.round));

			}
			break;
		case R.id.rr3:
			if (!rightmp3.isPlaying()) {
				rightmp3.reset();
				playroundmusicright3();
				rr3.setImageDrawable(getResources().getDrawable(
						R.drawable.roundgray));

			} else {
				rightmp3.pause();
				rr3.setImageDrawable(getResources().getDrawable(
						R.drawable.round));

			}
			break;
		case R.id.lr12:
			if (!leftmp12.isPlaying()) {
				leftmp12.reset();
				playroundmusic12();
				lr12.setImageDrawable(getResources().getDrawable(
						R.drawable.roundgray2));
			} else {
				leftmp12.pause();
				lr12.setImageDrawable(getResources().getDrawable(
						R.drawable.round1));
			}

			break;
		case R.id.lr22:

			if (!leftmp22.isPlaying()) {
				leftmp22.reset();
				playroundmusic22();
				lr22.setImageDrawable(getResources().getDrawable(
						R.drawable.roundgray2));
			} else {
				leftmp22.pause();
				lr22.setImageDrawable(getResources().getDrawable(
						R.drawable.round1));
			}

			break;
		case R.id.lr32:

			if (!leftmp32.isPlaying()) {
				leftmp32.reset();
				playroundmusic32();
				lr32.setImageDrawable(getResources().getDrawable(
						R.drawable.roundgray2));
			} else {
				leftmp32.pause();
				lr32.setImageDrawable(getResources().getDrawable(
						R.drawable.round1));
			}

			break;
		case R.id.rr12:
			if (!rightmp12.isPlaying()) {
				rightmp12.reset();
				playroundmusicright12();
				rr12.setImageDrawable(getResources().getDrawable(
						R.drawable.roundgray2));
			} else {
				rightmp12.pause();
				rr12.setImageDrawable(getResources().getDrawable(
						R.drawable.round1));

			}

			break;
		case R.id.rr22:
			if (!rightmp22.isPlaying()) {
				rightmp22.reset();
				playroundmusicright22();
				rr22.setImageDrawable(getResources().getDrawable(
						R.drawable.roundgray2));

			} else {
				rightmp22.pause();
				rr22.setImageDrawable(getResources().getDrawable(
						R.drawable.round1));

			}
			break;
		case R.id.rr32:
			if (!rightmp32.isPlaying()) {
				rightmp32.reset();
				playroundmusicright32();
				rr32.setImageDrawable(getResources().getDrawable(
						R.drawable.roundgray2));

			} else {
				rightmp32.pause();
				rr32.setImageDrawable(getResources().getDrawable(
						R.drawable.round1));

			}
			break;

		case R.id.lplay:
			if (leftdiskmp.isPlaying()) {
				leftdiskmp.pause();
				lplay.setImageDrawable(getResources().getDrawable(
						R.drawable.ic_action_play));
			} else {
				leftdiskmp.reset();
				playleftdisk("drums" + MusicLoopClass.mploo1 + ".ogg");
				lplay.setImageDrawable(getResources().getDrawable(
						R.drawable.ic_action_pause));
			}

			break;
		case R.id.lsync:
			isleftsync = !isleftsync;
			if (leftdiskmp.isPlaying()) {

				leftdiskmp.reset();
				playleftdisk("drums" + MusicLoopClass.mploo1 + ".ogg");
			}
			if (isleftsync) {
				lsync.setBackgroundColor(getResources().getColor(
						R.color.darkblue));
			} else {
				lsync.setBackgroundColor(getResources().getColor(R.color.blue));
			}

			break;
		case R.id.lplayarrow1:
			leftdiskmp.reset();
			int downname1 = Integer.valueOf(MusicLoopClass.mploo1) - 1;
			if (downname1 == 0) {
				MusicLoopClass.mploo1 = "4";
			} else {
				MusicLoopClass.mploo1 = downname1 + "";
			}
			lplaytext.setText("LOOP A" + MusicLoopClass.mploo1);
			playleftdisk("drums" + MusicLoopClass.mploo1 + ".ogg");
			lplay.setImageDrawable(getResources().getDrawable(
					R.drawable.ic_action_pause));

			break;
		case R.id.lplayarrow2:
			leftdiskmp.reset();
			int upname1 = Integer.valueOf(MusicLoopClass.mploo1) + 1;
			if (upname1 == 5) {
				MusicLoopClass.mploo1 = "1";
			} else {
				MusicLoopClass.mploo1 = upname1 + "";
			}
			lplaytext.setText("LOOP A" + MusicLoopClass.mploo1);
			playleftdisk("drums" + MusicLoopClass.mploo1 + ".ogg");
			lplay.setImageDrawable(getResources().getDrawable(
					R.drawable.ic_action_pause));

			break;
		case R.id.rplay:
			if (rightdiskmp.isPlaying()) {
				rightdiskmp.pause();
				rplay.setImageDrawable(getResources().getDrawable(
						R.drawable.ic_action_play));
			} else {
				rightdiskmp.reset();
				playrightdisk("music" + MusicLoopClass.mploo1 + ".ogg");
				rplay.setImageDrawable(getResources().getDrawable(
						R.drawable.ic_action_pause));
			}
			break;
		case R.id.rsync:
			isrightsync = !isrightsync;
			if (rightdiskmp.isPlaying()) {
				rightdiskmp.reset();
				playrightdisk("music" + MusicLoopClass.mploo2 + ".ogg");

			}
			if (isrightsync) {
				rsync.setBackgroundColor(getResources().getColor(
						R.color.darkblue));
			} else {
				rsync.setBackgroundColor(getResources().getColor(R.color.blue));
			}

			break;
		case R.id.rplayarrow1:
			rightdiskmp.reset();
			int downname2 = Integer.valueOf(MusicLoopClass.mploo2) - 1;
			if (downname2 == 0) {
				MusicLoopClass.mploo2 = "4";
			} else {
				MusicLoopClass.mploo2 = downname2 + "";
			}
			rplaytext.setText("LOOP B" + MusicLoopClass.mploo2);
			playrightdisk("music" + MusicLoopClass.mploo2 + ".ogg");
			rplay.setImageDrawable(getResources().getDrawable(
					R.drawable.ic_action_pause));
			break;
		case R.id.rplayarrow2:
			rightdiskmp.reset();
			int upname = Integer.valueOf(MusicLoopClass.mploo2) + 1;
			if (upname == 5) {
				MusicLoopClass.mploo2 = "1";
			} else {
				MusicLoopClass.mploo2 = upname + "";
			}
			rplaytext.setText("LOOP B" + MusicLoopClass.mploo2);
			playrightdisk("music" + MusicLoopClass.mploo2 + ".ogg");
			rplay.setImageDrawable(getResources().getDrawable(
					R.drawable.ic_action_pause));
			break;
		case R.id.addnew1:
			startActivity(new Intent(getactivity(), FileBrowserActivity.class));
			break;
		case R.id.newmenu1:
			String[] s = getResources().getStringArray(R.array.setting_Option);

			new AlertDialog.Builder(getactivity())
					.setItems(s, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							switch (which) {
							case 0:
								dbHandler.Delete_All();
								musiclist.clear();
								musicadapter.notifyDataSetChanged();
								List<Musicmodel> ei = dbHandler
										.Get_playList(false);
								if (ei.size() == 0) {
									AssetManager assetManager = getApplicationContext()
											.getAssets();
									String[] fields;
									try {
										fields = assetManager.list("sample");
										for (int count = 0; count < fields.length; count++) {

											Musicmodel m = new Musicmodel();
											m.name = fields[count];
											m.path = fields[count];
											musiclist.add(m);
										}
									} catch (IOException e1) {
										e1.printStackTrace();
									}
								}
								break;
							case 1:
								musiclist.clear();
								musicadapter.notifyDataSetChanged();
								List<Musicmodel> el = dbHandler
										.Get_playList(true);
								for (Musicmodel e : el) {
									musiclist.add(e);
								}
								musicadapter.notifyDataSetChanged();
								break;
							case 2:
								showalert();
								break;
							case 3:
								openplaylist();
								break;
							}
						}
					}).create().show();
			break;

		case R.id.djplay1:
			if (isDjPlaying1) {
				isDjPlaying1 = false;
				mpdj1.pause();
				djdisk1.setAnimation(null);
				djplaypause1.setImageResource(R.drawable.ic_action_play);

			} else {
				if (isdatasetDj1) {
					djplaypause1.setImageResource(R.drawable.ic_action_pause);
					djdisk1.startAnimation(anim);
					mpdj1.start();
					isDjPlaying1 = true;
				}
			}
			break;

		case R.id.djplay2:
			if (isDjPlaying2) {
				isDjPlaying2 = false;
				mpdj2.pause();
				djdisk2.setAnimation(null);
				djplaypause2.setImageResource(R.drawable.ic_action_play);

			} else {
				if (isdatasetDj2) {
					djplaypause2.setImageResource(R.drawable.ic_action_pause);
					djdisk2.startAnimation(anim);
					mpdj2.start();
					isDjPlaying2 = true;
				}
			}
			break;

		default:
			break;
		}
	}

	private MainActivity getactivity() {
		return MainActivity.this;
	}

	private void playroundmusic1() {
		try {
			AssetFileDescriptor descriptor = getAssets()
					.openFd("music/lm1.ogg");
			leftmp1.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			leftmp1.prepare();
			leftmp1.setLooping(true);
			leftmp1.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					leftmp1.start();
				}
			});
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playroundmusic2() {
		try {
			AssetFileDescriptor descriptor = getAssets()
					.openFd("music/lm2.ogg");
			leftmp2.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			leftmp2.prepare();
			leftmp2.setLooping(true);
			leftmp2.setOnPreparedListener(new OnPreparedListener() {

				@Override
				public void onPrepared(MediaPlayer mp) {
					leftmp2.start();
				}
			});

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playroundmusic3() {
		try {
			AssetFileDescriptor descriptor = getAssets()
					.openFd("music/lm3.ogg");
			leftmp3.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			leftmp3.prepare();
			leftmp3.setLooping(true);
			leftmp3.setOnPreparedListener(new OnPreparedListener() {

				@Override
				public void onPrepared(MediaPlayer mp) {
					leftmp3.start();
				}
			});

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playroundmusicright1() {
		try {
			AssetFileDescriptor descriptor = getAssets()
					.openFd("music/rm1.ogg");
			rightmp1.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			rightmp1.prepare();
			rightmp1.setLooping(true);
			rightmp1.setOnPreparedListener(new OnPreparedListener() {

				@Override
				public void onPrepared(MediaPlayer mp) {
					rightmp1.start();
				}
			});

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playroundmusicright2() {
		try {
			AssetFileDescriptor descriptor = getAssets()
					.openFd("music/rm2.ogg");
			rightmp2.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			rightmp2.prepare();
			rightmp2.setLooping(true);
			rightmp2.setOnPreparedListener(new OnPreparedListener() {

				@Override
				public void onPrepared(MediaPlayer mp) {
					rightmp2.start();
				}
			});

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playroundmusicright3() {
		try {
			AssetFileDescriptor descriptor = getAssets()
					.openFd("music/rm3.ogg");
			rightmp3.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			rightmp3.prepare();
			rightmp3.setLooping(true);
			rightmp3.setOnPreparedListener(new OnPreparedListener() {

				@Override
				public void onPrepared(MediaPlayer mp) {
					rightmp3.start();
				}
			});

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playroundmusic12() {
		try {
			AssetFileDescriptor descriptor = getAssets()
					.openFd("music/lm4.ogg");
			leftmp12.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			leftmp12.prepare();
			leftmp12.setLooping(true);
			leftmp12.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					leftmp12.start();
				}
			});
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playroundmusic22() {
		try {
			AssetFileDescriptor descriptor = getAssets()
					.openFd("music/lm5.ogg");
			leftmp22.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			leftmp22.prepare();
			leftmp22.setLooping(true);
			leftmp22.setOnPreparedListener(new OnPreparedListener() {

				@Override
				public void onPrepared(MediaPlayer mp) {
					leftmp22.start();
				}
			});

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playroundmusic32() {
		try {
			AssetFileDescriptor descriptor = getAssets()
					.openFd("music/lm6.ogg");
			leftmp32.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			leftmp32.prepare();
			leftmp32.setLooping(true);
			leftmp32.setOnPreparedListener(new OnPreparedListener() {

				@Override
				public void onPrepared(MediaPlayer mp) {
					leftmp32.start();
				}
			});

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playroundmusicright12() {
		try {
			AssetFileDescriptor descriptor = getAssets()
					.openFd("music/rm4.ogg");
			rightmp12.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			rightmp12.prepare();
			rightmp12.setLooping(true);
			rightmp12.setOnPreparedListener(new OnPreparedListener() {

				@Override
				public void onPrepared(MediaPlayer mp) {
					rightmp12.start();
				}
			});

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playroundmusicright22() {
		try {
			AssetFileDescriptor descriptor = getAssets()
					.openFd("music/rm5.ogg");
			rightmp22.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			rightmp22.prepare();
			rightmp22.setLooping(true);
			rightmp22.setOnPreparedListener(new OnPreparedListener() {

				@Override
				public void onPrepared(MediaPlayer mp) {
					rightmp22.start();
				}
			});

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playroundmusicright32() {
		try {
			AssetFileDescriptor descriptor = getAssets()
					.openFd("music/rm6.ogg");
			rightmp32.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			rightmp32.prepare();
			rightmp32.setLooping(true);
			rightmp32.setOnPreparedListener(new OnPreparedListener() {

				@Override
				public void onPrepared(MediaPlayer mp) {
					rightmp32.start();
				}
			});

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void setmusiclist1() {
		musiclist.clear();
		musicadapter.notifyDataSetChanged();
		List<Musicmodel> el = dbHandler.Get_playList(false);
		for (Musicmodel e : el) {
			musiclist.add(e);
		}
		musicadapter.notifyDataSetChanged();
		if (el.size() == 0) {
			AssetManager assetManager = getApplicationContext().getAssets();
			String[] fields;
			try {
				fields = assetManager.list("sample");
				for (int count = 0; count < fields.length; count++) {
					Musicmodel m = new Musicmodel();
					m.name = fields[count];
					m.path = fields[count];
					musiclist.add(m);
				}
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}

	private void showalert() {
		AlertDialog.Builder alert = new AlertDialog.Builder(this);
		alert.setTitle("Save PlayList");
		final EditText input = new EditText(this);
		alert.setView(input);
		alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				String value = input.getText().toString();
				if (!value.equals("")) {
					dbHandler.Add_play(value.toString().trim());
					for (int i = 0; i < musiclist.size(); i++) {
						Musicmodel md = new Musicmodel();
						md.name = musiclist.get(i).name;
						md.path = musiclist.get(i).path;
						Log.e("value", value);
						dbHandler.Add_Saveplaylist(md, value.toString().trim());
					}
				}
			}
		});
		alert.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
					}
				});

		alert.show();
	}

	private void openplaylist() {
		final String[] openlist = dbHandler.openlist();
		new AlertDialog.Builder(getactivity())
				.setItems(openlist, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						musiclist.clear();
						musicadapter.notifyDataSetChanged();
						List<Musicmodel> save = dbHandler.Get_saveplayList(
								false, openlist[which].toString().trim());
						for (Musicmodel e : save) {
							musiclist.add(e);
						}
						musicadapter.notifyDataSetChanged();
					}
				}).create().show();
	}

	@Override
	public void onBackPressed() {
		showclose();
		displayInterstitial();
	}

	@Override
	protected void onResume() {
		super.onResume();

		setmusiclist1();

	}

	private void showclose() {
		new AlertDialog.Builder(this)
				.setTitle("Do you really want to shut down the app?")
				.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {

								setrelease();
								finish();
								return;
							}
						})
				.setNegativeButton("No", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				}).show();
	}

	private void setrelease() {
		mpdj1.release();
		mpdj2.release();
		leftdiskmp.release();
		rightdiskmp.release();
		leftmp1.release();
		leftmp2.release();
		leftmp3.release();
		rightmp1.release();
		rightmp2.release();
		rightmp3.release();
		leftmp12.release();
		leftmp22.release();
		leftmp32.release();
		rightmp12.release();
		rightmp22.release();
		rightmp32.release();

		mp1.release();
		mp2.release();
		mp3.release();
		mp4.release();
		mp5.release();
		mp6.release();
		mp7.release();
		mp8.release();
		mp9.release();
		mp10.release();
		mp11.release();
		mp12.release();
		mp13.release();
		mp14.release();
		mp15.release();
		mp16.release();

		keymp1.release();
		keymp2.release();
		keymp3.release();
		keymp4.release();
		keymp5.release();
		keymp6.release();
		keymp7.release();
		keymp8.release();
		keymp9.release();
		keymp10.release();
		keymp11.release();
		keymp12.release();
		keymp13.release();
		keymp14.release();
		keymp15.release();
		keymp16.release();
		keymp17.release();

		keymp0.release();

	}

	class VisualizerView extends View {
		private byte[] mBytes;
		private float[] mPoints;
		private Rect mRect = new Rect();

		private Paint mForePaint = new Paint();

		public VisualizerView(Context context) {
			super(context);
			init();
		}

		private void init() {
			mBytes = null;

			mForePaint.setStrokeWidth(4f);
			mForePaint.setAntiAlias(true);
			mForePaint.setColor(Color.rgb(252, 193, 91));
		}

		public void updateVisualizer(byte[] bytes) {
			mBytes = bytes;
			// Log.e("bytes","by: " + bytes);
			invalidate();
		}

		@Override
		protected void onDraw(Canvas canvas) {
			super.onDraw(canvas);

			if (mBytes == null) {
				return;
			}

			if (mPoints == null || mPoints.length < mBytes.length * 4) {
				mPoints = new float[mBytes.length * 4];
			}

			mRect.set(0, 0, getWidth(), getHeight());

			for (int i = 0; i < mBytes.length - 1; i++) {
				mPoints[i * 4] = mRect.width() * i / (mBytes.length - 1);
				mPoints[i * 4 + 1] = mRect.height() / 2
						+ ((byte) (mBytes[i] + 128)) * (mRect.height() / 2)
						/ 128;
				mPoints[i * 4 + 2] = mRect.width() * (i + 1)
						/ (mBytes.length - 1);
				mPoints[i * 4 + 3] = mRect.height() / 2
						+ ((byte) (mBytes[i + 1] + 128)) * (mRect.height() / 2)
						/ 128;
			}

			float centerX = mRect.width();
			float centerY = mRect.height();
			double angle = 90;
			Matrix rotateMat = new Matrix();
			rotateMat.setRotate((float) angle, centerX, centerY);
			// rotateMat.mapPoints(mPoints);

			canvas.drawLines(mPoints, mForePaint);

		}
	}

	private void play(String sd, int id) {

		switch (id) {
		case 0:
			playkeyboard0(sd);
			break;
		case 1:
			playkeyboard1(sd);
			break;
		case 2:
			playkeyboard2(sd);
			break;
		case 3:
			playkeyboard3(sd);
			break;
		case 4:
			playkeyboard4(sd);
			break;
		case 5:
			playkeyboard5(sd);
			break;
		case 6:
			playkeyboard6(sd);
			break;
		case 7:
			playkeyboard7(sd);
			break;
		case 8:
			playkeyboard8(sd);
			break;
		case 9:
			playkeyboard9(sd);
			break;
		case 10:
			playkeyboard10(sd);
			break;
		case 11:
			playkeyboard11(sd);
			break;
		case 12:
			playkeyboard12(sd);
			break;
		case 13:
			playkeyboard13(sd);
			break;
		case 14:
			playkeyboard14(sd);
			break;
		case 15:
			playkeyboard15(sd);
			break;
		case 16:
			playkeyboard16(sd);
			break;
		case 17:
			playkeyboard17(sd);
			break;

		default:
			break;
		}

	}

	private void playkeyboard1(String musicname) {
		try {

			keymp1.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			keymp1.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp1.prepare();
			keymp1.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp1.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard2(String musicname) {
		try {

			keymp2.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			keymp2.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp2.prepare();
			keymp2.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp2.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard3(String musicname) {
		try {

			keymp3.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			keymp3.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp3.prepare();
			keymp3.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp3.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard4(String musicname) {
		try {

			keymp4.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			keymp4.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp4.prepare();
			keymp4.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp4.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard5(String musicname) {
		try {

			keymp5.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			keymp5.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp5.prepare();
			keymp5.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp5.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard6(String musicname) {
		try {

			keymp6.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			keymp6.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp6.prepare();
			keymp6.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp6.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard7(String musicname) {
		try {

			keymp7.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			keymp7.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp7.prepare();
			keymp7.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp7.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard8(String musicname) {
		try {

			keymp8.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			keymp8.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp8.prepare();
			keymp8.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp8.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard9(String musicname) {
		try {

			keymp9.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			keymp9.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp9.prepare();
			keymp9.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp9.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard10(String musicname) {
		try {

			keymp10.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			keymp10.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp10.prepare();
			keymp10.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp10.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard11(String musicname) {
		try {

			keymp11.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			keymp11.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp11.prepare();
			keymp11.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp11.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard12(String musicname) {
		try {

			keymp12.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			keymp12.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp12.prepare();
			keymp12.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp12.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard13(String musicname) {
		try {

			keymp13.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			keymp13.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp13.prepare();
			keymp13.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp13.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard14(String musicname) {
		try {

			keymp14.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			keymp14.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp14.prepare();
			keymp14.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp14.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard15(String musicname) {
		try {

			keymp15.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			keymp15.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp15.prepare();
			keymp15.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp15.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard16(String musicname) {
		try {

			keymp16.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			keymp16.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp16.prepare();
			keymp16.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp16.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard17(String musicname) {
		try {

			keymp17.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);
			keymp17.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp17.prepare();
			keymp17.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp17.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void playkeyboard0(String musicname) {
		try {

			keymp0.reset();
			AssetFileDescriptor descriptor = getAssets().openFd(
					"piano/" + musicname);

			keymp0.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			keymp0.prepare();
			keymp0.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					keymp0.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void playsound1(String path) {
		try {

			mp1.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp1.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp1.prepare();
			mp1.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp.start();
				}
			});

		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void playsound2(String path) {
		try {
			mp2.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp2.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp2.prepare();
			mp2.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp2.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound3(String path) {
		try {
			mp3.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp3.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp3.prepare();
			mp3.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp3.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound4(String path) {
		try {
			mp4.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp4.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp4.prepare();
			mp4.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp4.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound5(String path) {
		try {
			mp5.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp5.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp5.prepare();
			mp5.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp5.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound6(String path) {
		try {
			mp6.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp6.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp6.prepare();
			mp6.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp6.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound7(String path) {
		try {
			mp7.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp7.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp7.prepare();
			mp7.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp7.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound8(String path) {
		try {
			mp8.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp8.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp8.prepare();
			mp8.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp8.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound9(String path) {
		try {
			mp9.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp9.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp9.prepare();
			mp9.setLooping(true);
			mp9.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp9.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound10(String path) {
		try {
			mp10.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp10.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp10.prepare();
			mp10.setLooping(true);
			mp10.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp10.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound11(String path) {
		try {
			mp11.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp11.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp11.prepare();
			mp11.setLooping(true);
			mp11.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp11.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound12(String path) {
		try {
			mp12.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp12.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp12.prepare();
			mp12.setLooping(true);
			mp12.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp12.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound13(String path) {
		try {
			mp13.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp13.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp13.prepare();
			mp13.setLooping(true);
			mp13.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp13.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound14(String path) {
		try {
			mp14.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp14.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp14.prepare();
			mp14.setLooping(true);
			mp14.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp14.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound15(String path) {
		try {
			mp15.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp15.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp15.prepare();
			mp15.setLooping(true);
			mp15.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp15.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void playsound16(String path) {
		try {
			mp16.reset();
			AssetFileDescriptor descriptor = getAssets().openFd("pads/" + path);
			mp16.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			mp16.prepare();
			mp16.setLooping(true);
			mp16.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp16.start();
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void leftdjst(String path) {
		try {
			AssetFileDescriptor descriptor = getAssets()
					.openFd("music/" + path);
			leftdjst.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			leftdjst.prepare();
			leftdjst.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					leftdjst.start();

				}
			});

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void rightdjst(String path) {
		try {
			AssetFileDescriptor descriptor = getAssets()
					.openFd("music/" + path);
			rightdjst.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();
			rightdjst.prepare();
			rightdjst.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					rightdjst.start();
				}
			});

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
